<?php
use app\assets\AppAsset;
use app\components\FlashMessage;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php

$this->beginPage()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">

<head>
 <?php

$this->head()?>
   <meta charset="<?=Yii::$app->charset?>" />
    <?=Html::csrfMetaTags()?>
    <!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"	href="<?=$this->theme->getUrl('assets/images/favicon.ico')?>">
<title><?=Html::encode($this->title)?></title>
<link rel="stylesheet"
	href="<?=$this->theme->getUrl('css/line-awesome.min.css')?>">
<!-- Chart CSS -->
<link rel="stylesheet"
	href="<?=$this->theme->getUrl('css/morris.css')?>">
<!-- Main CSS -->
<link rel="stylesheet" href="<?=$this->theme->getUrl('css/style.css')?>">

<link rel="apple-touch-icon" sizes="57x57" href="<?=$this->theme->getUrl('icon/apple-icon-57x57.png')?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?=$this->theme->getUrl('icon/apple-icon-60x60.png')?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?=$this->theme->getUrl('icon/apple-icon-72x72.png')?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?=$this->theme->getUrl('icon/apple-icon-76x76.png')?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?=$this->theme->getUrl('icon/apple-icon-114x114.png')?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?=$this->theme->getUrl('icon/apple-icon-120x120.png')?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?=$this->theme->getUrl('icon/apple-icon-144x144.png')?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?=$this->theme->getUrl('icon/apple-icon-152x152.png')?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?=$this->theme->getUrl('icon/apple-icon-180x180.png')?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?=$this->theme->getUrl('icon/android-icon-192x192.png')?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?=$this->theme->getUrl('icon/favicon-32x32.png')?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?=$this->theme->getUrl('icon/favicon-96x96.png')?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?=$this->theme->getUrl('icon/favicon-16x16.png')?>">
</head>

<body class="account-page">
<?php

$this->beginBody()?>
  
	<section id="wrapper" class="w-100">
       	 <?=FlashMessage::widget(['type'=> 'toster'])?>
         <?=$content?>
         <div class="fixed-footer">
        <?=$this->render('_footer.php');?>
        </div>
    </section>
    
    
    
    	
			
			
    <!-- ADD FOOTER -->

	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->
	<script
		src="<?=$this->theme->getUrl('frontend/assets/plugins/bootstrap/js/tether.min.js')?>"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script
		src="<?=$this->theme->getUrl('frontend/js/jquery.slimscroll.js')?>"></script>
	<!--Wave Effects -->
	<script src="<?=$this->theme->getUrl('frontend/js/waves.js')?>"></script>
	<!--Menu sidebar -->
	<script src="<?=$this->theme->getUrl('frontend/js/sidebarmenu.js')?>"></script>
	<!--stickey kit -->
	<script
		src="<?=$this->theme->getUrl('frontend/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')?>"></script>
	<!--Custom JavaScript -->
	<script src="<?=$this->theme->getUrl('frontend/js/custom.min.js')?>"></script>
	<!-- ============================================================== -->
	<!-- Style switcher -->
	<!-- ============================================================== -->
	<script
		src="<?=$this->theme->getUrl('frontend/assets/plugins/styleswitcher/jQuery.style.switcher.js')?>"></script>


    <?php

    $this->endBody()?>
</body>
<?php

$this->endPage()?>
</html>