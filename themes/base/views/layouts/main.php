<?php
use app\assets\AppAsset;
use app\components\FlashMessage;
use app\modules\notification\widgets\NotificationWidget;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
use app\models\User;
use app\models\Client;

$user = Yii::$app->user->identity;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php

$this->beginPage()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">

<head>
	<?php

$this->head()?>
	<meta charset="<?=Yii::$app->charset?>" />
	<?=Html::csrfMetaTags()?>
	<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"
	href="<?=$this->theme->getUrl('assets/images/favicon.ico')?>">
<title><?=Html::encode($this->title)?></title>

<!-- Custom CSS -->
<link href="<?=$this->theme->getUrl('css/line-awesome.min.css')?>"
	rel="stylesheet">
<link href="<?=$this->theme->getUrl('css/morris.css')?>"
	rel="stylesheet">
<link href="<?=$this->theme->getUrl('css/select2.min.css')?>"
	rel="stylesheet">
<link href="<?=$this->theme->getUrl('css/style.css')?>" rel="stylesheet">
<link href="<?=$this->theme->getUrl('css/font-awesome.min.css')?>"
	rel="stylesheet">
<link href="<?=$this->theme->getUrl('css/glyphicon.css')?>"
	rel="stylesheet">

<link rel="apple-touch-icon" sizes="57x57" href="<?=$this->theme->getUrl('icon/apple-icon-57x57.png')?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?=$this->theme->getUrl('icon/apple-icon-60x60.png')?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?=$this->theme->getUrl('icon/apple-icon-72x72.png')?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?=$this->theme->getUrl('icon/apple-icon-76x76.png')?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?=$this->theme->getUrl('icon/apple-icon-114x114.png')?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?=$this->theme->getUrl('icon/apple-icon-120x120.png')?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?=$this->theme->getUrl('icon/apple-icon-144x144.png')?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?=$this->theme->getUrl('icon/apple-icon-152x152.png')?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?=$this->theme->getUrl('icon/apple-icon-180x180.png')?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?=$this->theme->getUrl('icon/android-icon-192x192.png')?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?=$this->theme->getUrl('icon/favicon-32x32.png')?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?=$this->theme->getUrl('icon/favicon-96x96.png')?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?=$this->theme->getUrl('icon/favicon-16x16.png')?>">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

</head>

<body>
	<?php
$url = (User::isDistributor()) ? URL::toRoute([
    '/client-reports/index'
]) : Url::home();
$this->beginBody();
?>

	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pages.scss -->
		<!-- ============================================================== -->
		<div class="header">
			<!-- Logo -->
			<div class="header-left">
				<a href="<?=$url?>" class="logo"> <h2>Amusoftech</h2>
				<!-- <img
					src="<?=$this->theme->getUrl('img/log4o.png')?>"
					class="icon-desktop" width="120" height="40" alt="logo"> <img
					src="<?=$this->theme->getUrl('img/mini-logo.png')?>"
					class="d-none icon-mobile" width="120" height="40" alt="logo"> -->
				</a>
			</div>
			<!-- /Logo -->
			<a id="toggle_btn" href="javascript:void(0);"> <span class="bar-icon">
					<span></span> <span></span> <span></span>
			</span>
			</a> <a id="mobile_btn" class="mobile_btn" href="#sidebar"><i
				class="fa fa-bars"></i></a>
			<!-- Header Menu -->
			<ul class="nav user-menu">
				<!-- Search -->
				<li class="nav-item">
					<div class="top-nav-search">
						<a href="javascript:void(0);" class="responsive-search"> <i
							class="fa fa-search"></i>
						</a>
						<!-- <form>
							<input class="form-control" type="text" placeholder="Search here">
							<button class="btn" type="submit">
								<i class="fa fa-search"></i>
							</button>
						</form> -->
					</div>
				</li>
				<!-- /Search -->

				<!-- Notifications -->
				<?php  NotificationWidget::widget()?> 
				<!-- /Notifications -->

				<!-- Message Notifications -->
				<!-- <li class="nav-item dropdown"><a href="#"
					class="dropdown-toggle nav-link" data-toggle="dropdown"> <i
						class="fa fa-comment-o"></i> <span class="badge badge-pill">8</span>
				</a>
					<div class="dropdown-menu notifications">
						<div class="topnav-dropdown-header">
							<span class="notification-title">Messages</span> <a
								href="javascript:void(0)" class="clear-noti"> Clear All </a>
						</div>
						<div class="noti-content">
							<ul class="notification-list">
								<li class="notification-message"><a href="#0">
										<div class="list-item">
											<div class="list-left">
												<span class="avatar"> <img alt="img" src="img/avatar-02.jpg">
												</span>
											</div>
											<div class="list-body">
												<span class="message-author">Richard Miles </span> <span
													class="message-time">12:28 AM</span>
												<div class="clearfix"></div>
												<span class="message-content">Lorem ipsum dolor sit amet,
													consectetur adipiscing</span>
											</div>
										</div>
								</a></li>

							</ul>
						</div>
						<div class="topnav-dropdown-footer">
							<a href="#0">View all Messages</a>
						</div>
					</div></li> -->
				<!-- /Message Notifications -->

				<li class="nav-item dropdown has-arrow main-drop"><a href="#"
					class="dropdown-toggle nav-link" data-toggle="dropdown"> <span
						class="user-img"><?=$user->displayImage($user->profile_file, [], 'default.jpg', true);?>  <span
							class="status online"></span></span> <span><?=StringHelper::mb_ucfirst($user->full_name)?></span>
				</a>

					<div class="dropdown-menu">
					
					<?php

    if (User::isClient()) {
        $clientId = Client::findOne([
            'created_by_id' => $user->id
        ]);
        $view = '/client/view';
        $id = $clientId->id;
    } else {
        $id = $user->id;
        $view = '/user/view';
    }
    ?>
						<a class="dropdown-item"
							href="<?=Url::toRoute([$view,'id' => $id])?>">My Profile</a> 
							<?php

    if (User::isAdmin()) {
        ?>
							<a class="dropdown-item" href="<?=Url::toRoute(['/setting']);?>">Settings</a>
							<?php
    }
    ?>
							 <a class="dropdown-item"
							href="<?=Url::toRoute(['/user/logout']);?>">Logout</a>
					</div></li>
			</ul>
			<!-- /Header Menu -->


			<!-- /Header Menu -->

			<!-- Mobile Menu -->
			<div class="dropdown mobile-user-menu">
				<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"
					aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
				<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item"
						href="<?=Url::toRoute(['/user/view','id' => $user->id])?>">My
						Profile</a> <a class="dropdown-item"
						href="<?=Url::toRoute(['/setting']);?>">Settings</a> <a
						class="dropdown-item" href="<?=Url::toRoute(['/user/logout']);?>">Logout</a>
				</div>
			</div>
			<!-- /Mobile Menu -->
		</div>

		<!-- ============================================================== -->
		<!-- Left Sidebar - style you can find in sidebar.scss  -->
		<!-- ============================================================== -->





		<div class="sidebar" id="sidebar">
			<div class="sidebar-inner slimscroll">
				<div id="sidebar-menu" class="sidebar-menu">

					<!-- Sidebar navigation-->
			<?php

if (method_exists($this->context, 'renderNav')) {

    echo Menu::widget([
        'encodeLabels' => false,
        'activateParents' => true,
        'items' => $this->context->renderNav(),
        'submenuTemplate' => "\n<ul>\n{items}\n</ul>\n"
    ]);
}
?>
			<!-- End Sidebar navigation -->
				</div>

				<!-- End Bottom points-->
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End Left Sidebar - style you can find in sidebar.scss  -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->

		<div class="page-wrapper">
			<div class="content container-fluid">


		<?php
if (yii::$app->hasModule('shadow')) {
    echo app\modules\shadow\components\ShadowWidget::widget();
}
?>	
	<?=FlashMessage::widget(['type' => 'toster' /* 'position' => 'bottom-right' */])?>

	<?=$content;?>

</div>
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>
	<footer class="footer">
		<p>
		<?=Yii::$app->params['company']?> ©️ All Rights Reserved. Developed By <a
				target="_blank" href="http://amusoftech.com/">Amusoftech Pvt.Ltd</a>
		</p>
	</footer>
<?php
yii\bootstrap4\Modal::begin([
    'headerOptions' => [ // 'class' => 'modal-header border-0 p-0 d-inline-block'
    ],
    'id' => 'modal',
    'class' => 'modal-dialog'
]);
echo "<div id='modalContent'></div>";
yii\bootstrap4\Modal::end();
?>

<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->
	<script>

	$(document).delegate('#stop-btn','click',function(){
		var id = $('#model-id').val();
		 $.ajax({
	         'url':  "<?php

        echo Url::toRoute([
            '/timer/stop'
        ])?>/"+id,
	         'success':function(response) {
	        	 if ( response.status == "OK" ) {
 	        	 
	        		 $("#timer-run").html(response.html);
 				 location.reload();
	}				 
	             },
	         'error':function(data){
		         console.log('data'+data);
	            // alert(data);
	         }
	     });
	});

	$(document).on( 'click', '.showActionModalButton', function() {
		console.log("sdfsdf");
		if ($('#modal').data('bs.modal').isShown) {
			console.log($(this).attr('value'));
			$('#modal').find('#modalContent').load($(this).attr('value'));
		// dynamiclly set the header for the modal via title tag
		// document.getElementById('modalHeader').innerHTML = '<h4
		// class="modal-title">'
		// + $(this).attr('title') + '</h4>';
	} else {
		// if modal isn't open; open it and load content
		$('#modal').modal('show').find('#modalContent').load(
			$(this).attr('value'));
		// dynamiclly set the header for the modal via title tag
		document.getElementById('modalHeader').innerHTML = '<button type="button" class="close" data-dismiss="modal"><span>×</span></button>';
	}
});


	$('.mobile_btn').on('click', function(){
	    $('.sidebar').toggleClass('marginminus');
	});
	
</script>
	<!-- Bootstrap tether Core JavaScript -->

	<script
		src="<?=$this->theme->getUrl('assets/plugins/bootstrap/js/tether.min.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/popper.min.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/jquery.slimscroll.min.js')?>"></script>
	<script
		src="<?=$this->theme->getUrl('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/select2.min.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/moment.min.js')?>"></script>
	<script
		src="<?=$this->theme->getUrl('js/bootstrap-datetimepicker.min.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/jquery.dataTables.min.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/app.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/scripts.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/custom.min.js')?>"></script>
	<script
		src="<?=$this->theme->getUrl('assets/plugins/styleswitcher/jQuery.style.switcher.js')?>"></script>
	<script src="<?=$this->theme->getUrl('js/timer.js')?>"></script>


<?php

$this->endBody();
?>
</body>
<?php

$this->endPage()?>
</html>
