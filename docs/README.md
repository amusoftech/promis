 <p align="center">
    <a href="http://toxsl.com" target="_blank">
        <img src="https://toxsl.com/themes/toxsl/img/toxsl_logo.png" width="400" alt="Yii Framework" />
    </a>
</p>


Working with Databases
----------------------

* [Database Access Objects](db/db-dao.md): Connecting to a database, basic queries, transactions, and schema manipulation
* [Query Builder](db/db-query-builder.md): Querying the database using a simple abstraction layer
* [Active Record](db/db-active-record.md): The Active Record ORM, retrieving and manipulating records, and defining relations
* [Migrations](db/db-migrations.md): Apply version control to your databases in a team development environment
* [MongoDB](https://www.yiiframework.com/extension/yiisoft/yii2-mongodb/doc/guide)


Getting Data from Users
-----------------------

* [Creating Forms](forms/input-forms.md)
* [Validating Input](forms/input-validation.md)
* [Uploading Files](forms/input-file-upload.md)
* [Collecting Tabular Input](forms/input-tabular-input.md)
* [Getting Data for Multiple Models](forms/input-multiple-models.md)
* [Extending ActiveForm on the Client Side](forms/input-form-javascript.md)


Security
--------

* [Security Overview](security/security-overview.md)
* [Authentication](security/security-authentication.md)
* [Authorization](security/security-authorization.md)
* [Working with Passwords](security/security-passwords.md)
* [Cryptography](security/security-cryptography.md)
* [Auth Clients](https://www.yiiframework.com/extension/yiisoft/yii2-authclient/doc/guide)
* [Best Practices](security/security-best-practices.md)


Widgets
-------

* [GridView](https://www.yiiframework.com/doc-2.0/yii-grid-gridview.html)
* [ListView](https://www.yiiframework.com/doc-2.0/yii-widgets-listview.html)
* [DetailView](https://www.yiiframework.com/doc-2.0/yii-widgets-detailview.html)
* [ActiveForm](https://www.yiiframework.com/doc-2.0/guide-input-forms.html#activerecord-based-forms-activeform)
* [Pjax](https://www.yiiframework.com/doc-2.0/yii-widgets-pjax.html)
* [Menu](https://www.yiiframework.com/doc-2.0/yii-widgets-menu.html)
* [LinkPager](https://www.yiiframework.com/doc-2.0/yii-widgets-linkpager.html)
* [LinkSorter](https://www.yiiframework.com/doc-2.0/yii-widgets-linksorter.html)
* [Bootstrap Widgets](https://www.yiiframework.com/extension/yiisoft/yii2-bootstrap/doc/guide)
* [jQuery UI Widgets](https://www.yiiframework.com/extension/yiisoft/yii2-jui/doc/guide)


Helpers
-------

* [Helpers Overview](helpers/helper-overview.md)
* [ArrayHelper](helpers/helper-array.md)
* [Html](helpers/helper-html.md)
* [Url](helpers/helper-url.md)

