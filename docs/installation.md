<h1 align="center">
    <a href="http://toxsl.com" title="toxsl" target="_blank">
        <img width = "20%" height = "20%" src="https://toxsl.com/themes/toxsl/img/toxsl_logo.png" alt="Toxsl Logo"/>
    </a>
    <br>
    yii2-base
    <hr>
</h1>

[![Stable Version](http://192.168.10.22/yii2/yii2-base-admin-panel-api.git)]

This is the Yii2-base-admin-panel that will help you with pre-installed modules and many more features on just one go .Few steps are going to be performed by the user to setup the project on their workspace.
This setup already has bunch of inbuild modules that are going to help you with your project and some of those are :-

* Yii2-base-admin-panel
* Blog Module
* Comments Module
* Installer Module
* Sitemap Module
* Seo Module
* Quote Module
* Shadow Module
* Backup Module
* Social Module
* Media Module
* Chat Module



> NOTE: This git respository will provide you enough modules that are going to help you with your on going projects.
        Make sure you follow the steps to make them working for your projects.

## Installation

The preferred way to install this BASE is through [script](http://192.168.10.21/common/scripts.git).
Make sure you place it in root of your htdocs .

To install script module

```
git clone http://192.168.10.23/common/scripts.git
```
To install yii2-base-admin-panel

```
bash script/setup.sh http://192.168.10.23/yii2/yii2-base-admin-panel-api.git
```
if you have composer.json

```
composer install --prefer-dist
```

if you need to update vendor again you can use followig commands 

```
composer update --prefer-dist 
```





## Release Changes

> NOTE: Refer the [CHANGE LOG](http://192.168.10.22/yii2/yii2-base-admin-panel-api/blob/master/CHANGELOG) for details on changes to various releases.

## Usage
Once setup is done you need to follow the final setup with the installer .

```
make sure you give READ/WRITE permission to your folder.
```

## License

**www.toxsl.com** 