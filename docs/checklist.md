#Before sending any merge request you need to add this in your comment and check the checkboxes for the points which has been done from your end.

**Security Checklist**
* [ ]  1. MOD Rewrite check
* [ ]  2. Check Access Rules
* [ ]  3. Add verb filter `No Deletion using GET method`
* [ ]  4. Password Strong Validation
* [ ]  5. Encode echo content `Html::encode($content)`
* [ ]  6. Enable csrf `protected/config/web.php >> 'enableCsrfValidation' => defined('YII_TEST') ? false : true,`
* [ ]  7. Cross access Test `Test seperate roles`
* [ ]  8. Download Action `query string should have random key or timestamp`

**Modules Checklist**

* [ ]  1. Sitemap
* [ ]  2. SEO/Meta Tags
* [ ]  3. Backup
* [ ]  4. Blog
* [ ]  5. Comment
* [ ]  6. Installer
* [ ]  7. Notification
* [ ]  8. File
* [ ]  9. Shadow
* [ ]  10. Logger

**Components**

* [ ]  1. For redirecting use `function getUrl()`
* [ ]  2. Check `isAllowed()` Function
* [ ]  3. Use `deleteRelatedAll()` function in `beforeDelete()`
* [ ]  4. Use `ajaxValidation()` (no client side validation)

**Database**

* [ ]  1. Schema `install.sql`
* [ ]  2. Database Auto Backups

**Initialization Defaults**

* [ ]  1. cookieValidationKey, id, name (web.php) 'cookieValidationKey' => 'YourProjectNameIdKEY',
* [ ]  2. Change project ID and NAME from protected/config/web.php
* [ ]  3 Change composer.json package name and description same as your project


**MOD ReWrite**

* [ ]  1. URL Manager Pretty Url's
* [ ]  2. www. and without www should be merge RewriteCond %{HTTP_HOST} ^www\.(.+) [NC]
* [ ]  3 Check htaccess Rules

**WaterMarks and Copyrights**

* [ ]  1. Schema `<img alt = "YourTitleHere"/> , <a href = "mailto:yourmail@toxsltech.com">`
* [ ]  2. Companies details never be deleted from the code . protected/base/TBaseController.php public $_author = '@toxsl';
* [ ]  3. Copyrights should be available in the footer (Never delete source code)
©2019 ToXSL Technologies Pvt. Ltd. All Rights Reserved.Hosted by jiWebHosting.com

**Extras**

* [ ]  1. Log error to Admin
* [ ]  2. Send Email to Admin on Register of User
* [ ]  3. Remove Commented Code
* [ ]  4. Remove Extra Files/Folders
* [ ]  5. No folder should be created in the root except themes,protected,docs and assets
* [ ]  6. Login History,Email Queue,Admin settings
* [ ]  7. Login error count
* [ ]  8. Remove css and js libraries if not required.