<?php
use yii\db\Migration;

/**
 * Class m200213_105802_alter_column_description_tbl_reminder
 */
class m200213_105802_alter_column_description_tbl_reminder extends Migration
{

    /**
     *
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('tbl_reminder', 'description', $this->text());
    }

    /**
     *
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('tbl_reminder', 'description', $this->text()
            ->notNull());
    }
}
