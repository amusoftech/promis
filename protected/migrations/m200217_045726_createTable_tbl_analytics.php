<?php
use yii\db\Migration;

/**
 * Class m200217_045726_createTable_tbl_analytics
 */
class m200217_045726_createTable_tbl_analytics extends Migration
{

    /**
     *
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%analytics}}', [

            'id' => $this->primarykey(),
            'name' => $this->string()
                ->notNull(),
            'case_id' => $this->integer()
                ->notNull(),
            'department_id' => $this->integer()
                ->notNull(),
            'user_id' => $this->integer()
                ->notNull(),
            'manager_id' => $this->integer()
                ->notNull(),
            'client_id' => $this->integer()
                ->notNull(),
            'date' => $this->dateTime()
                ->notNull(),
            'billing_item' => $this->string()
                ->defaultValue(Null),
            'cost' => $this->double()
                ->notNull(),
            'value' => $this->double()
                ->notNull(),
            'created_by_id' => $this->integer()
                ->notNull(),
            'state_id' => $this->integer()
                ->defaultValue(1),
            'type_id' => $this->integer()
                ->defaultValue(Null),
            'created_on' => $this->dateTime()
                ->notNull(),
            'updated_on' => $this->dateTime()
                ->defaultValue(Null)
        ]);
        $this->addForeignKey('fk_analytics_created_by_id', '{{%analytics}}', 'created_by_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_analytics_case_id', '{{%analytics}}', 'case_id', '{{%case}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_analytics_department_id', '{{%analytics}}', 'department_id', '{{%department}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_analytics_user_id', '{{%analytics}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_analytics_manager_id', '{{%analytics}}', 'manager_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_analytics_client_id', '{{%analytics}}', 'client_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    /**
     *
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_analytics_created_by_id', '{{%analytics}}', 'created_by_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->dropForeignKey('fk_analytics_case_id', '{{%analytics}}', 'case_id', '{{%case}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->dropForeignKey('fk_analytics_department_id', '{{%analytics}}', 'department_id', '{{%department}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->dropForeignKey('fk_analytics_user_id', '{{%analytics}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->dropForeignKey('fk_analytics_manager_id', '{{%analytics}}', 'manager_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->dropForeignKey('fk_analytics_client_id', '{{%analytics}}', 'client_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->dropTable('{{%analytics}}');
    }
}
