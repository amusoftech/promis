<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
return [
    'adminEmail' => 'admin@toxsl.in',
    'company' => 'iachieve',
    'companyUrl' => 'https://www.toxsl.com',
    'user.passwordResetTokenExpire' => 60 * 1024,
    'bsVersion' => '4.3.1',
    'useCrudModals' => false
];
