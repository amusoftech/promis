<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use app\models\Reminder;
use app\modules\notification\models\Notification;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\User;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * ReminderController implements the API actions for User model.
 */
class ReminderController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [
                [
                    'actions' => [
                        'add',
                        'update',
                        'reminder-list',
                        'delete-reminder'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isHR() || User::isAdmin();
                    }
                ],

                [
                    'actions' => [
                        'add',
                        'update',
                        'reminder-list',
                        'delete-reminder'
                    ],
                    'allow' => true,
                    'roles' => [

                        '@'
                    ]
                ],

                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    /**
     * Creates a new Reminder model.
     *
     * @return mixed
     */
    public function actionAdd($user_id = null)
    {
        $data = [];
        $model = new Reminder();
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post)) {
            $model->created_by_id = \Yii::$app->user->id;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['message'] = "Reminder Add Successfully";
                $data['detail'] = $model->asJson();
                $title = $model->title;
                $description = $model->description;
                // Notification::saveNotification($model, $user_id, $title, $description);
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No data Posted');
        }
        $this->response = $data;
    }

    public function actionUpdate($id)
    {
        $data = [];
        $model = Reminder::findOne($id);
        $post = \Yii::$app->request->bodyParams;
        if (! empty($model)) {
            if ($model->load($post)) {
                if ($model->save(false, [
                    'title',
                    'description',
                    'case_id',
                    'department_id',
                    'user_id',
                    'time'
                ])) {
                    $data['status'] = self::API_OK;
                    $data['msg'] = $model->asJson();
                } else {
                    $data['error'] = $model->getErrorsString();
                }
            } else {
                $data['error'] = \Yii::t('app', 'No data Posted');
            }
        } else {
            $data['error'] = \Yii::t('app', "no Reminder Found");
        }

        $this->response = $data;
    }

    public function actionReminderList($page = null, $user_id = null)
    {
        $data = [];

        $model = Reminder::find()->Where([
            'user_id' => \Yii::$app->user->id
        ]);
        if (! empty($user_id)) {
            $model = Reminder::find()->Where([
                'user_id' => $user_id
            ]);
        }
        if (User::isInvestigator()) {
            $model = Reminder::find()->Where([
                'created_by_id' => \Yii::$app->user->id
            ]);
            $model->orWhere([
                'user_id' => \Yii::$app->user->id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionDeleteReminder($reminder_id)
    {
        $data = [];
        $model = Reminder::findOne($reminder_id);
        $user = \Yii::$app->user->id;
        if (! empty($model)) {
            if ((User::isAdmin()) || ($user == $model->created_by_id)) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "reminder successfuly Deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Reminder Found");
        }
        $this->response = $data;
    }
}

