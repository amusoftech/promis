<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use app\models\LeaveRequest;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\User;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\modules\comment\models\Comment;

/**
 * LeaveController implements the API actions for User model.
 */
class LeaveController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [
                [
                    'actions' => [
                        'update-leave-request',
                        'delete-leave-request',
                        'add-comment',
                        'leave-comments'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isHR() || User::isAdmin();
                    }
                ],
                [
                    'actions' => [
                        'add',
                        'update',
                        'list-view-leave',
                        'update-leave-request-status',
                        'get-request-detail'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return ! User::isClient();
                    }
                ],

                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    /**
     * Creates a new LeaveRequest model.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $data = [];
        $model = new LeaveRequest();
        $post = \Yii::$app->request->bodyParams;
        $current_date = date('Y-m-d');
        if ($model->load($post)) {
            $model->state_id = LeaveRequest::STATE_PENDING;
            $new_date = $model->start_date;
            $is_halfday = $model->is_halfday;
            $dates = explode(",", $new_date);

            foreach ($dates as $key => $date) {
                if ($key != 0) {
                    if (strtotime($date . '-1 day') != strtotime($dates[$key - 1])) {
                        $previousDay = date('l', strtotime($date . '-1 day'));
                        if (! in_array($previousDay, [
                            'Sunday',
                            'Saturday'
                        ])) {
                            if (strtotime($dates[$key - 1]) > strtotime($date)) {

                                $data['error'] = "You are only allowed to add Leave in Incremental Order Only";
                            }
                            $data['error'] = "You are only allowed to add consecutive Leaves";
                        }
                    }
                }
            }
            $query = LeaveRequest::find()->where([
                'created_by_id' => \Yii::$app->user->id,
                'state_id' => LeaveRequest::STATE_PENDING
            ])
                ->andWhere([
                'IN',
                'start_date',
                $dates
            ])
                ->one();

            if (empty($query)) {
                if ($is_halfday != LeaveRequest::IS_HALFDAY_NO) {
                    $model->start_date = $new_date;
                }
                $havetoapplyon = date('Y-m-d', strtotime($dates[0] . ' - 3 day'));
                if ($model->save()) {
                    if ($current_date <= $havetoapplyon) {
                        $data['success'] = \Yii::t('app', "Your leave request has been applied successfully!");
                    } else {
                        $data['success'] = \Yii::t('app', "Your leave Applied Successfully");
                    }
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asJson();
                } else {
                    $data['error'] = $model->getErrorsString();
                }
            } else {
                $data['msg'] = \Yii::t('app', "You already apply for the same!");
            }
        } else {
            $data['error'] = \Yii::t('app', "Data not posted.");
        }
        $this->response = $data;
    }

    public function actionAddComment()
    {
        $data = [];
        $model = new Comment();
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post)) {
            $model->state_id = Comment::STATE_ACTIVE;
            $model->model_type = LeaveRequest::className();
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['message'] = "Comment Added Successfully";
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', "Data not posted.");
        }

        $this->response = $data;
    }

    public function actionUpdate($id)
    {
        $data = [];
        $model = LeaveRequest::findOne($id);
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post) && $model->validate()) {
            $model->state_id = LeaveRequest::STATE_PENDING;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['msg'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', "Data not posted.");
        }

        $this->response = $data;
    }

    public function actionListViewLeave($page = null, $user_id = null, $search = null, $flag = null)
    {
        $data = [];

        $query = LeaveRequest::find()->alias('leave_request_data')->joinWith([
            'createdBy as leave_creater_info'
        ]);

        if ($flag == LeaveRequest::FLAG_TRUE) {
            $query->andWhere([
                'leave_request_data.created_by_id' => \Yii::$app->user->id
            ]);
        } else {

            $query->andWhere([
                'not in',
                'leave_request_data.created_by_id',
                \Yii::$app->user->id
            ]);
        }

        if (! empty($user_id)) {
            $query->where([
                'leave_request_data.created_by_id' => $user_id
            ]);
        }

        if (! empty($search)) {

            $query->where([
                'like',
                'leave_creater_info.full_name',
                $search
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionGetRequestDetail($request_id, $page = null)
    {
        $data = [];
        if ((User::isAdmin()) || (User::isHR())) {
            $model = LeaveRequest::findOne($request_id);
        } else {
            $model = LeaveRequest::findOne([
                'id' => $request_id,
                'created_by_id' => \Yii::$app->user->id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionUpdateLeaveRequestStatus($leave_request_id)
    {
        $data = [];
        $model = LeaveRequest::findOne($leave_request_id);

        if (! empty($model)) {
            if (! empty($model->load(\Yii::$app->request->bodyParams))) {
                if (($model->state_id != LeaveRequest::STATE_APPROVED) || ($model->state_id != LeaveRequest::STATE_REJECTED)) {
                    if ($model->save(false, [
                        'state_id'
                    ])) {
                        $data['status'] = self::API_OK;
                        $data['message'] = \Yii::t('app', 'Status of Leave Request Successfuly Updated');
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "no Data Found");
        }
        $this->response = $data;
    }

    public function actionUpdateLeaveRequest($id)
    {
        $data = [];
        $model = LeaveRequest::findOne($id);

        if (! empty($model)) {
            if (User::isHR()) {
                if ($model->load(\Yii::$app->request->bodyParams)) {
                    if ($model->save(false, [
                        'state_id'
                    ])) {
                        $data['status'] = self::API_OK;
                        $data['message'] = \Yii::t('app', "Leave Request Updated Successfuly");
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "No Data Posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Found");
        }

        $this->response = $data;
    }

    public function actionDeleteLeaveRequest($leave_request_id)
    {
        $data = [];
        $model = LeaveRequest::findOne($leave_request_id);
        if (! empty($model)) {
            if ((User::isHR()) || (User::isAdmin())) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Leave Request Successfuly Deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "Data Not Found");
        }
        $this->response = $data;
    }

    public function actionLeaveComments($id, $page = null)
    {
        $data = [];
        $model = Comment::find()->where([
            'model_id' => $id,
            'model_type' => LeaveRequest::className()
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }
}