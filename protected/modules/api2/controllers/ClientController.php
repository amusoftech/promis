<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use app\models\Client;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\User;
use app\models\ClientReports;
use app\modules\file\models\File;
use yii\web\UploadedFile;
use app\models\Cases;
use app\models\CaseReport;
use app\models\ClientBills;
use app\models\Task;

/**
 * ClientController implements the API actions for User model.
 */
class ClientController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [
                [
                    'actions' => [
                        'add',
                        'update',
                        'list-view',
                        'delete-client',
                        'add-client-report',
                        'delete-client-bill'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isAdmin();
                    }
                ],
                [
                    'actions' => [
                        'add-client-report',
                        'update-client-report',
                        'delete-client-report',
                        'add-client-bill',
                        'update-client-bill',
                        'client-bill-list'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isAdmin() || User::isDistributor();
                    }
                ],
                [
                    'actions' => [
                        'client-report-list',
                        'reports'
                    ],
                    'allow' => true,
                    'roles' => [
                        '@'
                    ]
                ],

                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    public function actionAdd()
    {
        $data = [];
        $model = new Client();
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post)) {
            $model->state_id = Client::STATE_ACTIVE;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No Data Posted');
        }
        $this->response = $data;
    }

    public function actionUpdate($id)
    {
        $data = [];
        $model = Client::findOne($id);
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post) && $model->validate()) {
            $model->state_id = Client::STATE_ACTIVE;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No Data Posted');
        }

        $this->response = $data;
    }

    public function actionReports($id = null, $page = null)
    {
        $data = [];
        if (empty($id)) {
            $query = ClientReports::find();
        } else {
            $query = ClientReports::find()->where([
                'id' => $id
            ]);
        }
        if (User::isQualityControl()) {
            $ids = Task::find()->select('case_id')
                ->where([
                'assign_to_id' => \Yii::$app->user->id
            ])
                ->distinct()
                ->column();

            $query->andWhere([
                'in',
                'case_id',
                $ids
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionListView($page = null, $state = null)
    {
        $data = [];

        $query = Client::find();
        if (! empty($state)) {
            $query->where([
                'state_id' => $state
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionDeleteClient($client_id)
    {
        $data = [];
        $model = Client::findOne($client_id);
        if (! empty($model)) {
            if (User::isAdmin()) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Client Record Successfuly deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Client Found");
        }
        $this->response = $data;
    }

    public function actionAddClientReport()
    {
        $data = [];
        $model = new ClientReports();
        $file = new File();
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post)) {
            $model->created_by_id = \Yii::$app->user->id;
            $model->state_id = ClientReports::STATE_ACTIVE;
            if ($model->save()) {

                $file->title = UploadedFile::getInstances($file, 'title');
                $file->upload($model, [
                    'file_type' => File::FILE_TYPE_DEFAULT,
                    'delete' => true
                ]);
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asjson();
            } else {
                $data['error'] = \Yii::t('app', $model->getErrorsString());
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Posted");
        }
        $this->response = $data;
    }

    public function actionUpdateClientReport($client_report_id)
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = ClientReports::findOne($client_report_id);
        $file = File::findOne([
            'model_id' => $client_report_id,
            'model_type' => ClientReports::className()
        ]);
        $fileNew = new File();

        if (! empty($model)) {
            if ((User::isAdmin()) || (User::isDistributor())) {
                if ($model->load($post)) {
                    if ($model->save(false, [
                        'case_id',
                        'report',
                        'sent_by',
                        'date'
                    ])) {

                        if (! empty($_FILES)) {
                            $file->delete();
                            $fileNew->title = UploadedFile::getInstances($file, 'title');
                            $fileNew->upload($model, [
                                'file_type' => File::FILE_TYPE_DEFAULT,
                                'delete' => true
                            ]);
                        }

                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "No Data Posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Found");
        }
        $this->response = $data;
    }

    public function actionDeleteClientReport($client_report_id)
    {
        $data = [];
        $model = ClientReports::findOne($client_report_id);

        if (! empty($model)) {
            if ((User::isAdmin()) || (User::isDistributor())) {
                $file = File::findOne([
                    'model_id' => $model->id,
                    'model_type' => ClientReports::className()
                ]);
                if ($model->delete()) {
                    $file->delete();
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Client Report data Successfuly Deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Client Report Found");
        }
        $this->response = $data;
    }

    public function actionClientReportList($case_id = null, $page = null)
    {
        $data = [];
        $query = ClientReports::find();
        if (! empty($case_id)) {
            $query->where([
                'case_id' => $case_id
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionAddClientBill()
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = new ClientBills();
        if ($model->load($post)) {
            if ((User::isAdmin() || User::isDistributor())) {
                $model->created_by_id = \Yii::$app->user->id;
                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asjson();
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Posted");
        }
        $this->response = $data;
    }

    public function actionUpdateClientBill($client_bill_id)
    {
        $data = [];
        $model = ClientBills::findOne($client_bill_id);
        $post = \Yii::$app->request->bodyParams;
        if (! empty($model)) {
            if ((User::isAdmin()) || ($model->created_by_id == \Yii::$app->user->id)) {
                if ($model->load($post)) {
                    if ($model->save(false, [
                        'case_id',
                        'full_name',
                        'amount',
                        'date',
                        'sent_by'
                    ])) {
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "No Data Posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Found");
        }
        $this->response = $data;
    }

    public function actionClientBillList($case_id = null, $page = null)
    {
        $query = ClientBills::find();
        if (! empty($case_id)) {
            $query->where([
                'case_id' => $case_id
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionDeleteClientBill($client_bill_id)
    {
        $data = [];
        $model = ClientBills::findOne($client_bill_id);
        if (! empty($model)) {
            if (User::isAdmin()) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Client Bill Record Successfuly deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Client Bill Found");
        }
        $this->response = $data;
    }
}