<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\Cases;
use app\models\CasePlatform;
use app\models\Task;
use app\models\Team;
use app\models\User;
use app\models\Timer;
use yii\authclient\clients\Yandex;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\CaseReport;
use app\models\TaskEvent;
use app\modules\comment\models\Comment;

/**
 * CasesController implements the API actions for User model.
 */
class TaskController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [

                [
                    'actions' => [
                        'add-task',
                        'update-task',
                        'list',
                        'delete-task',
                        'update-task-status',
                        'add-task-event',
                        'update-task-event',
                        'delete-task-event',
                        'task-event-list-view'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isManager() || User::isAdmin();
                    }
                ],
                [
                    'actions' => [
                        'list',
                        'add-comment',
                        'task-comments',
                        'start-timer',
                        'stop-timer',
                        'task-timer-list'
                    ],
                    'allow' => true,
                    'roles' => [

                        '@'
                    ]
                ],

                [
                    'actions' => [
                        'start-task-timer',
                        'end-task-timer',
                        'update-task-status'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isEmployee();
                    }
                ],

                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    public function actionStartTimer($id)
    {
        $response = [];
        $model = Task::find()->where([
            'id' => $id
        ])->one();
        if (! empty($model)) {
            if (! $model->isAllowed())

                $response['error'] = \Yii::t('app', 'You are not allowed to access this page.');
        }

        Timer::start($model);
        $timer = Timer::getCurrentTimer();

        $response['html'] = '<div class="left-data">' . $timer->task->linkify() . '</div>            
						   <input type="hidden" id="model-id"
						   value="' . $timer->task_id . '"><a
						  class="search-submit" id="stop-btn"> <i
						class="fa fa-stop"></i>
									</a>';
        $response['status'] = self::API_OK;
        $this->response = $response;
    }

    public function actionStopTimer($id)
    {
        $response = [];
        $model = Task::find()->where([
            'id' => $id
        ])->one();

        if (! empty($model)) {
            if (! $model->isAllowed())
                $response['error'] = \Yii::t('app', 'You are not allowed to access this page.');
        }

        Timer::stop($model);
        $response['html'] = '<div class="left-data  text-center"><a href="" class="font12">Timer not Running</a></div>';
        $response['status'] = self::API_OK;
        $response['actual_time'] = ! empty($model->actualTime) ? $model->actualTime : 0;
        $this->response = $response;
    }

    public function actionAddComment()
    {
        $data = [];
        $model = new Comment();
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post)) {
            $model->state_id = Comment::STATE_ACTIVE;
            $model->model_type = Task::className();
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['message'] = "Comment Added Successfully";
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', "Data not posted.");
        }

        $this->response = $data;
    }

    public function actionTaskComments($id, $page = null)
    {
        $data = [];
        $model = Comment::find()->where([
            'model_id' => $id,
            'model_type' => Task::className()
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionTaskTimerList($page = null)
    {
        $data = [];
        $list = [];
        $task_ids = Timer::find()->select('task_id')
            ->where([
            'created_by_id' => \Yii::$app->user->id
        ])
            ->distinct()
            ->column();
        $ids = Task::find()->select('id')
            ->where([
            'in',
            'id',
            $task_ids
        ])
            ->column();

        if (! empty($ids) && is_array($ids)) {
            foreach ($ids as $id) {

                $model = Timer::find()->where([
                    'task_id' => $id
                ])
                    ->orderBy('id DESC')
                    ->one();
                $list[] = $model->asJson();
            }
        }

        $data['list'] = $list;
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    /**
     * Creates a new Task model.
     *
     * @return mixed
     */
    public function actionAddTask()
    {
        $data = [];
        $model = new Task();
        $post = \Yii::$app->request->bodyParams;
        if ((User::isAdmin()) || (User::isManager())) {
            if ($model->load($post)) {
                $model->created_by_id = \Yii::$app->user->id;
                $model->state_id = Task::STATE_ASSIGNED;
                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asjson();
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionUpdateTask($task_id)
    {
        $data = [];
        $model = Task::FindOne($task_id);
        $post = \Yii::$app->request->bodyParams;
        if (! empty($model)) {

            if ((User::isAdmin()) || (User::isManager())) {
                if ($model->load($post)) {
                    $model->created_by_id = \Yii::$app->user->id;
                    $model->state_id = Task::STATE_ASSIGNED;
                    if ($model->save()) {
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "No Data Posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Task Found");
        }
        $this->response = $data;
    }

    public function actionUpdateTaskStatus($task_id)
    {
        $data = [];
        $model = Task::findOne($task_id);
        $user = \Yii::$app->user->identity->role_id;
        $post = \Yii::$app->request->bodyParams;
        if (! empty($model)) {
            if ((User::isAdmin()) || (User::isManager()) || ($user == $model->user_id)) {
                if ($model->load($post)) {
                    if ($model->save(false, [
                        'state_id'
                    ])) {
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asJson();
                    } else {
                        $data['status'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "No Data Posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Task Found");
        }
        $this->response = $data;
    }

    public function actionList($id = null, $page = null, $user_id = null, $search = null, $case_name = null)
    {
        $data = [];
        $query = Task::find()->alias('task_detail')->joinWith([
            'createdBy as task_creater_info',
            'assignTo as task_assign_user_info',
            'case as case_tasks'
        ]);

        if (! empty($user_id)) {
            $query->where([
                'task_detail.assign_to_id' => $user_id
            ]);
        }

        if (! (User::isHR()) || (User::isAdmin())) {
            $query->where([
                'task_detail.assign_to_id' => \Yii::$app->user->id
            ]);
        }
        if (! empty($id)) {

            $query->where([
                'task_detail.case_id' => $id
            ]);
        }
        if (! empty($search)) {

            $query->where([
                'like',
                'task_creater_info.full_name',
                $search
            ])->orWhere([
                'like',
                'task_assign_user_info.full_name',
                $search
            ]);
        }

        if (! empty($case_name)) {

            $query->where([
                'like',
                'case_tasks.title',
                $case_name
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionDeleteTask($task_id)
    {
        $data = [];
        $model = Task::findOne($task_id);
        if (! empty($model)) {
            if ((User::isAdmin()) || (User::isManager())) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', 'Task Successfuly Deleted');
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Task Found");
        }
        $this->response = $data;
    }

    public function actionStartTaskTimer($task_id)
    {
        $data = [];
        $model = Task::findOne($task_id);
        $post = \Yii::$app->request->bodyParams;

        if (! empty($model)) {
            if ($model->load($post)) {
                if ($model->save(false, [
                    'start_time'
                ])) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asjson();
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Task Found");
        }
        $this->response = $data;
    }

    public function actionEndTaskTimer($task_id)
    {
        $data = [];
        $model = Task::findOne($task_id);
        $post = \Yii::$app->request->bodyParams;

        if (! empty($model)) {
            if ($model->load($post)) {
                if ($model->save(false, [
                    'end_time'
                ])) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asjson();
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Task Found");
        }
        $this->response = $data;
    }

    public function actionAddTaskEvent()
    {
        $data = [];

        if (User::isHR()) {
            $event = new TaskEvent();
            if ($event->load(\Yii::$app->request->bodyParams)) {
                $event->state_id = TaskEvent::STATE_ACTIVE;
                $event->created_by_id = \Yii::$app->user->id;
                $event->time = date('H:i:s');
                if ($event->save()) {
                    $data['status'] = self::API_OK;
                    $data['msg'] = $event->asJson();
                } else {
                    $data['error'] = $event->getErrorsString();
                }
            } else {
                $data['error'] = \Yii::t('app', 'No Data Posted');
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionUpdateTaskEvent($task_event_id)
    {
        $data = [];

        if (User::isAdmin() || User::isManager()) {
            $model = TaskEvent::findOne($task_event_id);
            if (! empty($model)) {
                if ($model->load(\Yii::$app->request->bodyParams)) {
                    if ($model->save(false, [
                        'date',
                        'type_id',
                        'user_id',
                        'time'
                    ])) {
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "no Data posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Found");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionDeleteTaskEvent($task_event_id)
    {
        $data = [];
        if (User::isHR()) {
            $model = TaskEvent::findOne($task_event_id);
            if (! empty($model)) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Attendence Event successfuly deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Found");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionTaskEventListView($page = null, $user_id)
    {
        $data = [];
        $total_event = '';
        $query = TaskEvent::find()->Where([
            'created_by_id' => \Yii::$app->user->id
        ]);
        if (! empty($user_id)) {
            $query->where([
                'user_id' => $user_id,
                'created_by_id' => \Yii::$app->user->id
            ]);
        }
        $total_event = $query->count();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $data['total_event'] = $total_event;
        $this->response = $data;
    }
    
    
    
    
    
}