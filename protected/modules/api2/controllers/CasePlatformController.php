<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use app\models\CasePlatform;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\Platform;

/**
 * CasePlatformController implements the API actions for User model.
 */
class CasePlatformController extends ApiTxController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = [];
        $behaviors['authenticator']['optional'] = [];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    public function actionAddPlatform()
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = new Platform();
        if ($model->load($post)) {
            $model->created_by_id = \Yii::$app->user->id;
            $model->state_id = Platform::STATE_ACTIVE;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asjson();
            } else {
                $data['error'] = \Yii::t('app', $model->getErrorsString());
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Posted");
        }
        $this->response = $data;
    }

    public function actionUpdatePlatform($platform_id)
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = Platform::findOne($platform_id);
        if (! empty($model)) {
            if ($model->load($post) && $model->validate()) {
                $model->state_id = Platform::STATE_ACTIVE;

                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['msg'] = $model->asJson();
                } else {
                    $data['error'] = $model->getErrorsString();
                }
            } else {
                $data['error'] = \Yii::t('app', 'No Data Posted');
            }
        } else {
            $data['error'] = \Yii::t('app', "no existing Platform Found");
        }
        $this->response = $data;
    }

    public function actionPlatformList($page = 0)
    {
        $data = [];
        $query = Platform::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionDeletePlatform($platform_id)
    {
        $data = [];
        $model = Platform::findOne($platform_id);
        if (! empty($model)) {
            if ($model->delete()) {
                $data['status'] = self::API_OK;
                $data['message'] = \Yii::t('app', "platform successfuly deleted");
            } else {
                $data['error'] = \Yii::t('app', $model->getErrorsString());
            }
        } else {
            $data['error'] = \Yii::t('app', "No Platform Found");
        }
        $this->response = $data;
    }

    public function actionUpdateCasePlatform($id)
    {
        $data = [];
        $model = CasePlatform::findOne($id);
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post) && $model->validate()) {

            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['msg'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No Data Posted');
        }

        $this->response = $data;
    }

    public function actionAddCasePlatform()
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = new CasePlatform();
        if ($model->load($post)) {
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asJson();
            } else {

                $data['error'] = \Yii::t('app', $model->getErrorsString());
            }
        } else {
            $data['error'] = \Yii::t('app', "no Data Posted");
        }
        $this->response = $data;
    }

    public function actionGetCase($state_id, $page = null)
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = CasePlatform::find()->where([
            'state_id' => $state_id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionListView($page = null, $state = null)
    {
        $data = [];

        $query = CasePlatform::find();
        if ($state) {
            $query->where([
                'state_id' => $state
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }
}