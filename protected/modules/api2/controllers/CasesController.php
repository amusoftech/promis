<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\Cases;
use app\models\CasePlatform;
use app\models\Task;
use app\models\Team;
use app\models\User;
use app\models\Timer;
use yii\authclient\clients\Yandex;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\CaseReport;
use app\modules\comment\models\Comment;
use app\models\TeamDepartment;

/**
 * CasesController implements the API actions for User model.
 */
class CasesController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [
            'start',
            'list-view',
            'get',
            'case-comments',
            'add-comment'
        ];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [

                [
                    'actions' => [
                        'add',
                        'update',
                        'list-view',
                        'task',
                        'update-status',
                        'start',
                        'add-case-platform',
                        'get-team',
                        'delete-team',
                        'update-case-team',
                        'add-case-team',
                        'case-report-list',
                        'case-comments',
                        'add-comment',
                        'get'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {
                        return User::isManager() || User::isAdmin() || User::isEmployee();
                    }
                ],

                [
                    'actions' => [
                        'delete-case',
                        'delete-case-report',
                        'delete-team'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isAdmin();
                    }
                ],
                [
                    'actions' => [
                        'add-case-report',
                        'update-case-report',
                        'get-case-report',
                        'case-report-list'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isDistributor();
                    }
                ],

                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\Cases";
        return $this->txget($id);
    }

    /**
     * Creates a new Case model.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $data = [];
        $case = new Cases();
        $post = \Yii::$app->request->bodyParams;
        if ($case->load($post)) {
            $case->state_id = Cases::STATE_INPROGRESS;
            if ($case->save()) {
                $data['status'] = self::API_OK;
                $data['msg'] = $case->asJson();
            } else {
                $data['error'] = $case->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No data Posted');
        }
        $this->response = $data;
    }

    public function actionUpdate($id)
    {
        $data = [];
        $model = Cases::findOne($id);
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post) && $model->validate()) {
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['msg'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No data Posted');
        }

        $this->response = $data;
    }

    public function actionAddComment()
    {
        $data = [];
        $model = new Comment();
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post)) {
            $model->state_id = Comment::STATE_ACTIVE;
            $model->model_type = Cases::className();
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['message'] = "Comment Added Successfully";
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', "Data not posted.");
        }

        $this->response = $data;
    }

    public function actionCaseComments($id, $page = null)
    {
        $data = [];
        $model = Comment::find()->where([
            'model_id' => $id,
            'model_type' => Cases::className()
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionListView($page = null, $state = null)
    {
        $data = [];

        $query = Cases::find();

        if (User::isEmployee() || User::isQualityControl()) {
            $ids = Task::find()->select('case_id')
                ->where([
                'assign_to_id' => \Yii::$app->user->id
            ])
                ->distinct()
                ->column();

            $query->andWhere([
                'in',
                'id',
                $ids
            ]);
        }

        $total_cases = $query->count();
        if (! empty($state)) {
            $query->where([
                'state_id' => $state
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $data['total_cases'] = $total_cases;
        $this->response = $data;
    }

    public function actionGetTeam($case_id, $page = null)
    {
        $data = [];
        $model = Team::findOne([
            'case_id' => $case_id
        ]);

        if (! empty($model)) {
            $data['status'] = self::API_OK;
            $data['detail'] = $model->asjson();
        } else {
            $data['error'] = \Yii::t('app', "No Team Found");
        }
        $this->response = $data;
    }

    public function actionUpdateCaseTeam($team_id)
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = Team::findOne($team_id);

        if (! empty($model)) {
            if ($model->load($post)) {
                if ($model->save(false, [
                    'case_id',
                    'user_id'
                ])) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asjson();
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['erro'] = \Yii::t('app', "no Data Posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Team Found");
        }
        $this->response = $data;
    }

    public function actionGetCaseTeam($case_id, $page = null)
    {
        $data = [];
        $model = Team::find()->where([
            'case_id' => $case_id
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionAddCasePlatform()
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = new CasePlatform();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($model->load($post)) {

                if (! empty($model->caseType)) {
                    $types = explode(',', $model->caseType);
                    if (is_array($types)) {
                        foreach ($types as $val) {
                            $casePlatform = new CasePlatform();
                            $casePlatform->case_id = $model->case_id;
                            $casePlatform->state_id = CasePlatform::STATE_ACTIVE;
                            $casePlatform->type_id = $val;

                            if (! $casePlatform->save()) {
                                $transaction->rollBack();
                                $data['error'] = $casePlatform->getErrorsString();
                                $this->response = $data;
                                return;
                            }
                        }
                        $transaction->commit();
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->$casePlatform();
                    }
                } else {
                    $transaction->rollBack();
                    $data['error'] = \Yii::t('app', "Case Type can't be empty");
                    $this->response = $data;
                    return;
                }
            } else {
                $transaction->rollBack();
                $data['error'] = "Data not posted.";
                $this->response = $data;
                return;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $data['error'] = \Yii::t('app', "Error !! ") . $e->getMessage();
        }
        $this->response = $data;
    }

    public function actionTask()
    {
        $data = [];
        $model = new Task();
        $post = \Yii::$app->request->bodyParams;
        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            if ($model->load($post)) {

                if (! empty($model->taskDetail)) {
                    $taskDetails = json_decode($model->taskDetail);
                    foreach ($taskDetails as $taskDetail) {
                        $tasks = new Task();
                        $tasks->title = $taskDetail->title;
                        $tasks->case_id = $model->case_id;
                        $tasks->state_id = Task::STATE_ASSIGNED;
                        $tasks->description = $taskDetail->description;
                        $tasks->platform_id = $taskDetail->platform_id;
                        $tasks->type_id = $taskDetail->type_id;
                        $tasks->estimated_time = $taskDetail->estimated_time;
                        $tasks->department_id = $taskDetail->department_id;
                        $tasks->start_date = $taskDetail->start_date;
                        $tasks->end_date = $taskDetail->end_date;
                        $tasks->assign_to_id = $taskDetail->assign_to_id;
                        if (! $tasks->save()) {
                            $transaction->rollBack();
                            $data['error'] = $tasks->getErrorsString();
                            $this->response = $data;
                            return;
                        }
                    }
                    $transaction->commit();
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asJson();
                } else {
                    $transaction->rollBack();
                    $data['error'] = \Yii::t('app', 'Empty Result.');
                    $this->response = $data;
                    return;
                }
            } else {
                $transaction->rollBack();
                $data['error'] = \Yii::t('app', 'No Data Posted');
                $this->response = $data;
                return;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $data['error'] = \Yii::t('app', "Error !! ") . $e->getMessage();
        }
        $this->response = $data;
    }

    public function actionUpdateStatus($id)
    {
        $data = [];
        $model = Task::findOne($id);
        $post = \Yii::$app->request->bodyParams;
        if (! empty($model)) {
            if ($model->load($post)) {
                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asJson();
                    $data['message'] = \Yii::t('app', "State change !");
                } else {
                    $data['error'] = $model->getErrorsString();
                }
            } else {
                $data['error'] = \Yii::t('app', 'No Data Posted');
            }
        } else {
            $data['error'] = \Yii::t('app', 'No Task Found');
        }

        $this->response = $data;
    }

    public function actionDeleteTeam($team_id)
    {
        $data = [];
        $model = Team::findOne($team_id);
        if (! empty($model)) {
            if ($model->delete()) {
                $data['status'] = self::API_OK;
                $data['message'] = \Yii::t('app', "team successfuly Deleted");
            } else {
                $data['error'] = \Yii::t('app', $model->getErrorsString());
            }
        } else {
            $data['error'] = \Yii::t('app', "No Team found");
        }
        $this->response = $data;
    }

    public function actionAddCaseTeam()
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $user = Cases::findOne([
            'assign_to_id' => \Yii::$app->user->id
        ]);
        if (! empty($user)) {
            $model = new Team();
            if ($model->load($post)) {
                $model->state_id = Cases::STATE_ACTIVE;
                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asjson();
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "no Data Posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are Not allowed to perform this action");
        }

        $this->response = $data;
    }

    public function actionStart()
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = new Timer();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($model->load($post)) {
                if (! empty($model->work_time)) {
                    $timeWorks = explode(',', $model->work_time);
                    if (is_array($timeWorks)) {
                        foreach ($timeWorks as $timeWork) {
                            $timer = new Timer();
                            $timer->created_on = date('Y-m-d H:i:s');
                            $timer->started_on = date('Y-m-d H:i:s');
                            $timer->task_id = $model->task_id;
                            $timer->work_time = $timeWork;
                            $timer->state_id = Timer::STATE_START;
                            if (! $timer->save()) {
                                $transaction->rollBack();
                                $data['error'] = $timer->getErrorsString();
                                $this->response = $data;
                                return;
                            }
                        }
                        $transaction->commit();
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asJson();
                    }
                } else {
                    $transaction->rollBack();
                    $data['error'] = \Yii::t('app', 'Invalid Work Time');
                    $this->response = $data;
                    return;
                }
            } else {
                $transaction->rollBack();
                $data['error'] = \Yii::t('app', 'No Data Posted');
                $this->response = $data;
                return;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $data['error'] = \Yii::t('app', "Error !! ") . $e->getMessage();
        }

        $this->response = $data;
    }

    public function actionDeleteCase($case_id)
    {
        $data = [];
        $user = User::findOne([
            'id' => \Yii::$app->user->id,
            'role_id' => User::ROLE_ADMIN
        ]);
        if (! empty($user)) {
            $model = Cases::findOne($case_id);
            if (! empty($model)) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "case successfuly deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "Case is not found");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    /* This will be added by distribution dept */
    public function actionAddCaseReport()
    {
        $data = [];
        $model = new CaseReport();
        if ($model->load(\Yii::$app->request->bodyParams)) {
            $model->created_by_id = \Yii::$app->user->id;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asjson();
            } else {
                $data['error'] = \Yii::t('app', $model->getErrorsString());
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Posted");
        }
        $this->response = $data;
    }

    public function actionUpdateCaseReport($case_report_id)
    {
        $data = [];
        $model = CaseReport::findOne($case_report_id);
        if (! empty($model)) {
            if ($model->load(\Yii::$app->request->bodyParams)) {
                if ($model->save(false, [
                    'title',
                    'description',
                    'case_id',
                    'submitted_to'
                ])) {
                    $data['status'] = \Yii::t('app', "Case Report Updated successfuly");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Case Report Found");
        }
        $this->response = $data;
    }

    public function actionGetCaseReport($page = null, $case_report_id)
    {
        $data = [];

        if ($case_report_id) {
            $model = CaseReport::findOne($case_report_id);
        } else {
            $model = CaseReport::find()->Where([
                'created_by_id' => \Yii::$app->user->id
            ])->orWhere([
                'submitted_to' => \Yii::$app->user->id
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionDeleteCaseReport($case_report_id)
    {
        $data = [];
        $model = CaseReport::findOne($case_report_id);

        if (User::isAdmin()) {
            if (! empty($model)) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Case Report Successfuly Deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "No Case Report Found");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionCaseReportList($page = null)
    {
        $data = [];

        $model = CaseReport::find()->Where([
            'created_by_id' => \Yii::$app->user->id
        ])->orWhere([
            'submitted_to' => \Yii::$app->user->id
        ]);
        if (User::isAdmin()) {
            $model = CaseReport::find();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionViewTeam($page = Null)
    {
        $data = [];
        $model = TeamDepartment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }
}
