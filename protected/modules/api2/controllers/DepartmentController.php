<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\Department;
use app\models\User;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\Cases;
use app\models\Team;
use app\models\Task;
use app\models\Ticket;
use app\models\Attendance;
use app\models\LeaveRequest;
use app\models\CaseReport;

/**
 * DepartmentController implements the API actions for User model.
 */
class DepartmentController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [
            'list-view'
        ];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [
                [
                    'actions' => [
                        'add',
                        'list-view',
                        'update',
                        'delete',
                        'case-list-view'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isAdmin();
                    }
                ],
                [
                    'actions' => [
                        'list-view',
                        'assign-task',
                        'my-assigned-task-list',
                        'update-assign-task',
                        'delete-task',
                        'update-task-state',
                        'case-list-view'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isManager();
                    }
                ],

                [
                    'actions' => [
                        'department-users',
                        'case-list-view',
                        'list-view-ticket',
                        'attendance-list-view',
                        'list-view-leave',
                        'case-report-list'
                    ],
                    'allow' => true,
                    'roles' => [

                        '@'
                    ]
                ],

                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    /**
     * Creates a new Department model.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $data = [];
        $model = new Department();
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post)) {
            $model->state_id = Department::STATE_ACTIVE;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No Data Posted');
        }
        $this->response = $data;
    }

    public function actionUpdate($id)
    {
        $data = [];
        $model = Department::findOne($id);
        $post = \Yii::$app->request->post();
        if ($model->load($post) && $model->validate()) {
            $model->state_id = Department::STATE_ACTIVE;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No Data Posted');
        }

        $this->response = $data;
    }

    public function actionDepartmentUsers($page = null, $dept_id = null)
    {
        $data = [];

        $query = User::find();
        if (! empty($dept_id)) {
            $query->where([
                'department_id' => $dept_id
            ]);
        }
        $query->andWhere([
            'not in',
            'role_id',
            User::ROLE_ADMIN
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionListView($page = null, $state = null)
    {
        $data = [];

        $query = Department::find();

        if (! empty($state)) {
            $query->where([
                'state_id' => $state
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionDeleteDepartment($id)
    {
        $data = [];

        if (User::isAdmin()) {
            $model = Department::findOne($id);
            if (! empty($model)) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "department Successfuly Deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', 'Department Not Found');
            }
        } else {
            $data['error'] = \Yii::t('app', "you are not allowed to [perform this action");
        }
        $this->response = $data;
    }

    public function actionAssignTask()
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $authorise_user = Cases::findOne([
            'assign_to_id' => \Yii::$app->user->id
        ]);

        if (! empty($authorise_user)) {
            $model = new Task();
            $model->scenario = Task::SCENARIO_ASSIGN_TASK;
            if ($model->load($post)) {
                $model->created_by_id = \Yii::$app->user->id;
                $model->state_id = Cases::STATE_ASSIGN;
                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asjson();
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "no data posted");
            }
        } else {
            $data['message'] = \Yii::t('app', "you are not allowed to create any task");
        }
        $this->response = $data;
    }

    public function actionUpdateAssignTask($task_id)
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;

        if ((User::isManager()) || (User::isAdmin())) {

            $model = Task::findOne($task_id);

            if (! empty($model)) {
                if ($model->load($post)) {
                    $model->scenario = Task::SCENARIO_UPDATE_TASK;
                    if ($model->save(false, [
                        'title',
                        'description',
                        'case_id',
                        'platform_id',
                        'assign_to_id',
                        'start_date',
                        'end_date',
                        'estimated_time'
                    ])) {
                        $data['status'] = self::API_OK;
                        $data['message'] = \Yii::t('app', "Task Successfuly updated");
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "no Data Posted");
                }
            } else {
                $data['error'] = \Yii::t('app', " No Task Found");
            }
        } else {
            $data['error'] = \Yii::t('app', "Only Department Manager can update the task");
        }
        $this->response = $data;
    }

    public function actionMyAssignedTaskList($page = null, $user_id = null)
    {
        $response = [];
        $task = Task::find()->where([
            'assign_to_id' => $user_id,
            'created_by_id' => \Yii::$app->user->id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $task,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [];

        $data = $pagination->serialize($dataProvider);
        $response['detail'] = $data;
        $response['status'] = self::API_OK;
        $this->response = $response;
    }

    public function actionDeleteTask($task_id)
    {
        $data = [];
        $model = Task::findOne($task_id);
        if (! empty($data)) {
            if ($model->delete()) {
                $data['status'] = self::API_OK;
                $data['message'] = \Yii::t('app', "Task Successfuly Deleted");
            } else {
                $data['error'] = \Yii::t('app', $model->getErrorsString());
            }
        } else {
            $data['error'] = \Yii::t('app', "No Task Found");
        }
        $this->response = $data;
    }

    public function actionUpdateTaskState($task_id)
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = Task::findOne($task_id);
        if (! empty($model)) {
            if ($model->load(\Yii::$app->request->bodyParams)) {
                if ($model->save(false, [
                    'state_id'
                ])) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Task State Successfuly Changed");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Task Found");
        }
        $this->response = $data;
    }

    public function actionGetTaskDetail($task_id)
    {
        $data = [];
        $model = Task::findOne($task_id);
        if (! empty($model)) {
            if ((User::isAdmin()) || ($model->assign_to_id == \Yii::$app->user->id) || ($model->created_by_id == \Yii::$app->user->id)) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asjson();
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Task Found");
        }
        $this->response = $data;
    }

    public function actionListViewTicket($page = null, $user_id = null, $search = null, $flag = null)
    {
        $data = [];
        $query = Ticket::find()->alias('ticket_detail')->joinWith([
            'createdBy as ticket_creater_info',
            'assignTo as ticket_assign_user_info'
        ]);
        if ($flag == Ticket::FLAG_TRUE) {
            $query->andWhere([
                'ticket_detail.created_by_id' => \Yii::$app->user->id
            ]);
        }

        if (! empty($user_id)) {
            $query->where([
                'ticket_detail.created_by_id' => $user_id
            ]);
        }

        if (! empty($search)) {

            $query->where([
                'like',
                'ticket_creater_info.full_name',
                $search
            ])->orWhere([
                'like',
                'ticket_assign_user_info.full_name',
                $search
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionListViewLeave($page = null, $user_id = null, $search = null, $flag = null)
    {
        $data = [];

        $query = LeaveRequest::find()->alias('leave_request_data')->joinWith([
            'createdBy as leave_creater_info'
        ]);

        if ($flag == LeaveRequest::FLAG_TRUE) {
            $query->andWhere([
                'leave_request_data.created_by_id' => \Yii::$app->user->id
            ]);
        } else {

            $query->andWhere([
                'not in',
                'leave_request_data.created_by_id',
                \Yii::$app->user->id
            ]);
        }

        if (! empty($user_id)) {
            $query->where([
                'leave_request_data.created_by_id' => $user_id
            ]);
        }

        if (! empty($search)) {

            $query->where([
                'like',
                'leave_creater_info.full_name',
                $search
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionCaseListView($page = null, $state = null)
    {
        $data = [];

        $query = Cases::find();
        if (User::isEmployee()) {

            $ids = Task::find()->select('case_id')
                ->where([
                'assign_to_id' => \Yii::$app->user->id
            ])
                ->distinct()
                ->column();

            $query->andWhere([
                'in',
                'id',
                $ids
            ]);
        }

        $total_cases = $query->count();
        if (! empty($state)) {
            $query->where([
                'state_id' => $state
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $data['total_cases'] = $total_cases;
        $this->response = $data;
    }

    public function actionAttendanceListView($page = null, $user_id = null, $search = null, $flag = null)
    {
        $data = [];

        $query = Attendance::find()->alias('attendance')->joinWith([
            'user as attendanceuser'
        ]);
        if (! empty($user_id)) {
            $query->where([
                'attendance.user_id' => $user_id
            ]);
        }

        if (! empty($search)) {

            $query->where([
                'like',
                'attendanceuser.full_name',
                $search
            ]);
        }

        if ($flag == Attendance::FLAG_TRUE) {

            $query->andWhere([
                'attendance.user_id' => \Yii::$app->user->id
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionCaseReportList($page = null)
    {
        $data = [];

        $model = CaseReport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }
}