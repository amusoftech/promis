<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use app\models\Reminder;
use app\modules\notification\models\Notification;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\Cases;
use app\models\User;
use app\models\CaseReport;

/**
 * QualityController implements the API actions for User model.
 */
class InvestigatorController extends ApiTxController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = [];
        $behaviors['authenticator']['optional'] = [];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Api to add client report
     */
    public function actionAddReport()
    {
        $data = [];
        $model = new CaseReport();
        $post = \Yii::$app->request->bodyParams;

        if ((User::isInvestigator())||(User::isAdmin())) {
            $model->state_id = CaseReport::STATE_ACTIVE;
            if ($model->load($post)) {
                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['detail'] = $model->asJson();
                } else {
                    $data['error'] = $model->getErrorsString();
                }
            } else {
                $data['error'] = \Yii::t('app', 'No Data Posted');
            }
        } else {
            $data['error'] = \Yii::t('app', 'You are not the right person to create report.');
        }
        $this->response = $data;
    }

    /**
     * Api to get all client case report added by respective investigator
     *
     * @param int $page
     */
    public function actionGetCaseReport($page = null)
    {
        $data = [];
        $query = CaseReport::find()->where([
            'created_by_id' => \Yii::$app->user->id
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data['status'] = self::API_OK;
        $data = $pagination->serialize($dataProvider);

        $this->response = $data;
    }
}