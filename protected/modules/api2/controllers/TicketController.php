<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use app\models\Ticket;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\modules\comment\models\Comment;
use app\models\User;
use app\models\TicketType;
use app\models\Department;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * TicketController implements the API actions for User model.
 */
class TicketController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [
                [
                    'actions' => [
                        'list-view-ticket',
                        'update-ticket-status'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isHR() || User::isAdmin();
                    }
                ],
                [
                    'actions' => [

                        'delete-ticket'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isAdmin();
                    }
                ],
                [
                    'actions' => [
                        'add',
                        'update',
                        'get-my-ticket',
                        'ticket-type-list',
                        'list-view-ticket',
                        'ticket-comments',
                        'list',
                        'ticket-assign-user'
                    ],
                    'allow' => true,
                    'roles' => [
                        '@'
                    ]
                ],

                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    public function actionAddComment()
    {
        $data = [];
        $model = new Comment();
        $post = \Yii::$app->request->bodyParams;
        if ($model->load($post)) {
            $model->state_id = Comment::STATE_ACTIVE;
            $model->model_type = Ticket::className();
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['message'] = "Comment Added Successfully";
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', "Data not posted.");
        }

        $this->response = $data;
    }

    /**
     * Creates a new Ticket model.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $data = [];
        $model = new Ticket();

        $post = \Yii::$app->request->post();
        if ($model->load($post)) {

            $model->state_id = Ticket::STATE_ASSIGNED;
            $model->created_by_id = \Yii::$app->user->id;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['message'] = \Yii::t('app', "Ticket Added successfuly");
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No data Posted');
        }
        $this->response = $data;
    }

    public function actionUpdate($id)
    {
        $data = [];
        $model = Ticket::findOne($id);
        $post = \Yii::$app->request->post();
        if ($model->load($post) && $model->validate()) {
            $model->state_id = Ticket::STATE_ASSIGNED;
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['msg'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error'] = \Yii::t('app', 'No data Posted');
        }

        $this->response = $data;
    }

    public function actionListViewTicket($page = null, $user_id = null, $search = null, $flag = null)
    {
        $data = [];
        $query = Ticket::find()->alias('ticket_detail')->joinWith([
            'createdBy as ticket_creater_info',
            'assignTo as ticket_assign_user_info'
        ]);
        if ($flag == Ticket::FLAG_TRUE) {
            $query->andWhere([
                'ticket_detail.created_by_id' => \Yii::$app->user->id
            ]);
        }

        if (! empty($user_id)) {
            $query->where([
                'ticket_detail.created_by_id' => $user_id
            ]);
        }

        if (! empty($search)) {

            $query->where([
                'like',
                'ticket_creater_info.full_name',
                $search
            ])->orWhere([
                'like',
                'ticket_assign_user_info.full_name',
                $search
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionDeleteTicket($id)
    {
        $response = [];
        $model = Ticket::findOne($id);
        if (User::isAdmin()) {
            if (! empty($model)) {
                if ($model->delete()) {
                    $response['status'] = self::API_OK;
                    $response['message'] = \Yii::t('app', "Comment Successfuly Deleted");
                } else {
                    $response['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $response['error'] = \Yii::t('app', 'No data Found');
            }
        } else {
            $response['error'] = \Yii::t('app', "You Are not allowed to perform this action");
        }
        $this->response = $response;
    }

    public function actionUpdateTicketStatus($ticket_id)
    {
        $data = [];
        $model = Ticket::findOne($ticket_id);
        if (! empty($model)) {
            if ($model->save(false, [
                'state_id'
            ])) {
                $data['status'] = self::API_OK;
                $data['message'] = \Yii::t('app', "State successfuly Updated");
                $data['detail'] = $model->asjson();
            } else {
                $data['error'] = \Yii::t('app', $model->getErrorsString());
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Found");
        }

        $this->response = $data;
    }

    public function actionView($id)
    {
        $data = [];
        $model = Ticket::findOne($id);
        if (! empty($model)) {
            $data['status'] = self::API_OK;
            $data['detail'] = $model->asjson();
        } else {
            $data['error'] = \Yii::t('app', "No Ticket Found");
        }

        $this->response = $data;
    }

    public function actionList($page = null)
    {
        $data = [];

        $ids = User::find()->select('id')
            ->where([
            'role_id' => User::ROLE_HR
        ])
            ->column();
        $model = Ticket::find()->where([
            'created_by_id' => \Yii::$app->user->id
        ])->andWhere([
            'in',
            'assign_to_id',
            $ids
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionTicketTypeList($page = null)
    {
        $data = [];
        $model = Department::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionTicketAssignUser($id, $page = null)
    {
        $data = [];
        $model = User::find()->where([
            'department_id' => $id
        ])->andWhere([
            'not in',
            'id',
            \Yii::$app->user->id
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionTicketComments($id, $page = null)
    {
        $data = [];
        $model = Comment::find()->where([
            'model_id' => $id,
            'model_type' => Ticket::className()
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }
}