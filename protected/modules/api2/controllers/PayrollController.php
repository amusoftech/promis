<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\Payroll;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\User;

/**
 * PayrollController implements the API actions for User model.
 */
class PayrollController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [ // 'list-view'
        ];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [

                [
                    'actions' => [
                        'add',
                        'update',
                        'list-view',
                        'delete-payroll'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isHR() || User::isAdmin();
                    }
                ],
                [
                    'actions' => [
                        'get-my-payroll'
                    ],
                    'allow' => true,
                    'roles' => [
                        '@'
                    ]
                ],

                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    /**
     * Creates a new Payroll model.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = Payroll::find()->where([
            'user_id' => $post['Payroll']['user_id']
        ])
            ->limit(1)
            ->one();

        if (empty($model)) {
            $model = new Payroll();
            if ((User::isHR()) || (User::isAdmin())) {
                if ($model->load($post)) {
                    if ($model->save()) {
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "No Data Posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "you are not allowed to perform this action");
            }
        } else {

            $data['error'] = \Yii::t('app', "payroll already paid to this employee");
        }
        $this->response = $data;
    }

    public function actionUpdate($payroll_id)
    {
        $data = [];
        $post = \Yii::$app->request->bodyParams;
        $model = Payroll::findOne($payroll_id);
        if (! empty($model)) {
            if ((User::isHR()) || (User::isAdmin())) {
                if ($model->load($post)) {
                    $model->scenario = Payroll::SCENARIO_PAYROLL_ADD;
                    if ($model->save(false, [
                        'salary',
                        'user_id',
                        'state_id'
                    ])) {
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "No Data Posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "You Are Not Allowed to Perfor this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Found");
        }
        $this->response = $data;
    }

    public function actionListView($page = null, $user_id = null, $search = null)
    {
        $data = [];

        $Admin = User::find()->where([
            'role_id' => User::ROLE_ADMIN
        ])->one();

        $query = Payroll::find()->alias('payroll_data')
            ->joinWith([
            'user as payroll_user_info',
            'createdBy as payroll_creater_info'
        ])
            ->where([
            'payroll_data.created_by_id' => \Yii::$app->user->id
        ])
            ->orWhere([
            'payroll_data.created_by_id' => ! empty($Admin) ? $Admin->id : \Yii::$app->user->identity->id
        ]);

        if ($user_id) {
            $query = $query->where([
                'payroll_data.user_id' => $user_id
            ]);
        }

        if (! empty($search)) {

            $query->where([
                'like',
                'payroll_user_info.full_name',
                $search
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);

        $pagination = new TPagination();
        $pagination->params = [
            true
        ];

        $data = $pagination->serialize($dataProvider);
        $data['status'] = empty($data) ? self::API_OK : self::API_OK;
        $this->response = $data;
    }

    public function actionGetMyPayroll($page = null)
    {
        $data = [];
        $model = Payroll::find()->Where([
            'user_id' => \Yii::$app->user->id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data['status'] = self::API_OK;
        $data = $pagination->serialize($dataProvider);

        $this->response = $data;
    }

    public function actionDeletePayroll($payroll_id)
    {
        $data = [];
        $model = Payroll::findOne($payroll_id);
        if (! empty($model)) {
            if ((User::isHR()) || (User::isAdmin())) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Payroll Record successfuly deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Data Found");
        }
        $this->response = $data;
    }
}