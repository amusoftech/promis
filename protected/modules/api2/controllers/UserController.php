<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\models\LoginForm;
use app\models\User;
use app\modules\api2\components\ApiTxController;
use Yii;
use app\modules\api2\models\DeviceDetail;
use app\models\Halogins;
use app\modules\logger\models\Log;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use yii\web\UploadedFile;
use app\models\Files;
use app\modules\file\models\File;

/**
 * UserController implements the API actions for User model.
 */
class UserController extends ApiTxController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = [
            'login',
            'signup',
            'recover',
            'social-login',
            'match-otp',
            'send-otp',
            'employee-filter',
            'employee-list',
            'upload-employee-document',
            'employee-document-list'

            // 'employee-detail'
        ];
        $behaviors['authenticator']['optional'] = [];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $this->modelClass = "app\models\User";
        return $this->txSave();
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $data = [];
        $model = User::findOne([
            'id' => \Yii::$app->user->id
        ]);
        $old_file = $model->profile_file;
        if ($model->load(Yii::$app->request->post())) {
            if (! $model->saveUploadedFile($model, 'profile_file', $old_file)) {
                $model->profile_file = $old_file;
            }
            if ($model->save()) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asJson();
            } else {
                $data['error'] = $model->getErrorsString();
            }
        } else {
            $data['error_post'] = \Yii::t('app', 'No Data Posted');
        }
        $this->response = $data;
    }

    public function actionSignup()
    {
        $data = [];
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {
            if (isset($model->email) && ! empty($model->email)) {
                $email_identify = User::findByEmail($model->email);
                if (! empty($email_identify)) {
                    $data['error'] = \Yii::t('app', 'Email Already Exists');
                    $this->response = $data;
                    return;
                }
            }
            $model->state_id = User::STATE_INACTIVE;
            $model->email_verified = User::EMAIL_NOT_VERIFIED;
            $model->setPassword($model->password);
            $model->generatePasswordResetToken();
            if (! empty($_FILES)) {
                $model->saveUploadedFile($model, 'profile_file');
            }
            if ($model->save()) {
                // $model->sendVerificationMailtoUser();
                // $model->sendRegistrationMailtoAdmin();
                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['success'] = \Yii::t('app', "Verification link is sent to your email.");
                } else {
                    $data['error'] = $model->getErrorsString();
                }
            }
        } else {
            $data['error'] = \Yii::t('app', "Data not posted.");
        }
        $this->response = $data;
    }

    public function actionCheck()
    {
        $data = [];
        $deviceToken = DeviceDetail::find()->where([
            'created_by_id' => \Yii::$app->user->id
        ])->one();
        if (! empty($deviceToken)) {

            if ($deviceToken->load(Yii::$app->request->post())) {
                if ($deviceToken->save()) {
                    $data['status'] = self::API_OK;
                    $data['access-token'] = ! empty($deviceToken->createdBy) ? $deviceToken->createdBy->access_token : null;
                    $data['detail'] = ! empty($deviceToken->createdBy) ? $deviceToken->createdBy->asJson(false) : null;
                } else {
                    $data['error'] = $deviceToken->getErrors();
                }
            } else {
                $data['error'] = \Yii::t('app', "No data posted");
            }
        } else {
            $data['error'] = \Yii::t('app', "No device token found");
        }

        $this->response = $data;
    }

    /**
     *
     * @return string|string[]|NULL[]
     */
    public function actionLogin()
    {
        $data = [];
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $user = User::findByUsername($model->username);
            if ($user) {
                if ($model->login()) {

                    if ($user->otp_verified == User::OTP_VERIFIED_NO) {

                        $user->otp = '1234'; // $user->createOtp();
                        $user->save(false, [
                            'otp'
                        ]);
                    }
                    $user->generateAccessToken();
                    $user->save(false, [
                        'access_token'
                    ]);
                    $data['status'] = self::API_OK;
                    $data['access-token'] = $user->access_token;
                    (new DeviceDetail())->appData($model);
                    $data['detail'] = $user->asJson();
                } else {
                    $data['error'] = $model->getErrorsString();
                }
            } else {
                $data['error'] = ' Incorrect Username';
            }
        } else {
            $data['error'] = "No data posted.";
        }
        $this->response = $data;
    }

    public function actionSendOtp()
    {
        $data = [];
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {

            $user = User::find()->where([
                'contact_no' => $model->contact_no
            ])->one();

            if (! empty($user)) {
                // $user->otp = $user->getCode();
                $user->otp = '1234';
                $user->updateAttributes([
                    'otp'
                ]);
                // $sms = $user->sendMessage($user);
                // if (! empty($sms)) {
                // $data['error'] = $sms;
                // $this->response = $data;
                // return;
                // } else {
                $data['otp'] = $user->otp;
                $data['status'] = self::API_OK;
                $data['msg'] = \Yii::t('app', 'Verification code sent successfully');
                $data['detail'] = $user->asJson();

                // }
            } else {
                $data['error'] = \Yii::t('app', 'No User found');
            }
        } else {
            $data['error'] = \Yii::t('app', 'No data posted');
        }

        $this->response = $data;
    }

    public function actionMatchOtp()
    {
        $data = [];
        $model = new User([
            'scenario' => User::SCENARIO_OTP
        ]);
        $login = new LoginForm([
            'scenario' => LoginForm::SCENARIO_OTP
        ]);
        $post = \Yii::$app->request->bodyParams;

        if ($model->load($post) && $login->load($post)) {
            $user = User::find()->where([
                'otp' => $model->otp,
                'contact_no' => $model->contact_no
            ])->one();
            if (! empty($user)) {
                if ($user->otp == $model->otp) {
                    $user->otp = "";
                    $user->state_id = User::STATE_ACTIVE;
                    $user->otp_verified = User::OTP_VERIFIED_YES;
                    $user->generateAccessToken();
                    if ($user->save(false, [
                        'state_id',
                        'otp',
                        'access_token',
                        'otp_verified'
                    ])) {
                        $login->username = $user->contact_no;
                        $login->password = $model->password;

                        if ($login->login()) {
                            (new DeviceDetail())->appData($login);
                            $data['status'] = self::API_OK;
                            $data['message'] = \Yii::t('app', "Your account successfully verified!");
                            $data['detail'] = $login->asJson();
                            $data['user_detail'] = $user->asJson();
                            $data['access-token'] = $user->access_token;
                        } else {
                            $data['error'] = $login->getErrorsString();
                        }
                    } else {
                        $data['error'] = $user->getErrorsString();
                    }
                } else {
                    $data['error'] = \Yii::t('app', 'Otp not Matched');
                }
            } else {
                $data['error'] = \Yii::t('app', 'Verification Code not matched');
            }
        } else {
            $data['error'] = \Yii::t('app', 'No data posted');
        }

        $this->response = $data;
    }

    public function actionLogout()
    {
        $data = [];
        $user = \Yii::$app->user->identity;
        if (\Yii::$app->user->logout()) {
            $user->access_token = '';

            $user->save(false, [
                'access_token'
            ]);
            (new DeviceDetail())->deleteOldAppData($user->id);
            $data['status'] = self::API_OK;
        }

        $this->response = $data;
    }

    public function actionChangePassword()
    {
        $data = [];
        $model = User::findOne([
            'id' => \Yii::$app->user->identity->id
        ]);

        $newModel = new User([
            'scenario' => 'changepassword'
        ]);
        if ($newModel->load(Yii::$app->request->post()) && $newModel->validate()) {
            if ($model->validatePassword($newModel->oldPassword)) {
                $model->setPassword($newModel->newPassword);
                if ($model->save()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', 'Password Updated successfully');
                } else {
                    $data['error'] = \Yii::t('app', 'Incorrect Password');
                }
            } else {
                $data['error'] = \Yii::t('app', 'Old password is incorrect');
            }
        }
        $this->response = $data;
    }

    public function actionAddLog()
    {
        $data = [];
        $model = new Log();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                $sub = "An Error/Crash was reported : " . \Yii::$app->params['company'];
                Yii::$app->mailer->compose([
                    'html' => 'errorlog'
                ], [
                    'user' => $model
                ])
                    ->setTo(\Yii::$app->params['adminEmail'])
                    ->setFrom(\Yii::$app->params['logEmail'])
                    ->setSubject($sub)
                    ->send();
            }
        }
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        return $this->txDelete($id, "User");
    }

    public function actionProfile($id = null)
    {
        $data = [];
        $id = ($id == null) ? \Yii::$app->user->id : $id;

        $model = User::findOne($id);
        if (! empty($model)) {
            $data['status'] = self::API_OK;
            $data['detail'] = $model->asCustomJson();
        }
        $this->response = $data;
    }

    /**
     * Social Login.
     *
     * @return mixed
     */
    public function actionSocialLogin()
    {
        $flag = false;
        $data = [];
        $params = \Yii::$app->request->getBodyParams();
        if (! empty($params['User'])) {
            $auth = Halogins::find()->where([
                'userId' => $params['User']['userId']
            ])->one();

            if (empty($auth)) { // not exist
                if ((! empty($params['User']['email']) && ! empty($params['User']['userId']))) {
                    $contact_no = $params['User']['contact_no'];
                    $email = $params['User']['email'];
                    $password = $params['User']['password'];
                    $address = $params['User']['address'];
                    $address_line = $params['User']['address_line'];
                    $firstname = $params['User']['first_name'];
                    $lastname = $params['User']['last_name'];
                    $username = $params['User']['username'];
                    $country = $params['User']['country'];
                    $zipcode = $params['User']['zipcode'];
                    $date_of_birth = $params['User']['date_of_birth'];
                    $state = $params['User']['state'];
                    $city = $params['User']['city'];
                    $id = $params['User']['userId'];
                    $provider = $params['User']['provider'];
                    $token = $params['LoginForm']['device_token'];
                    $type = $params['LoginForm']['device_type'];
                    $device_name = $params['LoginForm']['device_name'];
                    $email_identify = '';
                    $contact_identify = '';
                    $transaction = \Yii::$app->db->beginTransaction();
                    $email_identify = User::findByUsername($email);
                    $password = (new User())->hashPassword($id);
                    if (! empty($email_identify)) {
                        $user = $email_identify;
                    } else {
                        $user = new User([
                            'contact_no' => $contact_no,
                            'email' => $email,
                            'username' => $username,
                            'first_name' => $firstname,
                            'last_name' => $lastname,
                            'country' => $country,
                            'state' => $state,
                            'city' => $city,
                            'zipcode' => $zipcode,
                            'password' => $password,
                            'created_on' => date('Y-m-d H:i:s'),
                            'date_of_birth' => $date_of_birth,
                            'address' => $address,
                            'address_line' => $address_line,
                            'role_id' => User::ROLE_USER
                        ]);
                        if (! empty($params['img_url'])) {
                            $random = rand(0, 999) . 'dummy_img.png';
                            $user->profile_file = $random;
                            copy($params['img_url'], UPLOAD_PATH . $random);
                        }
                        $user->state_id = User::STATE_ACTIVE;
                    }
                    $user->generatePasswordResetToken();

                    $user->first_name = $firstname;
                    $user->last_name = $lastname;
                    if (! $user->save()) {

                        $data['msg'] = $user->getErrorsString();
                        $data['customError'] = \Yii::t('app', "user entry");
                        return $this->response = $data;
                    } else {
                        $user->generateAccessToken();
                        $flag = true;
                    }
                    $auth = new Halogins([
                        'userId' => (string) $id,
                        'loginProvider' => $provider,
                        'loginProviderIdentifier' => md5($id),
                        'user_id' => $user->id
                    ]);
                    if (! $auth->save()) {
                        $data['customError'] = "auth entry";
                        $data['msg'] = $auth->getErrorsString();
                        return $this->response = $data;
                    } else {
                        $flag = true;
                    }
                    $login_form = new LoginForm();
                    if (! $login_form->load(\Yii::$app->request->post())) {
                        $data['customError'] = \Yii::t('app', "post banned");
                        $data['msg'] = \Yii::t('app', "Data required for login can not be blank");
                        return $this->response = $data;
                    } else {

                        $flag = true;
                    }
                    if ($flag) {

                        $transaction->commit();
                        $login_form->username = $username;
                        $login_form->password = $id;
                        if ($login_form->login()) {

                            (new DeviceDetail())->appData($login_form);
                            $data['access-token'] = $user->access_token;
                            $data['is_login'] = "0";
                            $data['status'] = self::API_OK;
                            $data['detail'] = $user->asJson();
                            $data['msg'] = \yii::t('app', 'Signup');
                        }
                    } else {

                        $transaction->rollBack();
                    }
                } else {
                    $data['msg'] = \yii::t('app', 'Please fill all the Details');
                    return $this->response = $data;
                }
            } else { // already exist
                $user_model = User::findOne([
                    'id' => $auth->user_id
                ]);
                if ($user_model->state_id == User::STATE_BANNED) {
                    $data['customError'] = \Yii::t('app', "banned");
                    $data['msg'] = \Yii::t('app', 'Your account is blocked, Please contact Particulars Admin');
                    return $this->response = $data;
                }
                if ($user_model->state_id == User::STATE_INACTIVE) {
                    $data['customError'] = \Yii::t('app', "inactive");
                    $data['msg'] = \Yii::t('app', 'Your account is not verified by admin');
                    $data['id'] = $user_model->id;
                    return $this->response = $data;
                }
                $user = $auth->user;
                if (empty($user_model)) {
                    $data['customError'] = \Yii::t('app', "not found");
                    $data['msg'] = \Yii::t('app', "User not found");
                    return $this->response = $data;
                }
                $login_form = new LoginForm();
                $login_form->username = $user->email;
                $login_form->password = $user->password;
                if (! $login_form->load(\Yii::$app->request->post())) {
                    $data['customError'] = \Yii::t('app', "post banned");
                    $data['msg'] = \Yii::t('app', "Data required for login can not be blank");
                    return $this->response = $data;
                }
                if (\Yii::$app->user->login($user, 3600 * 24 * 30)) {
                    $user_model->generateAccessToken();
                    if ($user_model->save()) {
                        (new DeviceDetail())->appData($login_form);
                        $data['is_login'] = "1";
                        $data['detail'] = $user_model->asJson();
                        $data['success'] = yii::t('app', 'Login Successfully');
                        $data['status'] = self::API_OK;
                        $data['access-token'] = $user_model->access_token;
                    }
                }
            }
        } else {
            $data['msg'] = \Yii::t('app', 'No data posted');
        }
        $this->response = $data;
    }

    public function actionEmployeeFilter($page = null)
    {
        $response = [];
        $post = Yii::$app->request->post();
        $fullname = $post['User']['full_name'];
        $emp_email = $post['User']['email'];
        $contact_no = $post['User']['contact_no'];
        $role_id = $post['User']['role_id'];

        $query = User::find()->alias('usr')->joinWith([
            'department as department_info'
        ]);

        if ($fullname) {
            $query->where([
                'like',
                'usr.full_name',
                $fullname
            ]);
        } elseif ($emp_email) {
            $query->where([
                'like',
                'usr.email',
                $emp_email
            ]);
        } elseif ($contact_no) {
            $query->where([
                'like',
                'usr.contact_no',
                $contact_no
            ]);
        } else {

            $query = User::find();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];

        $data = $pagination->serialize($dataProvider);

        $response = $data;
        $response['status'] = self::API_OK;
        $this->response = $response;
    }

    public function actionEmployeeList($page = null)
    {
        $data = [];
        if ((User::isHR()) || (User::isAdmin())) {
            $model = User::find()->where([
                'state_id' => User::STATE_ACTIVE
            ]);
            $total_employee = $model->count();
        } else {
            $data['error'] = \Yii::t('app', "you are not allowed to perform this action");
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];

        $data = $pagination->serialize($dataProvider);
        $response = $data;
        $response['status'] = self::API_OK;
        $response['total_employee'] = $total_employee;
        $this->response = $response;
    }

    public function actionEmployeeDetail($employee_id)
    {
        $data = [];
        if (! empty($employee_id)) {
            $model = User::findOne($employee_id);
        } else {
            $model = User::findOne(\Yii::$app->user->id);
        }

        if (! empty($model)) {
            if ((User::isAdmin()) || (User::isHR())) {
                $data['status'] = self::API_OK;
                $data['detail'] = $model->asjson(true);
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Employee Found");
        }
        $this->response = $data;
    }

    public function actionDeleteEmployee($employee_id)
    {
        $data = [];
        $model = User::findOne($employee_id);
        if (! empty($model)) {
            if ((User::isAdmin()) || (User::isHR())) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Employee successfuly deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
            }
        } else {
            $data['error'] = \Yii::t('app', "No Employee Found");
        }
        $this->response = $data;
    }

    public function actionUploadEmployeeDocument($user_id = null)
    {
        $data = [];
        $model = User::findOne($user_id);
        $fileModel = new File();
        $imagefile = UploadedFile::getInstances($fileModel, 'filename_path');

        if (! empty($imagefile)) {

            $fileModel->uploadFiles($imagefile, $model, false);

            $data['status'] = self::API_OK;
            // $data['detail'] = $fileModel->asjson();
            $data['message'] = \Yii::t('app', "document successfuly uploaded");
        } else {
            $data['error'] = \Yii::t('app', "no data posted");
        }

        $this->response = $data;
    }

    public function actionEmployeeDocumentList($user_id = null, $page = null)
    {
        $data = [];
        $model = Files::find()->Where([
            'model_id' => $user_id,
            'model_type' => User::className()
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];

        $data = $pagination->serialize($dataProvider);

        $response = $data;
        $response['status'] = self::API_OK;
        $this->response = $response;
    }
}
