<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use app\models\Reminder;
use app\modules\notification\models\Notification;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\Cases;
use app\models\CaseReport;

/**
 * QualityController implements the API actions for User model.
 */
class QualityController extends ApiTxController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = [];
        $behaviors['authenticator']['optional'] = [];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Api to get all cases
     *
     * @param int $page
     */
    public function actionGetCases($page = null)
    {
        $data = [];
        $query = Cases::find()->where([
            'assign_to_id' => \Yii::$app->user->id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data['status'] = self::API_OK;
        $data = $pagination->serialize($dataProvider);

        $this->response = $data;
    }

    /**
     * Api to get all assigned report to the respective Quality controller
     *
     * @param int $page
     */
    public function actionGetAssignedReport($page = null)
    {
        $data = [];
        $query = CaseReport::find()->where([
            'submitted_to' => \Yii::$app->user->id
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data['status'] = self::API_OK;
        $data = $pagination->serialize($dataProvider);

        $this->response = $data;
    }
}