<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\modules\api2\controllers;

use app\modules\api2\components\ApiTxController;
use app\models\Attendance;
use app\models\AttendanceEvent;
use yii\data\ActiveDataProvider;
use app\modules\api2\components\TPagination;
use app\models\User;
use app\components\TActiveForm;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * AttendanceController implements the API actions for User model.
 */
class AttendanceController extends ApiTxController
{

    public function behaviors()
    {
        $unAuthorize = [];

        $optional = [
            'list-view',
            'attendence-event-list'
        ];

        $behaviors = parent::behaviors();
        $behaviors['authenticator']['optional'] = $optional;
        $behaviors['authenticator']['except'] = $unAuthorize;
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'rules' => [

                [
                    'actions' => [
                        'add',
                        'add-attendence-event',
                        'list-view',
                        'event-list',
                        'update-attendence-event',
                        'delete-attendence-event',
                        'attendance-event-list',
                        'update-attendance',
                        'delete-attendence',
                        'get-employee-attendance'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {

                        return User::isHR() || User::isAdmin();
                    }
                ],
                [
                    'actions' => $optional,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*',
                        '@'
                    ]
                ],

                [
                    'actions' => $unAuthorize,
                    'allow' => true,
                    'roles' => [
                        '?',
                        '*'
                    ]
                ]
            ],
            'denyCallback' => function ($rule, $action) {
                throw new \yii\web\ForbiddenHttpException(\Yii::t('app', 'You are not allowed to access this page'));
            }
        ];

        return $behaviors;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['get'] = [
            'GET'
        ];
        return $verbs;
    }

    /**
     * Displays a single User model.
     *
     * @return mixed
     */
    public function actionGet($id)
    {
        $this->modelClass = "app\models\User";
        return $this->txget($id);
    }

    /**
     * Creates a new Attendance model.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $data = [];
        $attendance = new Attendance();
        if ((User::isAdmin()) || (User::isHR())) {
            if ($attendance->load(\Yii::$app->request->bodyParams)) {
                $attendance->state_id = Attendance::STATE_PRESENT;
                $attendance->total_time = $attendance->getTotalTime();
                $attendance->created_by_id = \Yii::$app->user->id;
                if ($attendance->save()) {
                    $data['status'] = self::API_OK;
                    $data['msg'] = $attendance->asJson();
                    $data['total_time'] = $attendance->total_time;
                } else {
                    $data['error'] = $attendance->getErrorsString();
                }
            } else {
                $data['error'] = \Yii::t('app', 'No Data Posted');
            }
        } else {
            $data['error'] = \Yii::t('app', "you are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionUpdateAttendance($attendance_id)
    {
        $data = [];
        $model = Attendance::findOne($attendance_id);

        if ((User::isAdmin()) || (User::isHR())) {
            if (! empty($model)) {
                if ($model->state_id == Attendance::STATE_ABSENT) {
                    if ($model->load(\Yii::$app->request->bodyParams)) {
                        if ($model->save(false, [
                            'date',
                            'user_id',
                            'state_id'
                        ])) {
                            $data['status'] = self::API_OK;
                            $data['detail'] = $model->asjson();
                        } else {
                            $data['error'] = \Yii::t('app', $model->getErrorsString());
                        }
                    } else {
                        $data['error'] = \Yii::t('app', " No Data Posted");
                    }
                } else {
                    $data['error'] = \Yii::t('app', "State of Employee is already Present");
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Found");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionDeleteAttendence($attendence_id)
    {
        $data = [];
        $model = Attendance::findOne($attendence_id);
        if (! empty($model)) {
            if ((User::isHR()) || (User::isAdmin())) {

                $transaction = \Yii::$app->db->beginTransaction();
                try {

                    if ($model->delete()) {

                        $attendence_event = AttendanceEvent::find()->Where([
                            'user_id' => $model->user_id
                        ])->each();

                        if ($attendence_event > 0) {

                            foreach ($attendence_event as $events) {
                                if (! $events->delete()) {
                                    $transaction->rollBack();
                                    $data['error'] = \Yii::t('app', $events->getErrorsString());
                                }
                            }
                            $transaction->commit();
                        }

                        $data['status'] = self::API_OK;
                        $data['message'] = \Yii::t('app', "Attendence successfuly deleted");
                    } else {
                        $transaction->rollBack();
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    \Yii::$app->getSession()->setFlash('error', $e->getMessage());
                }
            } else {
                $data['error'] = \Yii::t('app', 'You are not allowed to perform this action');
            }
        } else {
            $data['error'] = \Yii::t('app', 'No Data Found');
        }
        $this->response = $data;
    }

    public function actionAddAttendenceEvent()
    {
        $data = [];

        if (User::isHR()) {
            $event = new AttendanceEvent();
            if ($event->load(\Yii::$app->request->bodyParams)) {
                $event->state_id = AttendanceEvent::STATE_ACTIVE;
                $event->created_by_id = \Yii::$app->user->id;
                $event->time = date('H:i:s');
                if ($event->save()) {
                    $data['status'] = self::API_OK;
                    $data['msg'] = $event->asJson();
                } else {
                    $data['error'] = $event->getErrorsString();
                }
            } else {
                $data['error'] = \Yii::t('app', 'No Data Posted');
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionUpdateAttendenceEvent($attendence_event_id)
    {
        $data = [];

        if (User::isHR()) {
            $model = AttendanceEvent::findOne($attendence_event_id);
            if (! empty($model)) {
                if ($model->load(\Yii::$app->request->bodyParams)) {
                    if ($model->save(false, [
                        'date',
                        'type_id',
                        'user_id',
                        'time'
                    ])) {
                        $data['status'] = self::API_OK;
                        $data['detail'] = $model->asjson();
                    } else {
                        $data['error'] = \Yii::t('app', $model->getErrorsString());
                    }
                } else {
                    $data['error'] = \Yii::t('app', "no Data posted");
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Found");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionDeleteAttendenceEvent($attendence_event_id)
    {
        $data = [];
        if (User::isHR()) {
            $model = AttendanceEvent::findOne($attendence_event_id);
            if (! empty($model)) {
                if ($model->delete()) {
                    $data['status'] = self::API_OK;
                    $data['message'] = \Yii::t('app', "Attendence Event successfuly deleted");
                } else {
                    $data['error'] = \Yii::t('app', $model->getErrorsString());
                }
            } else {
                $data['error'] = \Yii::t('app', "No Data Found");
            }
        } else {
            $data['error'] = \Yii::t('app', "You are not allowed to perform this action");
        }
        $this->response = $data;
    }

    public function actionListView($page = null, $user_id = null, $search = null, $flag = null)
    {
        $data = [];

        $query = Attendance::find()->alias('attendance')->joinWith([
            'user as attendanceuser'
        ]);
        if (! empty($user_id)) {
            $query->where([
                'attendance.user_id' => $user_id
            ]);
        }

        if (! empty($search)) {

            $query->where([
                'like',
                'attendanceuser.full_name',
                $search
            ]);
        }

        if ($flag == Attendance::FLAG_TRUE) {

            $query->andWhere([
                'attendance.user_id' => \Yii::$app->user->id
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            true
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $this->response = $data;
    }

    public function actionAttendanceEventList($page = null, $user_id)
    {
        $data = [];
        $total_event = '';
        $query = AttendanceEvent::find()->Where([
            'created_by_id' => \Yii::$app->user->id
        ]);
        if (! empty($user_id)) {
            $query->where([
                'user_id' => $user_id,
                'created_by_id' => \Yii::$app->user->id
            ]);
        }
        $total_event = $query->count();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
            'pagination' => [
                'pageSize' => '10',
                'page' => $page
            ]
        ]);
        $pagination = new TPagination();
        $pagination->params = [
            false
        ];
        $data = $pagination->serialize($dataProvider);
        $data['status'] = self::API_OK;
        $data['total_event'] = $total_event;
        $this->response = $data;
    }
}