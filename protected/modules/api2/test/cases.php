<?php
return [
    'cases' => [

        'add?access-token=' => [
            'Cases[title]' => '',
            'Cases[description]' => '',
            'Cases[client_id]' => '',
            'Cases[department_id]' => '',
            'Cases[start_date]' => '',
            'Cases[end_date]' => '',
            'Cases[assign_to_id]' => ''
        ],

        'update?id=&access-token=' => [
            'Cases[title]' => '',
            'Cases[description]' => '',
            'Cases[state_id]' => '',
            'Cases[client_id]' => '',
            'Cases[department_id]' => '',
            'Cases[end_date]' => '',
            'Cases[assign_to_id]' => ''
        ],
        'list-view?access-token=' => [],

        'add-case-platform?access-token=' => [
            'CasePlatform[case_id]' => '',
            'CasePlatform[caseType]' => '1,2,3'
        ],

        'add-case-team?access-token=' => [
            'Team[case_id]' => '',
            'Team[user_id]' => '',
            'Team[type_id]' => ''
        ],
        'delete-team?team_id=&access-token=' => [],
        'update-case-team?team_id=&access-token=' => [
            'Team[case_id]' => '',
            'Team[user_id]' => ''
        ],

        'update-status?id=&access-token=' => [
            'Task[state_id]' => ''
        ],

        'get-team?case_id=&page=0&access-token=' => [],

        'start?access-token=' => [
            'Timer[task_id]' => '',
            'Time[work_time]' => '' // comman separated
        ],
        'delete-case?case_id=&access-token=' => [],
        'add-case-report?access-token=' => [
            'CaseReport[title]' => '',
            'CaseReport[description]' => '',
            'CaseReport[case_id]' => '',
            'CaseReport[submitted_to]' => ''
        ],
        'update-case-report?case_report_id=&access-token=' => [
            'CaseReport[title]' => '',
            'CaseReport[description]' => '',
            'CaseReport[case_id]' => '',
            'CaseReport[submitted_to]' => ''
        ],
        'get-case-report?page=0&case_report_id=&access-token=' => [],
        'delete-case-report?case_report_id=&access-token=' => [],
        'case-report-list?page=&access-token=' => [],
        'case-comment?id=&page=&access-token=' => []
    ]
];
?>