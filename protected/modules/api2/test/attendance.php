<?php
return [
    'attendance' => [

        'add?access-token=' => [
            'Attendance[date]' => '',
            'Attendance[user_id]' => '',
            'Attendance[state_id]' => ''
        ],
        'update-attendance?attendance_id=&access-token=' => [
            'Attendance[date]' => '',
            'Attendance[user_id]' => '',
            'Attendance[state_id]' => ''
        ],
        'delete-attendence?attendence_id=&access-token=' => [],

        'add-attendence-event?access-token=' => [
            'AttendanceEvent[date]' => '',
            'AttendanceEvent[type_id]' => '',
            'AttendanceEvent[user_id]' => ''
        ],
        'update-attendence-event?attendence_event_id=&access-token=' => [
            'AttendanceEvent[date]' => '',
            'AttendanceEvent[type_id]' => '',
            'AttendanceEvent[user_id]' => '',
            'AttendanceEvent[time]' => ''
        ],
        'delete-attendence-event?attendence_event_id=&access-token=' => [],
        'list-view?page=&user_id=&search=&flag=&access-token=' => [],
       
        'attendance-event-list?page=&user_id=&access-token=' => []
    ]
];
?>