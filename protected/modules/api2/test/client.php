<?php
return [
    'client' => [

        'add?access-token=' => [
            'Client[name]' => '',
            'Client[email]' => '',
            'Client[phone]' => '',
            'Client[address]' => ''
        ],

        'update?id=&access-token=' => [
            'Client[name]' => '',
            'Client[email]' => '',
            'Client[phone]' => '',
            'Client[address]' => ''
        ],
        'list-view?page=&access-token=' => [],
        'delete-client?client_id=&access-token=' => [],

        'add-client-report?access-token=' => [
            'ClientReports[case_id]' => '',
            'ClientReports[report]' => '',
            'ClientReports[sent_by]' => '',
            'ClientReports[date]' => '',
            'File[title]' => ''
        ],
        'update-client-report?client_report_id=&access-token=' => [
            'ClientReports[case_id]' => '',
            'ClientReports[report]' => '',
            'ClientReports[sent_by]' => '',
            'ClientReports[date]' => '',
            'File[title]' => ''
        ],
        'delete-client-report?client_report_id=&access-token=' => [],
        'client-report-list?case_id=$page=&access-token=' => [],
        'add-client-bill?access-token=' => [
            'ClientBills[case_id]' => '',
            'ClientBills[full_name]' => '',
            'ClientBills[amount]' => '',
            'ClientBills[date]' => '',
            'ClientBills[sent_by]' => ''
        ],
        'update-client-bill?client_bill_id=&access-token=' => [
            'ClientBills[case_id]' => '',
            'ClientBills[full_name]' => '',
            'ClientBills[amount]' => '',
            'ClientBills[date]' => '',
            'ClientBills[sent_by]' => ''
        ],
        'client-bill-list?case_id=&page=&access-token=' => [],
        'delete-client-bill?client_bill_id=&access-token=' => []
    ]
];