<?php
return [
    'department' => [

        'add?access-token=' => [
            'Department[title]' => '',
            'Department[manager_id]' => ''
        ],

        'update?id=&access-token=' => [
            'Department[title]' => '',
            'Department[manager_id]' => ''
        ],
        'department-users?page=&dept_id=&access-token=' => [],
        'list-view?access-token=' => [],

        'assign-task?access-token=' => [
            'Task[title]' => '',
            'Task[description]' => '',
            'Task[case_id]' => '',
            'Task[platform_id]' => '',
            'Task[assign_to_id]' => '',
            'Task[start_date]' => '',
            'Task[end_date]' => '',
            'Task[estimated_time]' => ''
        ],

        'update-assign-task?task_id=&access-token=' => [
            'Task[title]' => '',
            'Task[description]' => '',
            'Task[case_id]' => '',
            'Task[platform_id]' => '',
            'Task[assign_to_id]' => '',
            'Task[start_date]' => '',
            'Task[end_date]' => '',
            'Task[estimated_time]' => ''
        ],
        'my-assigned-task-list?page=&user_id=&access-token=' => [],
        'delete-task?task_id=&access-token=' => [],
        'update-task-state?task_id=&access-token=' => [
            'Task[state_id]' => ''
        ],
        'get-task-detail?task_id=&access-token=' => [],
        'list-view-ticket?page=&user_id=&search=&flag=&access-token=' => [],
        'list-view-leave?page=&user_id=&search=&flag=&access-token=' => [],
        'case-list-view?access-token=' => [],
        'attendance-list-view?page=&user_id=&search=&flag=&access-token=' => [],
        'case-report-list?page=&access-token=' => []
    ]
];
?>