<?php
return [
    'case-platform' => [

        'add-platform?access-token=' => [
            'Platform[title]' => '',
            'Platform[description]' => ''
        ],
        'update-platform?platform_id=&access-token=' => [
            'Platform[title]' => '',
            'Platform[description]' => ''
        ],
        'platform-list?page=0&access-token=' => [],
        'delete-platform?platform_id=&access-token=' => [],

        'add-case-platform?access-token=' => [
            'CasePlatform[type_id]' => '',
            'CasePlatform[case_id]' => '',
            'CasePlatform[state_id]' => ''
        ],
        'update-case-platform?id=&access-token=' => [
            'CasePlatform[type_id]' => '',
            'CasePlatform[case_id]' => '',
            'CasePlatform[state_id]' => ''
        ],
        'get-case?state_id=&page=0&access-token=' => [],

        'list-view?access-token=' => []
    ]
];