<?php
return [
    'ticket' => [
        'ticket-type-list?page=access-token=' => [],

        'add?access-token=' => [
            'Ticket[title]' => '',
            'Ticket[description]' => '',
            'Ticket[type_id]' => '',
            'Ticket[assign_to_id]' => '',
            //'Ticket[assign_to_id]' => ''
        ],

        'update?id=&access-token=' => [
            'Ticket[title]' => '',
            'Ticket[description]' => '',
            'Ticket[type_id]' => '',
            'Ticket[assign_to_id]' => ''
        ],
        'list-view-ticket?page=&user_id=&search=&flag=&access-token=' => [],
        'delete-ticket?id=&access-token=' => [],
        'view?id=&access-token=' => [],
        'update-ticket-status?ticket_id=&access-token=' => [
            'Ticket[state_id]' => ''
        ],
    ]
];
?>