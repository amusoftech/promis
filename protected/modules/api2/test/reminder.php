<?php
return [
    'reminder' => [

        'add?access-token=' => [
            'Reminder[title]' => '',
            'Reminder[description]' => '',
            'Reminder[case_id]' => '',
            'Reminder[department_id]' => '',
            'Reminder[user_id]' => '',
            'Reminder[time]' => ''
        ],

        'update?id=&access-token=' => [
            'Reminder[title]' => '',
            'Reminder[description]' => '',
            'Reminder[case_id]' => '',
            'Reminder[department_id]' => '',
            'Reminder[user_id]' => '',
            'Reminder[time]' => ''
        ],

        'reminder-list?user_id=&access-token=' => [],
        'delete-reminder?reminder_id=&access-token=' => []
    ]
];
?>