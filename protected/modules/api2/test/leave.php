<?php
return [
    'leave' => [

        'add?access-token=' => [
            'LeaveRequest[description]' => '',
            'LeaveRequest[start_date]' => '',
            'LeaveRequest[is_halfday]' => '0'
        ],

        'update?id=&access-token=' => [
            'LeaveRequest[description]' => '',
            'LeaveRequest[start_date]' => '',
            'LeaveRequest[is_halfday]' => '0'
        ],

        // 'list-view-leave?page=&access-token=' => [],
        'list-view-leave?page=&user_id=&search=$flag=&access-token=' => [],
        'update-leave-request-status?leave_request_id=' => [
            'LeaveRequest[state_id]' => ''
        ],
        'update-leave-request?id=&access-token=' => [
            'LeaveRequest[state_id]' => ''
        ],
        'delete-leave-request?leave_request_id=&access-token=' => [],
        'get-request-detail?request_id=&access-token=' => []
    ]
];
?>