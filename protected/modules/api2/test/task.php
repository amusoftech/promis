<?php
return [
    'task' => [

        'add-task?access-token=' => [
            'Task[title]' => '',
            'Task[description]' => '',
            'Task[case_id]' => '',
            'Task[platform_id]' => '',
            'Task[start_date]' => '',
            'Task[end_date]' => '',
            'Task[assign_to_id]' => '',
            'Task[department_id]' => '',
            'Task[estimated_time]' => ''
        ],
        'update-task?task_id=&access-token=' => [
            'Task[title]' => '',
            'Task[description]' => '',
            'Task[case_id]' => '',
            'Task[platform_id]' => '',
            'Task[start_date]' => '',
            'Task[end_date]' => '',
            'Task[assign_to_id]' => '',
            'Task[department_id]' => '',
            'Task[estimated_time]' => ''
        ],
        'update-task-status?task_id=&access-token=' => [
            'Task[state_id]' => ''
        ],

        'get-task-list?page=&user_id=&search=&case_name=&access-token=' => [],
    
       /*  'start-task-timer?task_id=&access-token=' => [
            'Task[start_time]' => ''
        ],
        'end-task-timer?task_id=&access-token=' => [
            'Task[end_time]' => ''
        ], */
        'start-timer?id=&access-token=' => [
            'Task[start_time]' => ''
        ],
        'end-timer?id=&access-token=' => [
            'Task[end_time]' => ''
        ],
        'delete-task?task_id=&access-token=' => [],

        'add-task-event?access-token=' => [
            'TaskEvent[date]' => '',
            'TaskEvent[task_id]' => '',
            'TaskEvent[start_time]' => '',
            'TaskEvent[end_time]' => ''
        ],
        'update-task-event?task_event_id=&access-token=' => [
            'TaskEvent[date]' => '',
            'TaskEvent[task_id]' => '',
            'TaskEvent[start_time]' => '',
            'TaskEvent[end_time]' => ''
        ],
        'delete-task-event?task_event_id=&access-token=' => [],

        'task-event-list-view?page=&user_id=&access-token=' => []
    ]
];
?>