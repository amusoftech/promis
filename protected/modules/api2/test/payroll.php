<?php
return [
    'payroll' => [

        'add?access-token=' => [
            'Payroll[salary]' => '',
            'Payroll[user_id]' => '',
            'Payroll[state_id]' => ''
        ],

        'update?payroll_id=&access-token=' => [
            'Payroll[salary]' => '',
            'Payroll[user_id]' => '',
            'Payroll[state_id]' => ''
        ],

        'list-view?page=&user_id=&search=&access-token=' => [],
        'get-my-payroll?page=&access-token=' => [],
        'delete-payroll?payroll_id=&access-token=' => []
    ]
];
?>