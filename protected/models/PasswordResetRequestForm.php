<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models;

use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{

    public $email;

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'email',
                'filter',
                'filter' => 'trim'
            ],
            [
                'email',
                'required'
            ],
            [
                'email',
                'email'
            ],
            [
                'email',
                'exist',
                'targetClass' => '\app\models\User',
                'filter' => [
                    'state_id' => User::STATE_ACTIVE
                ],
                'message' => 'There is no user with such email.'
            ]
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'state_id' => User::STATE_ACTIVE,
            'email' => $this->email
        ]);
        if (! $user) {
            return false;
        }
        if (! User::isPasswordResetTokenValid($user->activation_key)) {
            $user->generatePasswordResetToken();
        }
        if (! $user->save()) {
            return false;
        }
        EmailQueue::add([
            'from' => \Yii::$app->params['adminEmail'],
            'to' => $this->email,
            'subject' => \Yii::$t('app', 'Password reset for ' . \Yii::$app->name),
            'view' => 'passwordResetToken',
            'viewArgs' => [
                'user' => $user
            ]
        ]);
        return true;
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;

        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }
}
