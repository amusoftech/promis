<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_analytics".
 *
 * @property integer $id
 * @property string $name
 * @property integer $case_id
 * @property integer $department_id
 * @property integer $user_id
 * @property integer $manager_id
 * @property integer $client_id
 * @property string $date
 * @property string $billing_item
 * @property double $cost
 * @property double $value
 * @property integer $created_by_id
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property string $updated_on === Related data ===
 * @property Case $case
 * @property User $client
 * @property User $createdBy
 * @property Department $department
 * @property User $manager
 * @property User $user
 */
namespace app\models;

use Yii;
use app\models\Feed;
use app\models\Cases;
use app\models\User;
use app\models\Department;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Analytics extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->name;
    }

    public static function getCaseOptions()
    {
        return ArrayHelper::Map(Cases::find()->each(), 'id', 'title');
    }

    public static function getDepartmentOptions()
    {
        return ArrayHelper::Map(Department::findActive()->each(), 'id', 'title');
        // return ArrayHelper::Map ( Department::findActive ()->all (), 'id', 'title' );
    }

    public static function getUserOptions()
    {
        return ArrayHelper::Map(User::findActive()->where([
            'IN',
            'role_id',
            [
                User::ROLE_EMPLOYEE,
                User::ROLE_HR,
                User::ROLE_INVESTIGATOR
            ]
        ])->each(), 'id', 'full_name');
    }

    public static function getManagerOptions()
    {
        return ArrayHelper::Map(User::findActive()->where([
            'IN',
            'role_id',
            [

                User::ROLE_MANAGER
            ]
        ])->each(), 'id', 'full_name');
    }

    public static function getClientOptions()
    {
        return ArrayHelper::Map(User::findActive()->where([
            'role_id' => User::ROLE_CLIENT
        ])->each(), 'id', 'full_name');
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;
        if (User::isManager())
            return true;

        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Deleted"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "default",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_INACTIVE => "Deactivate",
            self::STATE_ACTIVE => "Activate",
            self::STATE_DELETED => "Delete"
        ];
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (empty($this->user_id)) {
                $this->user_id = self::getCurrentUser();
            }
            if (empty($this->manager_id)) {
                $this->manager_id = self::getCurrentUser();
            }
            if (empty($this->created_by_id)) {
                $this->created_by_id = self::getCurrentUser();
            }
            if (empty($this->created_on)) {
                $this->created_on = date('Y-m-d H:i:s');
            }
            if (empty($this->updated_on)) {
                $this->updated_on = date('Y-m-d H:i:s');
            }
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%analytics}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'case_id',
                    'department_id',
                    'user_id',
                    'manager_id',
                    'client_id',
                    'date',
                    'cost',
                    'value',
                    'created_by_id',
                    'created_on'
                ],
                'required'
            ],
            [
                [
                    'case_id',
                    'department_id',
                    'user_id',
                    'manager_id',
                    'client_id',
                    'created_by_id',
                    'state_id',
                    'type_id'
                ],
                'integer'
            ],
            [
                [
                    'date',
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ],
            [
                [
                    'cost',
                    'value'
                ],
                'number'
            ],
            [
                [
                    'name',
                    'billing_item'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'case_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cases::className(),
                'targetAttribute' => [
                    'case_id' => 'id'
                ]
            ],
            [
                [
                    'client_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'client_id' => 'id'
                ]
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'department_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Department::className(),
                'targetAttribute' => [
                    'department_id' => 'id'
                ]
            ],
            [
                [
                    'manager_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'manager_id' => 'id'
                ]
            ],
            [
                [
                    'user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'user_id' => 'id'
                ]
            ],
            [
                [
                    'name',
                    'billing_item'
                ],
                'trim'
            ],
            [
                [
                    'name'
                ],
                'app\components\TNameValidator'
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'case_id' => Yii::t('app', 'Case'),
            'department_id' => Yii::t('app', 'Department'),
            'user_id' => Yii::t('app', 'User'),
            'manager_id' => Yii::t('app', 'Manager'),
            'client_id' => Yii::t('app', 'Client'),
            'date' => Yii::t('app', 'Date'),
            'billing_item' => Yii::t('app', 'Billing Item'),
            'cost' => Yii::t('app', 'Cost'),
            'value' => Yii::t('app', 'Value'),
            'created_by_id' => Yii::t('app', 'Created By'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCase()
    {
        return $this->hasOne(Cases::className(), [
            'id' => 'case_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), [
            'id' => 'client_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), [
            'id' => 'department_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), [
            'id' => 'manager_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['case_id'] = [
            'case',
            'Cases',
            'id'
        ];
        $relations['client_id'] = [
            'client',
            'User',
            'id'
        ];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['department_id'] = [
            'department',
            'Department',
            'id'
        ];
        $relations['manager_id'] = [
            'manager',
            'User',
            'id'
        ];
        $relations['user_id'] = [
            'user',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['name'] = $this->name;
        $json['case_id'] = $this->case_id;
        $json['department_id'] = $this->department_id;
        $json['user_id'] = $this->user_id;
        $json['manager_id'] = $this->manager_id;
        $json['client_id'] = $this->client_id;
        $json['date'] = $this->date;
        $json['billing_item'] = $this->billing_item;
        $json['cost'] = $this->cost;
        $json['value'] = $this->value;
        $json['created_by_id'] = $this->created_by_id;
        $json['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        $json['created_on'] = $this->created_on;
        if ($with_relations) {
            // case
            $list = $this->case;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['case'] = $relationData;
            } else {
                $json['case'] = $list;
            }
            // client
            $list = $this->client;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['client'] = $relationData;
            } else {
                $json['client'] = $list;
            }
            // createdBy
            $list = $this->createdBy;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createdBy'] = $relationData;
            } else {
                $json['createdBy'] = $list;
            }
            // department
            $list = $this->department;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['department'] = $relationData;
            } else {
                $json['department'] = $list;
            }
            // manager
            $list = $this->manager;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['manager'] = $relationData;
            } else {
                $json['manager'] = $list;
            }
            // user
            $list = $this->user;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['user'] = $relationData;
            } else {
                $json['user'] = $list;
            }
        }
        return $json;
    }

    public static function addTestData($count = 1)
    {
        $faker = \Faker\Factory::create();
        $states = array_keys(self::getStateOptions());
        for ($i = 0; $i < $count; $i ++) {
            $model = new self();

            $model->name = $faker->text(10);
            $model->case_id = 1;
            $model->department_id = 1;
            $model->user_id = 1;
            $model->manager_id = 1;
            $model->client_id = 1;
            $model->date = $faker->dateTime($max = 'now', $timezone = null);
            $model->billing_item = $faker->text(10);
            $model->cost = $faker->text(10);
            $model->value = $faker->text(10);
            $model->state_id = $states[rand(0, count($states))];
            $model->type_id = 0;
            $model->save();
        }
    }

    public static function addData($data)
    {
        $faker = \Faker\Factory::create();
        if (self::find()->count() != 0)
            return;
        foreach ($data as $item) {
            $model = new self();

            $model->name = isset($item['name']) ? $item['name'] : $faker->text(10);

            $model->case_id = isset($item['case_id']) ? $item['case_id'] : 1;

            $model->department_id = isset($item['department_id']) ? $item['department_id'] : 1;

            $model->user_id = isset($item['user_id']) ? $item['user_id'] : 1;

            $model->manager_id = isset($item['manager_id']) ? $item['manager_id'] : 1;

            $model->client_id = isset($item['client_id']) ? $item['client_id'] : 1;

            $model->date = isset($item['date']) ? $item['date'] : $faker->dateTime($max = 'now', $timezone = null);

            $model->billing_item = isset($item['billing_item']) ? $item['billing_item'] : $faker->text(10);

            $model->cost = isset($item['cost']) ? $item['cost'] : $faker->text(10);

            $model->value = isset($item['value']) ? $item['value'] : $faker->text(10);
            $model->state_id = self::STATE_ACTIVE;

            $model->type_id = isset($item['type_id']) ? $item['type_id'] : 0;
            if (! $model->save()) {
                self::log($model->getErrorsString());
            }
        }
    }
}
