<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_case".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $client_id
 * @property integer $department_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $type_id
 * @property integer $state_id
 * @property string $created_on
 * @property string $updated_on
 * @property integer $assign_to_id
 * @property integer $created_by_id === Related data ===
 * @property Client $client
 * @property User $createdBy
 * @property Department $department
 * @property CasePlatform[] $casePlatforms
 * @property Reminder[] $reminders
 * @property Team[] $teams
 */
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Cases extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->title;
    }

    public static function getClientOptions()
    {
        return ArrayHelper::Map(User::findActive()->where([
            'role_id' => User::ROLE_CLIENT
        ])->each(), 'id', 'full_name');
    }

    public static function getDepartmentOptions()
    {
        return ArrayHelper::Map(Department::findActive()->each(), 'id', 'title');
    }

    public static function getAssignToOptions()
    {
        return ArrayHelper::Map(User::find()->where([
            'state_id' => User::STATE_ACTIVE,
            'role_id' => User::ROLE_MANAGER
        ])->each(), 'id', 'full_name');
    }

    public static function getTypeOptions()
    {
        return ArrayHelper::Map(CaseType::findActive()->each(), 'id', 'title');
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public static function getPlatformOptions()
    {
        return ArrayHelper::Map(Platform::findActive()->each(), 'id', 'title');
    }

    public function getPlatform()
    {
        $list = self::getPlatformOptions();
        return isset($list[$this->case_platform]) ? $list[$this->case_platform] : 'Not Defined';
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;
        if (User::isManager())
            return true;
        if (User::isEmployee())
            return true;
        if (User::isClient())
            return true;
        if (User::isQualityControl())
            return true;
        if (User::isInvestigator())
            return true;
        if (User::isDistributor())
            return true;
        if ($this->hasAttribute('assign_to_id')) {
            return ($this->assign_to_id == Yii::$app->user->id);
        }
        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        return false;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    const STATE_NEW = 3;

    const STATE_PLANNING = 4;

    const STATE_INPROGRESS = 5;

    const STATE_MAINTENANCE = 6;

    const STATE_HOLD = 7;

    const STATE_CLOSED = 8;

    const STATE_ASSIGN = 9;

    public static function getStateOptions()
    {
        return [
            self::STATE_NEW => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_INACTIVE => "In Active",
            self::STATE_DELETED => "Deleted",
            self::STATE_PLANNING => "Planning",
            self::STATE_INPROGRESS => "In-Progress",
            self::STATE_MAINTENANCE => "Maintenance",
            self::STATE_HOLD => "OnHold",
            self::STATE_CLOSED => "Completed",
            self::STATE_ASSIGN => "Assign"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_NEW => "default",
            self::STATE_ACTIVE => "primary",
            self::STATE_INACTIVE => "success",
            self::STATE_DELETED => "warning",
            self::STATE_PLANNING => "success",
            self::STATE_INPROGRESS => "primary",
            self::STATE_MAINTENANCE => "info",
            self::STATE_HOLD => "warning",
            self::STATE_CLOSED => "success",
            self::STATE_ASSIGN => "primary"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_NEW => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_INACTIVE => "In Active",
            self::STATE_DELETED => "Deleted",
            self::STATE_PLANNING => "Planning",
            self::STATE_INPROGRESS => "In-Progress",
            self::STATE_MAINTENANCE => "Maintenance",
            self::STATE_HOLD => "OnHold",
            self::STATE_CLOSED => "Completed",
            self::STATE_ASSIGN => "Assign"
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->start_date))
                $this->start_date = date('Y-m-d');

            if (! isset($this->end_date))
                $this->end_date = date('Y-m-d H:i:s');
            if (! isset($this->created_on))
                $this->created_on = date('Y-m-d H:i:s');
            if (! isset($this->updated_on))
                $this->updated_on = date('Y-m-d H:i:s');
            if (! isset($this->created_by_id))
                $this->created_by_id = self::getCurrentUser();
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%case}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'description',
                    'client_id',
                    'department_id',
                    'start_date',
                    'end_date',
                    'type_id',
                    'assign_to_id',
                    'created_by_id',
                    'state_id',
                    'budget',
                    'case_platform'
                ],
                'required'
            ],
            [
                [
                    'description'
                ],
                'string'
            ],
            [
                [
                    'budget'
                ],
                'number',
                'min' => 1
            ],
            [
                'end_date',
                'compare',
                'compareAttribute' => 'start_date',
                'operator' => '>'
            ],
            [
                [
                    'client_id',
                    'department_id',
                    'type_id',
                    'state_id',
                    'assign_to_id',
                    'created_by_id',
                    'case_platform'
                ],
                'integer'
            ],
            [
                [
                    'start_date',
                    'end_date',
                    'created_on',
                    'updated_on',
                    'budget'
                ],
                'safe'
            ],
            [
                [
                    'title'
                ],
                'string',
                'max' => 128
            ],
            [
                [
                    'client_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'client_id' => 'id'
                ]
            ],
            [
                [
                    'assign_to_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'assign_to_id' => 'id'
                ]
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],

            [
                [
                    'department_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Department::className(),
                'targetAttribute' => [
                    'department_id' => 'id'
                ]
            ],
            [
                [
                    'title'
                ],
                'trim'
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'budget' => Yii::t('app', 'Budget'),
            'description' => Yii::t('app', 'Description'),
            'client_id' => Yii::t('app', 'Client'),
            'department_id' => Yii::t('app', 'Department'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'type_id' => Yii::t('app', 'Case Type'),
            'state_id' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'assign_to_id' => Yii::t('app', 'Assign To'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), [
            'id' => 'client_id'
        ]);
    }

    public function getAssignTo()
    {
        return $this->hasOne(User::className(), [
            'id' => 'assign_to_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), [
            'id' => 'department_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCasePlatforms()
    {
        return $this->hasMany(CasePlatform::className(), [
            'case_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), [
            'case_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReminders()
    {
        return $this->hasMany(Reminder::className(), [
            'case_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), [
            'case_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        $list = [];
        $models = Files::find()->where([
            'model_id' => $this->id
        ])->andWhere([
            'model_type' => self::className()
        ]);

        foreach ($models->each() as $model) {

            $list[] = $model->asJson();
        }
        return $list;
    }

    public function getCaseTotalTime()
    {
        // $list = [];
        $sum = '';
        $model = Task::find()->where([
            'case_id' => $this->id
        ]);

        foreach ($model->each() as $value) {

            $sum = TaskEvent::find()->where([
                'task_id' => $value->id
            ])->sum('start_time + end_time');
        }

        return (Yii::$app->formatter->asTime($sum));
    }

    public function getPendingHours()
    {
        // $list = [];
        $pending_time = '';
        $task_time_sum = '';
        $total_time = '';
        $model = Task::find()->where([
            'case_id' => $this->id
        ]);

        foreach ($model->each() as $value) {

            $task_time_sum = TaskEvent::find()->where([
                'task_id' => $value->id
            ])->sum('start_time + end_time');
            if (! empty($task_time_sum)) {
                if ($task_time_sum < $value->estimated_time) {

                    $pending_time = ($value->estimated_time) - ($task_time_sum->end_time);
                    $total_time = Yii::$app->formatter->asTime($pending_time);
                    return ($total_time);
                } else {
                    return 0;
                }
            }
        }
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['CasePlatforms'] = [
            'casePlatforms',
            'CasePlatform',
            'id',
            'case_id'
        ];
        $relations['Reminders'] = [
            'reminders',
            'Reminder',
            'id',
            'case_id'
        ];
        $relations['Teams'] = [
            'teams',
            'Team',
            'id',
            'case_id'
        ];
        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['client_id'] = [
            'client',
            'Client',
            'id'
        ];
        $relations['assign_to_id'] = [
            'assignTo',
            'User',
            'id'
        ];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['department_id'] = [
            'department',
            'Department',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        CasePlatform::deleteRelatedAll([
            'case_id' => $this->id
        ]);
        CaseReport::deleteRelatedAll([
            'case_id' => $this->id
        ]);
        Reminder::deleteRelatedAll([
            'case_id' => $this->id
        ]);
        Team::deleteRelatedAll([
            'case_id' => $this->id
        ]);
        Task::deleteRelatedAll([
            'case_id' => $this->id
        ]);
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function getTotalHours()
    {
        $total_time = 0;

        $model = Task::find()->where([
            'case_id' => $this->id
        ]);

        foreach ($model->each() as $value) {

            $total_time += $value->actual_time;
        }

        return \Yii::$app->formatter->asDuration($total_time);
    }

    public function getTaskCount($state = null)
    {
        $model = Task::find()->where([
            'case_id' => $this->id
        ]);
        if (! empty($state)) {
            $model->andWhere([
                'state_id' => $state
            ]);
        }
        return $model->count();
    }

    public function getComment()
    {
        return Comment::find()->where([
            'model_id' => $this->id,
            'model_type' => $this->className()
        ])
            ->each();
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['title'] = $this->title;
        $json['description'] = strip_tags($this->description);
        $json['budget'] = $this->budget;
        $json['client_id'] = $this->client_id;
        $json['client_name'] = ! empty($this->client) ? $this->client->full_name : "";
        $json['department_type'] = ! empty($this->department) ? $this->department->title : '';
        $json['start_date'] = $this->start_date;
        $json['end_date'] = $this->end_date;
        $json['type_id'] = $this->getType();
        $json['case_platform'] = $this->getPlatform();
        $json['total_hours'] = $this->getTotalHours();
        $json['state_id'] = $this->state_id;
        $json['created_on'] = $this->created_on;
        $json['assign_to_id'] = $this->assign_to_id;
        $json['assign_to_name'] = $this->assignTo->full_name;
        $json['created_by_id'] = $this->created_by_id;
        $json['created_by_name'] = $this->createdBy->full_name;
        $json['files'] = $this->getFiles();
        $json['task_count'] = $this->getTaskCount();
        $json['pending_task'] = $this->getTaskCount(Task::STATE_PENDING);
        // $json['comment'] = $this->getComment();

        if ($with_relations) {}
        return $json;
    }
}
