<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_client_reports".
 *
 * @property integer $id
 * @property integer $case_id
 * @property string $report
 * @property integer $sent_by
 * @property string $date
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 */
namespace app\models;

use Yii;
use app\models\Feed;
use app\models\User;
use yii\helpers\ArrayHelper;
use app\modules\file\models\File;
use yii\web\UploadedFile;

class ClientReports extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->case_id;
    }

    public static function getCaseOptions()
    {
        return ArrayHelper::map(Cases::find()->each(), 'id', 'title');
    }

    public function getReportSenderOptions()
    {
        return ArrayHelper::map(Client::findActive()->each(), 'id', 'name');
    }

    public function getCase()
    {
        $list = self::getCaseOptions();
        return isset($list[$this->case_id]) ? $list[$this->case_id] : 'Not Defined';
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    const STATE_SUBMITTED = 3;

    const IS_PDF = 1;

    const IS_EMAIL = 2;

    const IS_LEDES = 3;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Deleted",
            self::STATE_SUBMITTED => "Submitted"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "default",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger",
            self::STATE_SUBMITTED => "success"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getClientReportSentByOptions()
    {
        return [
            self::IS_PDF => "pdf",
            self::IS_EMAIL => "Email",
            self::IS_LEDES => "ledes"
        ];
    }

    public function getSentbyState()
    {
        $list = self::getClientReportSentByOptions();
        return isset($list[$this->sent_by]) ? $list[$this->sent_by] : 'Not Defined';
    }

    public function getSentbyBadge()
    {
        $list = [
            self::IS_PDF => "default",
            self::IS_EMAIL => "success",
            self::IS_LEDES => "danger"
        ];
        return isset($list[$this->sent_by]) ? \yii\helpers\Html::tag('span', $this->SentbyState, [
            'class' => 'label label-' . $list[$this->sent_by]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_INACTIVE => "Deactivate",
            self::STATE_ACTIVE => "Activate",
            self::STATE_DELETED => "Delete",
            self::STATE_SUBMITTED => "Submitted"
        ];
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (empty($this->created_on)) {
                $this->created_on = date('Y-m-d H:i:s');
            }
            if (empty($this->created_by_id)) {
                $this->created_by_id = self::getCurrentUser();
            }
        }
        return parent::beforeValidate();
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;
        if (User::isManager())
            return true;

        if (User::isEmployee())
            return true;
        if (User::isClient())
            return true;
        if (User::isQualityControl())
            return true;
        if (User::isInvestigator())
            return true;
        if (User::isDistributor())
            return true;
        if ($this->hasAttribute('assign_to_id')) {
            return ($this->assign_to_id == Yii::$app->user->id);
        }
        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        return false;
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_reports}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'case_id',
                    'sent_by',
                    'state_id',
                    'type_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'report',
                    'date',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'report'
                ],
                'string'
            ],
            [
                [
                    'date',
                    'created_on'
                ],
                'safe'
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'case_id' => Yii::t('app', 'Case'),
            'report' => Yii::t('app', 'Report'),
            'sent_by' => Yii::t('app', 'Sent By'),
            'date' => Yii::t('app', 'Date'),
            'state_id' => Yii::t('app', 'Status'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public function getReportSender()
    {
        return $this->hasOne(Client::className(), [
            'id' => 'sent_by'
        ]);
    }

    public function getCases()
    {
        return $this->hasOne(Cases::className(), [
            'id' => 'case_id'
        ]);
    }

    public function getFile()
    {
        $imagefile = File::find()->where([
            'model_id' => $this->id,
            'model_type' => get_class($this)
        ])->one();
        return $imagefile;
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function getClientReports()
    {
        return $this->hasOne(File::className(), [
            'model_id' => 'id'
        ])->andOnCondition([
            'model_type' => ClientReports::className()
        ]);
    }

    public function asJson($with_relations = false)
    {
        $val = array(
            "\n",
            "\r",
            "<p>",
            "</p>"
        );
        $json = [];
        $json['id'] = $this->id;
        $json['case_id'] = $this->case_id;
        $json['case_title'] = ! empty($this->cases) ? $this->cases->title : "";
        $json['case_amount'] = ! empty($this->cases) ? $this->cases->budget : "";
        $json['client_title'] = ! empty($this->cases) ? ! empty($this->cases->client) ? $this->cases->client->full_name : "" : "";
        $json['report'] = str_replace($val, "", $this->report);
        $json['sent_by'] = $this->getSentbyState();
        $json['date'] = $this->date;
        $json['state_id'] = $this->state_id;
        if (! empty($this->clientReports)) {
            $json['client_report_file'] = \Yii::$app->urlManager->createAbsoluteUrl([
                'file/file/files/',
                'file' => $this->clientReports->filename_path
            ]);
            $json['extension'] = $this->clientReports->extension;
        } else {
            $json['client_report_file'] = '';
        }

        $json['type_id'] = $this->type_id;
        $json['created_on'] = $this->created_on;
        $json['created_by_id'] = $this->created_by_id;
        $json['submitted_by_name'] = ! empty($this->createdBy) ? $this->createdBy->full_name : "";
        if ($with_relations) {}
        return $json;
    }

    public static function addTestData($count = 1)
    {
        $faker = \Faker\Factory::create();
        $states = array_keys(self::getStateOptions());
        for ($i = 0; $i < $count; $i ++) {
            $model = new self();

            $model->case_id = 1;
            $model->report = $faker->text;
            $model->sent_by = $faker->text(10);
            $model->date = $faker->date($format = 'Y-m-d', $max = 'now');
            $model->state_id = $states[rand(0, count($states))];
            $model->type_id = 0;
            $model->save();
        }
    }

    public static function addData($data)
    {
        $faker = \Faker\Factory::create();
        if (self::find()->count() != 0)
            return;
        foreach ($data as $item) {
            $model = new self();

            $model->case_id = isset($item['case_id']) ? $item['case_id'] : 1;

            $model->report = isset($item['report']) ? $item['report'] : $faker->text;

            $model->sent_by = isset($item['sent_by']) ? $item['sent_by'] : $faker->text(10);

            $model->date = isset($item['date']) ? $item['date'] : $faker->date($format = 'Y-m-d', $max = 'now');
            $model->state_id = self::STATE_ACTIVE;

            $model->type_id = isset($item['type_id']) ? $item['type_id'] : 0;
            if (! $model->save()) {
                self::log($model->getErrorsString());
            }
        }
    }
}
