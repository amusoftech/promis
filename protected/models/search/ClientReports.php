<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientReports as ClientReportsModel;

/**
 * ClientReports represents the model behind the search form about `app\models\ClientReports`.
 */
class ClientReports extends ClientReportsModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',

                    'state_id',
                    'type_id'
                ],
                'integer'
            ],
            [
                [
                    'report',
                    'date',
                    'created_on',
                    'case_id',
                    'sent_by',
                    'created_by_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientReportsModel::find()->alias('client_report_model')->joinWith([
            'createdBy as report_creater',

            'cases as case_info'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'client_report_model.state_id' => $this->state_id,
            'client_report_model.sent_by' => $this->sent_by,
            'client_report_model.type_id' => $this->type_id
        ]);

        $query->andFilterWhere([
            'like',
            'client_report_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'client_report_model.report',
            $this->report
        ])
            ->andFilterWhere([
            'like',
            'report_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'case_info.title',
            $this->case_id
        ])
            ->andFilterWhere([
            'like',
            'client_report_model.date',
            $this->date
        ])
            ->andFilterWhere([
            'like',
            'client_report_model.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
