<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Designation as DesignationModel;

/**
 * Designation represents the model behind the search form about `app\models\Designation`.
 */
class Designation extends DesignationModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'created_by_id',
                    'created_on'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DesignationModel::find()->alias('designation')->joinWith([
            'createdBy as designationcreated'
        ]);
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'designation.type_id' => $this->type_id,
            'designation.state_id' => $this->state_id
        ]);

        $query->andFilterWhere([
            'like',
            'designation.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'designationcreated.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'designation.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'designation.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
