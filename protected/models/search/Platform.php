<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Platform as PlatformModel;

/**
 * Platform represents the model behind the search form about `app\models\Platform`.
 */
class Platform extends PlatformModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'state_id',
                    'type_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'description',
                    'created_on',
                    'updated_on',
                    'created_by_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlatformModel::find()->alias('platform')->joinWith([
            'createdBy as platformcreated'
        ]);
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'platform.state_id' => $this->state_id,
            'platform.type_id' => $this->type_id
        ]);

        $query->andFilterWhere([
            'like',
            'platform.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'platform.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'platformcreated.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'platform.description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'platform.created_on',
            $this->created_on
        ])
            ->andFilterWhere([
            'like',
            'platform.updated_on',
            $this->updated_on
        ]);

        return $dataProvider;
    }
}
