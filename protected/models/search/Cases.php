<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cases as CasesModel;
use app\models\User;
use app\models\Task;

/**
 * Cases represents the model behind the search form about `app\models\Cases`.
 */
class Cases extends CasesModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id'
                ],
                'integer'
            ],
            [
                [
                    'id',
                    'client_id',
                    'department_id',
                    'type_id',
                    'state_id',
                    'assign_to_id',
                    'created_by_id',
                    'title',
                    'description',
                    'start_date',
                    'end_date',
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CasesModel::find()->alias('case_model')->joinWith([
            'createdBy as case_creater',
            'assignTo as case_assign_to',
            'client as case_client',
            'department as case_assign_to_department'
        ]);

        if (User::isEmployee()) {

            $ids = Task::find()->select('case_id')
                ->where([
                'assign_to_id' => \Yii::$app->user->id
            ])
                ->distinct()
                ->column();

            $query->andWhere([
                'in',
                'case_model.id',
                $ids
            ]);
        }
        if (User::isClient()) {
            $query->andWhere([
                'case_model.client_id' => \yii::$app->user->id
            ]);
        } /*
           *
           * if (User::isManager()) {
           * $query->andWhere([
           * 'case_model.assign_to_id' => \yii::$app->user->id
           * ]);
           * }
           */
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([

            'case_model.type_id' => $this->type_id,
            'case_model.state_id' => $this->state_id
        ]);

        $query->andFilterWhere([
            'like',
            'case_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'case_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'case_assign_to.full_name',
            $this->assign_to_id
        ])
            ->andFilterWhere([
            'like',
            'case_client.name',
            $this->client_id
        ])
            ->andFilterWhere([
            'like',
            'case_assign_to_department.title',
            $this->department_id
        ])
            ->andFilterWhere([
            'like',
            'case_model.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'case_model.description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'case_model.start_date',
            $this->start_date
        ])
            ->andFilterWhere([
            'like',
            'case_model.end_date',
            $this->end_date
        ])
            ->andFilterWhere([
            'like',
            'case_model.created_on',
            $this->created_on
        ])
            ->andFilterWhere([
            'like',
            'case_model.updated_on',
            $this->updated_on
        ]);

        return $dataProvider;
    }
}
