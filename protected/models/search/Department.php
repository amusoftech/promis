<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Department as DepartmentModel;

/**
 * Department represents the model behind the search form about `app\models\Department`.
 */
class Department extends DepartmentModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'created_on',
                    'created_by_id',
                    'id',
                    'type_id',
                    'state_id',
                    'manager_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DepartmentModel::find()->alias('dept')->joinWith([
            'createdBy as dept_creater',
            'manager as dept_manager'
        ]);

        $dataProvider = new ActiveDataProvider([

            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {

            return $dataProvider;
        }

        $query->andFilterWhere([
            'dept.id' => $this->id,
            // 'dept.type_id' => $this->type_id,
            'dept.state_id' => $this->state_id
        ]);

        $query->andFilterWhere([
            'like',
            'dept.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'dept_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'dept_manager.full_name',
            $this->manager_id
        ])
            ->andFilterWhere([
            'like',
            'dept.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
