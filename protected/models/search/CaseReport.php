<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CaseReport as CaseReportModel;

/**
 * CaseReport represents the model behind the search form about `app\models\CaseReport`.
 */
class CaseReport extends CaseReportModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'state_id',
                    'type_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'description',
                    'created_on',
                    'updated_on',
                    'submitted_to',
                    'created_by_id',
                    'case_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CaseReportModel::find()->alias('case_report_model')->joinWith([
            'createdBy as case_creater',
            'case as case_detail',
            'submittedTo as case_submitted_to'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            // 'case_id' => $this->case_id,
            // 'submitted_to' => $this->submitted_to,
            // 'created_by_id' => $this->created_by_id,
            'case_report_model.state_id' => $this->state_id,
            'case_report_model.type_id' => $this->type_id
        ]);

        $query->andFilterWhere([
            'like',
            'case_report_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'case_report_model.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'case_detail.title',
            $this->case_id
        ])
            ->andFilterWhere([
            'like',
            'case_submitted_to.full_name',
            $this->submitted_to
        ])
            ->andFilterWhere([
            'like',
            'case_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'case_report_model.description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'case_report_model.created_on',
            $this->created_on
        ])
            ->andFilterWhere([
            'like',
            'case_report_model.updated_on',
            $this->updated_on
        ]);

        return $dataProvider;
    }
}
