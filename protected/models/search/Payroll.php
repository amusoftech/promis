<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payroll as PayrollModel;

/**
 * Payroll represents the model behind the search form about `app\models\Payroll`.
 */
class Payroll extends PayrollModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'day',
                    'salary',
                    'state_id'
                ],
                'integer'
            ],
            [
                [
                    'created_on',
                    'department_id',
                    'type_id',
                    'created_by_id',
                    'user_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PayrollModel::find()->alias('payroll')->joinWith([
            'department as department',
            'user as user',
            'createdBy as createdBy'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'payroll.day' => $this->day,
            'payroll.salary' => $this->salary,
            'payroll.state_id' => $this->state_id
        ]);

        $query->andFilterWhere([
            'like',
            'payroll.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'payroll.created_on',
            $this->created_on
        ])
            ->andFilterWhere([
            'like',
            'department.title',
            $this->department_id
        ])
            ->andFilterWhere([
            'like',
            'user.full_name',
            $this->user_id
        ])
            ->andFilterWhere([
            'like',
            'createdBy.full_name',
            $this->created_by_id
        ]);

        return $dataProvider;
    }
}
