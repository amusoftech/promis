<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\User as UserModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * User represents the model behind the search form about `app\models\User`.
 */
class User extends UserModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'gender',
                    'tos',
                    'role_id',
                    'state_id',
                    'type_id',
                    'login_error_count',
                    'created_by_id',
                    'employee_id'
                ],
                'integer'
            ],
            [
                [
                    'full_name',
                    'email',
                    'password',
                    'date_of_birth',
                    'about_me',
                    'contact_no',
                    'address',
                    'latitude',
                    'longitude',
                    'city',
                    'country',
                    'zipcode',
                    'language',
                    'profile_file',
                    'last_visit_time',
                    'last_action_time',
                    'last_password_change',
                    'activation_key',
                    'timezone',
                    'created_on',
                    'updated_on',
                    'id',
                    'gender',
                    'tos',
                    'role_id',
                    'state_id',
                    'type_id',
                    'login_error_count',
                    'created_by_id',
                    'department_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $role = null, $post = null)
    {
        $query = UserModel::find()->alias('user')->
        // ->joinWith([
        // 'department as department_detail'
        // ])
        where([
            '!=',
            'role_id',
            User::ROLE_ADMIN
        ]);

        if (! empty($role)) {
            $query = $query->andWhere([
                'role_id' => $role
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if (! empty($post)) {
            $dataProvider->pagination->params = $post;
        }

        if (! empty($post['User']['full_name'])) {
            $query->andFilterWhere([
                'like',
                'user.full_name',
                $post['User']['full_name']
            ]);
        }
        if (! empty($post['User']['designation'])) {

            $query->andFilterWhere([
                'like',
                'user.designation',
                $post['User']['designation']
            ]);
        }

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.employee_id' => $this->employee_id,
            'user.date_of_birth' => $this->date_of_birth,
            'user.gender' => $this->gender,
            'user.tos' => $this->tos,
            'user.role_id' => $this->role_id,
            'user.state_id' => $this->state_id,
            'user.type_id' => $this->type_id,
            'user.last_visit_time' => $this->last_visit_time,
            'user.last_action_time' => $this->last_action_time,
            'user.last_password_change' => $this->last_password_change,
            'user.login_error_count' => $this->login_error_count,
            'user.updated_on' => $this->updated_on
        ]);

        $query->andFilterWhere([
            'like',
            'user.full_name',
            $this->full_name
        ])
            ->andFilterWhere([
            'like',
            'user.email',
            $this->email
        ])
            ->andFilterWhere([
            'like',
            'user.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'user.password',
            $this->password
        ])
            ->andFilterWhere([
            'like',
            'user.about_me',
            $this->about_me
        ])
            ->andFilterWhere([
            'like',
            'user.contact_no',
            $this->contact_no
        ])
            ->andFilterWhere([
            'like',
            'user.address',
            $this->address
        ])
            ->andFilterWhere([
            'like',
            'user.latitude',
            $this->latitude
        ])
            ->andFilterWhere([
            'like',
            'user.longitude',
            $this->longitude
        ])
            ->andFilterWhere([
            'like',
            'user.city',
            $this->city
        ])
            ->andFilterWhere([
            'like',
            'user.country',
            $this->country
        ])
            ->andFilterWhere([
            'like',
            'user.zipcode',
            $this->zipcode
        ])
            ->andFilterWhere([
            'like',
            'user.language',
            $this->language
        ])
            ->andFilterWhere([
            'like',
            'user.profile_file',
            $this->profile_file
        ])
            ->andFilterWhere([
            'like',
            'user.activation_key',
            $this->activation_key
        ])
            ->andFilterWhere([
            'like',
            'user.timezone',
            $this->timezone
        ])
            ->
        // ->andFilterWhere([
        // 'like',
        // 'department_detail.title',
        // $this->department_id
        // ])
        andFilterWhere([
            'like',
            'user.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
