<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reminder as ReminderModel;

/**
 * Reminder represents the model behind the search form about `app\models\Reminder`.
 */
class Reminder extends ReminderModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'state_id',
                    'type_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'description',
                    'time',
                    'created_on',
                    'department_id',
                    'user_id',
                    'created_by_id',
                    'case_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReminderModel::find()->alias("reminder_model")->joinWith([
            'createdBy as reminder_creater',
            'user  as reminder_assign_to_user',
            'case as reminder_for_case',
            'department as reminder_for_department'
        ]);
        
        if (!(User::isAdmin() ||  User::isHR())) {

            $query->andWhere([
                'reminder_model.user_id' => \yii::$app->user->id
            ])->orWhere([
                'reminder_model.created_by_id' => \yii::$app->user->id]);
        }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'reminder_model.time' => $this->time,
            'reminder_model.state_id' => $this->state_id,
            'reminder_model.type_id' => $this->type_id
        ]);

        $query->andFilterWhere([
            'like',
            'reminder_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'reminder_model.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'reminder_for_department.title',
            $this->department_id
        ])
            ->andFilterWhere([
            'like',
            'reminder_for_case.title',
            $this->case_id
        ])
            ->andFilterWhere([
            'like',
            'reminder_assign_to_user.full_name',
            $this->user_id
        ])
            ->andFilterWhere([
            'like',
            'reminder_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'reminder_model.description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'reminder_model.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
