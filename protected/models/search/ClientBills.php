<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientBills as ClientBillsModel;

/**
 * ClientBills represents the model behind the search form about `app\models\ClientBills`.
 */
class ClientBills extends ClientBillsModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'amount',
                    'state_id',
                    'type_id'
                ],
                'integer'
            ],
            [
                [
                    'full_name',
                    'date',
                    'created_on',
                    'case_id',
                    'sent_by',
                    'created_by_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientBillsModel::find()->alias('client_bill_model')->joinWith([
            'createdBy as client_bill_creater',
            'cases as client_case'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([

            'client_bill_model.amount' => $this->amount,
            'client_bill_model.sent_by' => $this->sent_by,
            'client_bill_model.state_id' => $this->state_id,
            'client_bill_model.type_id' => $this->type_id
        ]);

        $query->andFilterWhere([
            'like',
            'client_bill_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'client_bill_model.full_name',
            $this->full_name
        ])
            ->andFilterWhere([
            'like',
            'client_bill_model.date',
            $this->date
        ])
            ->andFilterWhere([
            'like',
            'client_bill_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'client_case.title',
            $this->case_id
        ])
            ->andFilterWhere([
            'like',
            'client_bill_model.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
