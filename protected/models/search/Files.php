<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Files as FilesModel;

/**
 * Files represents the model behind the search form about `app\models\Files`.
 */
class Files extends FilesModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'public',
                    'size',
                    'download_count',
                    'file_type',
                    'state_id',
                    'type_id',
                    'updated_by_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'model_id',
                    'model_type',
                    'target_url',
                    'description',
                    'filename_user',
                    'filename_path',
                    'extension',
                    'mimetype',
                    'seo_title',
                    'seo_alt',
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FilesModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'public' => $this->public,
            'size' => $this->size,
            'download_count' => $this->download_count,
            'file_type' => $this->file_type,
            'state_id' => $this->state_id,
            'type_id' => $this->type_id,
            'updated_by_id' => $this->updated_by_id,
            'created_by_id' => $this->created_by_id
        ]);

        $query->andFilterWhere([
            'like',
            'id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'model_id',
            $this->model_id
        ])
            ->andFilterWhere([
            'like',
            'model_type',
            $this->model_type
        ])
            ->andFilterWhere([
            'like',
            'target_url',
            $this->target_url
        ])
            ->andFilterWhere([
            'like',
            'description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'filename_user',
            $this->filename_user
        ])
            ->andFilterWhere([
            'like',
            'filename_path',
            $this->filename_path
        ])
            ->andFilterWhere([
            'like',
            'extension',
            $this->extension
        ])
            ->andFilterWhere([
            'like',
            'mimetype',
            $this->mimetype
        ])
            ->andFilterWhere([
            'like',
            'seo_title',
            $this->seo_title
        ])
            ->andFilterWhere([
            'like',
            'seo_alt',
            $this->seo_alt
        ])
            ->andFilterWhere([
            'like',
            'created_on',
            $this->created_on
        ])
            ->andFilterWhere([
            'like',
            'updated_on',
            $this->updated_on
        ]);

        return $dataProvider;
    }
}
