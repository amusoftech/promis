<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client as ClientModel;

/**
 * Client represents the model behind the search form about `app\models\Client`.
 */
class Client extends ClientModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id',
                    'phone'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'email',
                    'phone',
                    'alternate_email',
                    'skype_id',
                    'address',
                    'country',
                    'created_on',
                    'updated_on',
                    'id',
                    'type_id',
                    'state_id',
                    'created_by_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientModel::find()->alias('client_model')->joinWith([
            'createdBy as client_creater'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'client_model.type_id' => $this->type_id,
            'client_model.state_id' => $this->state_id
        ]);

        $query->andFilterWhere([
            'like',
            'client_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'client_model.name',
            $this->name
        ])
            ->andFilterWhere([
            'like',
            'client_model.email',
            $this->email
        ])
            ->andFilterWhere([
            'like',
            'client_model.phone',
            $this->phone
        ])
            ->andFilterWhere([
            'like',
            'client_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'client_model.alternate_email',
            $this->alternate_email
        ])
            ->andFilterWhere([
            'like',
            'client_model.skype_id',
            $this->skype_id
        ])
            ->andFilterWhere([
            'like',
            'client_model.address',
            $this->address
        ])
            ->andFilterWhere([
            'like',
            'client_model.country',
            $this->country
        ])
            ->andFilterWhere([
            'like',
            'client_model.created_on',
            $this->created_on
        ])
            ->andFilterWhere([
            'like',
            'client_model.updated_on',
            $this->updated_on
        ]);

        return $dataProvider;
    }
}
