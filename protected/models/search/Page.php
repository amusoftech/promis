<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Page as PageModel;

/**
 * Page represents the model behind the search form about `app\models\Page`.
 */
class Page extends PageModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'state_id',
                    'type_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'description',
                    'created_on',
                    'updated_on',
                    'created_by_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageModel::find()->alias("page")->joinWith([
            'createdBy as page_creater'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'page.state_id' => $this->state_id,
            'page.type_id' => $this->type_id
        ]);

        $query->andFilterWhere([
            'like',
            'page.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'page.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'page.description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'page.created_on',
            $this->created_on
        ])
            ->andFilterWhere([
            'like',
            'page_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'page.updated_on',
            $this->updated_on
        ]);

        return $dataProvider;
    }
}
