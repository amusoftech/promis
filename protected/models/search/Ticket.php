<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ticket as TicketModel;
use yii\rbac\Role;

/**
 * Ticket represents the model behind the search form about `app\models\Ticket`.
 */
class Ticket extends TicketModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'description',
                    'created_on',
                    'created_by_id',
                    'assign_to_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TicketModel::find()->alias('ticket_model')->joinWith([
            'createdBy as ticket_creater',
            'assignTo as ticket_assign'
        ]);
        if (! (User::isAdmin() && User::isManager())) {
            if (empty($params)) {
                $query->andWhere([
                    'ticket_model.created_by_id' => \yii::$app->user->id
                ]);
            }
        }
        if (isset($params) && ! empty($params['type'])) {
            if ($params['type'] == User::ROLE_EMPLOYEE) {
                $userIds = User::find()->select('id')
                    ->where([
                    'role_id' => User::ROLE_EMPLOYEE
                ])
                    ->column();
                $query->andWhere([
                    'in',
                    'ticket_model.created_by_id',
                    $userIds
                ]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ticket_model.type_id' => $this->type_id,
            'ticket_model.state_id' => $this->state_id
        ]);

        $query->andFilterWhere([
            'like',
            'ticket_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'ticket_model.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'ticket_model.description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'ticket_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'ticket_assign.full_name',
            $this->assign_to_id
        ])
            ->andFilterWhere([
            'like',
            'ticket_model.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
