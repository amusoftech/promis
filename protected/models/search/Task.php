<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task as TaskModel;
use app\models\User;

/**
 * Task represents the model behind the search form about `app\models\Task`.
 */
class Task extends TaskModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id'
                ],
                'integer'
            ],
            [
                [
                    'title',
                    'description',
                    'start_date',
                    'end_date',
                    'estimated_time',
                    'actual_time',
                    'created_on',
                    'updated_on',
                    'case_id',
                    'platform_id',
                    'department_id',
                    'assign_to_id',
                    'created_by_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskModel::find()->alias('task_model')->joinWith([
            'case as case_detail',
            'createdBy as task_creater_info',
            'assignTo as task_assignee_info',
            'platform as case_platform_info',
            'department as department_info'
        ]);

        if (User::isEmployee() || User::isQualityControl()) {

            $query->andWhere([
                'task_model.assign_to_id' => \Yii::$app->user->id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'task_model.type_id' => $this->type_id,
            'task_model.state_id' => $this->state_id,
            'task_model.estimated_time' => $this->estimated_time,
            'task_model.actual_time' => $this->actual_time
        ]);

        $query->andFilterWhere([
            'like',
            'task_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'task_model.title',
            $this->title
        ])
            ->andFilterWhere([
            'like',
            'case_detail.title',
            $this->case_id
        ])
            ->andFilterWhere([
            'like',
            'task_model.description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'case_platform_info.title',
            $this->platform_id
        ])
            ->andFilterWhere([
            'like',
            'department_info.title',
            $this->department_id
        ])
            ->andFilterWhere([
            'like',
            'task_model.start_date',
            $this->start_date
        ])
            ->andFilterWhere([
            'like',
            'task_creater_info.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'task_assignee_info.full_name',
            $this->assign_to_id
        ])
            ->andFilterWhere([
            'like',
            'task_model.end_date',
            $this->end_date
        ])
            ->andFilterWhere([
            'like',
            'task_model.created_on',
            $this->created_on
        ])
            ->andFilterWhere([
            'like',
            'task_model.updated_on',
            $this->updated_on
        ]);

        return $dataProvider;
    }
}
