<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AttendanceEvent as AttendanceEventModel;

/**
 * AttendanceEvent represents the model behind the search form about `app\models\AttendanceEvent`.
 */
class AttendanceEvent extends AttendanceEventModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id',
                    'user_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'date',
                    'time',
                    'created_on'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AttendanceEventModel::find()->alias('attendance_event_model')->joinWith([
            'attendance as  attendance_detail',
            'user as attendiee_detail',
            'createdBy as attendance_creater_detail'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        
        if (! (User::isAdmin() ||  User::isHR()) ) {
            $query->andWhere([
                'attendance_event_model.user_id' => \yii::$app->user->id
            ]);
        }
        

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'attendance_event_model.type_id' => $this->type_id,
            'attendance_event_model.time' => $this->time,
            'attendance_event_model.state_id' => $this->state_id,
            //'user_id' => $this->user_id,
            //'created_by_id' => $this->created_by_id
        ]);

        $query->andFilterWhere([
            'like',
            'attendance_event_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'attendance_event_model.date',
            $this->date
        ])
            ->andFilterWhere([
            'like',
            'attendance_event_model.created_on',
            $this->created_on
        ])
        ->andFilterWhere([
            'like',
            'attendance_creater_detail.full_name',
            $this->created_by_id
        ])
        ->andFilterWhere([
            'like',
            'attendiee_detail.full_name',
            $this->user_id
        ]);

        return $dataProvider;
    }
}
