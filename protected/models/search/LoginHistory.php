<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\models\search;

use app\models\LoginHistory as LoginHistoryModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LoginHistory represents the model behind the search form about `app\models\LoginHistory`.
 */
class LoginHistory extends LoginHistoryModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'state_id',
                    'type_id'
                ],
                'integer'
            ],
            [
                [
                    'user_ip',
                    'user_agent',
                    'failer_reason',
                    'code',
                    'created_on',
                    'user_id'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoginHistoryModel::find()->alias('login_history')->joinWith([
            'user as login_history_creater'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'login_history.id' => $this->id,
            // 'user_id' => $this->user_id,
            'login_history.state_id' => $this->state_id,
            'login_history.type_id' => $this->type_id,
            'login_history.created_on' => $this->created_on
        ]);

        $query->andFilterWhere([
            'like',
            'login_history.user_ip',
            $this->user_ip
        ])
            ->andFilterWhere([
            'like',
            'login_history.user_agent',
            $this->user_agent
        ])
            ->andFilterWhere([
            'like',
            'login_history.failer_reason',
            $this->failer_reason
        ])
            ->andFilterWhere([
            'like',
            'login_history_creater.full_name',
            $this->user_id
        ])
            ->andFilterWhere([
            'like',
            'login_history.code',
            $this->code
        ]);

        return $dataProvider;
    }
}
