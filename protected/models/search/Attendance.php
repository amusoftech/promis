<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Attendance as AttendanceModel;
use app\models\User;

/**
 * Attendance represents the model behind the search form about `app\models\Attendance`.
 */
class Attendance extends AttendanceModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id',
                    'total_time'
                ],
                'integer'
            ],
            [
                [
                    'date',
                    'created_on',
                    'created_by_id',
                    'user_id',
                    'id',
                    'type_id',
                    'state_id',
                    'total_time',
                    'in_time',
                    'out_time',
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AttendanceModel::find()->alias('attendence_model')->joinWith([
            'createdBy as attendence_creater',
            'user as attendies'
        ]);

        
        if (! (User::isAdmin() ||  User::isHR()) ) {
            $query->andWhere([
                'attendence_model.user_id' => \yii::$app->user->id
            ]);
        }
        
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
    


        $query->andFilterWhere([
            // 'user_id' => $this->user_id,
            'attendence_model.type_id' => $this->type_id,
            'attendence_model.state_id' => $this->state_id,
            'attendence_model.total_time' => $this->total_time
            // 'created_by_id' => $this->created_by_id
        ]);

        $query->andFilterWhere([
            'like',
            'attendence_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'attendence_model.date',
            $this->date
        ])
            ->andFilterWhere([
            'like',
            'attendence_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'attendies.full_name',
            $this->user_id
        ])
            ->andFilterWhere([
            'like',
            'attendence_model.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
