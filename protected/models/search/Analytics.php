<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Analytics as AnalyticsModel;

/**
 * Analytics represents the model behind the search form about `app\models\Analytics`.
 */
class Analytics extends AnalyticsModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'case_id', 'department_id', 'user_id', 'manager_id', 'client_id', 'created_by_id', 'state_id', 'type_id'], 'integer'],
            [['name', 'date', 'billing_item', 'cost', 'value', 'created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    public function beforeValidate(){
            return true;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AnalyticsModel::find();

		        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
						'defaultOrder' => [
								'id' => SORT_DESC
						]
				]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'case_id' => $this->case_id,
            'department_id' => $this->department_id,
            'user_id' => $this->user_id,
            'manager_id' => $this->manager_id,
            'client_id' => $this->client_id,
            'created_by_id' => $this->created_by_id,
            'state_id' => $this->state_id,
            'type_id' => $this->type_id,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'billing_item', $this->billing_item])
            ->andFilterWhere(['like', 'cost', $this->cost])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'created_on', $this->created_on])
            ->andFilterWhere(['like', 'updated_on', $this->updated_on]);

        return $dataProvider;
    }
}
