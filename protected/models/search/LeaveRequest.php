<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LeaveRequest as LeaveRequestModel;
use app\models\User;

/**
 * LeaveRequest represents the model behind the search form about `app\models\LeaveRequest`.
 */
class LeaveRequest extends LeaveRequestModel
{

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type_id',
                    'state_id'
                ],
                'integer'
            ],
            [
                [
                    'id',
                    'type_id',
                    'state_id',
                    'created_by_id',
                    'description',
                    'start_date',
                    'end_date',
                    'is_halfday',
                    'created_on'
                ],
                'safe'
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LeaveRequestModel::find()->alias('leave_request_model')->joinWith([
            'createdBy as leave_request_creater'
        ]);

        if (! (User::isAdmin() || User::isHR())) {
            if (empty($params)) {
                $query->andWhere([
                    'leave_request_model.created_by_id' => \yii::$app->user->id
                ]);
            }
        }

        if (isset($params) && ! empty($params['type'])) {
            if ($params['type'] == User::ROLE_EMPLOYEE) {
                $userIds = User::find()->select('id')
                    ->where([
                    'role_id' => User::ROLE_EMPLOYEE
                ])
                    ->column();

                $query->andWhere([
                    'in',
                    'leave_request_model.created_by_id',
                    $userIds
                ]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (! ($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'leave_request_model.type_id' => $this->type_id,
            'leave_request_model.state_id' => $this->state_id
        ]);

        $query->andFilterWhere([
            'like',
            'leave_request_model.id',
            $this->id
        ])
            ->andFilterWhere([
            'like',
            'leave_request_model.description',
            $this->description
        ])
            ->andFilterWhere([
            'like',
            'leave_request_model.start_date',
            $this->start_date
        ])
            ->andFilterWhere([
            'like',
            'leave_request_model.end_date',
            $this->end_date
        ])
            ->andFilterWhere([
            'like',
            'leave_request_model.is_halfday',
            $this->is_halfday
        ])
            ->andFilterWhere([
            'like',
            'leave_request_creater.full_name',
            $this->created_by_id
        ])
            ->andFilterWhere([
            'like',
            'leave_request_model.created_on',
            $this->created_on
        ]);

        return $dataProvider;
    }
}
