<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_leave_request".
 *
 * @property integer $id
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property integer $is_halfday
 * @property integer $type_id
 * @property integer $state_id
 * @property string $created_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 */
namespace app\models;

use Yii;

class LeaveRequest extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) ! empty($this->createdBy) ? $this->createdBy->full_name : '';
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;
        if (User::isHR())
            return true;
        if (User::isManager())
            return true;
        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    const STATE_PENDING = 0;

    const STATE_APPROVED = 1;

    const STATE_CANCELLED = 2;

    const STATE_REJECTED = 3;

    const IS_HALFDAY_YES = 1;

    const IS_HALFDAY_NO = 0;

    const IS_HALFDAY_time = '0.5';

    const FLAG_TRUE = 1;

    public static function getStateOptions()
    {
        return [

            self::STATE_APPROVED => "Approved",
            self::STATE_REJECTED => "Rejected",
            self::STATE_PENDING => "Pending",
            self::STATE_CANCELLED => "Cancelled"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_PENDING => "warning",
            self::STATE_APPROVED => "success",
            self::STATE_CANCELLED => "danger",
            self::STATE_REJECTED => "info"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getUserStateOptions()
    {
        return [

            self::STATE_PENDING => "Pending",
            self::STATE_CANCELLED => "Cancelled"
        ];
    }

    public function getUserState()
    {
        $list = self::getUserStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getUserStateBadge()
    {
        $list = [
            self::STATE_PENDING => "warning",
            self::STATE_CANCELLED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getUserState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_PENDING => "Pending",
            self::STATE_APPROVED => "Approved",
            self::STATE_CANCELLED => "Cancelled",
            self::STATE_REJECTED => "Rejected"
        ];
    }

    public static function getHalfdayOptions()
    {
        return [

            self::IS_HALFDAY_YES => "Yes",
            self::IS_HALFDAY_NO => "No"
        ];
    }

    public function getHalfday()
    {
        $list = self::getHalfdayOptions();
        return isset($list[$this->is_halfday]) ? $list[$this->is_halfday] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->start_date))
                $this->start_date = date('Y-m-d');
            if (! isset($this->created_on))
                $this->created_on = date('Y-m-d');

            if (! isset($this->created_by_id))
                $this->created_by_id = self::getCurrentUser();
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%leave_request}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'description',
                    'start_date',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'description'
                ],
                'string'
            ],
            [
                [
                    'start_date',
                    'end_date',
                    'created_on',
                    'end_date'
                ],
                'safe'
            ],
            [
                [
                    'is_halfday',
                    'type_id',
                    'state_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'is_halfday'
                ],
                'trim'
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'start_date' => Yii::t('app', 'Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'is_halfday' => Yii::t('app', 'Is Halfday'),
            'type_id' => Yii::t('app', 'Type'),
            'state_id' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    public function getDays()
    {
        $dates = explode(',', $this->start_date);
        $count = count($dates);
        if ($this->is_halfday == self::IS_HALFDAY_YES) {
            return self::IS_HALFDAY_time;
        } else {
            return $count;
        }
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['description'] = strip_tags($this->description);
        $json['start_date'] = $this->start_date;
        $json['is_halfday'] = $this->is_halfday;
        $json['halfday'] = $this->getHalfday();
        $json['days'] = $this->getDays();
        $json['type_id'] = $this->type_id;
        $json['state_id'] = $this->state_id;
        $json['state'] = $this->getStateBadge();
        $json['created_on'] = $this->created_on;
        $json['created_by_id'] = $this->created_by_id;
        $json['user_name'] = $this->createdBy->full_name;
        $json['user_role'] = $this->createdBy->role_id;
        if ($with_relations) {}
        return $json;
    }
}
