<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_payroll".
 *
 * @property integer $id
 * @property integer $department_id
 * @property integer $day
 * @property integer $salary
 * @property integer $user_id
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 * @property Department $department
 * @property User $user
 */
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Payroll extends \app\components\TActiveRecord
{

    const SCENARIO_PAYROLL_ADD = 'add_payroll';

    public function __toString()
    {
        return (string) ! empty($this->user) ? $this->user->full_name : '';
    }

    public static function getDepartmentOptions()
    {
        return ArrayHelper::Map(Department::findActive()->all(), 'id', 'title');
    }

    public static function getUserOptions()
    {
        return ArrayHelper::Map(User::findActive()->all(), 'id', 'full_name');
    }

    public function isAllowed()
    {
        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if (User::isAdmin())
            return true;

        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    const STATE_PAID = 1;

    const STATE_UNPAID = 0;

    const JANUARY = 1;

    const FEBRUARY = 2;

    const MARCH = 3;

    const APRIL = 4;

    const MAY = 5;

    const JUNE = 6;

    const JULY = 7;

    const AUGUST = 8;

    const SEPTEMBER = 9;

    const OCTOBER = 10;

    const NOVEMBER = 11;

    const DECEMBER = 12;

    const FLAG_TRUE = 1;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Delivered",
            self::STATE_DELETED => "On Hold"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public static function getMonthOptions()
    {
        return [
            self::JANUARY => "January",
            self::FEBRUARY => "February",
            self::MARCH => "March",
            self::APRIL => "April",
            self::MAY => "May",
            self::JUNE => "June",
            self::JULY => "July",
            self::AUGUST => "August",
            self::SEPTEMBER => "September",
            self::OCTOBER => "October",
            self::NOVEMBER => "November",
            self::DECEMBER => "December"
        ];
    }

    public function getMonth()
    {
        $list = self::getMonthOptions();
        return isset($list[$this->month]) ? $list[$this->month] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "primary",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Delivered",
            self::STATE_DELETED => "On Hold"
        ];
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (empty($this->user_id)) {
                $this->user_id = self::getCurrentUser();
            }
            if (empty($this->created_on)) {
                $this->created_on = date('Y-m-d H:i:s');
            }
            if (empty($this->created_by_id)) {
                $this->created_by_id = self::getCurrentUser();
            }
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payroll}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    // 'department_id',
                    // 'day',
                    'salary',
                    'user_id',
                    'created_by_id',
                    'month'
                ],
                'required'
            ],
            [
                'salary',
                'integer',
                'max' => 100000
            ],
            [
                'day',
                'integer',
                'max' => 31
            ],
            [
                [
                    'department_id',
                    'day',
                    'salary',
                    'user_id',
                    'state_id',
                    'type_id',
                    'created_by_id',
                    'month'
                ],
                'integer'
            ],
            [
                'month',
                'unique',
                'targetAttribute' => [
                    'month',
                    'user_id'
                ],
                'message' => 'Selected month payroll has already been taken by user.'
            ],

            [
                [
                    'salary',
                    'user_id',
                    'state_id'
                ],
                'required',
                'on' => Payroll::SCENARIO_PAYROLL_ADD
            ],

            [
                [
                    'salary',
                    'day'
                ],
                'number',
                'min' => 1
            ],
            [
                [
                    'created_on',
                    'department_id',
                    'day'
                ],
                'safe'
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'department_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Department::className(),
                'targetAttribute' => [
                    'department_id' => 'id'
                ]
            ],
            [
                [
                    'user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'user_id' => 'id'
                ]
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'department_id' => Yii::t('app', 'Department'),
            'day' => Yii::t('app', 'Day'),
            'month' => Yii::t('app', 'Month'),
            'salary' => Yii::t('app', 'Salary'),
            'user_id' => Yii::t('app', 'User'),
            'state_id' => Yii::t('app', 'Status'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios['add_payroll'] = [
            'salary',
            'user_id',
            'state_id',
            'department_id'
        ];

        return $scenarios;
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), [
            'id' => 'department_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['department_id'] = [
            'department',
            'Department',
            'id'
        ];
        $relations['user_id'] = [
            'user',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['day'] = $this->day;
        $json['day_name'] = date('D', strtotime($this->created_on));
        $json['salary'] = $this->salary;
        $json['user_name'] = ! empty($this->user) ? $this->user->full_name : "not set";
        $json['department_name'] = ! empty($this->department) ? $this->department->title : "not set";
        $json['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        $json['created_on'] = $this->created_on;
        $json['created_by_id'] = $this->created_by_id;
        if ($with_relations) {}
        return $json;
    }
}
