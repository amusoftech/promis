<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_attendance".
 *
 * @property integer $id
 * @property string $date
 * @property integer $user_id
 * @property integer $type_id
 * @property integer $state_id
 * @property integer $total_time
 * @property string $created_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 * @property User $user
 */
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Attendance extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->date;
    }

    public static function getUserOptions()
    {
        return ArrayHelper::Map(User::findActive()->where([
            'IN',
            'role_id',
            [
                User::ROLE_EMPLOYEE,
                User::ROLE_HR,
                User::ROLE_MANAGER,
                User::ROLE_INVESTIGATOR
            ]
        ])->each(), 'id', 'full_name');
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;
        if (User::isHR())
            return true;

        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    const STATE_ABSENT = 0;

    const STATE_PRESENT = 1;

    const STATE_HALFDAY = 2;

    const STATE_LEAVE = 3;

    const STATE_WEEKLY_OFF = 4;

    const FLAG_TRUE = 1;

    public static function getStateOptions()
    {
        return [
            self::STATE_ABSENT => "Absent",
            self::STATE_PRESENT => "Present",
            self::STATE_HALFDAY => "Halfday",
            self::STATE_LEAVE => "Leave",
            self::STATE_WEEKLY_OFF => "Weekly Off"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_ABSENT => "warning",
            self::STATE_PRESENT => "success",
            self::STATE_HALFDAY => "info",
            self::STATE_LEAVE => "info",
            self::STATE_WEEKLY_OFF => "default"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_ABSENT => "Absent",
            self::STATE_PRESENT => "Present",
            self::STATE_HALFDAY => "Halfday",
            self::STATE_LEAVE => "Leave",
            self::STATE_WEEKLY_OFF => "Weekly Off"
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->user_id))
                $this->user_id = self::getCurrentUser();

            if (! isset($this->created_on))
                $this->created_on = date('Y-m-d H:i:s');
            if (! isset($this->created_by_id))
                $this->created_by_id = self::getCurrentUser();
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attendance}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'date',
                    'created_by_id',
                    'user_id',
                    'in_time',
                    'out_time',
                    'state_id'
                ],
                'required'
            ],
            [
                [
                    'date',
                    'total_time',
                    'created_on',
                    'in_time',
                    'out_time'
                ],
                'safe'
            ],
            [
                'out_time',
                'compare',
                'operator' => '>',
                'compareAttribute' => 'in_time'
            ],
            [
                [
                    'user_id',
                    'type_id',
                    'state_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'user_id' => 'id'
                ]
            ],
/* Will use in future  */
           /*  [
                [
                    'inTime',
                    'outTime'
                ],
                'compareTime'
            ],
 */
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /*
     * public function compareTime()
     * {
     * $end_time = $this->date . ' ' . $this->outTime;
     * $end_time = strtotime(date("Y-m-d H:i", strtotime($end_time)));
     *
     * $start_time = $this->date . ' ' . $this->inTime;
     * $start_time = strtotime(date("Y-m-d H:i", strtotime($start_time)));
     *
     * if ($end_time < $start_time)
     * return $this->addError('outTime', 'Out time should be greater than In time');
     * }
     */

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'user_id' => Yii::t('app', 'User'),
            'type_id' => Yii::t('app', 'Type'),
            'state_id' => Yii::t('app', 'Status'),
            'total_time' => Yii::t('app', 'Total Time'),
            'created_on' => Yii::t('app', 'Created On'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    // public function getTotalTime()
    // {
    // $diff = 0;

    // $inTimeRecord = AttendanceEvent::find()->where([
    // 'user_id' => $this->user_id,
    // 'date' => date('Y-m-d'),
    // 'type_id' => AttendanceEvent::TYPE_IN
    // ])
    // ->orderBy([
    // 'id' => SORT_ASC
    // ])
    // ->one();

    // if (! empty($inTimeRecord)) {
    // $inTime = $inTimeRecord->time;

    // $outTimeRecord = AttendanceEvent::find()->where([
    // 'user_id' => $this->user_id,
    // 'date' => date('Y-m-d'),
    // 'type_id' => AttendanceEvent::TYPE_OUT
    // ])
    // ->orderBy([
    // 'id' => SORT_DESC
    // ])
    // ->one();
    // if (! empty($outTimeRecord)) {
    // $outTime = $outTimeRecord->time;

    // $in = strtotime($inTime);

    // $out = strtotime($outTime);

    // $total = $out - $in;

    // $diff = self::time_elapsed_string(gmdate("H:i", $total));
    // }
    // }
    // return $diff;
    // }
    public function getTotalTime()
    {
        $in = $this->getInTimeOptions();
        $out = $this->getOutTimeOptions();

        $diff = $out - $in;
        if ($out > 0) {
            $this->total_time = gmdate('H:i', $diff);

            $this->updateAttributes([
                'total_time'
            ]);
            return self::time_elapsed_string(gmdate("H:i", $diff));
        }
    }

    public function getInTimeOptions()
    {
        $in_time = $this->getEvents(AttendanceEvent::TYPE_IN)
            ->orderBy('created_on ASC')
            ->one();

        if (! empty($in_time)) {
            return strtotime($in_time->time);
        }
    }

    public function getOutTimeOptions($date = null)
    {
        $out_time = $this->getEvents(AttendanceEvent::TYPE_OUT)
            ->orderBy('created_on DESC')
            ->one();

        if (! empty($out_time)) {
            return strtotime($out_time->time);
        }
    }

    public function getEvents($type_id = null)
    {
        $start_date = date("Y-m-d ", strtotime($this->date));
        $end_date = date("Y-m-d 08:00:00", strtotime(' +1 day', strtotime($this->date)));

        $query = AttendanceEvent::find()->where([

            'user_id' => $this->user_id
        ])->andWhere([
            'between',
            'created_on',
            $start_date,
            $end_date
        ]);

        if (isset($type_id)) {

            $query->andWhere([

                'type_id' => $type_id
            ]);
        }
        return $query;
    }

    public function getTotalWorkHours()
    {
        if ($this->total_time) {
            return Yii::$app->formatter->asDuration($this->total_time);
        }
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     *
     */
    public function getAttendanceEvent()
    {
        return $this->hasMany(AttendanceEvent::className(), [
            'attendance_id' => 'id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['user_id'] = [
            'user',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        AttendanceEvent::deleteRelatedAll([
            'attendance_id' => $this->id
        ]);
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public static function getInTime($user_id = null)
    {
        $inTime = '00:00';

        $inTimeRecord = AttendanceEvent::find()->where([
            'user_id' => \Yii::$app->user->id,
            'date' => date('Y-m-d'),
            'type_id' => AttendanceEvent::TYPE_IN
        ])
            ->orderBy([
            'id' => SORT_ASC
        ])
            ->one();

        if (! empty($user_id)) {

            $inTimeRecord = AttendanceEvent::find()->where([
                'user_id' => $user_id,
                'date' => date('Y-m-d'),
                'type_id' => AttendanceEvent::TYPE_IN
            ])
                ->orderBy([
                'id' => SORT_ASC
            ])
                ->one();
        }

        if (! empty($inTimeRecord)) {
            $inTime = $inTimeRecord->time;
        }
        return $inTime;
    }

    public static function getOutTime($user_id = null)
    {
        $outTime = '00:00';
        $outTimeRecord = AttendanceEvent::find()->where([
            'user_id' => \Yii::$app->user->id,
            'date' => date('Y-m-d'),
            'type_id' => AttendanceEvent::TYPE_OUT
        ])
            ->orderBy([
            'id' => SORT_DESC
        ])
            ->one();

        if (! empty($user_id)) {

            $outTimeRecord = AttendanceEvent::find()->where([
                'user_id' => $user_id,
                'date' => date('Y-m-d'),
                'type_id' => AttendanceEvent::TYPE_OUT
            ])
                ->orderBy([
                'id' => SORT_DESC
            ])
                ->one();
        }

        if (! empty($outTimeRecord)) {
            $outTime = $outTimeRecord->time;
        }
        return $outTime;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['day_name'] = date('D', strtotime($this->date));
        $json['date'] = $this->date;
        $json['user_name'] = ! empty($this->user) ? $this->user->full_name : "not set";
        // $json['type_id'] = $this->type_id;
        $json['state_id'] = $this->state_id;

        $json['in_time'] = $this->in_time; // ! empty($this->getInTime($this->user_id)) ? $this->created_on : $this->created_on;
        $json['out_time'] = $this->out_time; // ! empty($this->getOutTime($this->user_id)) ? $this->created_on : $this->created_on;
        $json['total_time'] = $this->total_time;
        $json['created_on'] = $this->created_on;
        $json['created_by_id'] = $this->created_by_id;

        if ($with_relations) {}
        return $json;
    }

    public static function time_elapsed_string($datetime, $full = false)
    {
        $datetime = explode(':', $datetime);
        return $datetime[0] . ' hours ' . $datetime[1] . ' minutes';
    }
}
