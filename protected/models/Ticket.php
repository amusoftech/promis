<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_ticket".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $type_id
 * @property integer $state_id
 * @property string $created_on
 * @property integer $assign_to_id
 * @property integer $created_by_id === Related data ===
 * @property User $assignTo
 * @property User $createdBy
 */
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Ticket extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->title;
    }

    const TYPE_HARDWARE = 1;

    const TYPE_SOFTWARE = 2;

    public static function getTypeOptions()
    {
        return ArrayHelper::Map(TicketType::findActive()->each(), 'id', 'title');
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    const STATE_ASSIGNED = 0;

    const STATE_INPROGRESS = 1;

    const STATE_REJECTED = 2;

    const STATE_COMPLETE = 3;

    const FLAG_TRUE = 1;

    public static function getStateOptions()
    {
        return [
            self::STATE_ASSIGNED => "Assigned",
            self::STATE_INPROGRESS => "InProgress",
            self::STATE_REJECTED => "Rejected",
            self::STATE_COMPLETE => "Complete"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_ASSIGNED => "success",
            self::STATE_INPROGRESS => "primary",
            self::STATE_REJECTED => "danger",
            self::STATE_COMPLETE => "success"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getDepartmentOptions()
    {
        return ArrayHelper::Map(Department::findActive()->each(), 'id', 'title');
    }

    public function getDepartment()
    {
        $list = self::getDepartmentOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;

        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_ASSIGNED => "Assigned",
            self::STATE_INPROGRESS => "InProgress",
            self::STATE_REJECTED => "Rejected",
            self::STATE_COMPLETE => "Complete"
        ];
    }

    public static function getAssignToOptions()
    {
        return ArrayHelper::Map(User::findActive()->where([
            'role_id' => User::ROLE_HR
        ])->each(), 'id', 'full_name');
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (! isset($this->created_on))
                $this->created_on = date('Y-m-d H:i:s');
            if (! isset($this->created_by_id))
                $this->created_by_id = self::getCurrentUser();
        } else {
            // $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ticket}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'type_id',
                    'description',
                    'assign_to_id',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'description'
                ],
                'string'
            ],
            [
                [
                    'type_id',
                    'state_id',
                    'assign_to_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_on',
                    'updated_on'
                ],
                'safe'
            ],
            [
                [
                    'title'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'assign_to_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'assign_to_id' => 'id'
                ]
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'title'
                ],
                'trim'
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getDepartmentOptions())
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'type_id' => Yii::t('app', 'Type'),
            'state_id' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'assign_to_id' => Yii::t('app', 'Assign To'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    /*
     * public function getAssignTo()
     * {
     * return $this->hasOne(User::className(), [
     * 'id' => 'assign_to_id'
     * ]);
     * }
     */
    public function getAssignTo()
    {
        return $this->hasOne(User::className(), [
            'id' => 'assign_to_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['assign_to_id'] = [
            'assignTo',
            'Department',
            'id'
        ];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['title'] = $this->title;
        $json['description'] = strip_tags($this->description);
        $json['type_id'] = ! empty($this->assignTo->department) ? $this->assignTo->department->title : \Yii::t('app', "Not Set");
        $json['state_id'] = $this->state_id;
        $json['state'] = $this->getState();
        $json['created_on'] = $this->created_on;
        $json['assign_to_id'] = $this->assign_to_id;
        $json['department_name'] = ! empty($this->assignTo->department) ? $this->assignTo->department->title : \Yii::t('app', "Not Set");
        $json['assign_to_name'] = ! empty($this->assignTo) ? $this->assignTo->full_name : \Yii::t('app', "Not Set");
        $json['created_by_name'] = ! empty($this->createdBy) ? $this->createdBy->full_name : \Yii::t('app', "Not Set");

        if (! empty($this->createdBy)) {
            $json['created_by_image'] = \Yii::$app->urlManager->createAbsoluteUrl([
                'file/file/files',
                'file' => $this->createdBy->profile_file
            ]);
        } else {
            $json['created_by_image'] = '';
        }
        $json['created_by_id'] = $this->created_by_id;
        if ($with_relations) {}
        return $json;
    }
}
