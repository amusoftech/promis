<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_reminder".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $case_id
 * @property integer $department_id
 * @property string $time
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property integer $user_id
 * @property integer $created_by_id === Related data ===
 * @property Case $case
 * @property User $createdBy
 * @property Department $department
 * @property User $user
 */
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Reminder extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->title;
    }

    public static function getCaseOptions()
    {
        return ArrayHelper::Map(Cases::find()->each(), 'id', 'title');
    }

    public static function getDepartmentOptions()
    {
        return ArrayHelper::Map(Department::findActive()->each(), 'id', 'title');
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Deleted"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "default",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;

        if (User::isManager())
            return true;
        if (User::isInvestigator())
            return true;

        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_INACTIVE => "Deactivate",
            self::STATE_ACTIVE => "Activate",
            self::STATE_DELETED => "Delete"
        ];
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public static function getUserOptions()
    {
        return ArrayHelper::Map(User::findActive()->where([
            '!=',
            'role_id',
            User::ROLE_ADMIN
        ])->each(), 'id', 'full_name');
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {

            if (! isset($this->created_on))
                $this->created_on = date('Y-m-d H:i:s');
            if (! isset($this->time))
                $this->time = date('Y-m-d H:i:s');

            if (! isset($this->user_id))
                $this->user_id = self::getCurrentUser();
        } else {
            // $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reminder}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    // 'description',
                    'case_id',
                    'department_id',
                    'time',
                    'user_id',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'description'
                ],
                'string'
            ],
            [
                [
                    'case_id',
                    'department_id',
                    'state_id',
                    'type_id',
                    'user_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'time',
                    'created_on'
                ],
                'safe'
            ],
            [
                [
                    'title'
                ],
                'string',
                'max' => 1024
            ],
            [
                [
                    'case_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cases::className(),
                'targetAttribute' => [
                    'case_id' => 'id'
                ]
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'department_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Department::className(),
                'targetAttribute' => [
                    'department_id' => 'id'
                ]
            ],
            [
                [
                    'user_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'user_id' => 'id'
                ]
            ],
            [
                [
                    'title'
                ],
                'trim'
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'case_id' => Yii::t('app', 'Case'),
            'department_id' => Yii::t('app', 'Department'),
            'time' => Yii::t('app', 'Time'),
            'state_id' => Yii::t('app', 'Status'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'user_id' => Yii::t('app', 'User'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCase()
    {
        return $this->hasOne(Cases::className(), [
            'id' => 'case_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), [
            'id' => 'department_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['case_id'] = [
            'case',
            'Case',
            'id'
        ];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['department_id'] = [
            'department',
            'Department',
            'id'
        ];
        $relations['user_id'] = [
            'user',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['title'] = $this->title;
        $json['description'] = strip_tags($this->description);
        $json['case_id'] = $this->case_id;
        $json['case_title'] = ! empty($this->case) ? strip_tags($this->case->title) : "";
        $json['case_description'] = ! empty($this->case) ? strip_tags($this->case->description) : "";
        $json['department_id'] = $this->department_id;
        $json['department_name'] = ! empty($this->department) ? $this->department->title : "";
        $json['time'] = \Yii::$app->formatter->asRelativeTime($this->time);
        $json['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        $json['created_on'] = $this->created_on;
        $json['user_id'] = $this->user_id;
        $json['user_name'] = ! empty($this->user) ? $this->user->full_name : "";
        $json['created_by_id'] = $this->created_by_id;
        $json['created_by_user'] = ! empty($this->createdBy) ? $this->createdBy->full_name : "";
        if ($with_relations) {}
        return $json;
    }
}
