<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_task".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $case_id
 * @property integer $type_id
 * @property integer $state_id
 * @property integer $platform_id
 * @property integer $department_id
 * @property string $start_date
 * @property string $end_date
 * @property string $estimated_time
 * @property string $actual_time
 * @property string $created_on
 * @property string $updated_on
 * @property integer $assign_to_id
 * @property integer $created_by_id === Related data ===
 * @property User $assignTo
 * @property Case $case
 * @property User $createdBy
 * @property Department $department
 * @property Platform $platform
 * @property TaskTimer[] $taskTimers
 */
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Task extends \app\components\TActiveRecord
{

    public $taskDetail;

    public function __toString()
    {
        return (string) $this->title;
    }

    public static function getCaseOptions()
    {
        return ArrayHelper::Map(Cases::find()->all(), 'id', 'title');
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;
        if (User::isEmployee())
            return true;
        if (User::isClient())
            return true;
        if (User::isQualityControl())
            return true;
        if (User::isManager())
            return true;
        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    const STATE_ASSIGNED = 1;

    const STATE_INPROGRESS = 2;

    const STATE_CLOSED = 3;

    const STATE_IN_REVIEW = 4;

    const STATE_ON_HOLD = 5;

    const STATE_PENDING = 6;

    const STATE_COMPLETED = 7;

    const SCENARIO_ASSIGN_TASK = "assign-task";

    const SCENARIO_UPDATE_TASK = "update-task";

    public static function getStateOptions()
    {
        return [
            self::STATE_ASSIGNED => "Assigned",
            self::STATE_INPROGRESS => "In-Progress",
            self::STATE_CLOSED => "Closed",
            self::STATE_ON_HOLD => "Hold",
            self::STATE_PENDING => "Pending",
            self::STATE_COMPLETED => "Completed",
            self::STATE_IN_REVIEW => "In-Review"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_ASSIGNED => "info",
            self::STATE_INPROGRESS => "primary",
            self::STATE_CLOSED => "success",
            self::STATE_PENDING => "danger",
            self::STATE_ON_HOLD => "info",
            self::STATE_COMPLETED => "success",
            self::STATE_IN_REVIEW => "new"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_ASSIGNED => "Assigned",
            self::STATE_INPROGRESS => "In-Progress",
            self::STATE_CLOSED => "Completed",
            self::STATE_PENDING => "Pending",
            self::STATE_ON_HOLD => "Hold",
            self::STATE_COMPLETED => "completed",
            self::STATE_IN_REVIEW => "In-review"
        ];
    }

    public function getTotalTaskTime()

    {
        $model = TaskEvent::find()->where([
            'created_by_id' => \Yii::$app->user->id,
            'task_id' => $this->id
        ])->sum('time_difference');

        if (! empty($model)) {

            return gmdate("H:i:s", $model);
        }
        return 0;
    }

    public function getTaskExtraTime()
    {
        $model = TaskEvent::find()->where([
            'created_by_id' => \Yii::$app->user->id,
            'task_id' => $this->id
        ])->sum('time_difference');

        if (! empty($model)) {
            $task = Task::findOne($this->id);
            if (! empty($task)) {
                $actual_time = gmdate("H:i:s", $model);
                $actual_times = strtotime($actual_time);
                $estimated_time = strtotime($task->estimated_time);
                $difference = (($estimated_time) - ($actual_times));
                return $extra_time = gmdate("H:i:s", $difference);
            } else {

                $extra_time = 0;
            }
        }
        return $extra_time;
    }

    public function getTimerStatus()
    {
        $timer = Timer::find()->where([
            'task_id' => $this->id,
            'state_id' => Timer::STATE_START
        ])->one();
        if ($timer != null) {
            return Html::tag('span', 'Running', [
                'class' => 'badge badge-success'
            ]);
        }
        return Html::tag('span', 'Stopped', [
            'class' => 'badge badge-success'
        ]);
    }

    public function getTimerStatusApp()
    {
        $timer = Timer::find()->where([
            'task_id' => $this->id,
            'state_id' => Timer::STATE_START
        ])->one();
        if ($timer != null) {
            return Timer::STATE_START;
        }
        return Timer::STATE_STOP;
    }

    public function getActualTime()
    {
        $this->actual_time = $this->getTimers()
            ->where([
            'state_id' => Timer::STATE_STOP
        ])
            ->sum('work_time');

        if (is_null($this->actual_time)) {
            $this->actual_time = 0;
        }
        $this->updateAttributes([
            'actual_time'
        ]);

        return Yii::$app->formatter->asDuration($this->actual_time);
    }

    public function getActualTimeApp()
    {
        $this->actual_time = $this->getTimers()
            ->where([
            'state_id' => Timer::STATE_STOP
        ])
            ->sum('work_time');

        if (is_null($this->actual_time)) {
            $this->actual_time = 0;
        }
        $this->updateAttributes([
            'actual_time'
        ]);
        return gmdate("H:i:s", $this->actual_time);
    }

    public function getEvents($type_id = null)
    {
        $start_date = date("Y-m-d ", strtotime($this->date));
        $end_date = date("Y-m-d 08:00:00", strtotime(' +1 day', strtotime($this->date)));
        $query = TaskEvent::find()->where([

            'user_id' => $this->user_id
        ])->andWhere([
            'between',
            'created_on',
            $start_date,
            $end_date
        ]);

        if (isset($type_id)) {

            $query->andWhere([
                'type_id' => $type_id
            ]);
        }
        return $query;
    }

    public static function getPlatformOptions()
    {
        return ArrayHelper::Map(Platform::findActive()->each(), 'id', 'title');
    }

    public static function getDepartmentOptions()
    {
        return ArrayHelper::Map(Department::findActive()->each(), 'id', 'title');
    }

    public static function getAssignToOptions()
    {
        return ArrayHelper::Map(User::findActive()->where([
            '!=',
            'role_id',
            [
                User::ROLE_ADMIN,
                User::ROLE_CLIENT
            ]
        ])->each(), 'id', 'full_name');
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (empty($this->start_date)) {
                $this->start_date = date('Y-m-d');
            }
            if (empty($this->end_date)) {
                $this->end_date = date('Y-m-d');
            }
            if (empty($this->created_on)) {
                $this->created_on = date('Y-m-d H:i:s');
            }
            if (empty($this->updated_on)) {
                $this->updated_on = date('Y-m-d H:i:s');
            }
            if (empty($this->created_by_id)) {
                $this->created_by_id = self::getCurrentUser();
            }
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    public static function getTaskStatus($state)
    {
        return self::find()->where([
            'assign_to_id' => Yii::$app->user->id,
            'state_id' => $state
        ])->count();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'description',
                    'case_id',
                    'platform_id',
                    // 'department_id',
                    'start_date',
                    'estimated_time',
                    'assign_to_id',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'description'
                ],
                'string'
            ],
            [
                [
                    'case_id',
                    'type_id',
                    'state_id',
                    'platform_id',
                    'department_id',
                    'assign_to_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'start_date',
                    'end_date',
                    'estimated_time',
                    'actual_time',
                    'created_on',
                    'updated_on',
                    'start_time',
                    'end_time'
                ],
                'safe'
            ],
            [
                [
                    'title'
                ],
                'string',
                'max' => 256
            ],
            [
                [
                    'assign_to_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'assign_to_id' => 'id'
                ]
            ],
            [
                [
                    'case_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cases::className(),
                'targetAttribute' => [
                    'case_id' => 'id'
                ]
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'department_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Department::className(),
                'targetAttribute' => [
                    'department_id' => 'id'
                ]
            ],
            [
                [
                    'platform_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Platform::className(),
                'targetAttribute' => [
                    'platform_id' => 'id'
                ]
            ],
            [
                [
                    'title'
                ],
                'trim'
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios['assign-task'] = [
            'title',
            'description',
            'case_id',
            'platform_id',
            'assign_to_id',
            'start_date',
            'end_date',
            'estimated_time'
        ];
        $scenarios['update-task'] = [
            'title',
            'description',
            'case_id',
            'platform_id',
            'assign_to_id',
            'start_date',
            'end_date',
            'estimated_time'
        ];

        return $scenarios;
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'case_id' => Yii::t('app', 'Case'),
            'type_id' => Yii::t('app', 'Type'),
            'state_id' => Yii::t('app', 'Status'),
            'platform_id' => Yii::t('app', 'Platform'),
            'department_id' => Yii::t('app', 'Department'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'estimated_time' => Yii::t('app', 'Estimated Time'),
            'actual_time' => Yii::t('app', 'Actual Time'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'assign_to_id' => Yii::t('app', 'Assign To'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTimers()
    {
        return $this->hasMany(Timer::className(), [
            'task_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActiveTimer()
    {
        return Timer::find()->where([
            'task_id' => $this->id,
            'state_id' => Timer::STATE_START
        ])->one();
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAssignTo()
    {
        return $this->hasOne(User::className(), [
            'id' => 'assign_to_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCase()
    {
        return $this->hasOne(Cases::className(), [
            'id' => 'case_id'
        ]);
    }

    public function getTaskEvent()
    {
        return $this->hasMany(TaskEvent::className(), [
            'task_id' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), [
            'id' => 'department_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), [
            'id' => 'platform_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['assign_to_id'] = [
            'assignTo',
            'User',
            'id'
        ];
        $relations['case_id'] = [
            'case',
            'Case',
            'id'
        ];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['department_id'] = [
            'department',
            'Department',
            'id'
        ];
        $relations['platform_id'] = [
            'platform',
            'Platform',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        TaskEvent::deleteRelatedAll([
            'task_id' => $this->id
        ]);
        Timer::deleteRelatedAll([
            'task_id' => $this->id
        ]);
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['title'] = $this->title;
        $json['description'] = strip_tags($this->description);
        $json['case_id'] = $this->case_id;
        $json['case_name'] = ! empty($this->case) ? $this->case->title : \Yii::t('app', "not Set");
        $json['type_id'] = $this->type_id;
        $json['state_id'] = $this->state_id;
        $json['platform_id'] = $this->platform_id;
        $json['platform_name'] = ! empty($this->platform) ? $this->platform->title : \Yii::t('app', "not Set");
        $json['department_id'] = $this->department_id;
        $json['department_name'] = ! empty($this->department) ? $this->department->title : \Yii::t('app', "not Set");
        $json['department_code'] = ! empty($this->department) ? $this->department->department_code : \Yii::t('app', "not Set");
        $json['start_date'] = $this->start_date;
        $json['end_date'] = $this->end_date;
        $json['estimated_time'] = $this->estimated_time;
        $json['actual_time'] = ! empty($this->actualTime) ? $this->actualTime : 0;
        $json['start_time'] = ! empty($this->actualTimeApp) ? $this->actualTimeApp : 0;
        $json['extra_time'] = ! empty($this->taskEvent) ? $this->getTaskExtraTime() : 0;
        $json['timer_status'] = $this->getTimerStatusApp();
        $json['created_on'] = $this->created_on;
        $json['created_date'] = date("Y-m-d", strtotime($this->created_on));
        $json['assign_to_id'] = $this->assign_to_id;
        $json['assign_user_name'] = ! empty($this->assignTo) ? $this->assignTo->full_name : \Yii::t('app', "not Set");
        $json['created_by_id'] = $this->created_by_id;
        $json['user_name'] = ! empty($this->createdBy) ? $this->createdBy->full_name : \Yii::t('app', "not Set");
        $json['created_by_id'] = $this->created_by_id;
        if ($with_relations) {}
        return $json;
    }

    public static function time_elapsed_string($datetime, $full = false)
    {
        $datetime = explode(':', $datetime);
        return $datetime[0] . ' hours ' . $datetime[1] . ' minutes';
    }
}
