<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_files".
 *
 * @property integer $id
 * @property string $title
 * @property string $model_id
 * @property string $model_type
 * @property string $target_url
 * @property string $description
 * @property string $filename_user
 * @property string $filename_path
 * @property string $extension
 * @property integer $public
 * @property integer $size
 * @property integer $download_count
 * @property integer $file_type
 * @property string $mimetype
 * @property string $seo_title
 * @property string $seo_alt
 * @property integer $state_id
 * @property integer $type_id
 * @property string $created_on
 * @property string $updated_on
 * @property integer $updated_by_id
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 * @property User $updatedBy
 */
namespace app\models;

use Yii;
use app\models\Feed;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\helpers\Url;

class Files extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->title;
    }

    public static function getModelOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getModel()
    {
        $list = self::getModelOptions();
        return isset($list[$this->model_id]) ? $list[$this->model_id] : 'Not Defined';
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    const IS_PUBLIC = 1;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Deleted"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "default",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_INACTIVE => "Deactivate",
            self::STATE_ACTIVE => "Activate",
            self::STATE_DELETED => "Delete"
        ];
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public static function getUpdatedByOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
        // return ArrayHelper::Map ( User::findActive ()->all (), 'id', 'title' );
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (empty($this->created_on)) {
                $this->created_on = date('Y-m-d H:i:s');
            }
            if (empty($this->updated_on)) {
                $this->updated_on = date('Y-m-d H:i:s');
            }
            if (empty($this->created_by_id)) {
                $this->created_by_id = self::getCurrentUser();
            }
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    // 'model_id',
                    'model_type',
                    'target_url',
                    'description',
                    'filename_user',
                    'filename_path'
                ],
                'string'
            ],
            [
                [
                    'model_id'
                    // 'model_type',
                ],
                'required'
            ],
            [
                [
                    'public',
                    'size',
                    'download_count',
                    'file_type',
                    'state_id',
                    'type_id',
                    'updated_by_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_on',
                    'updated_on',
                    'filename_user',
                    'filename_path',
                    'extension',
                    'public',
                    'size',
                    'mimetype',
                    'model_id'
                ],
                'safe'
            ],
            [
                [
                    'extension',
                    'mimetype',
                    'seo_title',
                    'seo_alt'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'updated_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'updated_by_id' => 'id'
                ]
            ],
            [
                [
                    'extension',
                    'mimetype',
                    'seo_title',
                    'seo_alt'
                ],
                'trim'
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'model_id' => Yii::t('app', 'Model'),
            'model_type' => Yii::t('app', 'Model Type'),
            'target_url' => Yii::t('app', 'Target Url'),
            'description' => Yii::t('app', 'Description'),
            'filename_user' => Yii::t('app', 'Filename User'),
            'filename_path' => Yii::t('app', 'Filename Path'),
            'extension' => Yii::t('app', 'Extension'),
            'public' => Yii::t('app', 'Public'),
            'size' => Yii::t('app', 'Size'),
            'download_count' => Yii::t('app', 'Download Count'),
            'file_type' => Yii::t('app', 'File Type'),
            'mimetype' => Yii::t('app', 'Mimetype'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_alt' => Yii::t('app', 'Seo Alt'),
            'state_id' => Yii::t('app', 'State'),
            'type_id' => Yii::t('app', 'Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'updated_by_id' => Yii::t('app', 'Updated By'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'updated_by_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public function upload($model, $options = [])
    {
        $files_detail = [];
        if (! empty($this->title)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            foreach ($this->title as $file) {
                $real_name = md5(Yii::$app->user->id . time() . $file->baseName . microtime());
                $file_path = UPLOAD_PATH . '/' . $real_name . '.' . $file->extension;
                if ($file->saveAs($file_path)) {

                    $target = isset($options['targetUrl']) ? $options['targetUrl'] : Url::to([
                        'user/view',
                        'id' => $model->id
                    ]);
                    $files_detail[] = [
                        'filename_user' => $file->baseName . '.' . $file->extension,
                        'filename_path' => basename($file_path),
                        'public' => isset($options['public']) ? $options['public'] : self::TYPE_ON,
                        'size' => $file->size,
                        'extension' => $file->extension,
                        'mimetype' => finfo_file($finfo, $file_path),
                        'target_url' => $target,
                        'state_id' => Files::STATE_ACTIVE,
                        'model_type' => get_class($model),
                        'model_id' => $model->id,
                        'created_by_id' => Yii::$app->user->id,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s')
                    ];
                }
            }

            if (! empty($files_detail)) {
                $connection = new Query();
                $connection->createCommand()
                    ->batchInsert($this->tableName(), [
                    'filename_user',
                    'filename_path',
                    'public',
                    'size',
                    'extension',
                    'mimetype',
                    'target_url',
                    'state_id',
                    'model_type',
                    'model_id',
                    'created_by_id',
                    'created_on',
                    'updated_on'
                ], $files_detail)
                    ->execute();
            }
            return true;
        }
        return false;
    }

    public function multiplefiles($fileModel, $model)
    {
        $fileModel->title = UploadedFile::getInstances($fileModel, 'title');

        // print_r( $fileModel->title); exit();

        if (! empty($fileModel)) {
            foreach ($fileModel->title as $key => $file) {
                $file->saveAs(UPLOAD_PATH . time() . $file->baseName . '.' . $file->extension);
                $newFileModel = new Files();
                $newFileModel->title = time() . $file->baseName . '.' . $file->extension;
                // $newFileModel->file = time() . $file->baseName . '.' . $file->extension;
                $newFileModel->size = (string) $file->size;
                $newFileModel->type_id = null;
                $newFileModel->state_id = Files::STATE_ACTIVE;
                $newFileModel->model_id = $model->id;
                $newFileModel->extension = $file->extension;
                $newFileModel->model_type = get_class($model);
                $newFileModel->created_by_id = Yii::$app->user->id;
                $newFileModel->public = Files::IS_PUBLIC;

                if (! $newFileModel->save()) {
                    $newFileModel->getErrorsString();
                }
            }
        }
    }

    public static function add($model, $data = null, $filename = null)
    {
        $old = Files::find()->where([
            'model_type' => get_class($model),
            'model_id' => $model->id,
            'filename_user' => basename($filename)
        ])->one();

        if ($old) {
            return $old;
        }
        $attachment = new Files();
        $attachment->loadDefaultValues();
        $attachment->model_id = $model->id;
        $attachment->model_type = get_class($model);
        $attachment->type_id = null;
        if ($data) {

            if ($data instanceof UploadedFile) {
                $attachment->filename_user = $data->basename . '.' . $data->extension;
                $filename = UPLOAD_PATH . yii\helpers\StringHelper::basename($attachment->model_type) . '_' . $attachment->model_id . '_' . $attachment->filename_user;
                $filename = str_replace(' ', '-', $filename);
                if (file_exists($filename))
                    unlink($filename);
                $data->saveAs($filename);
            } else {
                $attachment->filename_user = basename($filename);
                $filename = UPLOAD_PATH . yii\helpers\StringHelper::basename($attachment->model_type) . '_' . $attachment->model_id . '_' . $attachment->filename_user;
                $filename = str_replace(' ', '-', $filename);
                if (file_exists($filename))
                    unlink($filename);
                file_put_contents($filename, $data);
            }
        }
        $attachment->size = @filesize($filename);
        $attachment->filename_user = basename($filename);

        if (! $attachment->save()) {
            VarDumper::dump($attachment->errors);
            return null;
        }
        return $attachment;
    }

    public function uploadedMultipleImage($files, $model)
    {
        if (isset($files) && ! empty($files)) {
            foreach ($files as $file) {
                $imagefile = new self();
                $path = UPLOAD_PATH;
                $fileName = time() . '-' . $file->name;
                $filename = $path . basename($fileName);
                if (file_exists($filename)) {
                    unlink($fileName);
                }
                $file->saveAs($filename);
                $imagefile->model_id = $model->id;
                $imagefile->model_type = get_class($model);
                $imagefile->state_id = 0;
                $imagefile->type_id = 0;
                $imagefile->created_by_id = \yii::$app->user->id;
                $imagefile->size = $file->size;
                $imagefile->title = $fileName;
                $imagefile->extension = $file->extension;
                if (! $imagefile->save()) {
                    return false;
                }
            }
            return true;
        }
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['updated_by_id'] = [
            'updatedBy',
            'User',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['title'] = $this->title;
        $json['model_id'] = $this->model_id;
        $json['model_type'] = $this->model_type;
        $json['target_url'] = $this->target_url;
        $json['description'] = $this->description;
        $json['filename_user'] = $this->filename_user;
        $json['filename_path'] = $this->filename_path;
        $json['extension'] = $this->extension;
        $json['public'] = $this->public;
        $json['size'] = $this->size;
        $json['download_count'] = $this->download_count;
        $json['file_type'] = $this->file_type;
        $json['mimetype'] = $this->mimetype;
        $json['seo_title'] = $this->seo_title;
        $json['seo_alt'] = $this->seo_alt;
        $json['state_id'] = $this->state_id;
        $json['type_id'] = $this->type_id;
        $json['created_on'] = $this->created_on;
        $json['updated_by_id'] = $this->updated_by_id;
        $json['created_by_id'] = $this->created_by_id;
        if (! empty($this->filename_path)) {
            $json['file'] = \Yii::$app->urlManager->createAbsoluteUrl([
                'file/file/files',
                'file' => $this->filename_path
            ]);
        } else {
            $json['file'] = '';
        }

        if ($with_relations) {
            // createdBy
            $list = $this->createdBy;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createdBy'] = $relationData;
            } else {
                $json['createdBy'] = $list;
            }
            // updatedBy
            $list = $this->updatedBy;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['updatedBy'] = $relationData;
            } else {
                $json['updatedBy'] = $list;
            }
        }
        return $json;
    }

    public static function addTestData($count = 1)
    {
        $faker = \Faker\Factory::create();
        $states = array_keys(self::getStateOptions());
        for ($i = 0; $i < $count; $i ++) {
            $model = new self();

            $model->title = $faker->text;
            $model->model_id = 1;
            $model->model_type = $faker->text;
            $model->target_url = $faker->text;
            $model->description = $faker->text;
            $model->filename_user = $faker->text;
            $model->filename_path = $faker->text;
            $model->extension = $faker->text(10);
            $model->public = $faker->text(10);
            $model->size = $faker->text(10);
            $model->download_count = $faker->text(10);
            $model->file_type = $faker->text(10);
            $model->mimetype = $faker->text(10);
            $model->seo_title = $faker->text(10);
            $model->seo_alt = $faker->text(10);
            $model->state_id = $states[rand(0, count($states))];
            $model->type_id = 0;
            $model->updated_by_id = 1;
            $model->save();
        }
    }

    public static function addData($data)
    {
        $faker = \Faker\Factory::create();
        if (self::find()->count() != 0)
            return;
        foreach ($data as $item) {
            $model = new self();

            $model->title = isset($item['title']) ? $item['title'] : $faker->text;

            $model->model_id = isset($item['model_id']) ? $item['model_id'] : 1;

            $model->model_type = isset($item['model_type']) ? $item['model_type'] : $faker->text;

            $model->target_url = isset($item['target_url']) ? $item['target_url'] : $faker->text;

            $model->description = isset($item['description']) ? $item['description'] : $faker->text;

            $model->filename_user = isset($item['filename_user']) ? $item['filename_user'] : $faker->text;

            $model->filename_path = isset($item['filename_path']) ? $item['filename_path'] : $faker->text;

            $model->extension = isset($item['extension']) ? $item['extension'] : $faker->text(10);

            $model->public = isset($item['public']) ? $item['public'] : $faker->text(10);

            $model->size = isset($item['size']) ? $item['size'] : $faker->text(10);

            $model->download_count = isset($item['download_count']) ? $item['download_count'] : $faker->text(10);

            $model->file_type = isset($item['file_type']) ? $item['file_type'] : $faker->text(10);

            $model->mimetype = isset($item['mimetype']) ? $item['mimetype'] : $faker->text(10);

            $model->seo_title = isset($item['seo_title']) ? $item['seo_title'] : $faker->text(10);

            $model->seo_alt = isset($item['seo_alt']) ? $item['seo_alt'] : $faker->text(10);
            $model->state_id = self::STATE_ACTIVE;

            $model->type_id = isset($item['type_id']) ? $item['type_id'] : 0;

            $model->updated_by_id = isset($item['updated_by_id']) ? $item['updated_by_id'] : 1;
            if (! $model->save()) {
                self::log($model->getErrorsString());
            }
        }
    }
}
