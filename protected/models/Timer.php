<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_timer".
 *
 * @property integer $id
 * @property string $work_time
 * @property integer $task_id
 * @property integer $type_id
 * @property integer $state_id
 * @property string $created_on
 * @property string $started_on
 * @property string $stopped_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 * @property Task $task
 */
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Timer extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->work_time;
    }

    public static function getTaskOptions()
    {
        return ArrayHelper::Map(Task::findActive()->all(), 'id', 'title');
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;

        if (User::isManager())
            return true;

        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    const STATE_START = 0;

    const STATE_STOP = 1;

    public static function getStateOptions()
    {
        return [
            self::STATE_START => "Start",
            self::STATE_STOP => "Stop"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_START => "default",
            self::STATE_STOP => "success"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_START => "start",
            self::STATE_STOP => "stop"
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (empty($this->created_on)) {
                $this->created_on = date('Y-m-d H:i:s');
            }
            if (empty($this->created_by_id)) {
                $this->created_by_id = self::getCurrentUser();
            }
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%timer}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'work_time',
                    'created_on',
                    'started_on',
                    'stopped_on'
                ],
                'safe'
            ],
            [
                [
                    'task_id',
                    'started_on',
                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'task_id',
                    'type_id',
                    'state_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'task_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Task::className(),
                'targetAttribute' => [
                    'task_id' => 'id'
                ]
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'work_time' => Yii::t('app', 'Work Time'),
            'task_id' => Yii::t('app', 'Task'),
            'type_id' => Yii::t('app', 'Type'),
            'state_id' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'started_on' => Yii::t('app', 'Started On'),
            'stopped_on' => Yii::t('app', 'Stopped On'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), [
            'id' => 'task_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['task_id'] = [
            'task',
            'Task',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }
        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['work_time'] = ! empty($this->task->actualTime) ? $this->task->actualTime : 0;
        $json['task_id'] = $this->task_id;
        $json['task_name'] = ! empty($this->task) ? $this->task->title : "";
        $json['case_name'] = ! empty($this->task) ? ! empty($this->task->case) ? $this->task->case->title : "not set" : "not set";
        $json['start_time'] = ! empty($this->task->actualTimeApp) ? $this->task->actualTimeApp : 0;
        $json['timer_status'] = ! empty($this->task) ? $this->task->getTimerStatusApp() : 0;
        $json['type_id'] = $this->type_id;
        $json['state_id'] = $this->state_id;
        $json['created_on'] = $this->created_on;
        $json['started_on'] = $this->started_on;
        $json['stopped_on'] = $this->stopped_on;
        $json['created_by_id'] = $this->created_by_id;
        if ($with_relations) {}
        return $json;
    }

    public static function start($model)
    {
        $timer_models = self::find()->where([
            'state_id' => self::STATE_START,
            'task_id' => $model->id
        ]);

        if ($timer_models != null) {
            foreach ($timer_models->each() as $timer_model) {
                $timer_model->stopTimer();
            }
        }

        $user_timers = self::find()->andWhere([
            'state_id' => self::STATE_START,
            'created_by_id' => User::getCurrentUser()
        ]);
        if (! empty($user_timers)) {
            foreach ($user_timers->each() as $user_timer) {
                $user_timer->stopTimer();
            }
        }

        $timer = new Timer();
        $timer->created_on = date('Y-m-d H:i:s');
        $timer->task_id = $model->id;
        $timer->state_id = self::STATE_START;
        $timer->work_time = "0";
        $timer->stopped_on = date('Y-m-d H:i:s');
        $timer->created_by_id = User::getCurrentUser();
        $timer->save(false);

        $task_model = Task::find()->andWhere([
            'id' => $model->id
        ])->one();

        if ($task_model->state_id != Task::STATE_INPROGRESS) {
            $task_model->state_id = Task::STATE_INPROGRESS;
            $task_model->save();
        }

        return true;
    }

    public static function stop($model)
    {
        $timers = self::find()->where([
            'state_id' => self::STATE_START,
            'task_id' => $model->id
        ]);

        if ($timers != null) {
            foreach ($timers->each() as $timer) {
                $timer->stopTimer();
            }
        }
        return true;
    }

    public static function getCurrentTimer()
    {
        $timer = self::find()->where([
            'created_by_id' => User::getCurrentUser(),
            'state_id' => self::STATE_START
        ])->one();

        return $timer;
    }

    public function stopTimer()
    {
        if ($this->state_id != self::STATE_STOP) {
            $this->stopped_on = date('Y-m-d H:i:s');

            $this->state_id = self::STATE_STOP;
            $start = new \DateTime($this->created_on);
            $stop = new \DateTime($this->stopped_on);

            $hours = (($start->diff($stop)->h) * 3600);
            $minutes = (($start->diff($stop)->i) * 60);
            $seconds = $start->diff($stop)->s;

            $this->work_time = $hours + $minutes + $seconds;
            if ($this->work_time == '0') {
                $this->delete();
            } else {
                $this->save();
            }

            $model = Task::find()->where([
                'id' => $this->task_id
            ])->one();

            $time = self::getTotalWorkTime($model);
            if ($model) {

                $model->updateAttributes([
                    'actual_time' => $time
                ]);
            }
        }
    }

    public static function getTotalWorkTime($model)
    {
        $work_time = 0;
        $time = 0;
        $query = self::find()->where([
            'task_id' => $model->id
        ]);

        if ($query != null) {
            foreach ($query->each() as $timer) {

                $time = $work_time + $timer->work_time;
                $work_time = $time;
            }
        }
        return $work_time;
    }
}
