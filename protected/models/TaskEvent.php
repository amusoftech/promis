<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */

/**
 * This is the model class for table "tbl_task_event".
 *
 * @property integer $id
 * @property string $date
 * @property string $time
 * @property integer $user_id
 * @property integer $task_id
 * @property string $start_time
 * @property string $end_time
 * @property integer $type_id
 * @property integer $state_id
 * @property string $created_on
 * @property string $updated_on
 * @property integer $created_by_id === Related data ===
 * @property User $createdBy
 * @property Task $task
 */
namespace app\models;

use Yii;
use app\models\Feed;
use app\models\User;
use app\models\Task;
use yii\helpers\ArrayHelper;

class TaskEvent extends \app\components\TActiveRecord
{

    public function __toString()
    {
        return (string) $this->date;
    }

    public static function getUserOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getUser()
    {
        $list = self::getUserOptions();
        return isset($list[$this->user_id]) ? $list[$this->user_id] : 'Not Defined';
    }

    public static function getTaskOptions()
    {
        return ArrayHelper::Map(Task::findActive()->where([
            'assign_to_id' => \Yii::$app->user->id,
            'state_id' => Task::STATE_ASSIGNED || Task::STATE_INPROGRESS
        ])->each(), 'id', 'title');
    }

    public static function getTypeOptions()
    {
        return [
            "TYPE1",
            "TYPE2",
            "TYPE3"
        ];
    }

    public function getType()
    {
        $list = self::getTypeOptions();
        return isset($list[$this->type_id]) ? $list[$this->type_id] : 'Not Defined';
    }

    const STATE_INACTIVE = 0;

    const STATE_ACTIVE = 1;

    const STATE_DELETED = 2;

    const TYPE_IN = 1;

    const TYPE_OUT = 0;

    public static function getStateOptions()
    {
        return [
            self::STATE_INACTIVE => "New",
            self::STATE_ACTIVE => "Active",
            self::STATE_DELETED => "Deleted"
        ];
    }

    public function getState()
    {
        $list = self::getStateOptions();
        return isset($list[$this->state_id]) ? $list[$this->state_id] : 'Not Defined';
    }

    public function getStateBadge()
    {
        $list = [
            self::STATE_INACTIVE => "default",
            self::STATE_ACTIVE => "success",
            self::STATE_DELETED => "danger"
        ];
        return isset($list[$this->state_id]) ? \yii\helpers\Html::tag('span', $this->getState(), [
            'class' => 'label label-' . $list[$this->state_id]
        ]) : 'Not Defined';
    }

    public static function getActionOptions()
    {
        return [
            self::STATE_INACTIVE => "Deactivate",
            self::STATE_ACTIVE => "Activate",
            self::STATE_DELETED => "Delete"
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            if (empty($this->user_id)) {
                $this->user_id = self::getCurrentUser();
            }
            if (empty($this->created_on)) {
                $this->created_on = date('Y-m-d H:i:s');
            }
            if (empty($this->updated_on)) {
                $this->updated_on = date('Y-m-d H:i:s');
            }
            if (empty($this->created_by_id)) {
                $this->created_by_id = self::getCurrentUser();
            }
        } else {
            $this->updated_on = date('Y-m-d H:i:s');
        }
        return parent::beforeValidate();
    }

    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task_event}}';
    }

    /*
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'date',
                    // 'time',
                    // 'user_id',
                    'task_id',

                    'created_by_id'
                ],
                'required'
            ],
            [
                [
                    'date',
                    'time',
                    'start_time',
                    'end_time',
                    'created_on',
                    'updated_on',
                    'user_id',
                    'start_time',
                    'end_time',
                    'time_difference'
                ],
                'safe'
            ],
            [
                [
                    'user_id',
                    'task_id',
                    'type_id',
                    'state_id',
                    'created_by_id'
                ],
                'integer'
            ],
            [
                [
                    'created_by_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => [
                    'created_by_id' => 'id'
                ]
            ],
            [
                [
                    'task_id'
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Task::className(),
                'targetAttribute' => [
                    'task_id' => 'id'
                ]
            ],
            [
                [
                    'type_id'
                ],
                'in',
                'range' => array_keys(self::getTypeOptions())
            ],
            [
                [
                    'state_id'
                ],
                'in',
                'range' => array_keys(self::getStateOptions())
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'time' => Yii::t('app', 'Time'),
            'user_id' => Yii::t('app', 'User'),
            'task_id' => Yii::t('app', 'Task'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
            'type_id' => Yii::t('app', 'Type'),
            'state_id' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'created_by_id' => Yii::t('app', 'Created By')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), [
            'id' => 'created_by_id'
        ]);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), [
            'id' => 'task_id'
        ]);
    }

    public static function getHasManyRelations()
    {
        $relations = [];

        $relations['feeds'] = [
            'feeds',
            'Feed',
            'model_id'
        ];
        return $relations;
    }

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;

        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this->hasAttribute('created_by_id')) {
            return ($this->created_by_id == Yii::$app->user->id);
        }

        if ($this->hasAttribute('user_id')) {
            return ($this->user_id == Yii::$app->user->id);
        }

        return false;
    }

    public static function getHasOneRelations()
    {
        $relations = [];
        $relations['created_by_id'] = [
            'createdBy',
            'User',
            'id'
        ];
        $relations['task_id'] = [
            'task',
            'Task',
            'id'
        ];
        return $relations;
    }

    public function beforeDelete()
    {
        if (! parent::beforeDelete()) {
            return false;
        }

        // TODO : start here
        return true;
    }

    public function beforeSave($insert)
    {
        if (! parent::beforeSave($insert)) {
            return false;
        }
        // TODO : start here

        return true;
    }

    public function asJson($with_relations = false)
    {
        $json = [];
        $json['id'] = $this->id;
        $json['date'] = $this->date;
        $json['time'] = $this->time;
        $json['user_id'] = $this->user_id;
        $json['task_id'] = $this->task_id;
        $json['start_time'] = $this->start_time;
        $json['end_time'] = $this->end_time;
        $json['type_id'] = $this->type_id;
        $json['state_id'] = $this->state_id;
        $json['created_on'] = $this->created_on;
        $json['created_by_id'] = $this->created_by_id;
        if ($with_relations) {
            // createdBy
            $list = $this->createdBy;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['createdBy'] = $relationData;
            } else {
                $json['createdBy'] = $list;
            }
            // task
            $list = $this->task;

            if (is_array($list)) {
                $relationData = [];
                foreach ($list as $item) {
                    $relationData[] = $item->asJson();
                }
                $json['task'] = $relationData;
            } else {
                $json['task'] = $list;
            }
        }
        return $json;
    }

    public static function addTestData($count = 1)
    {
        $faker = \Faker\Factory::create();
        $states = array_keys(self::getStateOptions());
        for ($i = 0; $i < $count; $i ++) {
            $model = new self();

            $model->date = $faker->date($format = 'Y-m-d', $max = 'now');
            $model->time = $faker->time($format = 'H:i:s', $max = 'now');
            $model->user_id = 1;
            $model->task_id = 1;
            $model->start_time = $faker->dateTime($max = 'now', $timezone = null);
            $model->end_time = $faker->dateTime($max = 'now', $timezone = null);
            $model->type_id = 0;
            $model->state_id = $states[rand(0, count($states))];
            $model->save();
        }
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios['add_task_event'] = [
            'full_name',
            'email',
            'confirm_password',
            'password'
        ];

        return $scenarios;
    }

    public static function addData($data)
    {
        $faker = \Faker\Factory::create();
        if (self::find()->count() != 0)
            return;
        foreach ($data as $item) {
            $model = new self();

            $model->date = isset($item['date']) ? $item['date'] : $faker->date($format = 'Y-m-d', $max = 'now');

            $model->time = isset($item['time']) ? $item['time'] : $faker->time($format = 'H:i:s', $max = 'now');

            $model->user_id = isset($item['user_id']) ? $item['user_id'] : 1;

            $model->task_id = isset($item['task_id']) ? $item['task_id'] : 1;

            $model->start_time = isset($item['start_time']) ? $item['start_time'] : $faker->dateTime($max = 'now', $timezone = null);

            $model->end_time = isset($item['end_time']) ? $item['end_time'] : $faker->dateTime($max = 'now', $timezone = null);

            $model->type_id = isset($item['type_id']) ? $item['type_id'] : 0;
            $model->state_id = self::STATE_ACTIVE;
            if (! $model->save()) {
                self::log($model->getErrorsString());
            }
        }
    }
}
