<?php
/**
 *
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author     : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 */
namespace app\base;

use app\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use app\modules\file\models\File;
use app\models\Files;

/**
 * This is the generic model class
 */
class TBaseActiveRecord extends ActiveRecord
{

    public function isAllowed()
    {
        if (User::isAdmin())
            return true;

        if ($this instanceof User) {
            return ($this->id == Yii::$app->user->id);
        }
        if ($this instanceof self) {
            if ($this->created_by_id == Yii::$app->user->id)
                return ($this->created_by_id == Yii::$app->user->id);
        }
        return false;
    }

    public function displayImage($file, $options = [], $defaultImg = 'default.jpg', $isThumb = false)
    {
        $opt = [
            'class' => 'img-fluid',
            'id' => 'profile_file'
        ];

        $arr = array_merge($opt, $options);
        if ($isThumb) {
            $url = [
                '/file/file/thumbnail',
                'filename' => $file
            ];
        } else {
            $url = [
                '/file/file/files',
                'file' => $file
            ];
        }

        if (! empty($file) && file_exists(UPLOAD_PATH . '/' . basename($file))) {
            return Html::img($url, $arr);
        } else {
            return Html::img(\Yii::$app->view->theme->getUrl('/img/') . $defaultImg, $arr);
        }
    }

    public function displayFileImages($model, $options = [])
    {
        $data = File::find()->where([
            'model_id' => $model->id,
            'model_type' => get_class($model)
        ]);

        if (! empty($data)) {
            foreach ($data->each() as $file) {

                echo $this->displayImage($file->title, $options) . Html::a('<i class="fa fa-times-circle"></i>', [
                    '/file/delete',
                    'id' => $file->id
                ], [
                    'data-confirm' => 'Do you really want to delete this photo?',
                    'data-method' => 'post'
                ]);
            }
        }
        return false;
    }

    public static function massDelete($action = 'delete')
    {
        $Ids = \Yii::$app->request->post('ids', []);
        $response['status'] = 'OK';
        if (! empty($Ids)) {
            try {
                foreach ($Ids as $Id) {
                    $model = self::findOne($Id);
                    if (! empty($model) && ($model instanceof ActiveRecord)) {
                        if ($action == 'delete') {
                            if (($model instanceof User) && ($model->id == \Yii::$app->user->id)) {
                                throw new \Exception('Could not delete');
                            } else {
                                $model->delete();
                            }
                        } else {
                            throw new \Exception('Delete Action not performed');
                        }
                    }
                }
            } catch (\Exception $e) {
                $response['status'] = 'NOK';
                $response['error'] = $e->getMessage();
            }
        }
        \Yii::$app->response->format = 'json';
        return $response;
    }
}
