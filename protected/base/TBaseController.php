<?php

/**
 *
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 */
namespace app\base;

use app\models\User;
use Yii;
use yii\web\Controller;

class TBaseController extends Controller
{

    public $layout = '//guest-main';

    public $menu = [];

    public $_author = '@toxsl';

    public $top_menu = [];

    public $side_menu = [];

    public $user_menu = [];

    public $tabs_data = null;

    public $tabs_name = null;

    public $dryRun = false;

    public $assetsDir = '@webroot/assets';

    public $ignoreDirs = [];

    public $nav_left = [];

    public $nav_left_logout = [];

    // nav-left-medium';
    protected $_pageCaption;

    protected $_pageDescription;

    protected $_pageKeywords;

    public function beforeAction($action)
    {
        if (! parent::beforeAction($action)) {
            return false;
        }

        if (! Yii::$app->user->isGuest) {
            $this->layout = 'main';
        }

        return true;
    }

    public static function addmenu($label, $link, $icon, $visible = null, $list = null)
    {
        if (! $visible)
            return null;
        $item = [
            'label' => '<i
							class="fa fa-' . $icon . '"></i> <span>' . $label . '</span>',
            'url' => [
                $link
            ]
        ];
        if ($list != null) {
            $item['options'] = [
                'class' => 'menu-list'
            ];

            $item['items'] = $list;
        }

        return $item;
    }

    public function renderNav()
    {
        if (User::isClient()) {
            $this->nav_left = [
                self::addMenu(Yii::t('app', 'Cases'), '//cases/index', 'home', User::isClient()),
                self::addMenu(Yii::t('app', 'All Reports'), '//case-report/index', 'home', User::isClient())
            ];
        }

        if (User::isDistributor()) {
            $this->nav_left = [
                self::addMenu(Yii::t('app', 'Client Reports'), '//client-reports/index', 'home', User::isDistributor()),
                self::addMenu(Yii::t('app', 'Client Bills'), '//client-bills/index', 'home', User::isDistributor())
            ];
        }

        if (User::isQualityControl()) {
            $this->nav_left = [
                self::addMenu(Yii::t('app', 'Cases'), '//cases/index', 'home', User::isQualityControl()),
                self::addMenu(Yii::t('app', 'Client Reports'), '//client-reports/index', 'home', User::isQualityControl()),
                self::addMenu(Yii::t('app', 'My Task'), '//task/index', 'pie-chart', User::isQualityControl())
            ];
        }

        if (User::isEmployee()) {
            $this->nav_left = [
                self::addMenu(Yii::t('app', 'Dashboard'), '//dashboard/employee-index', 'home', User::isEmployee()),
                self::addMenu(Yii::t('app', 'Project'), '//cases/index', 'product-hunt', User::isEmployee()),
                self::addMenu(Yii::t('app', 'My Task'), '//task/index', 'tasks', User::isEmployee()),
                // self::addMenu(Yii::t('app', 'Task Event'), '//task-event/index', 'home', User::isEmployee()),
                self::addMenu(Yii::t('app', 'Leave Requests'), '//leave-request/index', 'home', User::isEmployee()),
                self::addMenu(Yii::t('app', 'Ticket Requests'), '//ticket/index', 'ticket', User::isEmployee())
            ];
        }

        if (User::isHR()) {
            $this->nav_left = [
                self::addMenu(Yii::t('app', 'Home'), '//dashboard/hr-index', 'home', User::isHR()),
                self::addMenu(Yii::t('app', 'Payroll'), '//payroll/index', 'money', User::isHR()),
                self::addMenu(Yii::t('app', 'Attendance'), '//attendance/index', 'edit', User::isHR()),
                self::addMenu(Yii::t('app', 'Leave Requests'), '//leave-request/index', 'bullhorn', User::isHR()),
                self::addMenu(Yii::t('app', 'Ticket Requests'), '//ticket/index', 'ticket', User::isHR())
            ];
        }

        if (User::isInvestigator()) {
            $this->nav_left = [
                self::addMenu(Yii::t('app', 'Cases'), '//cases/index', 'home', User::isInvestigator()),
                self::addMenu(Yii::t('app', 'Reports'), '//case-report/index', 'home', User::isInvestigator()),
                self::addMenu(Yii::t('app', 'Reminders'), '//reminder/index', 'home', User::isInvestigator()),
                self::addMenu(Yii::t('app', 'Attendance'), '//attendance/index', 'home', User::isInvestigator()),
                self::addMenu(Yii::t('app', 'Leave Requests'), '//leave-request/index', 'home', User::isInvestigator()),
                self::addMenu(Yii::t('app', 'Ticket Requests'), '//ticket/index', 'home', User::isInvestigator())
            ];
        }

        if (User::isManager()) {
            $this->nav_left = [
                self::addMenu(Yii::t('app', 'Dashboard'), '//dashboard/manager-index', 'home', User::isManager()),
                self::addMenu(Yii::t('app', 'Cases'), '//cases/index', 'rocket', User::isManager()),
                self::addMenu(Yii::t('app', 'Reports'), '//case-report/index', 'pie-chart', User::isManager()),
                self::addMenu(Yii::t('app', 'Task'), '//task/index', 'pie-chart', User::isManager()),
                self::addMenu(Yii::t('app', 'Attendance'), '//attendance/index', 'home', User::isManager()),
                self::addMenu(Yii::t('app', 'Employee Leave Requests'), '//leave-request/index?type=7', 'bullhorn', User::isManager()),
                self::addMenu(Yii::t('app', 'Leave Requests'), '//leave-request/index', 'bullhorn', User::isManager()),
                self::addMenu(Yii::t('app', 'Ticket Requests'), '//ticket/index', 'ticket', User::isManager()),
                self::addMenu(Yii::t('app', 'Employee Ticket Requests'), '//ticket/index?type=7', 'ticket', User::isManager())
            ];
        }

        if (User::isAdmin()) {

            $this->nav_left = [
                self::addMenu(Yii::t('app', 'Dashboard'), '//dashboard/index', 'home', User::isAdmin()),

                'Members' => self::addMenu(Yii::t('app', 'Members'), '#', 'users', User::isAdmin(), [
                    self::addMenu(Yii::t('app', 'Managers'), '//user/index/' . User::ROLE_MANAGER, 'building', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Investigators'), '//user/index/' . User::ROLE_INVESTIGATOR, 'user', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Distributor'), '//user/index/' . User::ROLE_DISTRIBUTOR, 'user', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'QualityControllers'), '//user/index/' . User::ROLE_QUALITY_CONTROL, 'user', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Hr'), '//user/index/' . User::ROLE_HR, 'building', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Employee'), '//user/index/' . User::ROLE_EMPLOYEE, 'far fa-user-circle', User::isAdmin())
                ]),

               
                self::addMenu(Yii::t('app', 'Designation'), '//designation/index/', 'graduation-cap', User::isAdmin()),
                self::addMenu(Yii::t('app', 'Department'), '//department/index/', 'deviantart', User::isAdmin()),
                self::addMenu(Yii::t('app', 'Platforms'), '//platform/index/', 'puzzle-piece', User::isAdmin()),
                self::addMenu(Yii::t('app', 'Payroll'), '//payroll/index', 'home', User::isAdmin()),
                self::addMenu(Yii::t('app', 'Task'), '//task/index', 'tasks', User::isAdmin()),
                'Ticket' => self::addMenu(Yii::t('app', 'Ticket'), '#', 'tasks', User::isAdmin(), [
                    self::addMenu(Yii::t('app', 'Ticket Type'), '//ticket-type/index', 'home', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Ticket Requests'), '//ticket/index', 'home', User::isAdmin())
                ]),
                'Attendance' => self::addMenu(Yii::t('app', 'Attendance'), '#', 'address-card', User::isAdmin(), [
                    self::addMenu(Yii::t('app', 'Attendance'), '//attendance/index', 'home', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Attendance Event '), '//attendance-event/index', 'edit', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Leave Requests'), '//leave-request/index', 'home', User::isAdmin()),
                ]),

            
                'Client' => self::addMenu(Yii::t('app', 'Client And Project'), '#', 'tasks', User::isAdmin(), [
                    self::addMenu(Yii::t('app', 'Client'), '//client/index', 'user-secret', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Case Type'), '//case-type/index', 'bars', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Cases'), '//cases/index', 'check-circle-o', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Case Reports'), '//case-report/index', 'file-text', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Reminders'), '//reminder/index', 'clock-o', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Client Reports'), '//client-reports/index', 'file-text-o', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Client Bills'), '//client-bills/index', 'file-text', User::isAdmin())
                ]),
                'Manage' => self::addMenu(Yii::t('app', 'Manage'), '#', 'tasks', User::isAdmin(), [
                    self::addMenu(Yii::t('app', 'Logs'), '//log/index', 'file', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Email Queue'), '//email-queue/index', 'envelope', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Notices'), '//notice/index', 'tasks', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Login History'), '//login-history/index', 'history', User::isAdmin()),
                    self::addMenu(Yii::t('app', 'Settings'), '//setting/index', 'gear', User::isAdmin())
                ]),
               
                self::addMenu(Yii::t('app', 'Analytics'), '//analytics/index', 'pie-chart', User::isAdmin()),
                self::addMenu(Yii::t('app', 'Page'), '//page/', 'file-text', User::isAdmin()),
            ];
        }

        /*
         * $this->nav_left_logout = [
         * self::addMenu(Yii::t('app', 'Logout'), '//user/logout', 'lock', User::isAdmin() || User::isClient() || User::isDistributor() || User::isEmployee() || User::isHR() || User::isInvestigator() || User::isManager())
         * ];
         */
        $this->nav_left = array_merge($this->nav_left, $this->renderModuleNev(), $this->nav_left_logout);

        return $this->nav_left;
    }

    public function renderModuleNev()
    {
        $config = include (DB_CONFIG_PATH . 'web.php');
        $nav = [];
        if (! empty($config['modules'])) {
            foreach ($config['modules'] as $modules) {
                $class = isset($modules['class']) ? $modules['class'] : null;
                if (class_exists("$class") && method_exists($class, 'subNav')) {
                    if (! empty($class::subNav())) {
                        $nav[] = $class::subNav();
                    }
                }
            }
        }
        return $nav;
    }
}
