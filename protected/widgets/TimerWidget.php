<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\widgets;

use app\components\TBaseWidget;
use app\models\Task;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is just an example.
 */
class TimerWidget extends TBaseWidget
{

    public $model;

    public $readOnly = true;

    public function init()
    {
        parent::init();

        if ($this->model == null || ! ($this->model instanceof Task)) {
            // Throw Exception
            
            
        }
        if (in_array($this->model->state_id, [
        Task::STATE_ASSIGNED,
        Task::STATE_INPROGRESS
        ]) && ($this->model->assign_to_id == \Yii::$app->user->id)) {
        $this->readOnly = false;
        }
        
    }

    public function renderHtml()
    {
        $work_time = $this->model->getActualTime();

        $class = ($this->model->actual_time > $this->model->estimated_time) ? 'btn btn-danger' : 'btn btn-primary';
        echo $this->render('_view', [
            'model' => $this->model,
            'work_time' => $work_time,
            'timerClass' => $class
        ]);
    }
}
