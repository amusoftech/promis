<?php

use app\models\Timer;
use yii\helpers\Url;

?>
<?php

$timer_state = $model->activeTimer;

?>

<div class="input-group ">

	<input type="text" placeholder="00:00:00" class="timer  <?php echo $timerClass; ?>" id="event-title" data-toggle="tooltip" title="<?php echo $model; ?>" data-placement="bottom" name="timer" value="<?php echo isset($work_time) ? $work_time : null; ?>">



	<div class="input-group-btn">
		<button id="event-add" type="button" class="btn dropdown-toggle bg-danger text-white start-timer-btn">
			<i class="fa fa-play fa-fw"></i>
		</button>

		<button id="event-add" type="button" class="btn dropdown-toggle bg-danger text-white remove-timer-btn d-none">
			<i class="fa fa-stop fa-fw"></i>
		</button>

	</div>

	<!-- /btn-group -->
</div>
<div class="clearfix"></div>
<small style="color: grey; margin-left: 15px;">Keep the page open.</small>
<br>
<br>
<?php

$timer_state = 0;
?>


<script>
	$(document).ready(function() {
		<?php

		if ($timer_state != null && $timer_state == Timer::STATE_START) {
		?>
			start(false);
		<?php } ?>

	});

	function update() {
		$.ajax({
			'url': '<?php echo Url::toRoute(['timer/update', 'id' => $model->id]) ?>',
			'success': function(response) {
				console.log(response);

			}
		});
		setTimeout(function() {
			update();
		}, (5 * 60 * 1000));
	}

	function start($bool) {
		var start_time = $('.timer').val();

		var start_time_seconds = new Date('1970-01-01T' + start_time + 'Z').getTime() / 1000;

		hasTimer = true;
		$('.timer').timer({
			format: '%H:%M:%S',
			editable: true,
			seconds: start_time_seconds
		});

		$('.start-timer-btn').addClass('d-none');
		$('.remove-timer-btn').removeClass('d-none');
		var time = $('.timer').val();
		var timer = $("#eventTimer").val();
		//if($bool) {
		console.log('kkkk', time);

		startTimer('<?= $model->id ?>', time);
		/*  $.ajax({
	            'url': '<?php echo Url::toRoute(['timer/start-timer', 'id' => $model->id]) ?>',
				data: {
                time: time,
                task_id: '<?= $model->id ?>',
                _csrf: '<?= \yii::$app->request->getCsrfToken() ?>'
            },
            method: 'POST',
	            'success':function(response) {
	           	 if ( response.status == "OK" ) {
		           	 //$("#timer-run").html(response.html);
						console.log(response);
	           	 }
	                }
	        }); */
		//}
		// update(); 
	}

	function startTimer(task_id, time) {
	
		// console.log(task_id);
		$.ajax({
			url: '<?= Url::toRoute(['timer/start-timer']) ?>',
			data: {
				task_id: task_id,
				time: time,
				_csrf: '<?= \yii::$app->request->getCsrfToken() ?>'
			},
			method: 'POST',
			success: function(response) {
				
				if (response.status == 'OK') {
					if (response.state == '1') {
						console.log(response);
					} else {
						console.log(response);
					}
				}
				console.log(response);
			},
			error: function(xhr, status, error) {
				var err = eval("(" + xhr.responseText + ")");
				alert(err.Message);
			}
			
		});
	}

	function stop() {

		hasTimer = false;
		var time = $('.timer').val();
		//console.log(time);
		$("#eventTimer").val(time);
		$('.timer').timer('remove');
		$('.remove-timer-btn').addClass('d-none');
		$('.start-timer-btn').removeClass('d-none');
		$.ajax({
			'url': '<?php echo Url::toRoute(['timer/stop', 'id' => $model->id]) ?>',
			'success': function(response) {
				if (response.status == "OK") {
					$("#timer-run").html(response.html);
				}
			}
		});


	}

	(function() {
		var hasTimer = false;
		// Init timer start
		$('.start-timer-btn').on('click', function() {
			start(true);
		});

		// Remove timer
		$('.remove-timer-btn').on('click', function() {
			stop();
		});


	})();
</script>