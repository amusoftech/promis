SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------
SET AUTOCOMMIT=0;
START TRANSACTION;
SET SQL_QUOTE_SHOW_CREATE = 1;
-- -------------------------------------------

-- -------------------------------------------

-- START BACKUP

-- -------------------------------------------

-- -------------------------------------------

-- TABLE `ha_logins`

-- -------------------------------------------
DROP TABLE IF EXISTS `ha_logins`;
CREATE TABLE IF NOT EXISTS `ha_logins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `loginProvider` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `loginProviderIdentifier` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginProvider_2` (`loginProvider`,`loginProviderIdentifier`),
  KEY `loginProvider` (`loginProvider`),
  KEY `loginProviderIdentifier` (`loginProviderIdentifier`),
  KEY `userId` (`userId`),
  KEY `id` (`id`),
  KEY `user_id` (`id`),
  KEY `fk_ha_logins_created_by` (`user_id`),
  CONSTRAINT `fk_ha_logins_user` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_api_device_detail`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_api_device_detail`;
CREATE TABLE IF NOT EXISTS `tbl_api_device_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_token` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `device_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_type` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_api_device_detail_create_user` (`created_by_id`),
  CONSTRAINT `fk_api_device_detail_create_user` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_attendance`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_attendance`;
CREATE TABLE IF NOT EXISTS `tbl_attendance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time NOT NULL,
  `user_id` int DEFAULT NULL,
  `type_id` int DEFAULT '0',
  `state_id` int DEFAULT '1',
  `total_time` time DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `state_id` (`state_id`),
  KEY `fk_attendance_user_id` (`user_id`),
  KEY `fk_attendance_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_attendance_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_attendance_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_attendance_event`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_attendance_event`;
CREATE TABLE IF NOT EXISTS `tbl_attendance_event` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `type_id` int NOT NULL,
  `time` time NOT NULL,
  `state_id` int DEFAULT '0',
  `user_id` int NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  `attendance_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `type_id` (`type_id`),
  KEY `time` (`time`),
  KEY `fk_attendance_event_user` (`user_id`),
  KEY `fk_attendance_event_created_by` (`created_by_id`),
  KEY `fk_attendance_event_attendance_id` (`attendance_id`),
  CONSTRAINT `fk_attendance_event_attendance_id` FOREIGN KEY (`attendance_id`) REFERENCES `tbl_attendance` (`id`),
  CONSTRAINT `fk_attendance_event_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_attendance_event_user` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_blog_category`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_blog_category`;
CREATE TABLE IF NOT EXISTS `tbl_blog_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `state_id` (`state_id`),
  KEY `FK_blog_category_created_by_id` (`created_by_id`),
  CONSTRAINT `FK_blog_category_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_blog_post`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_blog_post`;
CREATE TABLE IF NOT EXISTS `tbl_blog_post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_count` int NOT NULL DEFAULT '0',
  `state_id` int NOT NULL,
  `type_id` int NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `keywords` (`keywords`),
  KEY `state_id` (`state_id`),
  KEY `created_on` (`created_on`),
  KEY `FK_blog_created_by_id` (`created_by_id`),
  CONSTRAINT `FK_blog_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_case`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_case`;
CREATE TABLE IF NOT EXISTS `tbl_case` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int NOT NULL,
  `department_id` int NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `type_id` int DEFAULT '0',
  `state_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `assign_to_id` int NOT NULL,
  `created_by_id` int NOT NULL,
  `budget` decimal(8,2) NOT NULL,
  `case_platform` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `client_id` (`client_id`),
  KEY `department_id` (`department_id`),
  KEY `state_id` (`state_id`),
  KEY `fk_case_client_id` (`client_id`),
  KEY `fk_case_department_id` (`department_id`),
  KEY `fk_case_assign_to_id` (`assign_to_id`),
  KEY `fk_case_created_by_id` (`created_by_id`),
  KEY `fk_case_platform` (`case_platform`),
  CONSTRAINT `fk_case_assign_to_id` FOREIGN KEY (`assign_to_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_case_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_case_department_id` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`),
  CONSTRAINT `fk_case_platform` FOREIGN KEY (`case_platform`) REFERENCES `tbl_platform` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_case_platform`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_case_platform`;
CREATE TABLE IF NOT EXISTS `tbl_case_platform` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_id` int NOT NULL,
  `case_id` int NOT NULL,
  `state_id` int DEFAULT '1',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `case_id` (`case_id`),
  KEY `state_id` (`state_id`),
  KEY `fk_case_platform_created_by_id` (`created_by_id`),
  KEY `fk_case_platform_case_id` (`case_id`),
  KEY `fk_case_platform_type_id` (`type_id`),
  CONSTRAINT `fk_case_platform_case_id` FOREIGN KEY (`case_id`) REFERENCES `tbl_case` (`id`),
  CONSTRAINT `fk_case_platform_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_case_platform_type_id` FOREIGN KEY (`type_id`) REFERENCES `tbl_platform` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_case_report`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_case_report`;
CREATE TABLE IF NOT EXISTS `tbl_case_report` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `case_id` int NOT NULL,
  `submitted_to` int NOT NULL,
  `created_by_id` int NOT NULL,
  `state_id` int NOT NULL,
  `type_id` int DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_case_report_created_by_id` (`created_by_id`),
  KEY `fk_case_report_submitted_to` (`submitted_to`),
  KEY `fk_case_report_case_id` (`case_id`),
  CONSTRAINT `fk_case_report_case_id` FOREIGN KEY (`case_id`) REFERENCES `tbl_case` (`id`),
  CONSTRAINT `fk_case_report_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_case_report_submitted_to` FOREIGN KEY (`submitted_to`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


-- -------------------------------------------

-- TABLE `tbl_case_type`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_case_type`;
CREATE TABLE IF NOT EXISTS `tbl_case_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `state_id` int DEFAULT '1',
  `type_id` int DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_case_type_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_case_type_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


-- -------------------------------------------

-- TABLE `tbl_category`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int NOT NULL,
  `type_id` int DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_created_by` (`created_by_id`),
  CONSTRAINT `fk_category_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_check`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_check`;
CREATE TABLE IF NOT EXISTS `tbl_check` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `street_name` text NOT NULL,
  `created_by_id` int NOT NULL,
  `qweqweqwk` int NOT NULL,
  `eqweqwa` int NOT NULL,
  `eqweouiy` int NOT NULL,
  `weqweqwl` int NOT NULL,
  `eqweqwk` int NOT NULL,
  `weqwewer` int NOT NULL,
  `qweqweerq` int NOT NULL,
  `qweqwewq` int NOT NULL,
  `qweqwwq` int NOT NULL,
  `ewqeqwewqe` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- -------------------------------------------

-- TABLE `tbl_client`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_client`;
CREATE TABLE IF NOT EXISTS `tbl_client` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `alternate_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `skype_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int DEFAULT '0',
  `state_id` int DEFAULT '1',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `name` (`name`),
  KEY `email_2` (`email`),
  KEY `country` (`country`),
  KEY `state_id` (`state_id`),
  KEY `fk_client_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_client_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_client_bills`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_client_bills`;
CREATE TABLE IF NOT EXISTS `tbl_client_bills` (
  `id` int NOT NULL AUTO_INCREMENT,
  `case_id` int DEFAULT NULL,
  `full_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `amount` int NOT NULL,
  `sent_by` int DEFAULT NULL,
  `date` date NOT NULL,
  `state_id` int NOT NULL DEFAULT '1',
  `type_id` int NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `state_id` (`state_id`),
  KEY `created_by_id` (`created_by_id`),
  KEY `fk_client_bills_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_client_bills_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_client_reports`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_client_reports`;
CREATE TABLE IF NOT EXISTS `tbl_client_reports` (
  `id` int NOT NULL AUTO_INCREMENT,
  `case_id` int DEFAULT NULL,
  `report` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sent_by` int DEFAULT NULL,
  `date` date NOT NULL,
  `state_id` int NOT NULL DEFAULT '1',
  `type_id` int NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `state_id` (`state_id`),
  KEY `created_by_id` (`created_by_id`),
  KEY `fk_client_reports_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_client_reports_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_comment`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_comment`;
CREATE TABLE IF NOT EXISTS `tbl_comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `model_id` int NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `comment` text,
  `state_id` int DEFAULT '1',
  `type_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_comment_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- -------------------------------------------

-- TABLE `tbl_department`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_department`;
CREATE TABLE IF NOT EXISTS `tbl_department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `department_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int NOT NULL DEFAULT '1',
  `state_id` int NOT NULL DEFAULT '1',
  `manager_id` int DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_designation`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_designation`;
CREATE TABLE IF NOT EXISTS `tbl_designation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int NOT NULL DEFAULT '1',
  `state_id` int NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_document`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_document`;
CREATE TABLE IF NOT EXISTS `tbl_document` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int NOT NULL DEFAULT '0',
  `state_id` int NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_document_created_by_id` (`created_by_id`),
  KEY `fk_document_user_id` (`user_id`),
  CONSTRAINT `fk_document_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_document_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_email_queue`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_email_queue`;
CREATE TABLE IF NOT EXISTS `tbl_email_queue` (
  `id` int NOT NULL AUTO_INCREMENT,
  `from_email` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_email` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_published` datetime DEFAULT NULL,
  `last_attempt` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `attempts` int DEFAULT NULL,
  `state_id` int DEFAULT NULL,
  `model_id` int DEFAULT NULL,
  `model_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_account_id` int DEFAULT NULL,
  `message_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_feed`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_feed`;
CREATE TABLE IF NOT EXISTS `tbl_feed` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `model_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int NOT NULL,
  `state_id` int NOT NULL DEFAULT '0',
  `type_id` int NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_files`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_files`;
CREATE TABLE IF NOT EXISTS `tbl_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `model_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `model_type` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `target_url` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `filename_user` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `filename_path` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `extension` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `public` int DEFAULT NULL,
  `size` bigint DEFAULT NULL,
  `download_count` bigint DEFAULT '0',
  `file_type` int DEFAULT '1',
  `mimetype` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_alt` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int DEFAULT NULL,
  `type_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by_id` int DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_files_created_by_id` (`created_by_id`),
  KEY `fk_files_updated_by_id` (`updated_by_id`),
  CONSTRAINT `fk_files_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_files_updated_by_id` FOREIGN KEY (`updated_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_language_option`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_language_option`;
CREATE TABLE IF NOT EXISTS `tbl_language_option` (
  `id` int NOT NULL AUTO_INCREMENT,
  `language_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int NOT NULL DEFAULT '1',
  `type_id` int NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_language_option_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_language_option_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_leave_request`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_leave_request`;
CREATE TABLE IF NOT EXISTS `tbl_leave_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `start_date` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `end_date` date DEFAULT NULL,
  `is_halfday` tinyint(1) DEFAULT '0',
  `type_id` int DEFAULT '0',
  `state_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `start_date` (`start_date`),
  KEY `state_id` (`state_id`),
  KEY `fk_leave_request_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_leave_request_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_log`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE IF NOT EXISTS `tbl_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `error` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `api` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `state_id` int DEFAULT '1',
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int DEFAULT '0',
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_logger_log`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_logger_log`;
CREATE TABLE IF NOT EXISTS `tbl_logger_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `error` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `api` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `state_id` int NOT NULL DEFAULT '1',
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_login_history`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_login_history`;
CREATE TABLE IF NOT EXISTS `tbl_login_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `user_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `failer_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int NOT NULL,
  `type_id` int NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_media_gallery`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_media_gallery`;
CREATE TABLE IF NOT EXISTS `tbl_media_gallery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `alt` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `thumb_file` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int DEFAULT NULL,
  `type_id` int DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  `createBy` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_migration`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- -------------------------------------------

-- TABLE `tbl_notice`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_notice`;
CREATE TABLE IF NOT EXISTS `tbl_notice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `model_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int NOT NULL,
  `state_id` int DEFAULT '0',
  `type_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notice_created_by` (`created_by_id`),
  CONSTRAINT `fk_notice_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_notification`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_notification`;
CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `model_id` int NOT NULL,
  `model_type` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_read` tinyint DEFAULT '0',
  `state_id` int DEFAULT '0',
  `type_id` int DEFAULT '0',
  `created_on` datetime NOT NULL,
  `to_user_id` int DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notification_created_by` (`created_by_id`),
  CONSTRAINT `fk_notification_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_page`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_page`;
CREATE TABLE IF NOT EXISTS `tbl_page` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int DEFAULT '1',
  `type_id` int DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_page_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_page_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_payroll`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_payroll`;
CREATE TABLE IF NOT EXISTS `tbl_payroll` (
  `id` int NOT NULL AUTO_INCREMENT,
  `department_id` int DEFAULT NULL,
  `day` int DEFAULT NULL,
  `salary` int NOT NULL,
  `user_id` int NOT NULL,
  `state_id` int DEFAULT '1',
  `type_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  `month` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  KEY `user_id` (`user_id`),
  KEY `state_id` (`state_id`),
  KEY `fk_payroll_department_id` (`department_id`),
  KEY `fk_payroll_user_id` (`user_id`),
  KEY `fk_payroll_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_payroll_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_payroll_department_id` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`),
  CONSTRAINT `fk_payroll_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_platform`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_platform`;
CREATE TABLE IF NOT EXISTS `tbl_platform` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int DEFAULT '1',
  `type_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `state_id` (`state_id`),
  KEY `fk_platform_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_platform_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_queue`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_queue`;
CREATE TABLE IF NOT EXISTS `tbl_queue` (
  `id` int NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) NOT NULL,
  `job` blob NOT NULL,
  `pushed_at` int NOT NULL,
  `ttr` int NOT NULL,
  `delay` int NOT NULL DEFAULT '0',
  `priority` int unsigned NOT NULL DEFAULT '1024',
  `reserved_at` int DEFAULT NULL,
  `attempt` int DEFAULT NULL,
  `done_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `channel` (`channel`),
  KEY `reserved_at` (`reserved_at`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- -------------------------------------------

-- TABLE `tbl_rating`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_rating`;
CREATE TABLE IF NOT EXISTS `tbl_rating` (
  `id` int NOT NULL AUTO_INCREMENT,
  `model_id` int NOT NULL,
  `model_type` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `state_id` int DEFAULT '1',
  `type_id` int DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rating_created_by_id` (`created_by_id`),
  CONSTRAINT `FK_rating_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_reminder`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_reminder`;
CREATE TABLE IF NOT EXISTS `tbl_reminder` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `case_id` int NOT NULL,
  `department_id` int NOT NULL,
  `time` time NOT NULL,
  `state_id` int DEFAULT '0',
  `type_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `user_id` int NOT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `case_id` (`case_id`),
  KEY `user_id` (`user_id`),
  KEY `department_id` (`department_id`),
  KEY `state_id` (`state_id`),
  KEY `fk_reminder_case_id` (`case_id`),
  KEY `fk_reminder_user_id` (`user_id`),
  KEY `fk_reminder_department_id` (`department_id`),
  KEY `fk_reminder_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_reminder_case_id` FOREIGN KEY (`case_id`) REFERENCES `tbl_case` (`id`),
  CONSTRAINT `fk_reminder_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_reminder_department_id` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`),
  CONSTRAINT `fk_reminder_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_seo`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_seo`;
CREATE TABLE IF NOT EXISTS `tbl_seo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `route` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seo_idx_route` (`route`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -------------------------------------------

-- TABLE `tbl_seo_analytics`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_seo_analytics`;
CREATE TABLE IF NOT EXISTS `tbl_seo_analytics` (
  `id` int NOT NULL AUTO_INCREMENT,
  `account` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `domain_name` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `additional_information` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int NOT NULL DEFAULT '1',
  `type_id` int NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_seo_analytics_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_seo_analytics_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_seo_redirect`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_seo_redirect`;
CREATE TABLE IF NOT EXISTS `tbl_seo_redirect` (
  `id` int NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `new_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int NOT NULL DEFAULT '0',
  `type_id` int NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_seo_redirect_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_seo_redirect_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_setting`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_setting`;
CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `type_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int DEFAULT '0',
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_task`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_task`;
CREATE TABLE IF NOT EXISTS `tbl_task` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `case_id` int NOT NULL,
  `type_id` int DEFAULT '0',
  `state_id` int DEFAULT '1',
  `platform_id` int NOT NULL,
  `department_id` int DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `estimated_time` time NOT NULL,
  `actual_time` int DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `assign_to_id` int NOT NULL,
  `created_by_id` int NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `case_id` (`case_id`),
  KEY `platform_id` (`platform_id`),
  KEY `department_id` (`department_id`),
  KEY `type_id` (`type_id`),
  KEY `state_id` (`state_id`),
  KEY `fk_task_case_id` (`case_id`),
  KEY `fk_task_created_by_id` (`created_by_id`),
  KEY `fk_task_assign_to_id` (`assign_to_id`),
  KEY `fk_task_platform_id` (`platform_id`),
  KEY `fk_task_department_id` (`department_id`),
  CONSTRAINT `fk_task_assign_to_id` FOREIGN KEY (`assign_to_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_task_case_id` FOREIGN KEY (`case_id`) REFERENCES `tbl_case` (`id`),
  CONSTRAINT `fk_task_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_task_department_id` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`),
  CONSTRAINT `fk_task_platform_id` FOREIGN KEY (`platform_id`) REFERENCES `tbl_platform` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_task_event`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_task_event`;
CREATE TABLE IF NOT EXISTS `tbl_task_event` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` time DEFAULT NULL,
  `user_id` int NOT NULL,
  `task_id` int NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  `state_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  `time_difference` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_task_event_task_id` (`task_id`),
  KEY `fk_task_event_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_task_event_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_task_event_task_id` FOREIGN KEY (`task_id`) REFERENCES `tbl_task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- -------------------------------------------

-- TABLE `tbl_team`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_team`;
CREATE TABLE IF NOT EXISTS `tbl_team` (
  `id` int NOT NULL AUTO_INCREMENT,
  `case_id` int NOT NULL,
  `user_id` int NOT NULL,
  `type_id` int DEFAULT '0',
  `state_id` int DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  `dept_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  KEY `user_id` (`user_id`),
  KEY `state_id` (`state_id`),
  KEY `fk_team_case_id` (`case_id`),
  KEY `fk_team_user_id` (`user_id`),
  KEY `fk_team_created_by_id` (`created_by_id`),
  KEY `fk_team_dept_id` (`dept_id`),
  CONSTRAINT `fk_team_case_id` FOREIGN KEY (`case_id`) REFERENCES `tbl_case` (`id`),
  CONSTRAINT `fk_team_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_team_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `tbl_department` (`id`),
  CONSTRAINT `fk_team_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_team_department`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_team_department`;
CREATE TABLE IF NOT EXISTS `tbl_team_department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `department_id` int NOT NULL,
  `employee_id` int NOT NULL,
  `state_id` tinyint(1) NOT NULL DEFAULT '1',
  `type_id` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_team_department_employee_id` (`employee_id`),
  KEY `fk_team_department_department_id` (`department_id`),
  KEY `fk_team_department_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_team_department_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_team_department_department_id` FOREIGN KEY (`department_id`) REFERENCES `tbl_department` (`id`),
  CONSTRAINT `fk_team_department_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- -------------------------------------------

-- TABLE `tbl_ticket`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_ticket`;
CREATE TABLE IF NOT EXISTS `tbl_ticket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int DEFAULT '0',
  `state_id` int DEFAULT '1',
  `created_on` datetime DEFAULT NULL,
  `assign_to_id` int NOT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `state_id` (`state_id`),
  KEY `fk_ticket_created_by_id` (`created_by_id`),
  KEY `fk_ticket_assign_to_id` (`assign_to_id`),
  CONSTRAINT `fk_ticket_assign_to_id` FOREIGN KEY (`assign_to_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_ticket_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_ticket_type`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_ticket_type`;
CREATE TABLE IF NOT EXISTS `tbl_ticket_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int DEFAULT NULL,
  `state_id` int NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ticket_type_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_ticket_type_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_timer`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_timer`;
CREATE TABLE IF NOT EXISTS `tbl_timer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `work_time` int DEFAULT NULL,
  `task_id` int NOT NULL,
  `type_id` int DEFAULT '0',
  `state_id` int DEFAULT '1',
  `created_on` datetime DEFAULT NULL,
  `started_on` datetime NOT NULL,
  `stopped_on` datetime DEFAULT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `task_id` (`task_id`),
  KEY `state_id` (`state_id`),
  KEY `fk_timer_task_id` (`task_id`),
  KEY `fk_timer_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_timer_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `fk_timer_task_id` FOREIGN KEY (`task_id`) REFERENCES `tbl_task` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_translator_language`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_translator_language`;
CREATE TABLE IF NOT EXISTS `tbl_translator_language` (
  `id` int NOT NULL AUTO_INCREMENT,
  `text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `attribute_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int NOT NULL DEFAULT '1',
  `type_id` int NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_translator_language_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_translator_language_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_user`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` int DEFAULT '0',
  `about_me` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `otp` int DEFAULT NULL,
  `department_id` int DEFAULT NULL,
  `designation` int NOT NULL DEFAULT '0',
  `employee_id` int DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `company_id` int DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT '0',
  `otp_verified` int DEFAULT '0',
  `profile_file` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tos` int DEFAULT NULL,
  `role_id` int NOT NULL,
  `state_id` int NOT NULL,
  `type_id` int DEFAULT '0',
  `last_visit_time` datetime DEFAULT NULL,
  `last_action_time` datetime DEFAULT NULL,
  `last_password_change` datetime DEFAULT NULL,
  `login_error_count` int DEFAULT NULL,
  `activation_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_department_id` (`department_id`),
  KEY `fk_user_designation_id` (`designation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_user_profile`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_user_profile`;
CREATE TABLE IF NOT EXISTS `tbl_user_profile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `private_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` int NOT NULL,
  `alternate_contact` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_tracking_required` int NOT NULL DEFAULT '1',
  `working_in_night_shift` tinyint(1) DEFAULT '0',
  `working_hours` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int NOT NULL DEFAULT '0',
  `state_id` int NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `created_by_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `private_email` (`private_email`),
  KEY `state_id` (`state_id`),
  KEY `created_on` (`created_on`),
  KEY `fk_user_profile_created_by_id` (`created_by_id`),
  CONSTRAINT `fk_user_profile_created_by_id` FOREIGN KEY (`created_by_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- -------------------------------------------

-- TABLE `tbl_visitor`

-- -------------------------------------------
DROP TABLE IF EXISTS `tbl_visitor`;
CREATE TABLE IF NOT EXISTS `tbl_visitor` (
  `id` bigint unsigned NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `group_date` int unsigned DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




INSERT INTO `tbl_api_device_detail` (`id`,`device_token`,`device_name`,`device_type`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("23","00000000055","iPhone 11 Pro Max","00000000055","0","2020-02-07 12:51:35","2020-02-13 11:50:22","29"),
("66","00000000055","iPhone 11 Pro Max","2","0","2020-02-10 10:46:28","2020-02-12 17:41:19","37"),
("75","f8870ef3e62baf05","","1","0","2020-02-13 10:07:46","2020-02-13 10:07:46","33"),
("89","00000000055","iPhone 11 Pro Max","2","0","2020-02-13 10:54:03","2020-02-13 16:22:24","45"),
("91","f8870ef3e62baf05","Android SDK built for x86","1","0","2020-02-13 10:56:32","2020-02-13 16:20:23","48");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_attendance` (`id`,`date`,`in_time`,`out_time`,`user_id`,`type_id`,`state_id`,`total_time`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1","2020-02-01","09:13:00","17:19:00","33","0","1","08:06:00","2020-02-01 17:13:01","2020-02-01 17:19:39","1"),
("5","2020-02-01","09:32:00","16:32:00","29","0","1","07:00:00","2020-02-04 16:32:40","0000-00-00 00:00:00","31"),
("6","2020-02-12","09:24:00","17:24:00","45","0","1","08:00:00","2020-02-12 17:28:26","","1"),
("7","2020-02-13","12:51:00","22:51:00","47","0","1","10:00:00","2020-02-13 12:51:50","","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_case` (`id`,`title`,`description`,`client_id`,`department_id`,`start_date`,`end_date`,`type_id`,`state_id`,`created_on`,`updated_on`,`assign_to_id`,`created_by_id`,`budget`,`case_platform`) VALUES
("1","Shopping Website","<p>Shopping Website</p>
","34","2","2020-02-08","2020-02-28","1","4","2020-02-07 10:39:07","2020-02-07 10:39:07","35","1","1222.00","4"),
("2","ecommerceTest","<p>ecommerceTestq</p>
","40","2","2020-02-10","2020-02-21","1","5","2020-02-10 11:10:35","2020-02-13 10:20:02","35","1","122.00","4"),
("4","ttfy","<p>yt</p>
","40","2","2020-02-13","2020-02-14","1","9","2020-02-13 11:25:23","2020-02-13 11:29:31","47","47","56.00","4");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_case_report` (`id`,`title`,`description`,`case_id`,`submitted_to`,`created_by_id`,`state_id`,`type_id`,`created_on`,`updated_on`) VALUES
("1","gfdgfd","<p>fgfd</p>
","1","34","37","1","0","2020-02-10 10:21:43","2020-02-10 10:21:43"),
("2","test","<p>test test</p>
","2","40","1","1","","2020-02-10 11:13:06","2020-02-10 11:13:06"),
("3","test","<p>test</p>
","2","29","1","1","","2020-02-11 11:38:59","2020-02-13 12:20:21");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_case_type` (`id`,`title`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1","Software Development","1","0","2020-02-06 13:31:13","2020-02-06 13:31:13","1"),
("2","App Development","1","0","2020-02-06 13:33:05","2020-02-06 13:33:27","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_client` (`id`,`name`,`email`,`phone`,`alternate_email`,`skype_id`,`address`,`country`,`type_id`,`state_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3","MR Johnson","johnsan@toxsl.com","+1025685656586","johnsan_001@gmail.com","john_001","Ontario","Canada","0","1","2020-01-31 12:34:10","2020-02-06 11:37:48","34"),
("4","TestUser","testuser@gmail.com","1234567890","","","#test","","0","1","2020-02-10 11:09:16","2020-02-10 11:09:16","40"),
("5","fcdgd","sdff@toxsl.com","222223333","","","","","0","1","2020-02-12 16:27:38","2020-02-12 16:27:38","46");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_client_bills` (`id`,`case_id`,`full_name`,`amount`,`sent_by`,`date`,`state_id`,`type_id`,`created_on`,`created_by_id`) VALUES
("1","1","retre","756","1","2020-02-11","1","0","2020-02-10 10:08:40","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_client_reports` (`id`,`case_id`,`report`,`sent_by`,`date`,`state_id`,`type_id`,`created_on`,`created_by_id`) VALUES
("1","1","<p>test</p>
","1","2020-02-11","1","0","2020-02-10 11:03:50","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_comment` (`id`,`model_id`,`model_type`,`comment`,`state_id`,`type_id`,`created_on`,`created_by_id`) VALUES
("1","24","app\\models\\LeaveRequest","Fdgdfgdfgdfg","1","0","2020-02-01 12:57:27","31"),
("2","24","app\\models\\LeaveRequest","Dfdgfdgfg","1","0","2020-02-01 12:58:10","31"),
("3","19","app\\models\\LeaveRequest","","1","0","2020-02-01 13:47:44","32"),
("4","19","app\\models\\LeaveRequest","AAAAA","1","0","2020-02-01 13:48:44","32"),
("5","19","app\\models\\LeaveRequest","pll","1","0","2020-02-01 15:20:32","32"),
("6","21","app\\models\\LeaveRequest","main nai krni","1","0","2020-02-01 15:21:53","32"),
("7","22","app\\models\\LeaveRequest","evil nai krni","1","0","2020-02-01 15:22:06","32"),
("8","22","app\\models\\LeaveRequest","no ","1","0","2020-02-01 15:22:46","32"),
("9","22","app\\models\\LeaveRequest","asde","1","0","2020-02-01 15:23:26","32"),
("10","21","app\\models\\LeaveRequest","rttyy","1","0","2020-02-01 15:31:54","32"),
("11","20","app\\models\\LeaveRequest","ddff","1","0","2020-02-01 15:33:42","32"),
("12","25","app\\models\\LeaveRequest","errr","1","0","2020-02-01 15:35:16","32"),
("13","23","app\\models\\LeaveRequest","edf","1","0","2020-02-01 15:36:10","32"),
("14","26","app\\models\\LeaveRequest","dfg","1","0","2020-02-01 15:39:50","32"),
("15","27","app\\models\\LeaveRequest","xxc","1","0","2020-02-01 15:41:04","32"),
("16","27","app\\models\\LeaveRequest","sdff","1","0","2020-02-01 15:43:38","32"),
("17","27","app\\models\\LeaveRequest","ffgg","1","0","2020-02-01 15:50:59","32"),
("18","28","app\\models\\LeaveRequest","dddff","1","0","2020-02-01 15:51:44","32"),
("19","28","app\\models\\LeaveRequest","ff","1","0","2020-02-01 15:52:46","32"),
("20","28","app\\models\\LeaveRequest","sefhjk","1","0","2020-02-01 17:26:57","32"),
("21","30","app\\models\\User","State Changed : Inactive to Active","1","0","2020-02-03 09:45:56","1"),
("22","19","app\\models\\LeaveRequest","State Changed : Pending to Approved","1","0","2020-02-03 10:23:34","1"),
("23","25","app\\models\\LeaveRequest","hopl","1","0","2020-02-03 10:26:15","32"),
("24","19","app\\models\\LeaveRequest","dasdas","0","0","2020-02-03 10:26:53","29"),
("25","19","app\\models\\LeaveRequest","Leave Approved after the discussion with manager","0","0","2020-02-03 10:28:55","1"),
("26","27","app\\models\\Ticket","State Changed : Assigned to InProgress","1","0","2020-02-03 10:34:17","1"),
("27","27","app\\models\\Ticket","State Changed : InProgress to Rejected","1","0","2020-02-03 10:34:24","1"),
("28","27","app\\models\\Ticket","State Changed : Rejected to InProgress","1","0","2020-02-03 10:34:29","1"),
("29","27","app\\models\\Ticket","State Changed : InProgress to Complete","1","0","2020-02-03 10:34:32","1"),
("30","27","app\\models\\Ticket","State Changed : Complete to Rejected","1","0","2020-02-03 10:46:49","1"),
("31","27","app\\models\\Ticket","State Changed : Rejected to Complete","1","0","2020-02-03 10:46:50","1"),
("32","30","app\\models\\LeaveRequest","","0","0","2020-02-03 14:46:10","31"),
("33","30","app\\models\\LeaveRequest","hfgh<br/><br/>File uploaded <a href=\"http://localhost/argus-app-yii2-1275/files/view/19\"></a>","0","0","2020-02-03 14:52:59","31"),
("34","30","app\\models\\LeaveRequest","<br/><br/>File uploaded <a href=\"http://localhost/argus-app-yii2-1275/files/view/20\"></a>","0","0","2020-02-03 14:53:05","31"),
("35","30","app\\models\\LeaveRequest","asdsad<br/><br/>File uploaded <a href=\"http://localhost/argus-app-yii2-1275/files/view/21\"></a>","0","0","2020-02-03 14:53:15","31"),
("38","32","app\\models\\LeaveRequest","Ghghg","1","0","2020-02-04 11:59:35","29"),
("39","19","app\\models\\LeaveRequest","Ghghg","1","0","2020-02-04 11:59:49","29"),
("40","28","app\\models\\Ticket","Sdsds","1","0","2020-02-04 13:17:48","29"),
("43","28","app\\models\\Ticket","Sdfss","1","0","2020-02-04 13:31:18","29"),
("47","28","app\\models\\Ticket","Sdsfdjbfdsfsfsdf","1","0","2020-02-04 17:30:00","29"),
("48","27","app\\models\\Ticket","Dfgdfgdfg","1","0","2020-02-04 17:30:12","29"),
("49","12","app\\models\\Ticket","Xcvgfxgdfg","1","0","2020-02-04 17:30:30","29"),
("50","11","app\\models\\LeaveRequest","Dsffdfd","1","0","2020-02-05 15:44:11","29"),
("51","12","app\\models\\LeaveRequest","Ghfhfg","1","0","2020-02-06 10:20:10","29"),
("52","12","app\\models\\LeaveRequest","Dfdfd","1","0","2020-02-06 10:21:31","29"),
("58","35","app\\models\\User","State Changed : Inactive to Active","1","0","0000-00-00 00:00:00","0"),
("59","4","app\\models\\Cases","State Changed : New to Planning","1","0","0000-00-00 00:00:00","0"),
("60","4","app\\models\\Task","Hghgh","1","0","2020-02-06 13:33:19","29"),
("61","4","app\\models\\Cases","Dfr","1","0","2020-02-06 13:40:27","29"),
("62","4","app\\models\\Cases","Sddsd","1","0","2020-02-06 14:32:35","29"),
("63","4","app\\models\\Cases","Bcvgd","1","0","2020-02-06 14:33:55","29"),
("64","13","app\\models\\Task","Vcbfg","1","0","2020-02-06 14:40:47","29"),
("65","13","app\\models\\Task","Gfhgfh","1","0","2020-02-06 14:40:50","29"),
("66","4","app\\models\\Cases","Rtyrty","1","0","2020-02-06 14:41:00","29"),
("67","4","app\\models\\Cases","Cvcv","1","0","2020-02-06 14:44:53","29"),
("68","4","app\\models\\Cases","Cvcv","1","0","2020-02-06 14:45:04","29"),
("69","4","app\\models\\Cases","Xcxc","1","0","2020-02-06 14:49:40","29"),
("70","2","app\\models\\Ticket","Fff
","1","0","2020-02-07 12:53:33","29"),
("71","1","app\\models\\Cases","State Changed : New to Planning","1","0","0000-00-00 00:00:00","0"),
("73","2","app\\models\\Cases","jiiii","1","0","2020-02-07 16:44:26","37"),
("75","32","app\\models\\LeaveRequest","hii","1","0","2020-02-07 16:46:59","37"),
("77","1","app\\models\\Cases","hii","1","0","2020-02-07 16:53:02","37"),
("78","31","app\\models\\LeaveRequest","ji","1","0","2020-02-07 16:57:12","37"),
("80","2","app\\models\\Cases","hh","1","0","2020-02-07 16:59:31","37"),
("83","1","app\\models\\Cases","uu","1","0","2020-02-07 17:01:20","37"),
("87","3","app\\models\\Ticket","jii","1","0","2020-02-07 17:06:57","37"),
("88","42","app\\models\\User","State Changed : Inactive to Active","1","0","2020-02-11 16:05:34","1"),
("89","41","app\\models\\User","State Changed : Inactive to Active","1","0","2020-02-11 16:07:10","1"),
("90","44","app\\models\\User","State Changed : Inactive to Active","1","0","2020-02-12 09:44:54","1"),
("91","5","app\\models\\Ticket","State Changed : Assigned to InProgress","1","0","2020-02-12 12:30:33","1"),
("92","5","app\\models\\Ticket","State Changed : InProgress to Assigned","1","0","2020-02-12 12:30:37","1"),
("93","2","app\\models\\Cases","State Changed : New to In-Progress","1","0","2020-02-12 12:36:06","1"),
("94","45","app\\models\\User","State Changed : Inactive to Active","1","0","2020-02-12 14:55:57","1"),
("95","47","app\\models\\User","State Changed : Inactive to Active","1","0","2020-02-13 09:49:36","1"),
("96","6","app\\models\\Ticket","hi","1","0","2020-02-13 09:55:01","45"),
("97","5","app\\models\\Ticket","yyyy","1","0","2020-02-13 09:55:11","45"),
("99","37","app\\models\\LeaveRequest","Jkljljkl","1","0","2020-02-13 10:44:14","45"),
("100","48","app\\models\\User","State Changed : Inactive to Active","1","0","2020-02-13 10:54:50","1"),
("103","4","app\\models\\Cases","State Changed : New to Maintenance","1","0","2020-02-13 11:26:25","47"),
("104","4","app\\models\\Cases","State Changed : Maintenance to OnHold","1","0","2020-02-13 11:26:41","47"),
("105","4","app\\models\\Cases","State Changed : OnHold to Completed","1","0","2020-02-13 11:27:14","47"),
("106","4","app\\models\\Cases","State Changed : Completed to Assign","1","0","2020-02-13 11:27:31","47"),
("107","7","app\\models\\Ticket","ZDXSdx","1","0","2020-02-13 11:40:51","45");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_department` (`id`,`title`,`department_code`,`type_id`,`state_id`,`manager_id`,`created_on`,`created_by_id`) VALUES
("1","Hr","HR01","1","1","0","2020-02-07 10:32:45","1"),
("2","Web Yii","Yii101","1","1","0","2020-02-07 10:38:26","1"),
("3","Distribution","Dist123","1","1","0","2020-02-10 10:20:16","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_designation` (`id`,`title`,`type_id`,`state_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1","Software Engineer","1","1","2020-02-01 10:54:47","0000-00-00 00:00:00","1"),
("2","Hr Admin","1","1","2020-02-01 11:01:08","0000-00-00 00:00:00","1"),
("3","Distribution","1","1","2020-02-10 10:51:06","0000-00-00 00:00:00","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1","Added new User : <a href=\"https://jupiter.toxsl.in/argus/user/view/1/admin\">Admin</a>","app\\models\\User","1","1","0","2019-12-18 16:12:23","2019-12-18 16:12:23","0"),
("2","Added new Setting : <a href=\"https://jupiter.toxsl.in/argus/setting/view/1/app-configration\">App Configration</a>","app\\models\\Setting","1","1","0","2019-12-18 16:12:23","2019-12-18 16:12:23","1"),
("3","Added new Setting : <a href=\"https://jupiter.toxsl.in/argus/setting/view/2/smtp-configration\">SMTP Configration</a>","app\\models\\Setting","2","1","0","2019-12-18 16:12:23","2019-12-18 16:12:23","1"),
("4","Added new Setting : <a href=\"https://jupiter.toxsl.in/argus/setting/view/3/firebase-configration\">Firebase Configration</a>","app\\models\\Setting","3","1","0","2019-12-18 16:12:23","2019-12-18 16:12:23","1"),
("16","Added new Device Detail : <a href=\"https://jupiter.toxsl.in/argus/device-detail/view/1/12131313\">12131313</a>","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2019-12-19 17:56:38","2019-12-19 17:56:38","2"),
("49","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2019-12-23 17:27:02","2019-12-23 17:27:02","11"),
("50","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2019-12-23 17:27:51","2019-12-23 17:27:51","11"),
("54","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","3","1","0","2019-12-23 17:28:49","2019-12-23 17:28:49","11"),
("55","Created new Ticket ","app\\models\\Ticket","1","1","0","2019-12-23 17:30:08","2019-12-23 17:30:08","11"),
("61","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","3","1","0","2019-12-23 17:59:05","2019-12-23 17:59:05","11"),
("62","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","3","1","0","2019-12-23 18:00:32","2019-12-23 18:00:32","11"),
("63","Created new Ticket ","app\\models\\Ticket","3","1","0","2019-12-23 18:00:42","2019-12-23 18:00:42","11"),
("68","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","4","1","0","2019-12-23 18:12:21","2019-12-23 18:12:21","11"),
("72","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","5","1","0","2019-12-23 18:14:03","2019-12-23 18:14:03","11"),
("79","Created new Platform ","app\\models\\Platform","1","1","0","2019-12-27 15:32:57","2019-12-27 15:32:57","1"),
("80","Created new Platform ","app\\models\\Platform","2","1","0","2019-12-27 15:34:13","2019-12-27 15:34:13","1"),
("82","Modified Platform ","app\\models\\Platform","2","1","0","2019-12-27 15:46:39","2019-12-27 15:46:39","1"),
("130","Created new Attendance Event ","app\\models\\AttendanceEvent","5","1","0","2020-01-09 12:20:31","2020-01-09 12:20:31","1"),
("131","Created new Attendance Event ","app\\models\\AttendanceEvent","6","1","0","2020-01-09 12:20:31","2020-01-09 12:20:31","1"),
("158","Created new Setting ","app\\models\\Setting","1","1","0","2020-01-09 19:25:02","2020-01-09 19:25:02","1"),
("159","Created new Setting ","app\\models\\Setting","2","1","0","2020-01-09 19:25:02","2020-01-09 19:25:02","1"),
("160","Created new Setting ","app\\models\\Setting","3","1","0","2020-01-09 19:25:03","2020-01-09 19:25:03","1"),
("182","Created new Task ","app\\models\\Task","1","1","0","2020-01-13 12:10:04","2020-01-13 12:10:04","13"),
("183","Modified Task ","app\\models\\Task","1","1","0","2020-01-13 12:29:35","2020-01-13 12:29:35","13"),
("184","Modified Task ","app\\models\\Task","1","1","0","2020-01-13 12:30:01","2020-01-13 12:30:01","13"),
("185","Modified Task ","app\\models\\Task","1","1","0","2020-01-13 12:30:57","2020-01-13 12:30:57","13"),
("186","Modified Task ","app\\models\\Task","1","1","0","2020-01-13 12:35:36","2020-01-13 12:35:36","13"),
("212","Created new Payroll ","app\\models\\Payroll","3","1","0","2020-01-15 17:50:05","2020-01-15 17:50:05","11"),
("213","Created new Payroll ","app\\models\\Payroll","4","1","0","2020-01-15 17:50:52","2020-01-15 17:50:52","11"),
("217","Created new Task ","app\\models\\Task","2","1","0","2020-01-17 13:09:46","2020-01-17 13:09:46","13"),
("218","Created new Task ","app\\models\\Task","3","1","0","2020-01-17 13:12:45","2020-01-17 13:12:45","13"),
("219","Created new Task ","app\\models\\Task","4","1","0","2020-01-17 13:14:19","2020-01-17 13:14:19","13"),
("381","Created new Task Event ","app\\models\\TaskEvent","3","1","0","2020-01-21 15:52:57","2020-01-21 15:52:57","13"),
("382","Modified Task Event ","app\\models\\TaskEvent","3","1","0","2020-01-21 15:58:12","2020-01-21 15:58:12","13"),
("396","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-01-22 11:15:23","2020-01-22 11:15:23","23"),
("415","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-01-22 15:03:54","2020-01-22 15:03:54","4"),
("418","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","8","1","0","2020-01-22 15:05:21","2020-01-22 15:05:21","4"),
("425","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-01-22 15:08:00","2020-01-22 15:08:00","4"),
("428","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","10","1","0","2020-01-22 15:08:47","2020-01-22 15:08:47","4"),
("471","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-01-22 15:28:22","2020-01-22 15:28:22","4"),
("472","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","12","1","0","2020-01-22 15:31:24","2020-01-22 15:31:24","23"),
("473","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","13","1","0","2020-01-22 15:32:26","2020-01-22 15:32:26","23"),
("474","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","14","1","0","2020-01-22 15:34:23","2020-01-22 15:34:23","23"),
("478","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","15","1","0","2020-01-22 15:39:57","2020-01-22 15:39:57","24"),
("481","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-01-22 15:47:24","2020-01-22 15:47:24","4"),
("482","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-01-22 15:47:27","2020-01-22 15:47:27","24"),
("486","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","20","1","0","2020-01-22 16:10:26","2020-01-22 16:10:26","21"),
("489","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-01-22 16:24:37","2020-01-22 16:24:37","4"),
("492","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-01-22 16:26:46","2020-01-22 16:26:46","4"),
("495","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","23","1","0","2020-01-22 16:28:05","2020-01-22 16:28:05","4"),
("498","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-01-22 16:28:56","2020-01-22 16:28:56","4"),
("499","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","25","1","0","2020-01-22 16:40:56","2020-01-22 16:40:56","20"),
("502","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","26","1","0","2020-01-22 16:44:07","2020-01-22 16:44:07","4"),
("503","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","27","1","0","2020-01-22 16:44:54","2020-01-22 16:44:54","20"),
("506","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","28","1","0","2020-01-22 16:46:25","2020-01-22 16:46:25","4"),
("507","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","29","1","0","2020-01-22 16:46:29","2020-01-22 16:46:29","20"),
("516","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","30","1","0","2020-01-22 17:03:50","2020-01-22 17:03:50","4"),
("517","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","31","1","0","2020-01-22 17:03:54","2020-01-22 17:03:54","20"),
("520","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","32","1","0","2020-01-22 17:06:33","2020-01-22 17:06:33","4"),
("521","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","33","1","0","2020-01-22 17:06:37","2020-01-22 17:06:37","20"),
("524","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","34","1","0","2020-01-22 17:10:25","2020-01-22 17:10:25","4"),
("525","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","35","1","0","2020-01-22 17:10:31","2020-01-22 17:10:31","20"),
("528","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","36","1","0","2020-01-22 17:11:02","2020-01-22 17:11:02","4"),
("529","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","37","1","0","2020-01-22 17:11:09","2020-01-22 17:11:09","20"),
("532","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","38","1","0","2020-01-22 17:16:02","2020-01-22 17:16:02","4"),
("533","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","39","1","0","2020-01-22 17:16:05","2020-01-22 17:16:05","20"),
("534","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","39","1","0","2020-01-22 17:16:57","2020-01-22 17:16:57","20"),
("537","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","40","1","0","2020-01-22 17:20:53","2020-01-22 17:20:53","4"),
("538","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","41","1","0","2020-01-22 17:24:20","2020-01-22 17:24:20","20"),
("541","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","42","1","0","2020-01-22 17:25:01","2020-01-22 17:25:01","4"),
("542","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","43","1","0","2020-01-22 17:25:06","2020-01-22 17:25:06","20"),
("545","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","44","1","0","2020-01-22 17:25:40","2020-01-22 17:25:40","4"),
("546","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","45","1","0","2020-01-22 17:25:49","2020-01-22 17:25:49","20"),
("549","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","46","1","0","2020-01-22 17:28:09","2020-01-22 17:28:09","4"),
("550","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","47","1","0","2020-01-22 17:28:23","2020-01-22 17:28:23","20"),
("553","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","48","1","0","2020-01-22 17:31:16","2020-01-22 17:31:16","4"),
("554","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","49","1","0","2020-01-22 17:31:22","2020-01-22 17:31:22","20"),
("557","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","50","1","0","2020-01-22 17:32:58","2020-01-22 17:32:58","4"),
("565","Created new Attendance Event ","app\\models\\AttendanceEvent","7","1","0","2020-01-22 17:50:28","2020-01-22 17:50:28","1"),
("566","Created new Attendance Event ","app\\models\\AttendanceEvent","8","1","0","2020-01-22 17:50:28","2020-01-22 17:50:28","1"),
("569","Created new Attendance Event ","app\\models\\AttendanceEvent","9","1","0","2020-01-22 18:56:20","2020-01-22 18:56:20","13"),
("570","Created new Attendance Event ","app\\models\\AttendanceEvent","10","1","0","2020-01-22 18:56:20","2020-01-22 18:56:20","13"),
("572","Created new Attendance Event ","app\\models\\AttendanceEvent","11","1","0","2020-01-22 19:08:46","2020-01-22 19:08:46","13"),
("573","Created new Attendance Event ","app\\models\\AttendanceEvent","12","1","0","2020-01-22 19:08:46","2020-01-22 19:08:46","13"),
("586","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","52","1","0","2020-01-23 10:02:17","2020-01-23 10:02:17","4"),
("587","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","53","1","0","2020-01-23 10:14:22","2020-01-23 10:14:22","7"),
("589","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 10:15:55","2020-01-23 10:15:55","23"),
("592","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","55","1","0","2020-01-23 10:17:11","2020-01-23 10:17:11","7"),
("593","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","56","1","0","2020-01-23 10:18:09","2020-01-23 10:18:09","7"),
("598","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:24:09","2020-01-23 11:24:09","23"),
("599","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:25:15","2020-01-23 11:25:15","23"),
("600","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:28:57","2020-01-23 11:28:57","23"),
("601","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:29:14","2020-01-23 11:29:14","23"),
("602","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:30:48","2020-01-23 11:30:48","23"),
("607","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:34:51","2020-01-23 11:34:51","23"),
("608","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:36:50","2020-01-23 11:36:50","23"),
("609","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:43:04","2020-01-23 11:43:04","23"),
("610","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:48:11","2020-01-23 11:48:11","23"),
("611","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:53:33","2020-01-23 11:53:33","23"),
("612","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:53:57","2020-01-23 11:53:57","23");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("613","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:54:22","2020-01-23 11:54:22","23"),
("614","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:55:57","2020-01-23 11:55:57","23"),
("615","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:56:47","2020-01-23 11:56:47","23"),
("619","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 11:58:19","2020-01-23 11:58:19","23"),
("621","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:00:45","2020-01-23 12:00:45","23"),
("622","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:03:28","2020-01-23 12:03:28","23"),
("623","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:04:12","2020-01-23 12:04:12","23"),
("624","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:07:23","2020-01-23 12:07:23","23"),
("625","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:09:15","2020-01-23 12:09:15","23"),
("626","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:10:23","2020-01-23 12:10:23","23"),
("627","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:11:07","2020-01-23 12:11:07","23"),
("628","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:12:02","2020-01-23 12:12:02","23"),
("629","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:13:29","2020-01-23 12:13:29","23"),
("630","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:15:34","2020-01-23 12:15:34","23"),
("631","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:16:37","2020-01-23 12:16:37","23"),
("632","Created new Attendance Event ","app\\models\\AttendanceEvent","13","1","0","2020-01-23 12:17:00","2020-01-23 12:17:00","13"),
("633","Created new Attendance Event ","app\\models\\AttendanceEvent","14","1","0","2020-01-23 12:17:00","2020-01-23 12:17:00","13"),
("635","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:18:35","2020-01-23 12:18:35","23"),
("636","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:19:42","2020-01-23 12:19:42","23"),
("637","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:27:46","2020-01-23 12:27:46","23"),
("640","Created new Attendance Event ","app\\models\\AttendanceEvent","17","1","0","2020-01-23 12:39:01","2020-01-23 12:39:01","13"),
("641","Created new Attendance Event ","app\\models\\AttendanceEvent","18","1","0","2020-01-23 12:39:01","2020-01-23 12:39:01","13"),
("643","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:41:01","2020-01-23 12:41:01","23"),
("644","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:42:07","2020-01-23 12:42:07","23"),
("645","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:44:59","2020-01-23 12:44:59","23"),
("646","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:50:55","2020-01-23 12:50:55","23"),
("647","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:57:25","2020-01-23 12:57:25","23"),
("648","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 12:59:21","2020-01-23 12:59:21","23"),
("649","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:00:00","2020-01-23 13:00:00","23"),
("650","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:01:05","2020-01-23 13:01:05","23"),
("651","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:07:54","2020-01-23 13:07:54","23"),
("652","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:08:04","2020-01-23 13:08:04","23"),
("653","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:08:20","2020-01-23 13:08:20","23"),
("654","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:09:03","2020-01-23 13:09:03","23"),
("655","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:09:56","2020-01-23 13:09:56","23"),
("656","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:10:35","2020-01-23 13:10:35","23"),
("657","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:19:12","2020-01-23 13:19:12","23"),
("658","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:22:31","2020-01-23 13:22:31","23"),
("659","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:26:14","2020-01-23 13:26:14","23"),
("660","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:27:10","2020-01-23 13:27:10","23"),
("662","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:29:49","2020-01-23 13:29:49","23"),
("664","Created new Attendance Event ","app\\models\\AttendanceEvent","21","1","0","2020-01-23 13:32:47","2020-01-23 13:32:47","13"),
("665","Created new Attendance ","app\\models\\Attendance","7","1","0","2020-01-23 13:32:47","2020-01-23 13:32:47","13"),
("666","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:37:57","2020-01-23 13:37:57","23"),
("667","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:41:12","2020-01-23 13:41:12","23"),
("668","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 13:41:23","2020-01-23 13:41:23","23"),
("671","Created new Attendance Event ","app\\models\\AttendanceEvent","22","1","0","2020-01-23 14:24:54","2020-01-23 14:24:54","1"),
("672","Created new Attendance Event ","app\\models\\AttendanceEvent","23","1","0","2020-01-23 14:24:54","2020-01-23 14:24:54","1"),
("673","Created new Attendance ","app\\models\\Attendance","8","1","0","2020-01-23 14:24:55","2020-01-23 14:24:55","1"),
("674","Created new Attendance Event ","app\\models\\AttendanceEvent","24","1","0","2020-01-23 14:38:56","2020-01-23 14:38:56","1"),
("675","Created new Attendance Event ","app\\models\\AttendanceEvent","25","1","0","2020-01-23 14:38:56","2020-01-23 14:38:56","1"),
("676","Created new Attendance ","app\\models\\Attendance","9","1","0","2020-01-23 14:38:56","2020-01-23 14:38:56","1"),
("677","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-01-23 14:59:57","2020-01-23 14:59:57","23"),
("679","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","57","1","0","2020-01-23 15:02:41","2020-01-23 15:02:41","23"),
("682","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","58","1","0","2020-01-23 15:05:15","2020-01-23 15:05:15","4"),
("683","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","59","1","0","2020-01-23 15:09:34","2020-01-23 15:09:34","2"),
("686","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","60","1","0","2020-01-23 15:10:00","2020-01-23 15:10:00","4"),
("688","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","61","1","0","2020-01-23 15:10:26","2020-01-23 15:10:26","2"),
("689","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","62","1","0","2020-01-23 15:11:37","2020-01-23 15:11:37","23"),
("691","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","62","1","0","2020-01-23 15:19:49","2020-01-23 15:19:49","23"),
("695","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-01-23 15:20:17","2020-01-23 15:20:17","4"),
("697","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","64","1","0","2020-01-23 15:22:39","2020-01-23 15:22:39","8"),
("700","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","65","1","0","2020-01-23 15:23:14","2020-01-23 15:23:14","4"),
("702","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","66","1","0","2020-01-23 15:25:36","2020-01-23 15:25:36","8"),
("705","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","67","1","0","2020-01-23 15:27:55","2020-01-23 15:27:55","4"),
("709","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","68","1","0","2020-01-23 15:28:08","2020-01-23 15:28:08","4"),
("711","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","69","1","0","2020-01-23 15:29:59","2020-01-23 15:29:59","16"),
("714","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","70","1","0","2020-01-23 15:30:16","2020-01-23 15:30:16","4"),
("718","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","71","1","0","2020-01-23 15:30:33","2020-01-23 15:30:33","4"),
("721","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","72","1","0","2020-01-23 15:34:29","2020-01-23 15:34:29","4"),
("724","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","73","1","0","2020-01-23 15:50:05","2020-01-23 15:50:05","16"),
("725","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","74","1","0","2020-01-23 15:50:41","2020-01-23 15:50:41","23"),
("726","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","73","1","0","2020-01-23 15:51:10","2020-01-23 15:51:10","16"),
("727","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","73","1","0","2020-01-23 15:52:05","2020-01-23 15:52:05","16"),
("730","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","75","1","0","2020-01-23 15:54:21","2020-01-23 15:54:21","4"),
("734","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","76","1","0","2020-01-23 15:54:58","2020-01-23 15:54:58","4"),
("737","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","77","1","0","2020-01-23 15:55:28","2020-01-23 15:55:28","4"),
("743","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","79","1","0","2020-01-23 15:55:51","2020-01-23 15:55:51","4"),
("746","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","80","1","0","2020-01-23 15:57:39","2020-01-23 15:57:39","4"),
("749","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","81","1","0","2020-01-23 15:57:59","2020-01-23 15:57:59","4"),
("756","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","82","1","0","2020-01-23 16:00:17","2020-01-23 16:00:17","4"),
("760","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","83","1","0","2020-01-23 16:06:01","2020-01-23 16:06:01","4"),
("763","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","84","1","0","2020-01-23 16:17:06","2020-01-23 16:17:06","4"),
("767","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","85","1","0","2020-01-23 16:17:13","2020-01-23 16:17:13","4"),
("768","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","86","1","0","2020-01-23 16:21:33","2020-01-23 16:21:33","8"),
("771","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","87","1","0","2020-01-23 16:23:31","2020-01-23 16:23:31","4"),
("774","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","88","1","0","2020-01-23 16:26:17","2020-01-23 16:26:17","4"),
("777","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-01-23 16:27:03","2020-01-23 16:27:03","4"),
("781","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","90","1","0","2020-01-23 16:27:23","2020-01-23 16:27:23","4"),
("782","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-01-23 16:30:43","2020-01-23 16:30:43","8"),
("783","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-01-23 16:31:16","2020-01-23 16:31:16","8"),
("784","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","92","1","0","2020-01-23 16:32:56","2020-01-23 16:32:56","8"),
("785","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","92","1","0","2020-01-23 16:33:14","2020-01-23 16:33:14","8"),
("786","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","93","1","0","2020-01-23 16:36:16","2020-01-23 16:36:16","23"),
("789","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","94","1","0","2020-01-23 16:39:00","2020-01-23 16:39:00","4"),
("791","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","95","1","0","2020-01-23 16:41:40","2020-01-23 16:41:40","23"),
("794","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","96","1","0","2020-01-23 16:44:43","2020-01-23 16:44:43","4"),
("795","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","95","1","0","2020-01-23 16:44:53","2020-01-23 16:44:53","23"),
("799","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","97","1","0","2020-01-23 16:47:19","2020-01-23 16:47:19","4"),
("800","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","95","1","0","2020-01-23 16:48:00","2020-01-23 16:48:00","23");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("801","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","95","1","0","2020-01-23 16:50:29","2020-01-23 16:50:29","23"),
("803","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","95","1","0","2020-01-23 17:02:18","2020-01-23 17:02:18","23"),
("807","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","98","1","0","2020-01-23 17:02:41","2020-01-23 17:02:41","4"),
("809","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","99","1","0","2020-01-23 17:10:56","2020-01-23 17:10:56","8"),
("810","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","99","1","0","2020-01-23 17:12:27","2020-01-23 17:12:27","8"),
("813","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","100","1","0","2020-01-23 17:13:29","2020-01-23 17:13:29","4"),
("815","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","101","1","0","2020-01-23 17:14:53","2020-01-23 17:14:53","23"),
("817","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","102","1","0","2020-01-23 17:20:04","2020-01-23 17:20:04","25"),
("820","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","103","1","0","2020-01-23 17:22:51","2020-01-23 17:22:51","4"),
("821","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","104","1","0","2020-01-23 17:24:02","2020-01-23 17:24:02","25"),
("823","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","105","1","0","2020-01-23 17:31:02","2020-01-23 17:31:02","4"),
("825","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","106","1","0","2020-01-23 17:32:14","2020-01-23 17:32:14","4"),
("828","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","107","1","0","2020-01-23 17:33:36","2020-01-23 17:33:36","4"),
("831","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","108","1","0","2020-01-23 17:36:10","2020-01-23 17:36:10","4"),
("833","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","109","1","0","2020-01-23 17:37:00","2020-01-23 17:37:00","4"),
("835","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","110","1","0","2020-01-23 17:37:32","2020-01-23 17:37:32","4"),
("840","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","111","1","0","2020-01-23 17:39:47","2020-01-23 17:39:47","25"),
("843","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","112","1","0","2020-01-23 17:42:39","2020-01-23 17:42:39","25"),
("846","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","113","1","0","2020-01-23 17:43:17","2020-01-23 17:43:17","25"),
("849","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","114","1","0","2020-01-23 17:43:32","2020-01-23 17:43:32","8"),
("852","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","115","1","0","2020-01-23 17:48:05","2020-01-23 17:48:05","8"),
("855","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","116","1","0","2020-01-23 17:48:27","2020-01-23 17:48:27","8"),
("858","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","117","1","0","2020-01-23 17:49:41","2020-01-23 17:49:41","8"),
("861","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","118","1","0","2020-01-23 17:49:59","2020-01-23 17:49:59","25"),
("862","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","118","1","0","2020-01-23 17:50:48","2020-01-23 17:50:48","25"),
("866","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","119","1","0","2020-01-23 17:51:10","2020-01-23 17:51:10","25"),
("870","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","120","1","0","2020-01-23 17:53:00","2020-01-23 17:53:00","25"),
("874","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","121","1","0","2020-01-23 17:53:13","2020-01-23 17:53:13","25"),
("876","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","101","1","0","2020-01-23 17:54:09","2020-01-23 17:54:09","23"),
("880","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","122","1","0","2020-01-23 17:55:47","2020-01-23 17:55:47","8"),
("883","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","123","1","0","2020-01-23 17:58:50","2020-01-23 17:58:50","8"),
("886","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","124","1","0","2020-01-23 17:59:19","2020-01-23 17:59:19","25"),
("890","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","125","1","0","2020-01-23 18:00:30","2020-01-23 18:00:30","23"),
("893","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","126","1","0","2020-01-23 18:00:59","2020-01-23 18:00:59","25"),
("897","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","127","1","0","2020-01-23 18:02:31","2020-01-23 18:02:31","23"),
("900","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","128","1","0","2020-01-23 18:02:40","2020-01-23 18:02:40","23"),
("903","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","129","1","0","2020-01-23 18:02:44","2020-01-23 18:02:44","23"),
("906","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","130","1","0","2020-01-23 18:03:50","2020-01-23 18:03:50","25"),
("910","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","131","1","0","2020-01-23 18:04:34","2020-01-23 18:04:34","25"),
("913","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","132","1","0","2020-01-23 18:05:09","2020-01-23 18:05:09","23"),
("916","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","133","1","0","2020-01-23 18:05:12","2020-01-23 18:05:12","8"),
("919","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","134","1","0","2020-01-23 18:05:34","2020-01-23 18:05:34","8"),
("922","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","135","1","0","2020-01-23 18:06:42","2020-01-23 18:06:42","8"),
("925","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","136","1","0","2020-01-23 18:08:55","2020-01-23 18:08:55","8"),
("928","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","137","1","0","2020-01-23 18:09:09","2020-01-23 18:09:09","8"),
("931","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","138","1","0","2020-01-23 18:11:11","2020-01-23 18:11:11","8"),
("934","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","139","1","0","2020-01-23 18:12:31","2020-01-23 18:12:31","4"),
("937","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","140","1","0","2020-01-23 18:13:57","2020-01-23 18:13:57","4"),
("941","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","141","1","0","2020-01-23 18:14:02","2020-01-23 18:14:02","4"),
("945","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","142","1","0","2020-01-23 18:14:08","2020-01-23 18:14:08","4"),
("948","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","143","1","0","2020-01-23 18:14:17","2020-01-23 18:14:17","4"),
("951","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","144","1","0","2020-01-23 18:14:51","2020-01-23 18:14:51","4"),
("954","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","145","1","0","2020-01-23 18:15:49","2020-01-23 18:15:49","4"),
("955","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","132","1","0","2020-01-23 18:17:10","2020-01-23 18:17:10","23"),
("957","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","132","1","0","2020-01-23 18:18:37","2020-01-23 18:18:37","23"),
("958","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","132","1","0","2020-01-23 18:19:35","2020-01-23 18:19:35","23"),
("963","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","146","1","0","2020-01-23 18:22:04","2020-01-23 18:22:04","4"),
("967","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","147","1","0","2020-01-23 18:23:18","2020-01-23 18:23:18","4"),
("970","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","148","1","0","2020-01-23 18:23:26","2020-01-23 18:23:26","4"),
("973","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","149","1","0","2020-01-23 18:23:56","2020-01-23 18:23:56","4"),
("974","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","132","1","0","2020-01-23 18:25:20","2020-01-23 18:25:20","23"),
("977","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","150","1","0","2020-01-23 18:28:13","2020-01-23 18:28:13","4"),
("980","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","151","1","0","2020-01-23 18:28:31","2020-01-23 18:28:31","4"),
("983","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","152","1","0","2020-01-23 18:28:43","2020-01-23 18:28:43","4"),
("986","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","153","1","0","2020-01-23 18:29:16","2020-01-23 18:29:16","4"),
("989","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","154","1","0","2020-01-23 18:29:27","2020-01-23 18:29:27","4"),
("992","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","155","1","0","2020-01-23 18:30:16","2020-01-23 18:30:16","4"),
("995","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","156","1","0","2020-01-23 18:30:31","2020-01-23 18:30:31","4"),
("998","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","157","1","0","2020-01-23 18:31:57","2020-01-23 18:31:57","4"),
("1001","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","158","1","0","2020-01-23 18:32:14","2020-01-23 18:32:14","4"),
("1004","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","159","1","0","2020-01-23 18:33:16","2020-01-23 18:33:16","4"),
("1008","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","160","1","0","2020-01-23 18:33:54","2020-01-23 18:33:54","4"),
("1011","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","161","1","0","2020-01-23 18:36:06","2020-01-23 18:36:06","4"),
("1014","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","162","1","0","2020-01-23 18:37:35","2020-01-23 18:37:35","4"),
("1017","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","163","1","0","2020-01-23 18:37:42","2020-01-23 18:37:42","4"),
("1020","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","164","1","0","2020-01-23 18:38:50","2020-01-23 18:38:50","4"),
("1023","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","165","1","0","2020-01-23 18:39:03","2020-01-23 18:39:03","4"),
("1026","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","166","1","0","2020-01-23 18:39:12","2020-01-23 18:39:12","4"),
("1029","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","167","1","0","2020-01-23 18:39:35","2020-01-23 18:39:35","4"),
("1034","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","168","1","0","2020-01-23 18:40:32","2020-01-23 18:40:32","4"),
("1039","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","169","1","0","2020-01-23 18:41:32","2020-01-23 18:41:32","4"),
("1046","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","170","1","0","2020-01-23 18:43:35","2020-01-23 18:43:35","25"),
("1049","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","170","1","0","2020-01-23 18:45:43","2020-01-23 18:45:43","25"),
("1052","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","171","1","0","2020-01-23 18:46:53","2020-01-23 18:46:53","23"),
("1055","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","172","1","0","2020-01-23 18:47:10","2020-01-23 18:47:10","4"),
("1058","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","173","1","0","2020-01-23 18:49:48","2020-01-23 18:49:48","4"),
("1059","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","170","1","0","2020-01-23 18:50:09","2020-01-23 18:50:09","25"),
("1066","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","175","1","0","2020-01-23 18:53:00","2020-01-23 18:53:00","25"),
("1069","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","176","1","0","2020-01-23 18:53:30","2020-01-23 18:53:30","25"),
("1070","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","176","1","0","2020-01-23 18:53:36","2020-01-23 18:53:36","25"),
("1074","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","177","1","0","2020-01-23 18:53:43","2020-01-23 18:53:43","25"),
("1077","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","178","1","0","2020-01-23 18:53:52","2020-01-23 18:53:52","25"),
("1078","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","178","1","0","2020-01-23 18:53:57","2020-01-23 18:53:57","25"),
("1082","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","179","1","0","2020-01-23 18:54:04","2020-01-23 18:54:04","25"),
("1085","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","180","1","0","2020-01-23 18:54:17","2020-01-23 18:54:17","25"),
("1089","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","181","1","0","2020-01-23 18:54:24","2020-01-23 18:54:24","25"),
("1092","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","182","1","0","2020-01-23 18:54:33","2020-01-23 18:54:33","25"),
("1096","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","183","1","0","2020-01-23 18:55:00","2020-01-23 18:55:00","25"),
("1097","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","183","1","0","2020-01-23 18:55:09","2020-01-23 18:55:09","25"),
("1098","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","183","1","0","2020-01-23 18:56:08","2020-01-23 18:56:08","25");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1102","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","184","1","0","2020-01-23 18:56:24","2020-01-23 18:56:24","25"),
("1103","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","184","1","0","2020-01-23 18:56:29","2020-01-23 18:56:29","25"),
("1107","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","185","1","0","2020-01-23 18:56:34","2020-01-23 18:56:34","25"),
("1110","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","186","1","0","2020-01-23 18:56:47","2020-01-23 18:56:47","25"),
("1114","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","187","1","0","2020-01-23 18:56:55","2020-01-23 18:56:55","25"),
("1115","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","187","1","0","2020-01-23 18:56:59","2020-01-23 18:56:59","25"),
("1119","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","188","1","0","2020-01-23 18:57:06","2020-01-23 18:57:06","25"),
("1122","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","189","1","0","2020-01-23 18:57:16","2020-01-23 18:57:16","25"),
("1126","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","190","1","0","2020-01-23 18:57:21","2020-01-23 18:57:21","25"),
("1127","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","190","1","0","2020-01-23 18:57:27","2020-01-23 18:57:27","25"),
("1131","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","191","1","0","2020-01-23 18:57:57","2020-01-23 18:57:57","25"),
("1132","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","191","1","0","2020-01-23 18:58:08","2020-01-23 18:58:08","25"),
("1136","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","192","1","0","2020-01-23 18:58:18","2020-01-23 18:58:18","25"),
("1139","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","193","1","0","2020-01-23 18:58:29","2020-01-23 18:58:29","25"),
("1140","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","193","1","0","2020-01-23 18:58:45","2020-01-23 18:58:45","25"),
("1141","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","193","1","0","2020-01-23 18:58:55","2020-01-23 18:58:55","25"),
("1145","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","194","1","0","2020-01-23 18:59:23","2020-01-23 18:59:23","25"),
("1146","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","194","1","0","2020-01-23 18:59:34","2020-01-23 18:59:34","25"),
("1149","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","195","1","0","2020-01-23 19:23:13","2020-01-23 19:23:13","21"),
("1152","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","196","1","0","2020-01-23 19:23:45","2020-01-23 19:23:45","21"),
("1155","Created new Attendance Event ","app\\models\\AttendanceEvent","26","1","0","2020-01-23 19:39:16","2020-01-23 19:39:16","13"),
("1156","Created new Attendance ","app\\models\\Attendance","10","1","0","2020-01-23 19:39:16","2020-01-23 19:39:16","13"),
("1159","Created new Attendance Event ","app\\models\\AttendanceEvent","27","1","0","2020-01-23 19:41:37","2020-01-23 19:41:37","1"),
("1160","Created new Attendance Event ","app\\models\\AttendanceEvent","28","1","0","2020-01-23 19:41:37","2020-01-23 19:41:37","1"),
("1161","Created new Attendance ","app\\models\\Attendance","11","1","0","2020-01-23 19:41:37","2020-01-23 19:41:37","1"),
("1163","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","194","1","0","2020-01-24 09:51:37","2020-01-24 09:51:37","25"),
("1170","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","197","1","0","2020-01-24 09:55:52","2020-01-24 09:55:52","25"),
("1174","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","198","1","0","2020-01-24 10:29:54","2020-01-24 10:29:54","21"),
("1177","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","199","1","0","2020-01-24 10:30:08","2020-01-24 10:30:08","21"),
("1178","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","199","1","0","2020-01-24 10:33:31","2020-01-24 10:33:31","21"),
("1179","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","199","1","0","2020-01-24 10:34:11","2020-01-24 10:34:11","21"),
("1180","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","199","1","0","2020-01-24 10:34:24","2020-01-24 10:34:24","21"),
("1183","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","200","1","0","2020-01-24 11:00:41","2020-01-24 11:00:41","21"),
("1187","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","201","1","0","2020-01-24 11:13:17","2020-01-24 11:13:17","21"),
("1190","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","202","1","0","2020-01-24 11:15:31","2020-01-24 11:15:31","23"),
("1193","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","203","1","0","2020-01-24 11:22:44","2020-01-24 11:22:44","23"),
("1196","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","204","1","0","2020-01-24 11:25:57","2020-01-24 11:25:57","21"),
("1199","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","205","1","0","2020-01-24 11:27:13","2020-01-24 11:27:13","23"),
("1202","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","206","1","0","2020-01-24 11:31:58","2020-01-24 11:31:58","23"),
("1205","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","207","1","0","2020-01-24 11:34:41","2020-01-24 11:34:41","23"),
("1208","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","208","1","0","2020-01-24 11:36:56","2020-01-24 11:36:56","23"),
("1215","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","209","1","0","2020-01-24 11:50:32","2020-01-24 11:50:32","23"),
("1216","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","209","1","0","2020-01-24 11:51:34","2020-01-24 11:51:34","23"),
("1217","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","209","1","0","2020-01-24 11:58:22","2020-01-24 11:58:22","23"),
("1220","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","210","1","0","2020-01-24 12:10:38","2020-01-24 12:10:38","23"),
("1223","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","211","1","0","2020-01-24 12:10:50","2020-01-24 12:10:50","23"),
("1226","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","212","1","0","2020-01-24 12:11:57","2020-01-24 12:11:57","23"),
("1229","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","213","1","0","2020-01-24 12:14:40","2020-01-24 12:14:40","23"),
("1232","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","214","1","0","2020-01-24 12:14:56","2020-01-24 12:14:56","23"),
("1235","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","215","1","0","2020-01-24 12:15:04","2020-01-24 12:15:04","23"),
("1238","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","216","1","0","2020-01-24 12:15:25","2020-01-24 12:15:25","23"),
("1241","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","217","1","0","2020-01-24 12:18:30","2020-01-24 12:18:30","23"),
("1244","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","218","1","0","2020-01-24 12:24:24","2020-01-24 12:24:24","23"),
("1247","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","219","1","0","2020-01-24 12:26:39","2020-01-24 12:26:39","23"),
("1250","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","220","1","0","2020-01-24 12:26:42","2020-01-24 12:26:42","23"),
("1253","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","221","1","0","2020-01-24 12:27:06","2020-01-24 12:27:06","23"),
("1256","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","222","1","0","2020-01-24 12:28:41","2020-01-24 12:28:41","23"),
("1259","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","223","1","0","2020-01-24 12:31:23","2020-01-24 12:31:23","23"),
("1262","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","224","1","0","2020-01-24 12:37:01","2020-01-24 12:37:01","23"),
("1265","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","225","1","0","2020-01-24 12:39:15","2020-01-24 12:39:15","23"),
("1268","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","226","1","0","2020-01-24 12:44:50","2020-01-24 12:44:50","23"),
("1271","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","227","1","0","2020-01-24 12:46:11","2020-01-24 12:46:11","23"),
("1274","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","228","1","0","2020-01-24 12:48:55","2020-01-24 12:48:55","23"),
("1277","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","229","1","0","2020-01-24 12:49:21","2020-01-24 12:49:21","23"),
("1280","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","230","1","0","2020-01-24 12:49:38","2020-01-24 12:49:38","23"),
("1283","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","231","1","0","2020-01-24 12:49:56","2020-01-24 12:49:56","23"),
("1286","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","232","1","0","2020-01-24 12:50:39","2020-01-24 12:50:39","23"),
("1289","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","233","1","0","2020-01-24 12:51:21","2020-01-24 12:51:21","23"),
("1292","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","234","1","0","2020-01-24 12:52:53","2020-01-24 12:52:53","23"),
("1299","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","235","1","0","2020-01-24 12:56:02","2020-01-24 12:56:02","23"),
("1302","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","236","1","0","2020-01-24 12:58:26","2020-01-24 12:58:26","23"),
("1303","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","236","1","0","2020-01-24 13:10:27","2020-01-24 13:10:27","23"),
("1307","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","237","1","0","2020-01-24 13:17:33","2020-01-24 13:17:33","23"),
("1311","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","238","1","0","2020-01-24 13:18:04","2020-01-24 13:18:04","23"),
("1312","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","238","1","0","2020-01-24 13:20:05","2020-01-24 13:20:05","23"),
("1316","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","239","1","0","2020-01-24 13:23:30","2020-01-24 13:23:30","23"),
("1317","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","239","1","0","2020-01-24 13:37:00","2020-01-24 13:37:00","23"),
("1318","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","239","1","0","2020-01-24 13:38:49","2020-01-24 13:38:49","23"),
("1321","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","240","1","0","2020-01-24 13:40:27","2020-01-24 13:40:27","23"),
("1324","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","241","1","0","2020-01-24 13:41:34","2020-01-24 13:41:34","23"),
("1327","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 13:42:01","2020-01-24 13:42:01","23"),
("1328","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 13:46:02","2020-01-24 13:46:02","23"),
("1329","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 14:35:04","2020-01-24 14:35:04","23"),
("1330","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 14:35:29","2020-01-24 14:35:29","23"),
("1331","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 14:47:25","2020-01-24 14:47:25","23"),
("1332","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 14:52:21","2020-01-24 14:52:21","23"),
("1333","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 14:53:21","2020-01-24 14:53:21","23"),
("1334","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 15:02:10","2020-01-24 15:02:10","23"),
("1335","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 15:03:02","2020-01-24 15:03:02","23"),
("1336","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","242","1","0","2020-01-24 16:08:45","2020-01-24 16:08:45","23"),
("1340","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 16:59:54","2020-01-24 16:59:54","23"),
("1343","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 17:39:29","2020-01-24 17:39:29","23"),
("1348","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 18:38:06","2020-01-24 18:38:06","23"),
("1351","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 18:40:26","2020-01-24 18:40:26","23"),
("1355","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 18:44:58","2020-01-24 18:44:58","23"),
("1357","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 19:01:35","2020-01-24 19:01:35","23"),
("1358","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 19:02:25","2020-01-24 19:02:25","23"),
("1359","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 19:03:08","2020-01-24 19:03:08","23"),
("1361","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 19:13:50","2020-01-24 19:13:50","23"),
("1364","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","243","1","0","2020-01-24 19:17:13","2020-01-24 19:17:13","23");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1373","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","244","1","0","2020-01-27 10:07:18","2020-01-27 10:07:18","23"),
("1377","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 10:11:29","2020-01-27 10:11:29","11"),
("1379","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 10:26:22","2020-01-27 10:26:22","11"),
("1380","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 10:53:45","2020-01-27 10:53:45","11"),
("1381","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 10:55:19","2020-01-27 10:55:19","11"),
("1382","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 11:00:09","2020-01-27 11:00:09","11"),
("1383","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 11:12:37","2020-01-27 11:12:37","11"),
("1384","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 11:16:11","2020-01-27 11:16:11","11"),
("1398","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 12:07:09","2020-01-27 12:07:09","11"),
("1400","Created new Attendance Event ","app\\models\\AttendanceEvent","29","1","0","2020-01-27 12:13:59","2020-01-27 12:13:59","13"),
("1401","Created new Attendance Event ","app\\models\\AttendanceEvent","30","1","0","2020-01-27 12:13:59","2020-01-27 12:13:59","13"),
("1402","Created new Attendance ","app\\models\\Attendance","12","1","0","2020-01-27 12:13:59","2020-01-27 12:13:59","13"),
("1404","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 12:46:42","2020-01-27 12:46:42","11"),
("1405","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 12:50:17","2020-01-27 12:50:17","11"),
("1406","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 12:54:36","2020-01-27 12:54:36","11"),
("1407","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 12:55:30","2020-01-27 12:55:30","11"),
("1408","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 12:56:27","2020-01-27 12:56:27","11"),
("1409","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 12:57:00","2020-01-27 12:57:00","11"),
("1410","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 12:59:16","2020-01-27 12:59:16","11"),
("1412","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:02:35","2020-01-27 13:02:35","11"),
("1413","Created new Attendance Event ","app\\models\\AttendanceEvent","31","1","0","2020-01-27 13:02:41","2020-01-27 13:02:41","11"),
("1414","Created new Attendance Event ","app\\models\\AttendanceEvent","32","1","0","2020-01-27 13:02:41","2020-01-27 13:02:41","11"),
("1416","Created new Attendance Event ","app\\models\\AttendanceEvent","33","1","0","2020-01-27 13:03:29","2020-01-27 13:03:29","11"),
("1417","Created new Attendance Event ","app\\models\\AttendanceEvent","34","1","0","2020-01-27 13:03:29","2020-01-27 13:03:29","11"),
("1418","Created new Attendance ","app\\models\\Attendance","14","1","0","2020-01-27 13:03:29","2020-01-27 13:03:29","11"),
("1419","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:03:31","2020-01-27 13:03:31","11"),
("1421","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:08:45","2020-01-27 13:08:45","11"),
("1422","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:09:09","2020-01-27 13:09:09","11"),
("1423","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:09:52","2020-01-27 13:09:52","11"),
("1424","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:14:41","2020-01-27 13:14:41","11"),
("1425","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:15:45","2020-01-27 13:15:45","11"),
("1428","Created new Attendance Event ","app\\models\\AttendanceEvent","35","1","0","2020-01-27 13:25:15","2020-01-27 13:25:15","11"),
("1429","Created new Attendance Event ","app\\models\\AttendanceEvent","36","1","0","2020-01-27 13:25:15","2020-01-27 13:25:15","11"),
("1430","Created new Attendance ","app\\models\\Attendance","15","1","0","2020-01-27 13:25:15","2020-01-27 13:25:15","11"),
("1431","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:25:48","2020-01-27 13:25:48","11"),
("1432","Created new Attendance Event ","app\\models\\AttendanceEvent","37","1","0","2020-01-27 13:29:46","2020-01-27 13:29:46","11"),
("1433","Created new Attendance Event ","app\\models\\AttendanceEvent","38","1","0","2020-01-27 13:29:46","2020-01-27 13:29:46","11"),
("1434","Created new Attendance ","app\\models\\Attendance","16","1","0","2020-01-27 13:29:46","2020-01-27 13:29:46","11"),
("1435","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 13:29:48","2020-01-27 13:29:48","11"),
("1437","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 14:35:11","2020-01-27 14:35:11","11"),
("1439","Created new Attendance Event ","app\\models\\AttendanceEvent","39","1","0","2020-01-27 14:37:52","2020-01-27 14:37:52","11"),
("1440","Created new Attendance Event ","app\\models\\AttendanceEvent","40","1","0","2020-01-27 14:37:52","2020-01-27 14:37:52","11"),
("1441","Created new Attendance ","app\\models\\Attendance","17","1","0","2020-01-27 14:37:52","2020-01-27 14:37:52","11"),
("1442","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 14:37:56","2020-01-27 14:37:56","11"),
("1443","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 14:38:58","2020-01-27 14:38:58","11"),
("1444","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 14:42:41","2020-01-27 14:42:41","11"),
("1445","Created new Attendance Event ","app\\models\\AttendanceEvent","41","1","0","2020-01-27 14:46:43","2020-01-27 14:46:43","11"),
("1446","Created new Attendance Event ","app\\models\\AttendanceEvent","42","1","0","2020-01-27 14:46:43","2020-01-27 14:46:43","11"),
("1448","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 14:59:57","2020-01-27 14:59:57","11"),
("1450","Created new Attendance Event ","app\\models\\AttendanceEvent","43","1","0","2020-01-27 15:08:19","2020-01-27 15:08:19","11"),
("1451","Created new Attendance Event ","app\\models\\AttendanceEvent","44","1","0","2020-01-27 15:08:19","2020-01-27 15:08:19","11"),
("1452","Created new Attendance ","app\\models\\Attendance","19","1","0","2020-01-27 15:08:20","2020-01-27 15:08:20","11"),
("1453","Created new Attendance Event ","app\\models\\AttendanceEvent","46","1","0","2020-01-27 15:11:54","2020-01-27 15:11:54","11"),
("1454","Created new Attendance Event ","app\\models\\AttendanceEvent","47","1","0","2020-01-27 15:11:54","2020-01-27 15:11:54","11"),
("1455","Created new Attendance ","app\\models\\Attendance","20","1","0","2020-01-27 15:11:54","2020-01-27 15:11:54","11"),
("1456","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:13:05","2020-01-27 15:13:05","11"),
("1459","Created new Attendance Event ","app\\models\\AttendanceEvent","48","1","0","2020-01-27 15:19:54","2020-01-27 15:19:54","11"),
("1460","Created new Attendance Event ","app\\models\\AttendanceEvent","49","1","0","2020-01-27 15:19:54","2020-01-27 15:19:54","11"),
("1461","Created new Attendance ","app\\models\\Attendance","22","1","0","2020-01-27 15:19:54","2020-01-27 15:19:54","11"),
("1462","Created new Attendance Event ","app\\models\\AttendanceEvent","50","1","0","2020-01-27 15:22:29","2020-01-27 15:22:29","11"),
("1463","Created new Attendance Event ","app\\models\\AttendanceEvent","51","1","0","2020-01-27 15:22:29","2020-01-27 15:22:29","11"),
("1464","Created new Attendance ","app\\models\\Attendance","23","1","0","2020-01-27 15:22:29","2020-01-27 15:22:29","11"),
("1465","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:27:14","2020-01-27 15:27:14","11"),
("1466","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:28:16","2020-01-27 15:28:16","11"),
("1467","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:29:49","2020-01-27 15:29:49","11"),
("1468","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:30:02","2020-01-27 15:30:02","11"),
("1469","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:31:25","2020-01-27 15:31:25","11"),
("1470","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:33:09","2020-01-27 15:33:09","11"),
("1471","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:34:39","2020-01-27 15:34:39","11"),
("1472","Created new Attendance Event ","app\\models\\AttendanceEvent","52","1","0","2020-01-27 15:35:52","2020-01-27 15:35:52","11"),
("1473","Created new Attendance Event ","app\\models\\AttendanceEvent","53","1","0","2020-01-27 15:35:52","2020-01-27 15:35:52","11"),
("1474","Created new Attendance ","app\\models\\Attendance","24","1","0","2020-01-27 15:35:52","2020-01-27 15:35:52","11"),
("1475","Modified Attendance Event ","app\\models\\AttendanceEvent","53","1","0","2020-01-27 15:35:52","2020-01-27 15:35:52","11"),
("1476","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:35:56","2020-01-27 15:35:56","11"),
("1477","Created new Attendance Event ","app\\models\\AttendanceEvent","54","1","0","2020-01-27 15:36:52","2020-01-27 15:36:52","11"),
("1478","Created new Attendance Event ","app\\models\\AttendanceEvent","55","1","0","2020-01-27 15:36:52","2020-01-27 15:36:52","11"),
("1479","Created new Attendance ","app\\models\\Attendance","25","1","0","2020-01-27 15:36:52","2020-01-27 15:36:52","11"),
("1480","Modified Attendance Event ","app\\models\\AttendanceEvent","55","1","0","2020-01-27 15:36:52","2020-01-27 15:36:52","11"),
("1481","Created new Attendance Event ","app\\models\\AttendanceEvent","56","1","0","2020-01-27 15:39:51","2020-01-27 15:39:51","11"),
("1482","Created new Attendance Event ","app\\models\\AttendanceEvent","57","1","0","2020-01-27 15:39:51","2020-01-27 15:39:51","11"),
("1483","Created new Attendance ","app\\models\\Attendance","26","1","0","2020-01-27 15:39:51","2020-01-27 15:39:51","11"),
("1484","Modified Attendance Event ","app\\models\\AttendanceEvent","56","1","0","2020-01-27 15:39:51","2020-01-27 15:39:51","11"),
("1485","Modified Attendance Event ","app\\models\\AttendanceEvent","57","1","0","2020-01-27 15:39:51","2020-01-27 15:39:51","11"),
("1486","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:40:48","2020-01-27 15:40:48","11"),
("1487","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:41:29","2020-01-27 15:41:29","11"),
("1491","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 15:56:30","2020-01-27 15:56:30","11"),
("1493","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:15:22","2020-01-27 16:15:22","11"),
("1494","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:15:56","2020-01-27 16:15:56","11"),
("1495","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:19:12","2020-01-27 16:19:12","11"),
("1496","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:20:50","2020-01-27 16:20:50","11"),
("1497","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:21:25","2020-01-27 16:21:25","11"),
("1498","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:28:04","2020-01-27 16:28:04","11"),
("1499","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:28:32","2020-01-27 16:28:32","11"),
("1500","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:44:21","2020-01-27 16:44:21","11"),
("1501","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:52:03","2020-01-27 16:52:03","11"),
("1502","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:54:56","2020-01-27 16:54:56","11"),
("1503","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 16:55:22","2020-01-27 16:55:22","11"),
("1506","Created new Attendance Event ","app\\models\\AttendanceEvent","58","1","0","2020-01-27 17:31:15","2020-01-27 17:31:15","11"),
("1507","Created new Attendance Event ","app\\models\\AttendanceEvent","59","1","0","2020-01-27 17:31:15","2020-01-27 17:31:15","11"),
("1508","Created new Attendance ","app\\models\\Attendance","27","1","0","2020-01-27 17:31:15","2020-01-27 17:31:15","11");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1509","Modified Attendance Event ","app\\models\\AttendanceEvent","58","1","0","2020-01-27 17:31:15","2020-01-27 17:31:15","11"),
("1510","Modified Attendance Event ","app\\models\\AttendanceEvent","59","1","0","2020-01-27 17:31:15","2020-01-27 17:31:15","11"),
("1520","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 18:11:45","2020-01-27 18:11:45","11"),
("1521","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 18:22:19","2020-01-27 18:22:19","11"),
("1522","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 18:25:41","2020-01-27 18:25:41","11"),
("1523","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 18:51:55","2020-01-27 18:51:55","11"),
("1524","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 19:02:22","2020-01-27 19:02:22","11"),
("1527","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 19:09:52","2020-01-27 19:09:52","11"),
("1537","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 19:28:59","2020-01-27 19:28:59","11"),
("1539","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 19:31:35","2020-01-27 19:31:35","11"),
("1540","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","245","1","0","2020-01-27 19:32:22","2020-01-27 19:32:22","11"),
("1543","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 10:14:48","2020-01-28 10:14:48","11"),
("1544","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 10:28:02","2020-01-28 10:28:02","11"),
("1545","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 11:02:57","2020-01-28 11:02:57","11"),
("1546","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 11:16:30","2020-01-28 11:16:30","11"),
("1547","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 11:17:34","2020-01-28 11:17:34","11"),
("1554","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","248","1","0","2020-01-28 11:30:44","2020-01-28 11:30:44","12"),
("1555","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","248","1","0","2020-01-28 11:31:27","2020-01-28 11:31:27","12"),
("1556","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 11:38:26","2020-01-28 11:38:26","11"),
("1557","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 11:42:00","2020-01-28 11:42:00","11"),
("1558","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 11:42:43","2020-01-28 11:42:43","11"),
("1562","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","249","1","0","2020-01-28 11:43:00","2020-01-28 11:43:00","12"),
("1566","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","250","1","0","2020-01-28 11:51:02","2020-01-28 11:51:02","12"),
("1567","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","250","1","0","2020-01-28 11:54:52","2020-01-28 11:54:52","12"),
("1571","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","250","1","0","2020-01-28 11:57:20","2020-01-28 11:57:20","12"),
("1572","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","250","1","0","2020-01-28 12:03:49","2020-01-28 12:03:49","12"),
("1576","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","251","1","0","2020-01-28 12:04:15","2020-01-28 12:04:15","12"),
("1580","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","252","1","0","2020-01-28 12:08:24","2020-01-28 12:08:24","12"),
("1584","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","253","1","0","2020-01-28 12:09:15","2020-01-28 12:09:15","12"),
("1585","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","253","1","0","2020-01-28 12:14:00","2020-01-28 12:14:00","12"),
("1590","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","254","1","0","2020-01-28 12:15:08","2020-01-28 12:15:08","12"),
("1591","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:23:43","2020-01-28 12:23:43","11"),
("1592","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:25:33","2020-01-28 12:25:33","11"),
("1593","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:26:38","2020-01-28 12:26:38","11"),
("1594","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:31:56","2020-01-28 12:31:56","11"),
("1595","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:32:25","2020-01-28 12:32:25","11"),
("1596","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:34:18","2020-01-28 12:34:18","11"),
("1597","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:41:39","2020-01-28 12:41:39","11"),
("1598","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:43:20","2020-01-28 12:43:20","11"),
("1599","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 12:43:47","2020-01-28 12:43:47","11"),
("1600","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 13:01:50","2020-01-28 13:01:50","11"),
("1601","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 13:06:44","2020-01-28 13:06:44","11"),
("1602","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 13:06:57","2020-01-28 13:06:57","11"),
("1603","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 13:08:51","2020-01-28 13:08:51","11"),
("1604","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 14:35:40","2020-01-28 14:35:40","11"),
("1605","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 14:38:59","2020-01-28 14:38:59","11"),
("1606","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 14:52:48","2020-01-28 14:52:48","11"),
("1607","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:02:58","2020-01-28 15:02:58","11"),
("1608","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:03:10","2020-01-28 15:03:10","11"),
("1609","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:04:00","2020-01-28 15:04:00","11"),
("1610","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:05:41","2020-01-28 15:05:41","11"),
("1611","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:08:23","2020-01-28 15:08:23","11"),
("1612","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:10:10","2020-01-28 15:10:10","11"),
("1613","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:11:40","2020-01-28 15:11:40","11"),
("1614","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:14:50","2020-01-28 15:14:50","11"),
("1615","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:16:16","2020-01-28 15:16:16","11"),
("1616","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:17:53","2020-01-28 15:17:53","11"),
("1617","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:22:06","2020-01-28 15:22:06","11"),
("1618","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:22:28","2020-01-28 15:22:28","11"),
("1619","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:23:02","2020-01-28 15:23:02","11"),
("1620","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:24:53","2020-01-28 15:24:53","11"),
("1621","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:26:36","2020-01-28 15:26:36","11"),
("1622","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:29:48","2020-01-28 15:29:48","11"),
("1623","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:34:38","2020-01-28 15:34:38","11"),
("1624","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:36:30","2020-01-28 15:36:30","11"),
("1625","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:36:53","2020-01-28 15:36:53","11"),
("1626","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:45:45","2020-01-28 15:45:45","11"),
("1627","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:47:56","2020-01-28 15:47:56","11"),
("1628","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:48:46","2020-01-28 15:48:46","11"),
("1629","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:49:56","2020-01-28 15:49:56","11"),
("1630","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 15:53:51","2020-01-28 15:53:51","11"),
("1633","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 16:03:30","2020-01-28 16:03:30","11"),
("1634","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","254","1","0","2020-01-28 16:04:25","2020-01-28 16:04:25","12"),
("1640","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","256","1","0","2020-01-28 16:05:39","2020-01-28 16:05:39","26"),
("1641","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 16:18:12","2020-01-28 16:18:12","11"),
("1642","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 16:19:44","2020-01-28 16:19:44","11"),
("1643","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 16:20:11","2020-01-28 16:20:11","11"),
("1644","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 16:23:45","2020-01-28 16:23:45","11"),
("1647","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","257","1","0","2020-01-28 16:30:00","2020-01-28 16:30:00","26"),
("1650","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","258","1","0","2020-01-28 16:31:35","2020-01-28 16:31:35","26"),
("1652","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","258","1","0","2020-01-28 17:05:31","2020-01-28 17:05:31","26"),
("1655","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:06:29","2020-01-28 17:06:29","26"),
("1660","Created new Comment ","app\\modules\\comment\\models\\Comment","3","1","0","2020-01-28 17:21:47","2020-01-28 17:21:47","11"),
("1661","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:26:03","2020-01-28 17:26:03","26"),
("1662","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:26:32","2020-01-28 17:26:32","26"),
("1664","Created new Comment ","app\\modules\\comment\\models\\Comment","4","1","0","2020-01-28 17:30:59","2020-01-28 17:30:59","11"),
("1666","Created new Comment ","app\\modules\\comment\\models\\Comment","5","1","0","2020-01-28 17:31:01","2020-01-28 17:31:01","11"),
("1668","Created new Comment ","app\\modules\\comment\\models\\Comment","6","1","0","2020-01-28 17:33:28","2020-01-28 17:33:28","11"),
("1670","Created new Comment ","app\\modules\\comment\\models\\Comment","7","1","0","2020-01-28 17:33:30","2020-01-28 17:33:30","11"),
("1671","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:35:39","2020-01-28 17:35:39","26"),
("1672","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:36:56","2020-01-28 17:36:56","26"),
("1675","Created new Comment ","app\\modules\\comment\\models\\Comment","8","1","0","2020-01-28 17:38:01","2020-01-28 17:38:01","11"),
("1679","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:38:30","2020-01-28 17:38:30","26"),
("1688","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:39:48","2020-01-28 17:39:48","26"),
("1693","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:40:35","2020-01-28 17:40:35","26"),
("1695","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:51:26","2020-01-28 17:51:26","26"),
("1696","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:53:34","2020-01-28 17:53:34","26"),
("1697","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:57:55","2020-01-28 17:57:55","26"),
("1698","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 17:59:46","2020-01-28 17:59:46","26"),
("1699","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:07:38","2020-01-28 18:07:38","11");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1700","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:09:56","2020-01-28 18:09:56","26"),
("1701","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:11:15","2020-01-28 18:11:15","26"),
("1703","Created new Comment ","app\\modules\\comment\\models\\Comment","16","1","0","2020-01-28 18:11:31","2020-01-28 18:11:31","11"),
("1705","Created new Comment ","app\\modules\\comment\\models\\Comment","17","1","0","2020-01-28 18:11:34","2020-01-28 18:11:34","11"),
("1706","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:11:47","2020-01-28 18:11:47","11"),
("1707","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:11:56","2020-01-28 18:11:56","26"),
("1708","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:12:07","2020-01-28 18:12:07","11"),
("1709","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:13:16","2020-01-28 18:13:16","26"),
("1710","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:13:26","2020-01-28 18:13:26","26"),
("1711","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:13:57","2020-01-28 18:13:57","26"),
("1713","Created new Comment ","app\\modules\\comment\\models\\Comment","18","1","0","2020-01-28 18:14:43","2020-01-28 18:14:43","11"),
("1715","Created new Comment ","app\\modules\\comment\\models\\Comment","19","1","0","2020-01-28 18:14:45","2020-01-28 18:14:45","11"),
("1716","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:14:47","2020-01-28 18:14:47","26"),
("1717","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:16:07","2020-01-28 18:16:07","26"),
("1718","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:16:22","2020-01-28 18:16:22","11"),
("1719","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:16:24","2020-01-28 18:16:24","26"),
("1720","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:16:40","2020-01-28 18:16:40","26"),
("1721","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:16:55","2020-01-28 18:16:55","26"),
("1722","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:17:17","2020-01-28 18:17:17","11"),
("1723","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","259","1","0","2020-01-28 18:17:30","2020-01-28 18:17:30","26"),
("1724","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:17:44","2020-01-28 18:17:44","11"),
("1726","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:18:23","2020-01-28 18:18:23","11"),
("1727","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:19:20","2020-01-28 18:19:20","11"),
("1735","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 18:30:11","2020-01-28 18:30:11","11"),
("1738","Created new Comment ","app\\modules\\comment\\models\\Comment","20","1","0","2020-01-28 18:37:41","2020-01-28 18:37:41","13"),
("1740","Created new Comment ","app\\modules\\comment\\models\\Comment","21","1","0","2020-01-28 18:37:43","2020-01-28 18:37:43","13"),
("1746","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 19:04:29","2020-01-28 19:04:29","11"),
("1748","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","246","1","0","2020-01-28 19:07:48","2020-01-28 19:07:48","11"),
("1758","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","261","1","0","2020-01-29 10:49:44","2020-01-29 10:49:44","11"),
("1834","Created new File ","app\\modules\\file\\models\\File","38","1","0","2020-01-29 18:33:53","2020-01-29 18:33:53","26"),
("1836","Created new File ","app\\modules\\file\\models\\File","39","1","0","2020-01-29 18:43:04","2020-01-29 18:43:04","26"),
("1837","Created new File ","app\\modules\\file\\models\\File","40","1","0","2020-01-29 18:44:53","2020-01-29 18:44:53","26"),
("1838","Created new File ","app\\modules\\file\\models\\File","41","1","0","2020-01-29 18:46:42","2020-01-29 18:46:42","26"),
("1839","Created new File ","app\\modules\\file\\models\\File","42","1","0","2020-01-29 18:47:13","2020-01-29 18:47:13","26"),
("1840","Created new File ","app\\modules\\file\\models\\File","43","1","0","2020-01-29 18:47:38","2020-01-29 18:47:38","26"),
("1842","Created new File ","app\\modules\\file\\models\\File","44","1","0","2020-01-29 18:52:17","2020-01-29 18:52:17","26"),
("1843","Created new File ","app\\modules\\file\\models\\File","45","1","0","2020-01-29 18:52:36","2020-01-29 18:52:36","26"),
("1846","Created new File ","app\\modules\\file\\models\\File","46","1","0","2020-01-29 18:54:37","2020-01-29 18:54:37","26"),
("1847","Created new File ","app\\modules\\file\\models\\File","47","1","0","2020-01-29 18:57:14","2020-01-29 18:57:14","26"),
("1848","Created new File ","app\\modules\\file\\models\\File","48","1","0","2020-01-29 18:57:28","2020-01-29 18:57:28","26"),
("1850","Created new File ","app\\modules\\file\\models\\File","49","1","0","2020-01-29 19:02:30","2020-01-29 19:02:30","26"),
("1855","Created new File ","app\\modules\\file\\models\\File","50","1","0","2020-01-29 19:21:42","2020-01-29 19:21:42","26"),
("1856","Created new File ","app\\modules\\file\\models\\File","51","1","0","2020-01-29 19:23:20","2020-01-29 19:23:20","26"),
("1857","Created new File ","app\\modules\\file\\models\\File","52","1","0","2020-01-29 19:23:44","2020-01-29 19:23:44","26"),
("1858","Created new File ","app\\modules\\file\\models\\File","53","1","0","2020-01-29 19:23:54","2020-01-29 19:23:54","26"),
("1859","Created new File ","app\\modules\\file\\models\\File","54","1","0","2020-01-29 19:24:21","2020-01-29 19:24:21","26"),
("1860","Created new File ","app\\modules\\file\\models\\File","55","1","0","2020-01-29 19:24:58","2020-01-29 19:24:58","26"),
("1861","Created new File ","app\\modules\\file\\models\\File","56","1","0","2020-01-29 19:25:29","2020-01-29 19:25:29","26"),
("1862","Created new File ","app\\modules\\file\\models\\File","57","1","0","2020-01-29 19:27:19","2020-01-29 19:27:19","26"),
("1863","Created new File ","app\\modules\\file\\models\\File","58","1","0","2020-01-29 19:28:02","2020-01-29 19:28:02","26"),
("1864","Created new Files ","app\\models\\Files","59","1","0","2020-01-29 19:49:05","2020-01-29 19:49:05","26"),
("1865","Created new Files ","app\\models\\Files","60","1","0","2020-01-29 19:50:18","2020-01-29 19:50:18","26"),
("1866","Created new Files ","app\\models\\Files","61","1","0","2020-01-29 19:50:49","2020-01-29 19:50:49","26"),
("1951","Created new Files ","app\\models\\Files","62","1","0","2020-01-30 15:47:04","2020-01-30 15:47:04","0"),
("2014","Created new User ","app\\models\\User","28","1","0","2020-01-31 12:02:58","2020-01-31 12:02:58","1"),
("2016","Created new User ","app\\models\\User","29","1","0","2020-01-31 12:13:29","2020-01-31 12:13:29","1"),
("2017","Modified User ","app\\models\\User","29","1","0","2020-01-31 12:13:41","2020-01-31 12:13:41","1"),
("2018","Created new Comment ","app\\modules\\comment\\models\\Comment","23","1","0","2020-01-31 12:13:41","2020-01-31 12:13:41","1"),
("2019","Modified User ","app\\models\\User","28","1","0","2020-01-31 12:24:16","2020-01-31 12:24:16","1"),
("2020","Created new Comment ","app\\modules\\comment\\models\\Comment","24","1","0","2020-01-31 12:24:16","2020-01-31 12:24:16","1"),
("2021","Created new Login History ","app\\models\\LoginHistory","1","1","0","2020-01-31 12:27:22","2020-01-31 12:27:22","0"),
("2022","Created new Login History ","app\\models\\LoginHistory","2","1","0","2020-01-31 12:30:29","2020-01-31 12:30:29","0"),
("2023","Created new Login History ","app\\models\\LoginHistory","3","1","0","2020-01-31 12:32:07","2020-01-31 12:32:07","0"),
("2024","Created new Login History ","app\\models\\LoginHistory","4","1","0","2020-01-31 12:33:04","2020-01-31 12:33:04","0"),
("2025","Created new Client ","app\\models\\Client","3","1","0","2020-01-31 12:34:10","2020-01-31 12:34:10","1"),
("2028","Created new Comment ","app\\modules\\comment\\models\\Comment","25","1","0","2020-01-31 12:39:00","2020-01-31 12:39:00","1"),
("2030","Created new Comment ","app\\modules\\comment\\models\\Comment","26","1","0","2020-01-31 12:39:04","2020-01-31 12:39:04","1"),
("2032","Created new Comment ","app\\modules\\comment\\models\\Comment","27","1","0","2020-01-31 12:40:20","2020-01-31 12:40:20","1"),
("2034","Created new Comment ","app\\modules\\comment\\models\\Comment","28","1","0","2020-01-31 12:40:23","2020-01-31 12:40:23","1"),
("2036","Created new Platform ","app\\models\\Platform","4","1","0","2020-01-31 12:52:29","2020-01-31 12:52:29","1"),
("2038","Created new Leave Request ","app\\models\\LeaveRequest","19","1","0","2020-01-31 13:00:31","2020-01-31 13:00:31","29"),
("2039","Created new Ticket Type ","app\\models\\TicketType","2","1","0","2020-01-31 13:01:48","2020-01-31 13:01:48","1"),
("2040","Created new User ","app\\models\\User","30","1","0","2020-01-31 13:05:45","2020-01-31 13:05:45","1"),
("2041","Created new Ticket ","app\\models\\Ticket","12","1","0","2020-01-31 13:06:20","2020-01-31 13:06:20","29"),
("2043","Created new Comment ","app\\modules\\comment\\models\\Comment","29","1","0","2020-01-31 15:30:07","2020-01-31 15:30:07","28"),
("2045","Created new Comment ","app\\modules\\comment\\models\\Comment","30","1","0","2020-01-31 15:30:20","2020-01-31 15:30:20","28"),
("2046","Created new Login History ","app\\models\\LoginHistory","5","1","0","2020-02-01 09:42:17","2020-02-01 09:42:17","0"),
("2047","Created new Login History ","app\\models\\LoginHistory","6","1","0","2020-02-01 09:42:26","2020-02-01 09:42:26","0"),
("2048","Created new Login History ","app\\models\\LoginHistory","7","1","0","2020-02-01 09:42:32","2020-02-01 09:42:32","0"),
("2049","Created new Login History ","app\\models\\LoginHistory","8","1","0","2020-02-01 09:42:37","2020-02-01 09:42:37","0"),
("2050","Created new Login History ","app\\models\\LoginHistory","9","1","0","2020-02-01 09:44:53","2020-02-01 09:44:53","0"),
("2051","Created new Login History ","app\\models\\LoginHistory","10","1","0","2020-02-01 09:44:54","2020-02-01 09:44:54","0"),
("2052","Created new Login History ","app\\models\\LoginHistory","11","1","0","2020-02-01 09:45:53","2020-02-01 09:45:53","0"),
("2053","Created new Login History ","app\\models\\LoginHistory","12","1","0","2020-02-01 09:45:56","2020-02-01 09:45:56","0"),
("2054","Created new Login History ","app\\models\\LoginHistory","13","1","0","2020-02-01 09:45:59","2020-02-01 09:45:59","0"),
("2055","Created new Login History ","app\\models\\LoginHistory","14","1","0","2020-02-01 09:46:04","2020-02-01 09:46:04","0"),
("2056","Created new Login History ","app\\models\\LoginHistory","15","1","0","2020-02-01 09:46:12","2020-02-01 09:46:12","0"),
("2057","Created new Login History ","app\\models\\LoginHistory","16","1","0","2020-02-01 09:46:19","2020-02-01 09:46:19","0"),
("2058","Created new Login History ","app\\models\\LoginHistory","17","1","0","2020-02-01 09:46:30","2020-02-01 09:46:30","0"),
("2059","Created new Login History ","app\\models\\LoginHistory","18","1","0","2020-02-01 09:46:37","2020-02-01 09:46:37","0"),
("2060","Created new Login History ","app\\models\\LoginHistory","19","1","0","2020-02-01 09:46:57","2020-02-01 09:46:57","0"),
("2061","Created new Login History ","app\\models\\LoginHistory","20","1","0","2020-02-01 09:47:23","2020-02-01 09:47:23","0"),
("2062","Created new Login History ","app\\models\\LoginHistory","21","1","0","2020-02-01 09:47:28","2020-02-01 09:47:28","0"),
("2063","Created new Login History ","app\\models\\LoginHistory","22","1","0","2020-02-01 09:47:32","2020-02-01 09:47:32","0"),
("2064","Created new Login History ","app\\models\\LoginHistory","23","1","0","2020-02-01 09:47:35","2020-02-01 09:47:35","0"),
("2065","Created new Login History ","app\\models\\LoginHistory","24","1","0","2020-02-01 09:47:42","2020-02-01 09:47:42","0"),
("2066","Created new Login History ","app\\models\\LoginHistory","25","1","0","2020-02-01 09:48:02","2020-02-01 09:48:02","0"),
("2067","Created new User ","app\\models\\User","31","1","0","2020-02-01 09:50:46","2020-02-01 09:50:46","1"),
("2068","Modified User ","app\\models\\User","31","1","0","2020-02-01 09:51:06","2020-02-01 09:51:06","1"),
("2069","Created new Comment ","app\\modules\\comment\\models\\Comment","31","1","0","2020-02-01 09:51:07","2020-02-01 09:51:07","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("2070","Modified User ","app\\models\\User","31","1","0","2020-02-01 09:51:59","2020-02-01 09:51:59","0"),
("2071","Created new Login History ","app\\models\\LoginHistory","26","1","0","2020-02-01 09:52:01","2020-02-01 09:52:01","0"),
("2072","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 09:52:02","2020-02-01 09:52:02","31"),
("2073","Created new Login History ","app\\models\\LoginHistory","27","1","0","2020-02-01 09:56:11","2020-02-01 09:56:11","0"),
("2074","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 09:58:33","2020-02-01 09:58:33","31"),
("2075","Created new User ","app\\models\\User","32","1","0","2020-02-01 10:00:57","2020-02-01 10:00:57","1"),
("2076","Modified User ","app\\models\\User","32","1","0","2020-02-01 10:01:20","2020-02-01 10:01:20","1"),
("2077","Created new Comment ","app\\modules\\comment\\models\\Comment","32","1","0","2020-02-01 10:01:22","2020-02-01 10:01:22","1"),
("2078","Modified User ","app\\models\\User","32","1","0","2020-02-01 10:03:44","2020-02-01 10:03:44","0"),
("2079","Created new Login History ","app\\models\\LoginHistory","28","1","0","2020-02-01 10:03:45","2020-02-01 10:03:45","0"),
("2080","Created new Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:03:45","2020-02-01 10:03:45","32"),
("2081","Modified User ","app\\models\\User","31","1","0","2020-02-01 10:03:50","2020-02-01 10:03:50","1"),
("2082","Modified User ","app\\models\\User","31","1","0","2020-02-01 10:06:52","2020-02-01 10:06:52","1"),
("2083","Created new Department ","app\\models\\Department","6","1","0","2020-02-01 10:07:03","2020-02-01 10:07:03","1"),
("2084","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:09:56","2020-02-01 10:09:56","32"),
("2085","Modified User ","app\\models\\User","31","1","0","2020-02-01 10:10:08","2020-02-01 10:10:08","1"),
("2086","Modified User ","app\\models\\User","32","1","0","2020-02-01 10:10:51","2020-02-01 10:10:51","1"),
("2087","Created new User ","app\\models\\User","33","1","0","2020-02-01 10:12:58","2020-02-01 10:12:58","1"),
("2088","Modified User ","app\\models\\User","33","1","0","2020-02-01 10:13:07","2020-02-01 10:13:07","1"),
("2089","Created new Comment ","app\\modules\\comment\\models\\Comment","33","1","0","2020-02-01 10:13:07","2020-02-01 10:13:07","1"),
("2090","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:19:47","2020-02-01 10:19:47","32"),
("2091","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:20:25","2020-02-01 10:20:25","32"),
("2092","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:21:34","2020-02-01 10:21:34","32"),
("2093","Created new Leave Request ","app\\models\\LeaveRequest","20","1","0","2020-02-01 10:23:19","2020-02-01 10:23:19","32"),
("2094","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:29:37","2020-02-01 10:29:37","32"),
("2095","Created new Ticket ","app\\models\\Ticket","13","1","0","2020-02-01 10:29:58","2020-02-01 10:29:58","32"),
("2096","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:30:48","2020-02-01 10:30:48","32"),
("2097","Created new Ticket ","app\\models\\Ticket","14","1","0","2020-02-01 10:31:18","2020-02-01 10:31:18","32"),
("2098","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:32:05","2020-02-01 10:32:05","32"),
("2099","Created new Ticket ","app\\models\\Ticket","15","1","0","2020-02-01 10:32:23","2020-02-01 10:32:23","32"),
("2100","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:34:35","2020-02-01 10:34:35","32"),
("2101","Created new Leave Request ","app\\models\\LeaveRequest","21","1","0","2020-02-01 10:34:51","2020-02-01 10:34:51","32"),
("2102","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:35:23","2020-02-01 10:35:23","32"),
("2103","Created new Ticket ","app\\models\\Ticket","16","1","0","2020-02-01 10:35:39","2020-02-01 10:35:39","32"),
("2104","Created new Designation ","app\\models\\Designation","1","1","0","2020-02-01 10:42:47","2020-02-01 10:42:47","1"),
("2105","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:54:30","2020-02-01 10:54:30","32"),
("2106","Created new Designation ","app\\models\\Designation","1","1","0","2020-02-01 10:54:47","2020-02-01 10:54:47","1"),
("2107","Created new Ticket ","app\\models\\Ticket","17","1","0","2020-02-01 10:55:17","2020-02-01 10:55:17","32"),
("2108","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:55:32","2020-02-01 10:55:32","32"),
("2109","Created new Ticket ","app\\models\\Ticket","18","1","0","2020-02-01 10:55:49","2020-02-01 10:55:49","32"),
("2110","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:57:19","2020-02-01 10:57:19","32"),
("2111","Created new Ticket ","app\\models\\Ticket","19","1","0","2020-02-01 10:58:11","2020-02-01 10:58:11","32"),
("2112","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 10:59:47","2020-02-01 10:59:47","32"),
("2113","Created new Ticket ","app\\models\\Ticket","20","1","0","2020-02-01 10:59:57","2020-02-01 10:59:57","32"),
("2114","Modified User ","app\\models\\User","31","1","0","2020-02-01 11:00:52","2020-02-01 11:00:52","1"),
("2115","Created new Designation ","app\\models\\Designation","2","1","0","2020-02-01 11:01:09","2020-02-01 11:01:09","1"),
("2116","Modified User ","app\\models\\User","30","1","0","2020-02-01 11:01:20","2020-02-01 11:01:20","1"),
("2117","Modified User ","app\\models\\User","30","1","0","2020-02-01 11:01:59","2020-02-01 11:01:59","1"),
("2118","Created new Leave Request ","app\\models\\LeaveRequest","22","1","0","2020-02-01 11:02:03","2020-02-01 11:02:03","31"),
("2119","Modified Leave Request ","app\\models\\LeaveRequest","22","1","0","2020-02-01 11:02:10","2020-02-01 11:02:10","31"),
("2120","Modified Leave Request ","app\\models\\LeaveRequest","22","1","0","2020-02-01 11:02:40","2020-02-01 11:02:40","31"),
("2121","Modified User ","app\\models\\User","30","1","0","2020-02-01 11:03:45","2020-02-01 11:03:45","1"),
("2122","Modified User ","app\\models\\User","33","1","0","2020-02-01 11:05:56","2020-02-01 11:05:56","1"),
("2123","Modified User ","app\\models\\User","32","1","0","2020-02-01 11:06:00","2020-02-01 11:06:00","1"),
("2124","Modified User ","app\\models\\User","31","1","0","2020-02-01 11:06:05","2020-02-01 11:06:05","1"),
("2125","Modified User ","app\\models\\User","29","1","0","2020-02-01 11:06:16","2020-02-01 11:06:16","1"),
("2126","Modified User ","app\\models\\User","28","1","0","2020-02-01 11:06:23","2020-02-01 11:06:23","1"),
("2127","Modified User ","app\\models\\User","28","1","0","2020-02-01 11:06:31","2020-02-01 11:06:31","1"),
("2128","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:10:44","2020-02-01 11:10:44","32"),
("2131","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:15:27","2020-02-01 11:15:27","31"),
("2132","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:16:51","2020-02-01 11:16:51","31"),
("2133","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:19:02","2020-02-01 11:19:02","31"),
("2134","Created new Payroll ","app\\models\\Payroll","2","1","0","2020-02-01 11:21:13","2020-02-01 11:21:13","1"),
("2135","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:21:27","2020-02-01 11:21:27","32"),
("2136","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:21:31","2020-02-01 11:21:31","31"),
("2142","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:24:32","2020-02-01 11:24:32","32"),
("2143","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:24:57","2020-02-01 11:24:57","31"),
("2144","Created new Leave Request ","app\\models\\LeaveRequest","23","1","0","2020-02-01 11:25:04","2020-02-01 11:25:04","32"),
("2145","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:26:24","2020-02-01 11:26:24","31"),
("2147","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:27:56","2020-02-01 11:27:56","32"),
("2148","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:28:47","2020-02-01 11:28:47","32"),
("2149","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:29:27","2020-02-01 11:29:27","32"),
("2152","Created new Leave Request ","app\\models\\LeaveRequest","24","1","0","2020-02-01 11:37:18","2020-02-01 11:37:18","31"),
("2153","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:37:50","2020-02-01 11:37:50","32"),
("2154","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:41:39","2020-02-01 11:41:39","31"),
("2155","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:46:28","2020-02-01 11:46:28","31"),
("2156","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 11:46:47","2020-02-01 11:46:47","31"),
("2157","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:48:04","2020-02-01 11:48:04","32"),
("2158","Created new Ticket ","app\\models\\Ticket","21","1","0","2020-02-01 11:51:51","2020-02-01 11:51:51","31"),
("2159","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:56:16","2020-02-01 11:56:16","32"),
("2160","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:56:54","2020-02-01 11:56:54","32"),
("2161","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 11:58:05","2020-02-01 11:58:05","32"),
("2162","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:07:19","2020-02-01 12:07:19","32"),
("2163","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:08:05","2020-02-01 12:08:05","31"),
("2164","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:08:32","2020-02-01 12:08:32","32"),
("2165","Modified Leave Request ","app\\models\\LeaveRequest","24","1","0","2020-02-01 12:09:16","2020-02-01 12:09:16","31"),
("2166","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:09:36","2020-02-01 12:09:36","32"),
("2167","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:10:21","2020-02-01 12:10:21","32"),
("2168","Modified Leave Request ","app\\models\\LeaveRequest","24","1","0","2020-02-01 12:11:55","2020-02-01 12:11:55","31"),
("2169","Created new Ticket ","app\\models\\Ticket","22","1","0","2020-02-01 12:12:41","2020-02-01 12:12:41","31"),
("2170","Created new Ticket ","app\\models\\Ticket","23","1","0","2020-02-01 12:13:14","2020-02-01 12:13:14","31"),
("2171","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:22:01","2020-02-01 12:22:01","31"),
("2172","Modified Leave Request ","app\\models\\LeaveRequest","24","1","0","2020-02-01 12:22:15","2020-02-01 12:22:15","31"),
("2173","Modified Leave Request ","app\\models\\LeaveRequest","23","1","0","2020-02-01 12:22:23","2020-02-01 12:22:23","31"),
("2174","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:23:38","2020-02-01 12:23:38","32"),
("2175","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:23:47","2020-02-01 12:23:47","31"),
("2176","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:27:47","2020-02-01 12:27:47","32"),
("2177","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:29:24","2020-02-01 12:29:24","32"),
("2178","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:31:14","2020-02-01 12:31:14","32"),
("2179","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:32:17","2020-02-01 12:32:17","32");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("2180","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:33:18","2020-02-01 12:33:18","32"),
("2181","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:34:31","2020-02-01 12:34:31","31"),
("2182","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:38:56","2020-02-01 12:38:56","32"),
("2183","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:41:06","2020-02-01 12:41:06","32"),
("2184","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:42:33","2020-02-01 12:42:33","32"),
("2185","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:44:17","2020-02-01 12:44:17","32"),
("2186","Created new Ticket ","app\\models\\Ticket","24","1","0","2020-02-01 12:44:31","2020-02-01 12:44:31","32"),
("2187","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:45:46","2020-02-01 12:45:46","31"),
("2188","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:46:10","2020-02-01 12:46:10","31"),
("2189","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:52:20","2020-02-01 12:52:20","31"),
("2190","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:54:41","2020-02-01 12:54:41","31"),
("2191","Created new Comment ","app\\modules\\comment\\models\\Comment","1","1","0","2020-02-01 12:57:27","2020-02-01 12:57:27","31"),
("2192","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 12:58:02","2020-02-01 12:58:02","31"),
("2193","Created new Comment ","app\\modules\\comment\\models\\Comment","2","1","0","2020-02-01 12:58:10","2020-02-01 12:58:10","31"),
("2194","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 12:58:52","2020-02-01 12:58:52","32"),
("2195","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:03:52","2020-02-01 13:03:52","32"),
("2196","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:06:34","2020-02-01 13:06:34","31"),
("2197","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:07:17","2020-02-01 13:07:17","31"),
("2198","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:08:17","2020-02-01 13:08:17","31"),
("2199","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:08:36","2020-02-01 13:08:36","32"),
("2200","Modified Leave Request ","app\\models\\LeaveRequest","24","1","0","2020-02-01 13:08:41","2020-02-01 13:08:41","32"),
("2201","Modified Leave Request ","app\\models\\LeaveRequest","23","1","0","2020-02-01 13:09:06","2020-02-01 13:09:06","32"),
("2202","Modified Leave Request ","app\\models\\LeaveRequest","22","1","0","2020-02-01 13:09:45","2020-02-01 13:09:45","32"),
("2203","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:10:05","2020-02-01 13:10:05","31"),
("2204","Modified Leave Request ","app\\models\\LeaveRequest","22","1","0","2020-02-01 13:12:14","2020-02-01 13:12:14","32"),
("2205","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:12:35","2020-02-01 13:12:35","32"),
("2206","Modified Leave Request ","app\\models\\LeaveRequest","21","1","0","2020-02-01 13:12:41","2020-02-01 13:12:41","32"),
("2207","Modified Leave Request ","app\\models\\LeaveRequest","20","1","0","2020-02-01 13:13:40","2020-02-01 13:13:40","32"),
("2208","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:20:30","2020-02-01 13:20:30","32"),
("2209","Modified Leave Request ","app\\models\\LeaveRequest","21","1","0","2020-02-01 13:20:39","2020-02-01 13:20:39","32"),
("2210","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:21:01","2020-02-01 13:21:01","32"),
("2211","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:21:22","2020-02-01 13:21:22","32"),
("2212","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:29:29","2020-02-01 13:29:29","31"),
("2213","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:32:15","2020-02-01 13:32:15","31"),
("2214","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:33:38","2020-02-01 13:33:38","31"),
("2215","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:34:18","2020-02-01 13:34:18","31"),
("2216","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 13:34:58","2020-02-01 13:34:58","31"),
("2217","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:47:18","2020-02-01 13:47:18","32"),
("2218","Created new Comment ","app\\modules\\comment\\models\\Comment","3","1","0","2020-02-01 13:47:44","2020-02-01 13:47:44","32"),
("2219","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:48:34","2020-02-01 13:48:34","32"),
("2220","Created new Comment ","app\\modules\\comment\\models\\Comment","4","1","0","2020-02-01 13:48:44","2020-02-01 13:48:44","32"),
("2221","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:52:36","2020-02-01 13:52:36","32"),
("2222","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 13:57:07","2020-02-01 13:57:07","32"),
("2223","Created new Login History ","app\\models\\LoginHistory","29","1","0","2020-02-01 14:42:15","2020-02-01 14:42:15","0"),
("2224","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 14:47:46","2020-02-01 14:47:46","32"),
("2225","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 14:48:36","2020-02-01 14:48:36","32"),
("2226","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 14:50:02","2020-02-01 14:50:02","32"),
("2227","Created new Login History ","app\\models\\LoginHistory","30","1","0","2020-02-01 14:54:02","2020-02-01 14:54:02","0"),
("2228","Created new Login History ","app\\models\\LoginHistory","31","1","0","2020-02-01 14:54:24","2020-02-01 14:54:24","0"),
("2229","Created new Login History ","app\\models\\LoginHistory","32","1","0","2020-02-01 14:54:31","2020-02-01 14:54:31","0"),
("2230","Created new Login History ","app\\models\\LoginHistory","33","1","0","2020-02-01 14:55:03","2020-02-01 14:55:03","0"),
("2231","Created new Login History ","app\\models\\LoginHistory","34","1","0","2020-02-01 14:55:10","2020-02-01 14:55:10","0"),
("2232","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 14:57:14","2020-02-01 14:57:14","31"),
("2233","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 14:58:44","2020-02-01 14:58:44","31"),
("2235","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 15:05:36","2020-02-01 15:05:36","31"),
("2236","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 15:05:54","2020-02-01 15:05:54","31"),
("2237","Created new Login History ","app\\models\\LoginHistory","35","1","0","2020-02-01 15:08:47","2020-02-01 15:08:47","0"),
("2238","Created new Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-01 15:09:04","2020-02-01 15:09:04","33"),
("2239","Created new Platform ","app\\models\\Platform","5","1","0","2020-02-01 15:17:05","2020-02-01 15:17:05","1"),
("2241","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:17:41","2020-02-01 15:17:41","32"),
("2242","Created new Ticket ","app\\models\\Ticket","25","1","0","2020-02-01 15:19:54","2020-02-01 15:19:54","31"),
("2243","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:20:01","2020-02-01 15:20:01","32"),
("2244","Created new Comment ","app\\modules\\comment\\models\\Comment","5","1","0","2020-02-01 15:20:32","2020-02-01 15:20:32","32"),
("2245","Created new Ticket ","app\\models\\Ticket","26","1","0","2020-02-01 15:20:56","2020-02-01 15:20:56","33"),
("2246","Modified Leave Request ","app\\models\\LeaveRequest","21","1","0","2020-02-01 15:21:45","2020-02-01 15:21:45","32"),
("2247","Created new Comment ","app\\modules\\comment\\models\\Comment","6","1","0","2020-02-01 15:21:53","2020-02-01 15:21:53","32"),
("2248","Created new Comment ","app\\modules\\comment\\models\\Comment","7","1","0","2020-02-01 15:22:06","2020-02-01 15:22:06","32"),
("2249","Created new Comment ","app\\modules\\comment\\models\\Comment","8","1","0","2020-02-01 15:22:46","2020-02-01 15:22:46","32"),
("2250","Created new Comment ","app\\modules\\comment\\models\\Comment","9","1","0","2020-02-01 15:23:26","2020-02-01 15:23:26","32"),
("2251","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 15:24:03","2020-02-01 15:24:03","31"),
("2252","Created new Payroll ","app\\models\\Payroll","3","1","0","2020-02-01 15:26:44","2020-02-01 15:26:44","1"),
("2253","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 15:28:37","2020-02-01 15:28:37","31"),
("2254","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:30:11","2020-02-01 15:30:11","32"),
("2255","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:30:36","2020-02-01 15:30:36","32"),
("2256","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:31:27","2020-02-01 15:31:27","32"),
("2257","Created new Comment ","app\\modules\\comment\\models\\Comment","10","1","0","2020-02-01 15:31:54","2020-02-01 15:31:54","32"),
("2258","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:32:27","2020-02-01 15:32:27","32"),
("2259","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:33:29","2020-02-01 15:33:29","32"),
("2260","Created new Comment ","app\\modules\\comment\\models\\Comment","11","1","0","2020-02-01 15:33:42","2020-02-01 15:33:42","32"),
("2261","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:34:32","2020-02-01 15:34:32","32"),
("2262","Created new Comment ","app\\modules\\comment\\models\\Comment","12","1","0","2020-02-01 15:35:16","2020-02-01 15:35:16","32"),
("2263","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:35:33","2020-02-01 15:35:33","32"),
("2264","Created new Comment ","app\\modules\\comment\\models\\Comment","13","1","0","2020-02-01 15:36:10","2020-02-01 15:36:10","32"),
("2265","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:38:43","2020-02-01 15:38:43","32"),
("2266","Created new Leave Request ","app\\models\\LeaveRequest","26","1","0","2020-02-01 15:39:18","2020-02-01 15:39:18","32"),
("2267","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:39:41","2020-02-01 15:39:41","32"),
("2268","Created new Comment ","app\\modules\\comment\\models\\Comment","14","1","0","2020-02-01 15:39:50","2020-02-01 15:39:50","32"),
("2269","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:40:30","2020-02-01 15:40:30","32"),
("2270","Created new Leave Request ","app\\models\\LeaveRequest","27","1","0","2020-02-01 15:40:43","2020-02-01 15:40:43","32"),
("2271","Created new Comment ","app\\modules\\comment\\models\\Comment","15","1","0","2020-02-01 15:41:04","2020-02-01 15:41:04","32"),
("2272","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:43:19","2020-02-01 15:43:19","32"),
("2273","Created new Comment ","app\\modules\\comment\\models\\Comment","16","1","0","2020-02-01 15:43:38","2020-02-01 15:43:38","32"),
("2274","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:44:45","2020-02-01 15:44:45","32"),
("2275","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:47:42","2020-02-01 15:47:42","32"),
("2276","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:48:56","2020-02-01 15:48:56","32"),
("2277","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:49:09","2020-02-01 15:49:09","32"),
("2278","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:50:38","2020-02-01 15:50:38","32"),
("2279","Created new Comment ","app\\modules\\comment\\models\\Comment","17","1","0","2020-02-01 15:50:59","2020-02-01 15:50:59","32"),
("2280","Created new Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 15:51:15","2020-02-01 15:51:15","32"),
("2281","Created new Comment ","app\\modules\\comment\\models\\Comment","18","1","0","2020-02-01 15:51:44","2020-02-01 15:51:44","32");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("2282","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 15:52:15","2020-02-01 15:52:15","31"),
("2283","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 15:52:38","2020-02-01 15:52:38","32"),
("2284","Created new Comment ","app\\modules\\comment\\models\\Comment","19","1","0","2020-02-01 15:52:46","2020-02-01 15:52:46","32"),
("2285","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 15:52:51","2020-02-01 15:52:51","32"),
("2286","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:01:40","2020-02-01 16:01:40","32"),
("2287","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:02:19","2020-02-01 16:02:19","32"),
("2288","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:05:29","2020-02-01 16:05:29","32"),
("2289","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:09:15","2020-02-01 16:09:15","32"),
("2290","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/1\">File</a>","app\\modules\\file\\models\\File","1","1","0","2020-02-01 16:23:28","2020-02-01 16:23:28","1"),
("2291","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/2\">File</a>","app\\modules\\file\\models\\File","2","1","0","2020-02-01 16:23:28","2020-02-01 16:23:28","1"),
("2292","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/3\">File</a>","app\\modules\\file\\models\\File","3","1","0","2020-02-01 16:23:28","2020-02-01 16:23:28","1"),
("2293","Modified Leave Request ","app\\models\\LeaveRequest","24","1","0","2020-02-01 16:27:20","2020-02-01 16:27:20","31"),
("2294","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:33:06","2020-02-01 16:33:06","32"),
("2295","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:33:15","2020-02-01 16:33:15","32"),
("2296","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:41:05","2020-02-01 16:41:05","32"),
("2297","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 16:42:29","2020-02-01 16:42:29","31"),
("2298","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:48:02","2020-02-01 16:48:02","32"),
("2299","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 16:51:55","2020-02-01 16:51:55","32"),
("2300","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:04:38","2020-02-01 17:04:38","32"),
("2302","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:05:45","2020-02-01 17:05:45","32"),
("2303","Added new Attendance : <a href=\"http://localhost/argus-app-yii2-1275/attendance/view/7/2020-02-01\">2020-02-01</a>","app\\models\\Attendance","7","1","0","2020-02-01 17:09:23","2020-02-01 17:09:23","1"),
("2304","Added new Attendance : <a href=\"http://localhost/argus-app-yii2-1275/attendance/view/1/2020-02-01\">2020-02-01</a>","app\\models\\Attendance","1","1","0","2020-02-01 17:13:01","2020-02-01 17:13:01","1"),
("2305","Added new Attendance Event : <a href=\"http://localhost/argus-app-yii2-1275/attendance-event/view/3/2020-02-01\">2020-02-01</a>","app\\models\\AttendanceEvent","3","1","0","2020-02-01 17:13:10","2020-02-01 17:13:10","1"),
("2306","Added new Attendance Event : <a href=\"http://localhost/argus-app-yii2-1275/attendance-event/view/4/2020-02-01\">2020-02-01</a>","app\\models\\AttendanceEvent","4","1","0","2020-02-01 17:13:10","2020-02-01 17:13:10","1"),
("2307","Modified Attendance ","app\\models\\Attendance","1","1","0","2020-02-01 17:13:10","2020-02-01 17:13:10","1"),
("2308","Added new Attendance Event : <a href=\"http://localhost/argus-app-yii2-1275/attendance-event/view/1/2020-02-01\">2020-02-01</a>","app\\models\\AttendanceEvent","1","1","0","2020-02-01 17:14:10","2020-02-01 17:14:10","1"),
("2309","Added new Attendance Event : <a href=\"http://localhost/argus-app-yii2-1275/attendance-event/view/2/2020-02-01\">2020-02-01</a>","app\\models\\AttendanceEvent","2","1","0","2020-02-01 17:14:10","2020-02-01 17:14:10","1"),
("2310","Modified Attendance ","app\\models\\Attendance","1","1","0","2020-02-01 17:14:10","2020-02-01 17:14:10","1"),
("2311","Modified Attendance ","app\\models\\Attendance","1","1","0","2020-02-01 17:14:47","2020-02-01 17:14:47","1"),
("2312","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:16:50","2020-02-01 17:16:50","32"),
("2313","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:17:14","2020-02-01 17:17:14","32"),
("2314","Modified Attendance ","app\\models\\Attendance","1","1","0","2020-02-01 17:18:00","2020-02-01 17:18:00","1"),
("2315","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 17:18:50","2020-02-01 17:18:50","31"),
("2316","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:18:56","2020-02-01 17:18:56","32"),
("2317","Modified Attendance ","app\\models\\Attendance","1","1","0","2020-02-01 17:19:01","2020-02-01 17:19:01","1"),
("2318","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:19:13","2020-02-01 17:19:13","32"),
("2319","Modified Attendance ","app\\models\\Attendance","1","1","0","2020-02-01 17:19:39","2020-02-01 17:19:39","1"),
("2320","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:26:38","2020-02-01 17:26:38","32"),
("2321","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=20&amp;title=sefhjk\">sefhjk</a>","app\\modules\\comment\\models\\Comment","20","1","0","2020-02-01 17:26:57","2020-02-01 17:26:57","32"),
("2323","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:34:17","2020-02-01 17:34:17","32"),
("2324","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:34:32","2020-02-01 17:34:32","32"),
("2325","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 17:34:54","2020-02-01 17:34:54","31"),
("2326","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 17:35:10","2020-02-01 17:35:10","31"),
("2327","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 17:35:31","2020-02-01 17:35:31","31"),
("2328","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 17:36:06","2020-02-01 17:36:06","31"),
("2329","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","1","1","0","2020-02-01 17:38:03","2020-02-01 17:38:03","31"),
("2330","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:41:40","2020-02-01 17:41:40","32"),
("2331","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:42:25","2020-02-01 17:42:25","32"),
("2332","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:42:31","2020-02-01 17:42:31","32"),
("2333","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:42:58","2020-02-01 17:42:58","32"),
("2334","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:44:11","2020-02-01 17:44:11","32"),
("2335","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:44:49","2020-02-01 17:44:49","32"),
("2336","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:45:01","2020-02-01 17:45:01","32"),
("2337","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:45:49","2020-02-01 17:45:49","32"),
("2339","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:48:53","2020-02-01 17:48:53","32"),
("2340","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:49:05","2020-02-01 17:49:05","32"),
("2341","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:49:16","2020-02-01 17:49:16","32"),
("2343","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/6\">File</a>","app\\modules\\file\\models\\File","6","1","0","2020-02-01 17:49:21","2020-02-01 17:49:21","33"),
("2345","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/8\">File</a>","app\\modules\\file\\models\\File","8","1","0","2020-02-01 17:49:21","2020-02-01 17:49:21","33"),
("2346","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/9\">File</a>","app\\modules\\file\\models\\File","9","1","0","2020-02-01 17:49:21","2020-02-01 17:49:21","33"),
("2348","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:50:07","2020-02-01 17:50:07","32"),
("2349","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:52:13","2020-02-01 17:52:13","32"),
("2350","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:52:26","2020-02-01 17:52:26","32"),
("2351","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:53:53","2020-02-01 17:53:53","32"),
("2352","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:54:05","2020-02-01 17:54:05","32"),
("2353","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 17:58:23","2020-02-01 17:58:23","32"),
("2354","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 17:58:35","2020-02-01 17:58:35","32"),
("2355","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:00:45","2020-02-01 18:00:45","32"),
("2356","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:05:10","2020-02-01 18:05:10","32"),
("2357","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 18:05:28","2020-02-01 18:05:28","32"),
("2358","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 18:05:51","2020-02-01 18:05:51","32"),
("2359","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 18:06:09","2020-02-01 18:06:09","32"),
("2360","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-01 18:06:19","2020-02-01 18:06:19","32"),
("2361","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:14:56","2020-02-01 18:14:56","32"),
("2362","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:16:04","2020-02-01 18:16:04","32"),
("2363","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:18:18","2020-02-01 18:18:18","32"),
("2364","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:19:08","2020-02-01 18:19:08","32"),
("2365","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:20:24","2020-02-01 18:20:24","32"),
("2366","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:23:33","2020-02-01 18:23:33","32"),
("2367","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:24:33","2020-02-01 18:24:33","32"),
("2368","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:29:20","2020-02-01 18:29:20","32"),
("2369","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:32:07","2020-02-01 18:32:07","32"),
("2370","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:37:10","2020-02-01 18:37:10","32"),
("2371","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:40:31","2020-02-01 18:40:31","32"),
("2372","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-01 18:49:00","2020-02-01 18:49:00","32"),
("2373","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/36/1\">1</a>","app\\models\\LoginHistory","36","1","0","2020-02-03 09:37:21","2020-02-03 09:37:21","0"),
("2374","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/37/1\">1</a>","app\\models\\LoginHistory","37","1","0","2020-02-03 09:40:35","2020-02-03 09:40:35","0"),
("2375","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/38/1\">1</a>","app\\models\\LoginHistory","38","1","0","2020-02-03 09:41:01","2020-02-03 09:41:01","0"),
("2376","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/39/1\">1</a>","app\\models\\LoginHistory","39","1","0","2020-02-03 09:41:11","2020-02-03 09:41:11","0"),
("2377","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/40/30\">30</a>","app\\models\\LoginHistory","40","1","0","2020-02-03 09:45:53","2020-02-03 09:45:53","0"),
("2378","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=21&amp;title=state-changed-inactive-to-active\">State Changed : Inactive to Active</a>","app\\modules\\comment\\models\\Comment","21","1","0","2020-02-03 09:45:56","2020-02-03 09:45:56","1"),
("2379","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/41/30\">30</a>","app\\models\\LoginHistory","41","1","0","2020-02-03 09:46:01","2020-02-03 09:46:01","0"),
("2382","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/12\">File</a>","app\\modules\\file\\models\\File","12","1","0","2020-02-03 09:46:56","2020-02-03 09:46:56","30"),
("2383","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/13\">File</a>","app\\modules\\file\\models\\File","13","1","0","2020-02-03 09:47:35","2020-02-03 09:47:35","30"),
("2384","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","2","1","0","2020-02-03 09:48:26","2020-02-03 09:48:26","32"),
("2385","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/14\">File</a>","app\\modules\\file\\models\\File","14","1","0","2020-02-03 09:51:57","2020-02-03 09:51:57","30"),
("2386","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/15\">File</a>","app\\modules\\file\\models\\File","15","1","0","2020-02-03 09:52:12","2020-02-03 09:52:12","30"),
("2387","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/16\">File</a>","app\\modules\\file\\models\\File","16","1","0","2020-02-03 09:53:47","2020-02-03 09:53:47","30"),
("2388","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/17\">File</a>","app\\modules\\file\\models\\File","17","1","0","2020-02-03 09:54:31","2020-02-03 09:54:31","30"),
("2389","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 09:58:49","2020-02-03 09:58:49","32");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("2390","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/42/29\">29</a>","app\\models\\LoginHistory","42","1","0","2020-02-03 10:10:18","2020-02-03 10:10:18","0"),
("2391","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/27/regarding-salary-slip\">Regarding Salary Slip</a>","app\\models\\Ticket","27","1","0","2020-02-03 10:15:41","2020-02-03 10:15:41","29"),
("2392","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/43/32\">32</a>","app\\models\\LoginHistory","43","1","0","2020-02-03 10:19:06","2020-02-03 10:19:06","0"),
("2393","Modified User ","app\\models\\User","32","1","0","2020-02-03 10:19:07","2020-02-03 10:19:07","32"),
("2394","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/3/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","3","1","0","2020-02-03 10:19:07","2020-02-03 10:19:07","32"),
("2395","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/44/32\">32</a>","app\\models\\LoginHistory","44","1","0","2020-02-03 10:19:10","2020-02-03 10:19:10","0"),
("2396","Modified User ","app\\models\\User","32","1","0","2020-02-03 10:19:10","2020-02-03 10:19:10","32"),
("2397","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/4/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","4","1","0","2020-02-03 10:19:10","2020-02-03 10:19:10","32"),
("2398","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/45/32\">32</a>","app\\models\\LoginHistory","45","1","0","2020-02-03 10:19:21","2020-02-03 10:19:21","0"),
("2399","Modified User ","app\\models\\User","32","1","0","2020-02-03 10:19:21","2020-02-03 10:19:21","32"),
("2400","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/5/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","5","1","0","2020-02-03 10:19:21","2020-02-03 10:19:21","32"),
("2401","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/46/32\">32</a>","app\\models\\LoginHistory","46","1","0","2020-02-03 10:20:29","2020-02-03 10:20:29","0"),
("2402","Modified User ","app\\models\\User","32","1","0","2020-02-03 10:20:29","2020-02-03 10:20:29","32"),
("2403","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/6/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:20:29","2020-02-03 10:20:29","32"),
("2404","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:22:00","2020-02-03 10:22:00","32"),
("2405","Modified Leave Request ","app\\models\\LeaveRequest","19","1","0","2020-02-03 10:22:56","2020-02-03 10:22:56","29"),
("2406","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=22&amp;title=state-changed-pending-to-approved\">State Changed : Pending to Approved</a>","app\\modules\\comment\\models\\Comment","22","1","0","2020-02-03 10:23:34","2020-02-03 10:23:34","1"),
("2407","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:23:48","2020-02-03 10:23:48","32"),
("2408","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:24:11","2020-02-03 10:24:11","32"),
("2409","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:25:49","2020-02-03 10:25:49","32"),
("2410","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=23&amp;title=hopl\">hopl</a>","app\\modules\\comment\\models\\Comment","23","1","0","2020-02-03 10:26:15","2020-02-03 10:26:15","32"),
("2411","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=24&amp;title=dasdas\">dasdas</a>","app\\modules\\comment\\models\\Comment","24","1","0","2020-02-03 10:26:53","2020-02-03 10:26:53","29"),
("2412","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=25&amp;title=leave-approved-after-the-discussion-with-manager\">Leave Approved after the discussion with manager</a>","app\\modules\\comment\\models\\Comment","25","1","0","2020-02-03 10:28:55","2020-02-03 10:28:55","1"),
("2414","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=26&amp;title=state-changed-assigned-to-inprogress\">State Changed : Assigned to InProgress</a>","app\\modules\\comment\\models\\Comment","26","1","0","2020-02-03 10:34:17","2020-02-03 10:34:17","1"),
("2415","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=27&amp;title=state-changed-inprogress-to-rejected\">State Changed : InProgress to Rejected</a>","app\\modules\\comment\\models\\Comment","27","1","0","2020-02-03 10:34:24","2020-02-03 10:34:24","1"),
("2416","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=28&amp;title=state-changed-rejected-to-inprogress\">State Changed : Rejected to InProgress</a>","app\\modules\\comment\\models\\Comment","28","1","0","2020-02-03 10:34:29","2020-02-03 10:34:29","1"),
("2417","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=29&amp;title=state-changed-inprogress-to-complete\">State Changed : InProgress to Complete</a>","app\\modules\\comment\\models\\Comment","29","1","0","2020-02-03 10:34:32","2020-02-03 10:34:32","1"),
("2418","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:36:32","2020-02-03 10:36:32","32"),
("2419","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:37:09","2020-02-03 10:37:09","32"),
("2420","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:37:29","2020-02-03 10:37:29","32"),
("2421","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=30&amp;title=state-changed-complete-to-rejected\">State Changed : Complete to Rejected</a>","app\\modules\\comment\\models\\Comment","30","1","0","2020-02-03 10:46:49","2020-02-03 10:46:49","1"),
("2422","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=31&amp;title=state-changed-rejected-to-complete\">State Changed : Rejected to Complete</a>","app\\modules\\comment\\models\\Comment","31","1","0","2020-02-03 10:46:50","2020-02-03 10:46:50","1"),
("2423","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:50:28","2020-02-03 10:50:28","32"),
("2424","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:51:02","2020-02-03 10:51:02","32"),
("2425","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:51:50","2020-02-03 10:51:50","32"),
("2426","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:52:17","2020-02-03 10:52:17","32"),
("2427","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:53:08","2020-02-03 10:53:08","32"),
("2428","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:53:43","2020-02-03 10:53:43","32"),
("2429","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 10:59:34","2020-02-03 10:59:34","32"),
("2430","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:00:52","2020-02-03 11:00:52","32"),
("2431","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 11:01:14","2020-02-03 11:01:14","32"),
("2432","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:01:47","2020-02-03 11:01:47","32"),
("2433","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 11:01:56","2020-02-03 11:01:56","32"),
("2434","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 11:02:06","2020-02-03 11:02:06","32"),
("2435","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:02:24","2020-02-03 11:02:24","32"),
("2436","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:06:13","2020-02-03 11:06:13","32"),
("2437","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:07:26","2020-02-03 11:07:26","32"),
("2438","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 11:07:38","2020-02-03 11:07:38","32"),
("2439","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:08:49","2020-02-03 11:08:49","32"),
("2440","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 11:08:57","2020-02-03 11:08:57","32"),
("2441","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 11:09:00","2020-02-03 11:09:00","32"),
("2442","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 11:09:50","2020-02-03 11:09:50","32"),
("2443","Modified Leave Request ","app\\models\\LeaveRequest","25","1","0","2020-02-03 11:09:53","2020-02-03 11:09:53","32"),
("2444","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:13:19","2020-02-03 11:13:19","32"),
("2445","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-03 11:13:38","2020-02-03 11:13:38","32"),
("2446","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:20:19","2020-02-03 11:20:19","32"),
("2447","Added new Leave Request : <a href=\"http://localhost/argus-app-yii2-1275/leave-request/view/30/admin\">Admin</a>","app\\models\\LeaveRequest","30","1","0","2020-02-03 11:20:52","2020-02-03 11:20:52","1"),
("2448","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-03 11:21:55","2020-02-03 11:21:55","32"),
("2449","Modified Leave Request ","app\\models\\LeaveRequest","30","1","0","2020-02-03 11:22:46","2020-02-03 11:22:46","1"),
("2450","Modified Leave Request ","app\\models\\LeaveRequest","30","1","0","2020-02-03 11:22:54","2020-02-03 11:22:54","1"),
("2451","Modified Leave Request ","app\\models\\LeaveRequest","30","1","0","2020-02-03 11:23:02","2020-02-03 11:23:02","1"),
("2452","Modified Leave Request ","app\\models\\LeaveRequest","30","1","0","2020-02-03 11:23:10","2020-02-03 11:23:10","1"),
("2453","Modified Leave Request ","app\\models\\LeaveRequest","30","1","0","2020-02-03 11:30:31","2020-02-03 11:30:31","1"),
("2459","Modified Ticket ","app\\models\\Ticket","27","1","0","2020-02-03 13:12:56","2020-02-03 13:12:56","29"),
("2460","Modified Ticket ","app\\models\\Ticket","12","1","0","2020-02-03 13:25:42","2020-02-03 13:25:42","29"),
("2461","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=32&amp;title=\"></a>","app\\modules\\comment\\models\\Comment","32","1","0","2020-02-03 14:46:10","2020-02-03 14:46:10","31"),
("2462","Added new Files : <a href=\"http://localhost/argus-app-yii2-1275/files/view/19\"></a>","app\\models\\Files","19","1","0","2020-02-03 14:52:59","2020-02-03 14:52:59","31"),
("2463","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=33&amp;title=hfghfile-uploaded\">hfgh<br/><br/>File uploaded <a href=\"http://localhost/argus-app-yii2-1275/files/view/19\"></a></a>","app\\modules\\comment\\models\\Comment","33","1","0","2020-02-03 14:52:59","2020-02-03 14:52:59","31"),
("2464","Added new Files : <a href=\"http://localhost/argus-app-yii2-1275/files/view/20\"></a>","app\\models\\Files","20","1","0","2020-02-03 14:53:05","2020-02-03 14:53:05","31"),
("2465","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=34&amp;title=file-uploaded\"><br/><br/>File uploaded <a href=\"http://localhost/argus-app-yii2-1275/files/view/20\"></a></a>","app\\modules\\comment\\models\\Comment","34","1","0","2020-02-03 14:53:05","2020-02-03 14:53:05","31"),
("2466","Added new Files : <a href=\"http://localhost/argus-app-yii2-1275/files/view/21\"></a>","app\\models\\Files","21","1","0","2020-02-03 14:53:15","2020-02-03 14:53:15","31"),
("2467","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=35&amp;title=asdsadfile-uploaded\">asdsad<br/><br/>File uploaded <a href=\"http://localhost/argus-app-yii2-1275/files/view/21\"></a></a>","app\\modules\\comment\\models\\Comment","35","1","0","2020-02-03 14:53:15","2020-02-03 14:53:15","31"),
("2468","Added new Leave Request : <a href=\"http://localhost/argus-app-yii2-1275/leave-request/view/31/lalit-employee\">lalit Employee</a>","app\\models\\LeaveRequest","31","1","0","2020-02-03 16:56:57","2020-02-03 16:56:57","31"),
("2471","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/47/29\">29</a>","app\\models\\LoginHistory","47","1","0","2020-02-04 09:27:02","2020-02-04 09:27:02","0"),
("2472","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/48/1\">1</a>","app\\models\\LoginHistory","48","1","0","2020-02-04 09:50:02","2020-02-04 09:50:02","0"),
("2473","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/49/30\">30</a>","app\\models\\LoginHistory","49","1","0","2020-02-04 09:50:12","2020-02-04 09:50:12","0"),
("2474","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/50/29\">29</a>","app\\models\\LoginHistory","50","1","0","2020-02-04 09:52:49","2020-02-04 09:52:49","0"),
("2475","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/51/1\">1</a>","app\\models\\LoginHistory","51","1","0","2020-02-04 10:04:12","2020-02-04 10:04:12","0"),
("2476","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/52/1\">1</a>","app\\models\\LoginHistory","52","1","0","2020-02-04 10:04:13","2020-02-04 10:04:13","0"),
("2477","Modified User ","app\\models\\User","29","1","0","2020-02-04 10:05:59","2020-02-04 10:05:59","0"),
("2478","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/53/29\">29</a>","app\\models\\LoginHistory","53","1","0","2020-02-04 10:05:59","2020-02-04 10:05:59","0"),
("2479","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/54/29\">29</a>","app\\models\\LoginHistory","54","1","0","2020-02-04 10:06:29","2020-02-04 10:06:29","0"),
("2480","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/55/29\">29</a>","app\\models\\LoginHistory","55","1","0","2020-02-04 10:06:48","2020-02-04 10:06:48","0"),
("2481","Modified User ","app\\models\\User","29","1","0","2020-02-04 10:07:11","2020-02-04 10:07:11","1"),
("2482","Modified User ","app\\models\\User","29","1","0","2020-02-04 10:07:32","2020-02-04 10:07:32","1"),
("2483","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/56/29\">29</a>","app\\models\\LoginHistory","56","1","0","2020-02-04 10:07:37","2020-02-04 10:07:37","0"),
("2484","Modified User ","app\\models\\User","29","1","0","2020-02-04 10:07:37","2020-02-04 10:07:37","29"),
("2485","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/7/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:07:37","2020-02-04 10:07:37","29"),
("2486","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:20:59","2020-02-04 10:20:59","29"),
("2487","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:22:13","2020-02-04 10:22:13","29"),
("2488","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:25:58","2020-02-04 10:25:58","29"),
("2489","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:29:05","2020-02-04 10:29:05","29"),
("2490","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:30:50","2020-02-04 10:30:50","29"),
("2491","Added new Leave Request : <a href=\"http://192.168.2.143/argus-app-yii2-1275/leave-request/view/32/lalit-employee\">lalit Employee</a>","app\\models\\LeaveRequest","32","1","0","2020-02-04 10:33:18","2020-02-04 10:33:18","29"),
("2492","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:41:36","2020-02-04 10:41:36","29"),
("2493","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:42:58","2020-02-04 10:42:58","29"),
("2494","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:44:56","2020-02-04 10:44:56","29"),
("2495","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:45:35","2020-02-04 10:45:35","29"),
("2496","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","7","1","0","2020-02-04 10:50:03","2020-02-04 10:50:03","29"),
("2497","Modified Leave Request ","app\\models\\LeaveRequest","32","1","0","2020-02-04 10:50:15","2020-02-04 10:50:15","29");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("2498","Modified User ","app\\models\\User","29","1","0","2020-02-04 10:50:46","2020-02-04 10:50:46","0"),
("2499","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/57/31\">31</a>","app\\models\\LoginHistory","57","1","0","2020-02-04 10:51:01","2020-02-04 10:51:01","0"),
("2500","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/58/31\">31</a>","app\\models\\LoginHistory","58","1","0","2020-02-04 10:51:08","2020-02-04 10:51:08","0"),
("2501","Modified User ","app\\models\\User","31","1","0","2020-02-04 10:51:08","2020-02-04 10:51:08","31"),
("2502","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/8/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","8","1","0","2020-02-04 10:51:08","2020-02-04 10:51:08","31"),
("2503","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","8","1","0","2020-02-04 10:54:09","2020-02-04 10:54:09","31"),
("2504","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","8","1","0","2020-02-04 10:57:40","2020-02-04 10:57:40","31"),
("2505","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","8","1","0","2020-02-04 10:58:08","2020-02-04 10:58:08","31"),
("2506","Modified Leave Request ","app\\models\\LeaveRequest","30","1","0","2020-02-04 11:15:53","2020-02-04 11:15:53","31"),
("2507","Modified User ","app\\models\\User","31","1","0","2020-02-04 11:16:05","2020-02-04 11:16:05","0"),
("2508","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/59/29\">29</a>","app\\models\\LoginHistory","59","1","0","2020-02-04 11:16:30","2020-02-04 11:16:30","0"),
("2509","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/60/29\">29</a>","app\\models\\LoginHistory","60","1","0","2020-02-04 11:16:38","2020-02-04 11:16:38","0"),
("2510","Modified User ","app\\models\\User","29","1","0","2020-02-04 11:16:38","2020-02-04 11:16:38","29"),
("2511","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/9/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 11:16:38","2020-02-04 11:16:38","29"),
("2512","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 11:18:36","2020-02-04 11:18:36","29"),
("2513","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 11:19:44","2020-02-04 11:19:44","29"),
("2514","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 11:33:18","2020-02-04 11:33:18","29"),
("2516","Modified Leave Request ","app\\models\\LeaveRequest","32","1","0","2020-02-04 11:37:10","2020-02-04 11:37:10","29"),
("2517","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 11:44:45","2020-02-04 11:44:45","29"),
("2522","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 11:53:24","2020-02-04 11:53:24","29"),
("2525","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 11:55:52","2020-02-04 11:55:52","29"),
("2526","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 11:59:18","2020-02-04 11:59:18","29"),
("2527","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=38&amp;title=ghghg\">Ghghg</a>","app\\modules\\comment\\models\\Comment","38","1","0","2020-02-04 11:59:35","2020-02-04 11:59:35","29"),
("2528","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=39&amp;title=ghghg\">Ghghg</a>","app\\modules\\comment\\models\\Comment","39","1","0","2020-02-04 11:59:49","2020-02-04 11:59:49","29"),
("2529","Added new Ticket : <a href=\"http://192.168.2.143/argus-app-yii2-1275/ticket/view/28/fgfg\">fgfg</a>","app\\models\\Ticket","28","1","0","2020-02-04 12:00:32","2020-02-04 12:00:32","29"),
("2530","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 12:02:37","2020-02-04 12:02:37","29"),
("2531","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 12:03:24","2020-02-04 12:03:24","29"),
("2543","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 12:12:56","2020-02-04 12:12:56","29"),
("2556","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 12:16:38","2020-02-04 12:16:38","29"),
("2605","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 12:43:59","2020-02-04 12:43:59","29"),
("2606","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 13:15:18","2020-02-04 13:15:18","29"),
("2613","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=40&amp;title=sdsds\">Sdsds</a>","app\\modules\\comment\\models\\Comment","40","1","0","2020-02-04 13:17:48","2020-02-04 13:17:48","29"),
("2619","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 13:31:08","2020-02-04 13:31:08","29"),
("2620","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=43&amp;title=sdfss\">Sdfss</a>","app\\modules\\comment\\models\\Comment","43","1","0","2020-02-04 13:31:18","2020-02-04 13:31:18","29"),
("2621","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 13:32:19","2020-02-04 13:32:19","29"),
("2622","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 13:33:40","2020-02-04 13:33:40","29"),
("2623","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 13:36:27","2020-02-04 13:36:27","29"),
("2632","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 13:37:17","2020-02-04 13:37:17","29"),
("2633","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-04 15:55:51","2020-02-04 15:55:51","32"),
("2634","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-04 15:56:15","2020-02-04 15:56:15","32"),
("2635","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-04 15:57:16","2020-02-04 15:57:16","32"),
("2636","Added new Attendance : <a href=\"http://localhost/argus-app-yii2-1275/attendance/view/5/2020-02-01\">2020-02-01</a>","app\\models\\Attendance","5","1","0","2020-02-04 16:32:40","2020-02-04 16:32:40","31"),
("2640","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-04 16:41:37","2020-02-04 16:41:37","32"),
("2641","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/22\">File</a>","app\\modules\\file\\models\\File","22","1","0","2020-02-04 16:56:34","2020-02-04 16:56:34","31"),
("2642","Modified User ","app\\models\\User","28","1","0","2020-02-04 17:19:05","2020-02-04 17:19:05","1"),
("2643","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/61/28\">28</a>","app\\models\\LoginHistory","61","1","0","2020-02-04 17:19:17","2020-02-04 17:19:17","0"),
("2644","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 17:29:38","2020-02-04 17:29:38","29"),
("2645","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=47&amp;title=sdsfdjbfdsfsfsdf\">Sdsfdjbfdsfsfsdf</a>","app\\modules\\comment\\models\\Comment","47","1","0","2020-02-04 17:30:00","2020-02-04 17:30:00","29"),
("2646","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=48&amp;title=dfgdfgdfg\">Dfgdfgdfg</a>","app\\modules\\comment\\models\\Comment","48","1","0","2020-02-04 17:30:13","2020-02-04 17:30:13","29"),
("2647","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=49&amp;title=xcvgfxgdfg\">Xcvgfxgdfg</a>","app\\modules\\comment\\models\\Comment","49","1","0","2020-02-04 17:30:30","2020-02-04 17:30:30","29"),
("2648","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 17:34:36","2020-02-04 17:34:36","29"),
("2649","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","9","1","0","2020-02-04 17:46:50","2020-02-04 17:46:50","29"),
("2650","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/62/29\">29</a>","app\\models\\LoginHistory","62","1","0","2020-02-04 17:48:13","2020-02-04 17:48:13","29"),
("2651","Modified User ","app\\models\\User","29","1","0","2020-02-04 17:48:13","2020-02-04 17:48:13","29"),
("2652","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/10/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","10","1","0","2020-02-04 17:48:13","2020-02-04 17:48:13","29"),
("2653","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/63/29\">29</a>","app\\models\\LoginHistory","63","1","0","2020-02-04 17:51:37","2020-02-04 17:51:37","29"),
("2654","Modified User ","app\\models\\User","29","1","0","2020-02-04 17:51:37","2020-02-04 17:51:37","29"),
("2655","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/11/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-04 17:51:38","2020-02-04 17:51:38","29"),
("2656","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-04 18:38:29","2020-02-04 18:38:29","29"),
("2657","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-04 18:38:34","2020-02-04 18:38:34","32"),
("2658","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-04 18:39:20","2020-02-04 18:39:20","29"),
("2659","Modified Leave Request ","app\\models\\LeaveRequest","32","1","0","2020-02-04 18:40:23","2020-02-04 18:40:23","29"),
("2660","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-04 18:45:59","2020-02-04 18:45:59","29"),
("2661","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-04 18:49:32","2020-02-04 18:49:32","32"),
("2662","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-04 18:53:14","2020-02-04 18:53:14","32"),
("2663","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/64/28\">28</a>","app\\models\\LoginHistory","64","1","0","2020-02-05 09:27:06","2020-02-05 09:27:06","0"),
("2664","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 09:42:11","2020-02-05 09:42:11","29"),
("2665","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-05 09:55:29","2020-02-05 09:55:29","32"),
("2666","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","6","1","0","2020-02-05 09:56:07","2020-02-05 09:56:07","32"),
("2667","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 09:58:04","2020-02-05 09:58:04","29"),
("2668","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 10:46:47","2020-02-05 10:46:47","29"),
("2669","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 10:47:27","2020-02-05 10:47:27","29"),
("2670","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 10:47:54","2020-02-05 10:47:54","29"),
("2671","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 10:51:56","2020-02-05 10:51:56","29"),
("2672","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 10:52:52","2020-02-05 10:52:52","29"),
("2674","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/29/test\">test</a>","app\\models\\Ticket","29","1","0","2020-02-05 11:11:55","2020-02-05 11:11:55","28"),
("2675","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 11:35:28","2020-02-05 11:35:28","29"),
("2676","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 11:37:46","2020-02-05 11:37:46","29"),
("2677","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 11:57:37","2020-02-05 11:57:37","29"),
("2678","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 12:00:39","2020-02-05 12:00:39","29"),
("2679","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 12:05:25","2020-02-05 12:05:25","29"),
("2680","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 12:09:11","2020-02-05 12:09:11","29"),
("2681","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 12:33:09","2020-02-05 12:33:09","29"),
("2682","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 12:44:03","2020-02-05 12:44:03","29"),
("2683","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 12:49:47","2020-02-05 12:49:47","29"),
("2684","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 13:14:58","2020-02-05 13:14:58","29"),
("2685","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 13:20:45","2020-02-05 13:20:45","29"),
("2686","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 13:21:47","2020-02-05 13:21:47","29"),
("2687","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","11","1","0","2020-02-05 13:31:58","2020-02-05 13:31:58","29"),
("2688","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/65/1\">1</a>","app\\models\\LoginHistory","65","1","0","2020-02-05 13:40:19","2020-02-05 13:40:19","0"),
("2689","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/66/29\">29</a>","app\\models\\LoginHistory","66","1","0","2020-02-05 13:42:32","2020-02-05 13:42:32","0"),
("2690","Modified User ","app\\models\\User","29","1","0","2020-02-05 13:42:32","2020-02-05 13:42:32","29"),
("2691","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/12/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","12","1","0","2020-02-05 13:42:33","2020-02-05 13:42:33","29"),
("2692","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/67/32\">32</a>","app\\models\\LoginHistory","67","1","0","2020-02-05 13:43:07","2020-02-05 13:43:07","0"),
("2693","Modified User ","app\\models\\User","32","1","0","2020-02-05 13:43:07","2020-02-05 13:43:07","32"),
("2694","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/13/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","13","1","0","2020-02-05 13:43:07","2020-02-05 13:43:07","32"),
("2695","Modified User ","app\\models\\User","32","1","0","2020-02-05 13:43:10","2020-02-05 13:43:10","0"),
("2696","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/68/29\">29</a>","app\\models\\LoginHistory","68","1","0","2020-02-05 14:37:00","2020-02-05 14:37:00","29"),
("2697","Modified User ","app\\models\\User","29","1","0","2020-02-05 14:37:01","2020-02-05 14:37:01","29"),
("2698","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/14/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","14","1","0","2020-02-05 14:37:01","2020-02-05 14:37:01","29");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("2699","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/69/29\">29</a>","app\\models\\LoginHistory","69","1","0","2020-02-05 14:47:04","2020-02-05 14:47:04","0"),
("2700","Modified User ","app\\models\\User","29","1","0","2020-02-05 14:47:04","2020-02-05 14:47:04","29"),
("2701","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/15/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","15","1","0","2020-02-05 14:47:04","2020-02-05 14:47:04","29"),
("2702","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","15","1","0","2020-02-05 14:48:48","2020-02-05 14:48:48","29"),
("2703","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/70/29\">29</a>","app\\models\\LoginHistory","70","1","0","2020-02-05 14:52:35","2020-02-05 14:52:35","29"),
("2704","Modified User ","app\\models\\User","29","1","0","2020-02-05 14:52:35","2020-02-05 14:52:35","29"),
("2705","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/16/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 14:52:35","2020-02-05 14:52:35","29"),
("2706","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 15:13:05","2020-02-05 15:13:05","29"),
("2707","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 15:14:45","2020-02-05 15:14:45","29"),
("2708","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 15:43:15","2020-02-05 15:43:15","29"),
("2709","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 15:43:59","2020-02-05 15:43:59","29"),
("2710","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=50&amp;title=dsffdfd\">Dsffdfd</a>","app\\modules\\comment\\models\\Comment","50","1","0","2020-02-05 15:44:11","2020-02-05 15:44:11","29"),
("2711","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 15:52:41","2020-02-05 15:52:41","29"),
("2712","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:05:52","2020-02-05 16:05:52","29"),
("2713","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:06:37","2020-02-05 16:06:37","29"),
("2714","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:07:14","2020-02-05 16:07:14","29"),
("2715","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:08:04","2020-02-05 16:08:04","29"),
("2716","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:09:59","2020-02-05 16:09:59","29"),
("2717","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:10:57","2020-02-05 16:10:57","29"),
("2722","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:48:45","2020-02-05 16:48:45","29"),
("2723","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:49:22","2020-02-05 16:49:22","29"),
("2724","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:51:00","2020-02-05 16:51:00","29"),
("2725","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:54:04","2020-02-05 16:54:04","29"),
("2726","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 16:54:43","2020-02-05 16:54:43","29"),
("2727","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:16:31","2020-02-05 17:16:31","29"),
("2728","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:17:24","2020-02-05 17:17:24","29"),
("2729","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:18:12","2020-02-05 17:18:12","29"),
("2730","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:19:19","2020-02-05 17:19:19","29"),
("2731","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/71/32\">32</a>","app\\models\\LoginHistory","71","1","0","2020-02-05 17:19:57","2020-02-05 17:19:57","0"),
("2732","Modified User ","app\\models\\User","32","1","0","2020-02-05 17:19:57","2020-02-05 17:19:57","32"),
("2733","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/17/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 17:19:57","2020-02-05 17:19:57","32"),
("2734","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 17:20:26","2020-02-05 17:20:26","32"),
("2735","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:21:23","2020-02-05 17:21:23","29"),
("2736","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 17:22:32","2020-02-05 17:22:32","32"),
("2737","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:26:29","2020-02-05 17:26:29","29"),
("2738","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:27:47","2020-02-05 17:27:47","29"),
("2739","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 17:30:42","2020-02-05 17:30:42","32"),
("2740","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 17:31:38","2020-02-05 17:31:38","32"),
("2741","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 17:31:51","2020-02-05 17:31:51","32"),
("2742","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:37:16","2020-02-05 17:37:16","29"),
("2743","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:38:48","2020-02-05 17:38:48","29"),
("2744","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:41:21","2020-02-05 17:41:21","29"),
("2745","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 17:49:37","2020-02-05 17:49:37","29"),
("2749","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 17:51:44","2020-02-05 17:51:44","32"),
("2760","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:08:10","2020-02-05 18:08:10","29"),
("2765","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:14:36","2020-02-05 18:14:36","29"),
("2768","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 18:19:57","2020-02-05 18:19:57","32"),
("2769","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:20:04","2020-02-05 18:20:04","29"),
("2772","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 18:20:43","2020-02-05 18:20:43","32"),
("2773","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 18:21:28","2020-02-05 18:21:28","32"),
("2774","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:22:40","2020-02-05 18:22:40","29"),
("2781","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/72/28\">28</a>","app\\models\\LoginHistory","72","1","0","2020-02-05 18:23:36","2020-02-05 18:23:36","0"),
("2794","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","17","1","0","2020-02-05 18:32:10","2020-02-05 18:32:10","32"),
("2795","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:36:13","2020-02-05 18:36:13","29"),
("2796","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:45:45","2020-02-05 18:45:45","29"),
("2810","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:47:45","2020-02-05 18:47:45","29"),
("2814","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:49:45","2020-02-05 18:49:45","29"),
("2820","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:50:34","2020-02-05 18:50:34","29"),
("2824","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:51:10","2020-02-05 18:51:10","29"),
("2844","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:53:03","2020-02-05 18:53:03","29"),
("2852","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:54:15","2020-02-05 18:54:15","29"),
("2858","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:55:11","2020-02-05 18:55:11","29"),
("2873","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:56:54","2020-02-05 18:56:54","29"),
("2878","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 18:58:09","2020-02-05 18:58:09","29"),
("2890","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 19:00:24","2020-02-05 19:00:24","29"),
("2899","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-05 19:02:02","2020-02-05 19:02:02","29"),
("2902","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/73/1\">1</a>","app\\models\\LoginHistory","73","1","0","2020-02-06 09:27:18","2020-02-06 09:27:18","0"),
("2903","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/74/1\">1</a>","app\\models\\LoginHistory","74","1","0","2020-02-06 09:27:32","2020-02-06 09:27:32","0"),
("2904","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 09:27:52","2020-02-06 09:27:52","1"),
("2905","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/75/1\">1</a>","app\\models\\LoginHistory","75","1","0","2020-02-06 09:28:19","2020-02-06 09:28:19","0"),
("2906","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 09:42:43","2020-02-06 09:42:43","1"),
("2907","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 09:46:48","2020-02-06 09:46:48","29"),
("2908","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 09:47:52","2020-02-06 09:47:52","29"),
("2909","Modified User ","app\\models\\User","28","1","0","2020-02-06 09:50:09","2020-02-06 09:50:09","1"),
("2910","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 09:50:09","2020-02-06 09:50:09","1"),
("2911","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 10:06:29","2020-02-06 10:06:29","29"),
("2912","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 10:15:34","2020-02-06 10:15:34","29"),
("2913","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/34/mr-johnson\">MR Johnson</a>","app\\models\\User","34","1","0","2020-02-06 10:16:01","2020-02-06 10:16:01","1"),
("2914","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 10:16:01","2020-02-06 10:16:01","1"),
("2915","Modified User ","app\\models\\User","34","1","0","2020-02-06 10:18:16","2020-02-06 10:18:16","1"),
("2916","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 10:18:16","2020-02-06 10:18:16","1"),
("2917","Modified User ","app\\models\\User","34","1","0","2020-02-06 10:18:22","2020-02-06 10:18:22","1"),
("2918","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 10:18:23","2020-02-06 10:18:23","1"),
("2919","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/76/34\">34</a>","app\\models\\LoginHistory","76","1","0","2020-02-06 10:18:54","2020-02-06 10:18:54","0"),
("2924","Modified User ","app\\models\\User","34","1","0","2020-02-06 10:19:20","2020-02-06 10:19:20","1"),
("2925","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 10:19:20","2020-02-06 10:19:20","1"),
("2928","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 10:20:02","2020-02-06 10:20:02","29"),
("2929","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=51&amp;title=ghfhfg\">Ghfhfg</a>","app\\modules\\comment\\models\\Comment","51","1","0","2020-02-06 10:20:10","2020-02-06 10:20:10","29"),
("2930","Modified User ","app\\models\\User","34","1","0","2020-02-06 10:20:13","2020-02-06 10:20:13","1"),
("2931","Modified User ","app\\models\\User","34","1","0","2020-02-06 10:20:20","2020-02-06 10:20:20","1"),
("2932","Modified User ","app\\models\\User","34","1","0","2020-02-06 10:20:42","2020-02-06 10:20:42","1"),
("2933","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=52&amp;title=dfdfd\">Dfdfd</a>","app\\modules\\comment\\models\\Comment","52","1","0","2020-02-06 10:21:31","2020-02-06 10:21:31","29"),
("2934","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 10:23:19","2020-02-06 10:23:19","29"),
("2938","Modified User ","app\\models\\User","34","1","0","2020-02-06 10:25:01","2020-02-06 10:25:01","1"),
("2939","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 10:25:02","2020-02-06 10:25:02","1"),
("2940","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 10:37:49","2020-02-06 10:37:49","29"),
("2941","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 10:39:53","2020-02-06 10:39:53","29"),
("2942","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 10:41:48","2020-02-06 10:41:48","29"),
("2943","Modified User ","app\\models\\User","34","1","0","2020-02-06 11:07:20","2020-02-06 11:07:20","1"),
("2944","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 11:07:20","2020-02-06 11:07:20","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("2946","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 11:19:07","2020-02-06 11:19:07","29"),
("2947","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/77/1\">1</a>","app\\models\\LoginHistory","77","1","0","2020-02-06 11:36:01","2020-02-06 11:36:01","0"),
("2948","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/78/34\">34</a>","app\\models\\LoginHistory","78","1","0","2020-02-06 11:36:20","2020-02-06 11:36:20","0"),
("2949","Modified User ","app\\models\\User","34","1","0","2020-02-06 11:37:48","2020-02-06 11:37:48","1"),
("2950","Modified Client ","app\\models\\Client","3","1","0","2020-02-06 11:37:48","2020-02-06 11:37:48","1"),
("2951","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/79/1\">1</a>","app\\models\\LoginHistory","79","1","0","2020-02-06 11:38:02","2020-02-06 11:38:02","0"),
("2952","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/80/34\">34</a>","app\\models\\LoginHistory","80","1","0","2020-02-06 11:38:30","2020-02-06 11:38:30","0"),
("2953","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/81/34\">34</a>","app\\models\\LoginHistory","81","1","0","2020-02-06 11:38:37","2020-02-06 11:38:37","0"),
("2954","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/82/34\">34</a>","app\\models\\LoginHistory","82","1","0","2020-02-06 11:38:43","2020-02-06 11:38:43","0"),
("2955","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","16","1","0","2020-02-06 11:41:26","2020-02-06 11:41:26","29"),
("2956","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/83/29\">29</a>","app\\models\\LoginHistory","83","1","0","2020-02-06 11:42:21","2020-02-06 11:42:21","0"),
("2957","Modified User ","app\\models\\User","29","1","0","2020-02-06 11:42:21","2020-02-06 11:42:21","29"),
("2958","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/18/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","18","1","0","2020-02-06 11:42:21","2020-02-06 11:42:21","29"),
("2959","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/84/1\">1</a>","app\\models\\LoginHistory","84","1","0","2020-02-06 11:42:50","2020-02-06 11:42:50","0"),
("2960","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/85/1\">1</a>","app\\models\\LoginHistory","85","1","0","2020-02-06 11:43:00","2020-02-06 11:43:00","0"),
("2961","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/86/34\">34</a>","app\\models\\LoginHistory","86","1","0","2020-02-06 11:43:21","2020-02-06 11:43:21","0"),
("2962","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/87/29\">29</a>","app\\models\\LoginHistory","87","1","0","2020-02-06 11:44:18","2020-02-06 11:44:18","29"),
("2963","Modified User ","app\\models\\User","29","1","0","2020-02-06 11:44:18","2020-02-06 11:44:18","29"),
("2964","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/19/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 11:44:18","2020-02-06 11:44:18","29"),
("2965","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/88/34\">34</a>","app\\models\\LoginHistory","88","1","0","2020-02-06 11:47:29","2020-02-06 11:47:29","0"),
("2966","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/35/naresh-saini\">Naresh Saini</a>","app\\models\\User","35","1","0","2020-02-06 12:00:00","2020-02-06 12:00:00","1"),
("2969","Added new Department : <a href=\"http://localhost/argus-app-yii2-1275/department/view/7/web-yii\">Web Yii</a>","app\\models\\Department","7","1","0","2020-02-06 12:06:33","2020-02-06 12:06:33","1"),
("2970","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=58&amp;title=state-changed-inactive-to-active\">State Changed : Inactive to Active</a>","app\\modules\\comment\\models\\Comment","58","1","0","2020-02-06 12:06:43","2020-02-06 12:06:43","1"),
("2971","Modified User ","app\\models\\User","35","1","0","2020-02-06 12:06:56","2020-02-06 12:06:56","1"),
("2972","Added new Cases : <a href=\"http://localhost/argus-app-yii2-1275/cases/view/4/shopping-website\">Shopping Website</a>","app\\models\\Cases","4","1","0","2020-02-06 12:21:43","2020-02-06 12:21:43","1"),
("2973","Modified Cases ","app\\models\\Cases","4","1","0","2020-02-06 12:24:35","2020-02-06 12:24:35","1"),
("2974","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=59&amp;title=state-changed-new-to-planning\">State Changed : New to Planning</a>","app\\modules\\comment\\models\\Comment","59","1","0","2020-02-06 12:24:52","2020-02-06 12:24:52","1"),
("2975","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/89/35\">35</a>","app\\models\\LoginHistory","89","1","0","2020-02-06 12:31:59","2020-02-06 12:31:59","0"),
("2976","Added new Team : <a href=\"http://localhost/argus-app-yii2-1275/team/view/4/shopping-website\">Shopping Website</a>","app\\models\\Team","4","1","0","2020-02-06 12:32:37","2020-02-06 12:32:37","1"),
("2977","Modified User ","app\\models\\User","29","1","0","2020-02-06 12:33:06","2020-02-06 12:33:06","1"),
("2978","Added new Task : <a href=\"http://localhost/argus-app-yii2-1275/task/view/13/project-setup\">Project Setup</a>","app\\models\\Task","13","1","0","2020-02-06 12:34:13","2020-02-06 12:34:13","1"),
("2979","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 12:50:23","2020-02-06 12:50:23","29"),
("2980","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/71/0\">0</a>","app\\models\\Timer","71","1","0","2020-02-06 12:52:18","2020-02-06 12:52:18","29"),
("2981","Modified Task ","app\\models\\Task","13","1","0","2020-02-06 12:52:18","2020-02-06 12:52:18","29"),
("2982","Modified Timer ","app\\models\\Timer","71","1","0","2020-02-06 12:52:23","2020-02-06 12:52:23","29"),
("2983","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:07:33","2020-02-06 13:07:33","29"),
("2984","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:19:20","2020-02-06 13:19:20","29"),
("2985","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:20:26","2020-02-06 13:20:26","29"),
("2986","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:21:37","2020-02-06 13:21:37","29"),
("2987","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/23\">File</a>","app\\modules\\file\\models\\File","23","1","0","2020-02-06 13:25:22","2020-02-06 13:25:22","1"),
("2988","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/24\">File</a>","app\\modules\\file\\models\\File","24","1","0","2020-02-06 13:25:25","2020-02-06 13:25:25","1"),
("2989","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/25\">File</a>","app\\modules\\file\\models\\File","25","1","0","2020-02-06 13:25:28","2020-02-06 13:25:28","1"),
("2990","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/26\">File</a>","app\\modules\\file\\models\\File","26","1","0","2020-02-06 13:25:32","2020-02-06 13:25:32","1"),
("2991","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/27\">File</a>","app\\modules\\file\\models\\File","27","1","0","2020-02-06 13:25:35","2020-02-06 13:25:35","1"),
("2992","Added new Case Type : <a href=\"http://localhost/argus-app-yii2-1275/case-type/view/1/software-development\">Software Development</a>","app\\models\\CaseType","1","1","0","2020-02-06 13:31:13","2020-02-06 13:31:13","1"),
("2993","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:32:28","2020-02-06 13:32:28","29"),
("2994","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:33:03","2020-02-06 13:33:03","29"),
("2995","Added new Case Type : <a href=\"http://localhost/argus-app-yii2-1275/case-type/view/2/app-development\">App Development</a>","app\\models\\CaseType","2","1","0","2020-02-06 13:33:05","2020-02-06 13:33:05","1"),
("2996","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=60&amp;title=hghgh\">Hghgh</a>","app\\modules\\comment\\models\\Comment","60","1","0","2020-02-06 13:33:19","2020-02-06 13:33:19","29"),
("2997","Modified Case Type ","app\\models\\CaseType","2","1","0","2020-02-06 13:33:27","2020-02-06 13:33:27","1"),
("2998","Modified Cases ","app\\models\\Cases","4","1","0","2020-02-06 13:36:04","2020-02-06 13:36:04","1"),
("2999","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:36:53","2020-02-06 13:36:53","29"),
("3000","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:37:36","2020-02-06 13:37:36","29"),
("3001","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 13:40:15","2020-02-06 13:40:15","29"),
("3002","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=61&amp;title=dfr\">Dfr</a>","app\\modules\\comment\\models\\Comment","61","1","0","2020-02-06 13:40:27","2020-02-06 13:40:27","29"),
("3003","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=62&amp;title=sddsd\">Sddsd</a>","app\\modules\\comment\\models\\Comment","62","1","0","2020-02-06 14:32:35","2020-02-06 14:32:35","29"),
("3004","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=63&amp;title=bcvgd\">Bcvgd</a>","app\\modules\\comment\\models\\Comment","63","1","0","2020-02-06 14:33:55","2020-02-06 14:33:55","29"),
("3005","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","19","1","0","2020-02-06 14:38:02","2020-02-06 14:38:02","29"),
("3006","Modified User ","app\\models\\User","29","1","0","2020-02-06 14:38:05","2020-02-06 14:38:05","0"),
("3007","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/90/32\">32</a>","app\\models\\LoginHistory","90","1","0","2020-02-06 14:38:24","2020-02-06 14:38:24","0"),
("3008","Modified User ","app\\models\\User","32","1","0","2020-02-06 14:38:24","2020-02-06 14:38:24","32"),
("3009","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/20/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","20","1","0","2020-02-06 14:38:24","2020-02-06 14:38:24","32"),
("3010","Modified Leave Request ","app\\models\\LeaveRequest","28","1","0","2020-02-06 14:39:00","2020-02-06 14:39:00","32"),
("3011","Modified User ","app\\models\\User","32","1","0","2020-02-06 14:39:05","2020-02-06 14:39:05","0"),
("3012","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/91/29\">29</a>","app\\models\\LoginHistory","91","1","0","2020-02-06 14:40:31","2020-02-06 14:40:31","0"),
("3013","Modified User ","app\\models\\User","29","1","0","2020-02-06 14:40:31","2020-02-06 14:40:31","29"),
("3014","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/21/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 14:40:32","2020-02-06 14:40:32","29"),
("3015","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=64&amp;title=vcbfg\">Vcbfg</a>","app\\modules\\comment\\models\\Comment","64","1","0","2020-02-06 14:40:48","2020-02-06 14:40:48","29"),
("3016","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=65&amp;title=gfhgfh\">Gfhgfh</a>","app\\modules\\comment\\models\\Comment","65","1","0","2020-02-06 14:40:50","2020-02-06 14:40:50","29"),
("3017","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=66&amp;title=rtyrty\">Rtyrty</a>","app\\modules\\comment\\models\\Comment","66","1","0","2020-02-06 14:41:00","2020-02-06 14:41:00","29"),
("3018","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 14:43:28","2020-02-06 14:43:28","29"),
("3019","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=67&amp;title=cvcv\">Cvcv</a>","app\\modules\\comment\\models\\Comment","67","1","0","2020-02-06 14:44:54","2020-02-06 14:44:54","29"),
("3020","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=68&amp;title=cvcv\">Cvcv</a>","app\\modules\\comment\\models\\Comment","68","1","0","2020-02-06 14:45:04","2020-02-06 14:45:04","29"),
("3021","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 14:47:45","2020-02-06 14:47:45","29"),
("3022","Added new User : <a href=\"http://192.168.2.143/argus-app-yii2-1275/user/view/36/poll\">poll</a>","app\\models\\User","36","1","0","2020-02-06 14:47:58","2020-02-06 14:47:58","1"),
("3023","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 14:49:29","2020-02-06 14:49:29","29"),
("3024","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=69&amp;title=xcxc\">Xcxc</a>","app\\modules\\comment\\models\\Comment","69","1","0","2020-02-06 14:49:40","2020-02-06 14:49:40","29"),
("3025","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 14:52:02","2020-02-06 14:52:02","29"),
("3026","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 14:54:42","2020-02-06 14:54:42","29"),
("3027","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 14:56:03","2020-02-06 14:56:03","29"),
("3028","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:13:07","2020-02-06 15:13:07","29"),
("3029","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:14:11","2020-02-06 15:14:11","29"),
("3030","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:14:30","2020-02-06 15:14:30","29"),
("3031","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:19:53","2020-02-06 15:19:53","29"),
("3032","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:24:57","2020-02-06 15:24:57","29"),
("3033","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:26:39","2020-02-06 15:26:39","29"),
("3034","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:28:53","2020-02-06 15:28:53","29"),
("3035","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:30:41","2020-02-06 15:30:41","29"),
("3036","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:31:54","2020-02-06 15:31:54","29"),
("3037","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/72/0\">0</a>","app\\models\\Timer","72","1","0","2020-02-06 15:32:46","2020-02-06 15:32:46","29"),
("3038","Modified Timer ","app\\models\\Timer","72","1","0","2020-02-06 15:32:51","2020-02-06 15:32:51","29"),
("3039","Added new Reminder : <a href=\"http://localhost/argus-app-yii2-1275/reminder/view/3/test\">test</a>","app\\models\\Reminder","3","1","0","2020-02-06 15:55:16","2020-02-06 15:55:16","1"),
("3040","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 15:59:11","2020-02-06 15:59:11","29"),
("3041","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 16:17:06","2020-02-06 16:17:06","29"),
("3042","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 16:58:27","2020-02-06 16:58:27","29"),
("3043","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:16:44","2020-02-06 17:16:44","29"),
("3044","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:17:41","2020-02-06 17:17:41","29"),
("3045","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:18:25","2020-02-06 17:18:25","29"),
("3046","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:20:51","2020-02-06 17:20:51","29"),
("3047","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/73/0\">0</a>","app\\models\\Timer","73","1","0","2020-02-06 17:21:20","2020-02-06 17:21:20","29");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3048","Modified Timer ","app\\models\\Timer","73","1","0","2020-02-06 17:22:13","2020-02-06 17:22:13","29"),
("3049","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/74/0\">0</a>","app\\models\\Timer","74","1","0","2020-02-06 17:22:14","2020-02-06 17:22:14","29"),
("3050","Modified Timer ","app\\models\\Timer","74","1","0","2020-02-06 17:22:15","2020-02-06 17:22:15","29"),
("3051","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/75/0\">0</a>","app\\models\\Timer","75","1","0","2020-02-06 17:22:15","2020-02-06 17:22:15","29"),
("3052","Modified Timer ","app\\models\\Timer","75","1","0","2020-02-06 17:22:19","2020-02-06 17:22:19","29"),
("3053","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/76/0\">0</a>","app\\models\\Timer","76","1","0","2020-02-06 17:22:19","2020-02-06 17:22:19","29"),
("3054","Modified Timer ","app\\models\\Timer","76","1","0","2020-02-06 17:22:21","2020-02-06 17:22:21","29"),
("3055","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/77/0\">0</a>","app\\models\\Timer","77","1","0","2020-02-06 17:22:21","2020-02-06 17:22:21","29"),
("3056","Modified Timer ","app\\models\\Timer","77","1","0","2020-02-06 17:22:23","2020-02-06 17:22:23","29"),
("3057","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/78/0\">0</a>","app\\models\\Timer","78","1","0","2020-02-06 17:22:25","2020-02-06 17:22:25","29"),
("3058","Modified Timer ","app\\models\\Timer","78","1","0","2020-02-06 17:22:27","2020-02-06 17:22:27","29"),
("3059","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:25:35","2020-02-06 17:25:35","29"),
("3060","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/79/0\">0</a>","app\\models\\Timer","79","1","0","2020-02-06 17:27:31","2020-02-06 17:27:31","29"),
("3061","Modified Timer ","app\\models\\Timer","79","1","0","2020-02-06 17:27:57","2020-02-06 17:27:57","29"),
("3062","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/80/0\">0</a>","app\\models\\Timer","80","1","0","2020-02-06 17:27:57","2020-02-06 17:27:57","29"),
("3063","Modified Timer ","app\\models\\Timer","80","1","0","2020-02-06 17:28:04","2020-02-06 17:28:04","29"),
("3064","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/81/0\">0</a>","app\\models\\Timer","81","1","0","2020-02-06 17:28:04","2020-02-06 17:28:04","29"),
("3065","Modified Timer ","app\\models\\Timer","81","1","0","2020-02-06 17:28:09","2020-02-06 17:28:09","29"),
("3066","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/82/0\">0</a>","app\\models\\Timer","82","1","0","2020-02-06 17:28:11","2020-02-06 17:28:11","29"),
("3067","Modified Timer ","app\\models\\Timer","82","1","0","2020-02-06 17:28:12","2020-02-06 17:28:12","29"),
("3068","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/83/0\">0</a>","app\\models\\Timer","83","1","0","2020-02-06 17:28:12","2020-02-06 17:28:12","29"),
("3069","Modified Timer ","app\\models\\Timer","83","1","0","2020-02-06 17:28:16","2020-02-06 17:28:16","29"),
("3070","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/84/0\">0</a>","app\\models\\Timer","84","1","0","2020-02-06 17:28:16","2020-02-06 17:28:16","29"),
("3071","Modified Timer ","app\\models\\Timer","84","1","0","2020-02-06 17:28:17","2020-02-06 17:28:17","29"),
("3072","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/85/0\">0</a>","app\\models\\Timer","85","1","0","2020-02-06 17:28:17","2020-02-06 17:28:17","29"),
("3073","Modified Timer ","app\\models\\Timer","85","1","0","2020-02-06 17:28:19","2020-02-06 17:28:19","29"),
("3074","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/86/0\">0</a>","app\\models\\Timer","86","1","0","2020-02-06 17:28:20","2020-02-06 17:28:20","29"),
("3075","Modified Timer ","app\\models\\Timer","86","1","0","2020-02-06 17:28:20","2020-02-06 17:28:20","29"),
("3076","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/87/0\">0</a>","app\\models\\Timer","87","1","0","2020-02-06 17:28:20","2020-02-06 17:28:20","29"),
("3077","Modified Timer ","app\\models\\Timer","87","1","0","2020-02-06 17:28:23","2020-02-06 17:28:23","29"),
("3078","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/88/0\">0</a>","app\\models\\Timer","88","1","0","2020-02-06 17:28:23","2020-02-06 17:28:23","29"),
("3079","Modified Timer ","app\\models\\Timer","88","1","0","2020-02-06 17:28:25","2020-02-06 17:28:25","29"),
("3080","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/89/0\">0</a>","app\\models\\Timer","89","1","0","2020-02-06 17:28:25","2020-02-06 17:28:25","29"),
("3081","Modified Timer ","app\\models\\Timer","89","1","0","2020-02-06 17:28:26","2020-02-06 17:28:26","29"),
("3082","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/90/0\">0</a>","app\\models\\Timer","90","1","0","2020-02-06 17:30:22","2020-02-06 17:30:22","29"),
("3083","Modified Timer ","app\\models\\Timer","90","1","0","2020-02-06 17:31:16","2020-02-06 17:31:16","29"),
("3084","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/91/0\">0</a>","app\\models\\Timer","91","1","0","2020-02-06 17:31:16","2020-02-06 17:31:16","29"),
("3085","Modified Timer ","app\\models\\Timer","91","1","0","2020-02-06 17:31:27","2020-02-06 17:31:27","29"),
("3086","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/92/0\">0</a>","app\\models\\Timer","92","1","0","2020-02-06 17:31:27","2020-02-06 17:31:27","29"),
("3087","Modified Timer ","app\\models\\Timer","92","1","0","2020-02-06 17:31:34","2020-02-06 17:31:34","29"),
("3088","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/93/0\">0</a>","app\\models\\Timer","93","1","0","2020-02-06 17:31:34","2020-02-06 17:31:34","29"),
("3089","Modified Timer ","app\\models\\Timer","93","1","0","2020-02-06 17:32:07","2020-02-06 17:32:07","29"),
("3090","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/94/0\">0</a>","app\\models\\Timer","94","1","0","2020-02-06 17:32:08","2020-02-06 17:32:08","29"),
("3091","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:34:34","2020-02-06 17:34:34","29"),
("3092","Modified Timer ","app\\models\\Timer","94","1","0","2020-02-06 17:34:39","2020-02-06 17:34:39","29"),
("3093","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/95/0\">0</a>","app\\models\\Timer","95","1","0","2020-02-06 17:34:40","2020-02-06 17:34:40","29"),
("3094","Modified Timer ","app\\models\\Timer","95","1","0","2020-02-06 17:36:20","2020-02-06 17:36:20","29"),
("3095","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:37:56","2020-02-06 17:37:56","29"),
("3096","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/96/0\">0</a>","app\\models\\Timer","96","1","0","2020-02-06 17:38:07","2020-02-06 17:38:07","29"),
("3097","Modified Timer ","app\\models\\Timer","96","1","0","2020-02-06 17:38:12","2020-02-06 17:38:12","29"),
("3098","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/97/0\">0</a>","app\\models\\Timer","97","1","0","2020-02-06 17:38:12","2020-02-06 17:38:12","29"),
("3099","Modified Timer ","app\\models\\Timer","97","1","0","2020-02-06 17:38:16","2020-02-06 17:38:16","29"),
("3100","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/98/0\">0</a>","app\\models\\Timer","98","1","0","2020-02-06 17:38:24","2020-02-06 17:38:24","29"),
("3101","Modified Timer ","app\\models\\Timer","98","1","0","2020-02-06 17:39:00","2020-02-06 17:39:00","29"),
("3102","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/99/0\">0</a>","app\\models\\Timer","99","1","0","2020-02-06 17:39:00","2020-02-06 17:39:00","29"),
("3103","Modified Timer ","app\\models\\Timer","99","1","0","2020-02-06 17:39:08","2020-02-06 17:39:08","29"),
("3104","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/100/0\">0</a>","app\\models\\Timer","100","1","0","2020-02-06 17:39:08","2020-02-06 17:39:08","29"),
("3105","Modified Timer ","app\\models\\Timer","100","1","0","2020-02-06 17:39:10","2020-02-06 17:39:10","29"),
("3106","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/101/0\">0</a>","app\\models\\Timer","101","1","0","2020-02-06 17:41:54","2020-02-06 17:41:54","29"),
("3107","Modified Timer ","app\\models\\Timer","101","1","0","2020-02-06 17:42:13","2020-02-06 17:42:13","29"),
("3108","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/102/0\">0</a>","app\\models\\Timer","102","1","0","2020-02-06 17:42:13","2020-02-06 17:42:13","29"),
("3109","Modified Timer ","app\\models\\Timer","102","1","0","2020-02-06 17:42:18","2020-02-06 17:42:18","29"),
("3110","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/103/0\">0</a>","app\\models\\Timer","103","1","0","2020-02-06 17:42:18","2020-02-06 17:42:18","29"),
("3111","Modified Timer ","app\\models\\Timer","103","1","0","2020-02-06 17:42:21","2020-02-06 17:42:21","29"),
("3112","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/104/0\">0</a>","app\\models\\Timer","104","1","0","2020-02-06 17:42:26","2020-02-06 17:42:26","29"),
("3113","Modified Timer ","app\\models\\Timer","104","1","0","2020-02-06 17:42:43","2020-02-06 17:42:43","29"),
("3114","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/105/0\">0</a>","app\\models\\Timer","105","1","0","2020-02-06 17:42:43","2020-02-06 17:42:43","29"),
("3115","Modified Timer ","app\\models\\Timer","105","1","0","2020-02-06 17:42:50","2020-02-06 17:42:50","29"),
("3116","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/106/0\">0</a>","app\\models\\Timer","106","1","0","2020-02-06 17:42:50","2020-02-06 17:42:50","29"),
("3117","Modified Timer ","app\\models\\Timer","106","1","0","2020-02-06 17:43:01","2020-02-06 17:43:01","29"),
("3118","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/107/0\">0</a>","app\\models\\Timer","107","1","0","2020-02-06 17:43:01","2020-02-06 17:43:01","29"),
("3119","Modified Timer ","app\\models\\Timer","107","1","0","2020-02-06 17:43:03","2020-02-06 17:43:03","29"),
("3120","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:46:52","2020-02-06 17:46:52","29"),
("3121","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/108/0\">0</a>","app\\models\\Timer","108","1","0","2020-02-06 17:47:04","2020-02-06 17:47:04","29"),
("3122","Modified Timer ","app\\models\\Timer","108","1","0","2020-02-06 17:47:10","2020-02-06 17:47:10","29"),
("3123","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/109/0\">0</a>","app\\models\\Timer","109","1","0","2020-02-06 17:47:10","2020-02-06 17:47:10","29"),
("3124","Modified Timer ","app\\models\\Timer","109","1","0","2020-02-06 17:47:17","2020-02-06 17:47:17","29"),
("3125","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/110/0\">0</a>","app\\models\\Timer","110","1","0","2020-02-06 17:47:17","2020-02-06 17:47:17","29"),
("3126","Modified Timer ","app\\models\\Timer","110","1","0","2020-02-06 17:47:28","2020-02-06 17:47:28","29"),
("3127","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/111/0\">0</a>","app\\models\\Timer","111","1","0","2020-02-06 17:47:28","2020-02-06 17:47:28","29"),
("3128","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:51:32","2020-02-06 17:51:32","29"),
("3129","Modified Timer ","app\\models\\Timer","111","1","0","2020-02-06 17:52:24","2020-02-06 17:52:24","29"),
("3130","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/112/0\">0</a>","app\\models\\Timer","112","1","0","2020-02-06 17:52:24","2020-02-06 17:52:24","29"),
("3131","Modified Timer ","app\\models\\Timer","112","1","0","2020-02-06 17:52:29","2020-02-06 17:52:29","29"),
("3132","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/113/0\">0</a>","app\\models\\Timer","113","1","0","2020-02-06 17:52:29","2020-02-06 17:52:29","29"),
("3133","Modified Timer ","app\\models\\Timer","113","1","0","2020-02-06 17:52:35","2020-02-06 17:52:35","29"),
("3134","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:56:01","2020-02-06 17:56:01","29"),
("3135","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 17:58:46","2020-02-06 17:58:46","29"),
("3136","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/114/0\">0</a>","app\\models\\Timer","114","1","0","2020-02-06 17:59:06","2020-02-06 17:59:06","29"),
("3137","Modified Leave Request ","app\\models\\LeaveRequest","32","1","0","2020-02-06 17:59:29","2020-02-06 17:59:29","29"),
("3138","Modified Timer ","app\\models\\Timer","114","1","0","2020-02-06 18:00:06","2020-02-06 18:00:06","29"),
("3139","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/115/0\">0</a>","app\\models\\Timer","115","1","0","2020-02-06 18:00:06","2020-02-06 18:00:06","29"),
("3140","Modified Timer ","app\\models\\Timer","115","1","0","2020-02-06 18:00:28","2020-02-06 18:00:28","29"),
("3141","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/116/0\">0</a>","app\\models\\Timer","116","1","0","2020-02-06 18:00:28","2020-02-06 18:00:28","29"),
("3142","Modified Timer ","app\\models\\Timer","116","1","0","2020-02-06 18:04:19","2020-02-06 18:04:19","29"),
("3143","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/117/0\">0</a>","app\\models\\Timer","117","1","0","2020-02-06 18:04:19","2020-02-06 18:04:19","29"),
("3144","Modified Timer ","app\\models\\Timer","117","1","0","2020-02-06 18:04:35","2020-02-06 18:04:35","29"),
("3145","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/118/0\">0</a>","app\\models\\Timer","118","1","0","2020-02-06 18:04:35","2020-02-06 18:04:35","29"),
("3146","Modified Timer ","app\\models\\Timer","118","1","0","2020-02-06 18:07:32","2020-02-06 18:07:32","29"),
("3147","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/119/0\">0</a>","app\\models\\Timer","119","1","0","2020-02-06 18:07:32","2020-02-06 18:07:32","29");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3148","Modified Timer ","app\\models\\Timer","119","1","0","2020-02-06 18:07:41","2020-02-06 18:07:41","29"),
("3149","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/120/0\">0</a>","app\\models\\Timer","120","1","0","2020-02-06 18:07:41","2020-02-06 18:07:41","29"),
("3150","Modified Timer ","app\\models\\Timer","120","1","0","2020-02-06 18:09:13","2020-02-06 18:09:13","29"),
("3151","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/121/0\">0</a>","app\\models\\Timer","121","1","0","2020-02-06 18:09:13","2020-02-06 18:09:13","29"),
("3152","Modified Timer ","app\\models\\Timer","121","1","0","2020-02-06 18:09:27","2020-02-06 18:09:27","29"),
("3153","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/122/0\">0</a>","app\\models\\Timer","122","1","0","2020-02-06 18:09:27","2020-02-06 18:09:27","29"),
("3154","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:12:09","2020-02-06 18:12:09","29"),
("3155","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:12:40","2020-02-06 18:12:40","29"),
("3156","Modified Timer ","app\\models\\Timer","122","1","0","2020-02-06 18:14:21","2020-02-06 18:14:21","29"),
("3157","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/123/0\">0</a>","app\\models\\Timer","123","1","0","2020-02-06 18:14:44","2020-02-06 18:14:44","29"),
("3158","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:19:09","2020-02-06 18:19:09","29"),
("3159","Modified Timer ","app\\models\\Timer","123","1","0","2020-02-06 18:19:18","2020-02-06 18:19:18","29"),
("3160","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/124/0\">0</a>","app\\models\\Timer","124","1","0","2020-02-06 18:19:21","2020-02-06 18:19:21","29"),
("3161","Modified Timer ","app\\models\\Timer","124","1","0","2020-02-06 18:19:24","2020-02-06 18:19:24","29"),
("3162","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/125/0\">0</a>","app\\models\\Timer","125","1","0","2020-02-06 18:19:47","2020-02-06 18:19:47","29"),
("3163","Modified Timer ","app\\models\\Timer","125","1","0","2020-02-06 18:20:00","2020-02-06 18:20:00","29"),
("3164","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:22:02","2020-02-06 18:22:02","29"),
("3165","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/126/0\">0</a>","app\\models\\Timer","126","1","0","2020-02-06 18:22:16","2020-02-06 18:22:16","29"),
("3166","Modified Timer ","app\\models\\Timer","126","1","0","2020-02-06 18:22:24","2020-02-06 18:22:24","29"),
("3167","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:24:43","2020-02-06 18:24:43","29"),
("3168","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:29:14","2020-02-06 18:29:14","29"),
("3169","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/127/0\">0</a>","app\\models\\Timer","127","1","0","2020-02-06 18:29:33","2020-02-06 18:29:33","29"),
("3170","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:32:40","2020-02-06 18:32:40","29"),
("3171","Modified Timer ","app\\models\\Timer","127","1","0","2020-02-06 18:32:48","2020-02-06 18:32:48","29"),
("3172","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/128/0\">0</a>","app\\models\\Timer","128","1","0","2020-02-06 18:32:53","2020-02-06 18:32:53","29"),
("3173","Modified Timer ","app\\models\\Timer","128","1","0","2020-02-06 18:32:55","2020-02-06 18:32:55","29"),
("3174","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:33:39","2020-02-06 18:33:39","29"),
("3175","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/129/0\">0</a>","app\\models\\Timer","129","1","0","2020-02-06 18:33:58","2020-02-06 18:33:58","29"),
("3176","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:36:08","2020-02-06 18:36:08","29"),
("3177","Modified Timer ","app\\models\\Timer","129","1","0","2020-02-06 18:36:48","2020-02-06 18:36:48","29"),
("3178","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/130/0\">0</a>","app\\models\\Timer","130","1","0","2020-02-06 18:36:50","2020-02-06 18:36:50","29"),
("3179","Modified Timer ","app\\models\\Timer","130","1","0","2020-02-06 18:36:52","2020-02-06 18:36:52","29"),
("3180","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/131/0\">0</a>","app\\models\\Timer","131","1","0","2020-02-06 18:36:53","2020-02-06 18:36:53","29"),
("3181","Modified Timer ","app\\models\\Timer","131","1","0","2020-02-06 18:36:54","2020-02-06 18:36:54","29"),
("3188","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/138/0\">0</a>","app\\models\\Timer","138","1","0","2020-02-06 18:37:01","2020-02-06 18:37:01","29"),
("3189","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:37:41","2020-02-06 18:37:41","29"),
("3190","Modified Timer ","app\\models\\Timer","138","1","0","2020-02-06 18:38:11","2020-02-06 18:38:11","29"),
("3191","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:46:23","2020-02-06 18:46:23","29"),
("3192","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/139/0\">0</a>","app\\models\\Timer","139","1","0","2020-02-06 18:46:40","2020-02-06 18:46:40","29"),
("3193","Modified Timer ","app\\models\\Timer","139","1","0","2020-02-06 18:46:49","2020-02-06 18:46:49","29"),
("3194","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/140/0\">0</a>","app\\models\\Timer","140","1","0","2020-02-06 18:47:19","2020-02-06 18:47:19","29"),
("3195","Modified Timer ","app\\models\\Timer","140","1","0","2020-02-06 18:47:44","2020-02-06 18:47:44","29"),
("3196","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:49:18","2020-02-06 18:49:18","29"),
("3197","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/28\">File</a>","app\\modules\\file\\models\\File","28","1","0","2020-02-06 18:50:56","2020-02-06 18:50:56","1"),
("3200","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:52:11","2020-02-06 18:52:11","29"),
("3201","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/141/0\">0</a>","app\\models\\Timer","141","1","0","2020-02-06 18:52:27","2020-02-06 18:52:27","29"),
("3202","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/31\">File</a>","app\\modules\\file\\models\\File","31","1","0","2020-02-06 18:52:46","2020-02-06 18:52:46","1"),
("3203","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 18:54:01","2020-02-06 18:54:01","29"),
("3204","Modified Timer ","app\\models\\Timer","141","1","0","2020-02-06 18:54:27","2020-02-06 18:54:27","29"),
("3205","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/142/0\">0</a>","app\\models\\Timer","142","1","0","2020-02-06 18:54:35","2020-02-06 18:54:35","29"),
("3206","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 19:03:52","2020-02-06 19:03:52","29"),
("3207","Modified Timer ","app\\models\\Timer","142","1","0","2020-02-06 19:04:00","2020-02-06 19:04:00","29"),
("3208","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/143/0\">0</a>","app\\models\\Timer","143","1","0","2020-02-06 19:04:09","2020-02-06 19:04:09","29"),
("3209","Modified Timer ","app\\models\\Timer","143","1","0","2020-02-06 19:04:23","2020-02-06 19:04:23","29"),
("3210","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/144/0\">0</a>","app\\models\\Timer","144","1","0","2020-02-06 19:12:12","2020-02-06 19:12:12","29"),
("3211","Modified Timer ","app\\models\\Timer","144","1","0","2020-02-06 19:12:25","2020-02-06 19:12:25","29"),
("3212","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/145/0\">0</a>","app\\models\\Timer","145","1","0","2020-02-06 19:13:43","2020-02-06 19:13:43","29"),
("3213","Modified Timer ","app\\models\\Timer","145","1","0","2020-02-06 19:14:06","2020-02-06 19:14:06","29"),
("3214","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 19:16:22","2020-02-06 19:16:22","29"),
("3215","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-06 19:17:10","2020-02-06 19:17:10","29"),
("3216","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/146/0\">0</a>","app\\models\\Timer","146","1","0","2020-02-06 19:17:19","2020-02-06 19:17:19","29"),
("3217","Modified Timer ","app\\models\\Timer","146","1","0","2020-02-06 19:17:35","2020-02-06 19:17:35","29"),
("3218","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/147/0\">0</a>","app\\models\\Timer","147","1","0","2020-02-06 19:17:39","2020-02-06 19:17:39","29"),
("3219","Modified Timer ","app\\models\\Timer","147","1","0","2020-02-06 19:17:48","2020-02-06 19:17:48","29"),
("3220","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/148/0\">0</a>","app\\models\\Timer","148","1","0","2020-02-06 19:18:01","2020-02-06 19:18:01","29"),
("3221","Modified Timer ","app\\models\\Timer","148","1","0","2020-02-06 19:18:27","2020-02-06 19:18:27","29"),
("3222","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/149/0\">0</a>","app\\models\\Timer","149","1","0","2020-02-06 19:18:39","2020-02-06 19:18:39","29"),
("3223","Modified Timer ","app\\models\\Timer","149","1","0","2020-02-06 19:18:49","2020-02-06 19:18:49","29"),
("3224","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/150/0\">0</a>","app\\models\\Timer","150","1","0","2020-02-06 19:19:03","2020-02-06 19:19:03","29"),
("3225","Modified Timer ","app\\models\\Timer","150","1","0","2020-02-06 19:19:12","2020-02-06 19:19:12","29"),
("3226","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 09:44:46","2020-02-07 09:44:46","29"),
("3227","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 09:54:27","2020-02-07 09:54:27","29"),
("3228","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/92/34\">34</a>","app\\models\\LoginHistory","92","1","0","2020-02-07 09:57:32","2020-02-07 09:57:32","0"),
("3229","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/93/34\">34</a>","app\\models\\LoginHistory","93","1","0","2020-02-07 09:57:38","2020-02-07 09:57:38","0"),
("3230","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/151/0\">0</a>","app\\models\\Timer","151","1","0","2020-02-07 10:07:04","2020-02-07 10:07:04","29"),
("3231","Modified Timer ","app\\models\\Timer","151","1","0","2020-02-07 10:07:13","2020-02-07 10:07:13","29"),
("3232","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/152/0\">0</a>","app\\models\\Timer","152","1","0","2020-02-07 10:07:16","2020-02-07 10:07:16","29"),
("3233","Modified Timer ","app\\models\\Timer","152","1","0","2020-02-07 10:07:27","2020-02-07 10:07:27","29"),
("3234","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/153/0\">0</a>","app\\models\\Timer","153","1","0","2020-02-07 10:07:29","2020-02-07 10:07:29","29"),
("3235","Modified Timer ","app\\models\\Timer","153","1","0","2020-02-07 10:07:44","2020-02-07 10:07:44","29"),
("3236","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:09:00","2020-02-07 10:09:00","29"),
("3237","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/154/0\">0</a>","app\\models\\Timer","154","1","0","2020-02-07 10:09:06","2020-02-07 10:09:06","29"),
("3238","Modified Timer ","app\\models\\Timer","154","1","0","2020-02-07 10:09:11","2020-02-07 10:09:11","29"),
("3239","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/155/0\">0</a>","app\\models\\Timer","155","1","0","2020-02-07 10:09:13","2020-02-07 10:09:13","29"),
("3240","Modified Timer ","app\\models\\Timer","155","1","0","2020-02-07 10:09:25","2020-02-07 10:09:25","29"),
("3241","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/156/0\">0</a>","app\\models\\Timer","156","1","0","2020-02-07 10:09:27","2020-02-07 10:09:27","29"),
("3242","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/94/1\">1</a>","app\\models\\LoginHistory","94","1","0","2020-02-07 10:10:41","2020-02-07 10:10:41","0"),
("3243","Modified Timer ","app\\models\\Timer","156","1","0","2020-02-07 10:10:43","2020-02-07 10:10:43","29"),
("3244","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/157/0\">0</a>","app\\models\\Timer","157","1","0","2020-02-07 10:10:52","2020-02-07 10:10:52","29"),
("3245","Added new User : <a href=\"http://192.168.2.143/argus-app-yii2-1275/user/view/37/qw\">qw</a>","app\\models\\User","37","1","0","2020-02-07 10:12:10","2020-02-07 10:12:10","1"),
("3246","Modified User ","app\\models\\User","37","1","0","2020-02-07 10:14:19","2020-02-07 10:14:19","0"),
("3247","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/95/37\">37</a>","app\\models\\LoginHistory","95","1","0","2020-02-07 10:14:20","2020-02-07 10:14:20","0"),
("3248","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/22/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 10:14:20","2020-02-07 10:14:20","37"),
("3249","Modified Timer ","app\\models\\Timer","157","1","0","2020-02-07 10:15:37","2020-02-07 10:15:37","29"),
("3250","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/158/0\">0</a>","app\\models\\Timer","158","1","0","2020-02-07 10:16:01","2020-02-07 10:16:01","29"),
("3251","Modified Timer ","app\\models\\Timer","158","1","0","2020-02-07 10:16:27","2020-02-07 10:16:27","29"),
("3252","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/159/0\">0</a>","app\\models\\Timer","159","1","0","2020-02-07 10:16:40","2020-02-07 10:16:40","29"),
("3253","Modified Timer ","app\\models\\Timer","159","1","0","2020-02-07 10:20:32","2020-02-07 10:20:32","29"),
("3254","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:27:06","2020-02-07 10:27:06","29"),
("3255","Added new Department : <a href=\"http://localhost/argus-app-yii2-1275/department/view/1/hr\">Hr</a>","app\\models\\Department","1","1","0","2020-02-07 10:32:45","2020-02-07 10:32:45","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3256","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:33:38","2020-02-07 10:33:38","29"),
("3257","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:37:09","2020-02-07 10:37:09","29"),
("3258","Added new Department : <a href=\"http://localhost/argus-app-yii2-1275/department/view/2/web-yii\">Web Yii</a>","app\\models\\Department","2","1","0","2020-02-07 10:38:27","2020-02-07 10:38:27","1"),
("3259","Modified User ","app\\models\\User","35","1","0","2020-02-07 10:38:36","2020-02-07 10:38:36","1"),
("3260","Added new Cases : <a href=\"http://localhost/argus-app-yii2-1275/cases/view/1/shopping-website\">Shopping Website</a>","app\\models\\Cases","1","1","0","2020-02-07 10:39:07","2020-02-07 10:39:07","1"),
("3261","Modified User ","app\\models\\User","29","1","0","2020-02-07 10:39:28","2020-02-07 10:39:28","1"),
("3262","Modified Task ","app\\models\\Task","13","1","0","2020-02-07 10:39:48","2020-02-07 10:39:48","1"),
("3263","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/160/0\">0</a>","app\\models\\Timer","160","1","0","2020-02-07 10:40:31","2020-02-07 10:40:31","29"),
("3264","Modified Timer ","app\\models\\Timer","160","1","0","2020-02-07 10:40:36","2020-02-07 10:40:36","29"),
("3265","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:43:33","2020-02-07 10:43:33","29"),
("3266","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:44:02","2020-02-07 10:44:02","29"),
("3267","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:44:53","2020-02-07 10:44:53","29"),
("3268","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/161/0\">0</a>","app\\models\\Timer","161","1","0","2020-02-07 10:45:05","2020-02-07 10:45:05","29"),
("3269","Modified Timer ","app\\models\\Timer","161","1","0","2020-02-07 10:45:18","2020-02-07 10:45:18","29"),
("3270","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/162/0\">0</a>","app\\models\\Timer","162","1","0","2020-02-07 10:45:23","2020-02-07 10:45:23","29"),
("3271","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:45:58","2020-02-07 10:45:58","29"),
("3272","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:46:55","2020-02-07 10:46:55","29"),
("3273","Modified Timer ","app\\models\\Timer","162","1","0","2020-02-07 10:47:02","2020-02-07 10:47:02","29"),
("3274","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/163/0\">0</a>","app\\models\\Timer","163","1","0","2020-02-07 10:47:05","2020-02-07 10:47:05","29"),
("3275","Modified Timer ","app\\models\\Timer","163","1","0","2020-02-07 10:47:09","2020-02-07 10:47:09","29"),
("3276","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 10:47:43","2020-02-07 10:47:43","29"),
("3277","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 10:49:21","2020-02-07 10:49:21","37"),
("3278","Modified User ","app\\models\\User","37","1","0","2020-02-07 10:57:26","2020-02-07 10:57:26","1"),
("3279","Added new Task : <a href=\"http://localhost/argus-app-yii2-1275/task/view/14/project-setup\">Project Setup</a>","app\\models\\Task","14","1","0","2020-02-07 11:00:14","2020-02-07 11:00:14","1"),
("3280","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/96/29\">29</a>","app\\models\\LoginHistory","96","1","0","2020-02-07 11:02:46","2020-02-07 11:02:46","0"),
("3281","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/97/29\">29</a>","app\\models\\LoginHistory","97","1","0","2020-02-07 11:02:53","2020-02-07 11:02:53","0"),
("3282","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 11:09:35","2020-02-07 11:09:35","29"),
("3283","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/164/0\">0</a>","app\\models\\Timer","164","1","0","2020-02-07 11:10:23","2020-02-07 11:10:23","29"),
("3284","Modified Task ","app\\models\\Task","14","1","0","2020-02-07 11:10:23","2020-02-07 11:10:23","29"),
("3285","Modified Timer ","app\\models\\Timer","164","1","0","2020-02-07 11:10:27","2020-02-07 11:10:27","29"),
("3286","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 11:31:33","2020-02-07 11:31:33","37"),
("3287","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 11:37:08","2020-02-07 11:37:08","37"),
("3288","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 11:38:11","2020-02-07 11:38:11","37"),
("3289","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 11:41:35","2020-02-07 11:41:35","37"),
("3290","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 11:45:33","2020-02-07 11:45:33","37"),
("3291","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 11:46:09","2020-02-07 11:46:09","37"),
("3292","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 11:46:52","2020-02-07 11:46:52","37"),
("3293","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 11:52:20","2020-02-07 11:52:20","29"),
("3294","Modified Ticket ","app\\models\\Ticket","28","1","0","2020-02-07 11:53:46","2020-02-07 11:53:46","29"),
("3295","Modified Ticket ","app\\models\\Ticket","28","1","0","2020-02-07 11:53:57","2020-02-07 11:53:57","29"),
("3296","Modified Ticket ","app\\models\\Ticket","28","1","0","2020-02-07 11:55:30","2020-02-07 11:55:30","29"),
("3297","Modified User ","app\\models\\User","36","1","0","2020-02-07 11:55:51","2020-02-07 11:55:51","1"),
("3298","Modified User ","app\\models\\User","33","1","0","2020-02-07 11:56:27","2020-02-07 11:56:27","1"),
("3299","Modified User ","app\\models\\User","32","1","0","2020-02-07 11:56:33","2020-02-07 11:56:33","1"),
("3300","Modified User ","app\\models\\User","31","1","0","2020-02-07 11:56:37","2020-02-07 11:56:37","1"),
("3301","Modified User ","app\\models\\User","30","1","0","2020-02-07 11:56:42","2020-02-07 11:56:42","1"),
("3302","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 12:01:03","2020-02-07 12:01:03","37"),
("3303","Added new Ticket Type : <a href=\"http://localhost/argus-app-yii2-1275/ticket-type/view/3/it\">IT</a>","app\\models\\TicketType","3","1","0","2020-02-07 12:01:55","2020-02-07 12:01:55","1"),
("3304","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:02:33","2020-02-07 12:02:33","29"),
("3305","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 12:05:52","2020-02-07 12:05:52","37"),
("3306","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 12:06:21","2020-02-07 12:06:21","37"),
("3307","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/30/ftp\">ftp</a>","app\\models\\Ticket","30","1","0","2020-02-07 12:06:24","2020-02-07 12:06:24","29"),
("3308","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 12:06:45","2020-02-07 12:06:45","37"),
("3309","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 12:17:39","2020-02-07 12:17:39","37"),
("3310","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:18:26","2020-02-07 12:18:26","29"),
("3311","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:20:34","2020-02-07 12:20:34","29"),
("3312","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:21:11","2020-02-07 12:21:11","29"),
("3313","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:22:22","2020-02-07 12:22:22","29"),
("3314","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 12:23:15","2020-02-07 12:23:15","37"),
("3315","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/165/0\">0</a>","app\\models\\Timer","165","1","0","2020-02-07 12:24:32","2020-02-07 12:24:32","29"),
("3316","Modified Timer ","app\\models\\Timer","165","1","0","2020-02-07 12:24:35","2020-02-07 12:24:35","29"),
("3317","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/1/regarding-salary-slip\">Regarding Salary Slip</a>","app\\models\\Ticket","1","1","0","2020-02-07 12:24:44","2020-02-07 12:24:44","1"),
("3318","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","22","1","0","2020-02-07 12:27:12","2020-02-07 12:27:12","37"),
("3319","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/2/notebook\">notebook</a>","app\\models\\Ticket","2","1","0","2020-02-07 12:27:32","2020-02-07 12:27:32","29"),
("3320","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:31:34","2020-02-07 12:31:34","29"),
("3321","Modified Reminder ","app\\models\\Reminder","3","1","0","2020-02-07 12:32:42","2020-02-07 12:32:42","1"),
("3322","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:33:16","2020-02-07 12:33:16","29"),
("3323","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:34:16","2020-02-07 12:34:16","29"),
("3324","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:35:05","2020-02-07 12:35:05","29"),
("3325","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:36:43","2020-02-07 12:36:43","29"),
("3326","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:37:25","2020-02-07 12:37:25","29"),
("3327","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:40:40","2020-02-07 12:40:40","29"),
("3328","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:42:05","2020-02-07 12:42:05","29"),
("3329","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:42:20","2020-02-07 12:42:20","29"),
("3330","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:42:42","2020-02-07 12:42:42","29"),
("3331","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","21","1","0","2020-02-07 12:44:09","2020-02-07 12:44:09","29"),
("3332","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/98/29\">29</a>","app\\models\\LoginHistory","98","1","0","2020-02-07 12:51:35","2020-02-07 12:51:35","0"),
("3333","Modified User ","app\\models\\User","29","1","0","2020-02-07 12:51:35","2020-02-07 12:51:35","29"),
("3334","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/23/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","23","1","0","2020-02-07 12:51:35","2020-02-07 12:51:35","29"),
("3335","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=70&amp;title=fff\">Fff
</a>","app\\modules\\comment\\models\\Comment","70","1","0","2020-02-07 12:53:33","2020-02-07 12:53:33","29"),
("3336","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/99/37\">37</a>","app\\models\\LoginHistory","99","1","0","2020-02-07 13:00:35","2020-02-07 13:00:35","0"),
("3337","Modified User ","app\\models\\User","37","1","0","2020-02-07 13:00:36","2020-02-07 13:00:36","37"),
("3338","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/24/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-02-07 13:00:36","2020-02-07 13:00:36","37"),
("3339","Added new Timer : <a href=\"http://192.168.2.143/argus-app-yii2-1275/timer/view/166/0\">0</a>","app\\models\\Timer","166","1","0","2020-02-07 13:00:47","2020-02-07 13:00:47","29"),
("3340","Modified Timer ","app\\models\\Timer","166","1","0","2020-02-07 13:00:57","2020-02-07 13:00:57","29"),
("3341","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-02-07 13:01:19","2020-02-07 13:01:19","37"),
("3342","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-02-07 13:02:08","2020-02-07 13:02:08","37"),
("3343","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-02-07 13:16:00","2020-02-07 13:16:00","37"),
("3344","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-02-07 13:16:59","2020-02-07 13:16:59","37"),
("3345","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-02-07 13:17:22","2020-02-07 13:17:22","37"),
("3346","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-02-07 13:31:00","2020-02-07 13:31:00","37"),
("3347","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=71&amp;title=state-changed-new-to-planning\">State Changed : New to Planning</a>","app\\modules\\comment\\models\\Comment","71","1","0","2020-02-07 13:32:00","2020-02-07 13:32:00","1"),
("3348","Added new Task : <a href=\"http://localhost/argus-app-yii2-1275/task/view/15/fdgfd\">fdgfd</a>","app\\models\\Task","15","1","0","2020-02-07 13:32:08","2020-02-07 13:32:08","1"),
("3349","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","24","1","0","2020-02-07 13:37:25","2020-02-07 13:37:25","37"),
("3350","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/100/37\">37</a>","app\\models\\LoginHistory","100","1","0","2020-02-07 13:37:40","2020-02-07 13:37:40","0"),
("3351","Modified User ","app\\models\\User","37","1","0","2020-02-07 13:37:40","2020-02-07 13:37:40","37"),
("3352","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/25/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","25","1","0","2020-02-07 13:37:40","2020-02-07 13:37:40","37"),
("3353","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/101/1\">1</a>","app\\models\\LoginHistory","101","1","0","2020-02-07 13:41:30","2020-02-07 13:41:30","0"),
("3354","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/102/37\">37</a>","app\\models\\LoginHistory","102","1","0","2020-02-07 13:42:02","2020-02-07 13:42:02","0"),
("3355","Modified User ","app\\models\\User","37","1","0","2020-02-07 13:42:02","2020-02-07 13:42:02","37");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3356","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/26/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","26","1","0","2020-02-07 13:42:02","2020-02-07 13:42:02","37"),
("3357","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","26","1","0","2020-02-07 13:42:48","2020-02-07 13:42:48","37"),
("3358","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","26","1","0","2020-02-07 14:36:03","2020-02-07 14:36:03","37"),
("3359","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/103/37\">37</a>","app\\models\\LoginHistory","103","1","0","2020-02-07 14:40:14","2020-02-07 14:40:14","0"),
("3360","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:40:14","2020-02-07 14:40:14","37"),
("3361","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/27/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","27","1","0","2020-02-07 14:40:15","2020-02-07 14:40:15","37"),
("3362","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/104/37\">37</a>","app\\models\\LoginHistory","104","1","0","2020-02-07 14:40:27","2020-02-07 14:40:27","0"),
("3363","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:40:27","2020-02-07 14:40:27","37"),
("3364","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/28/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","28","1","0","2020-02-07 14:40:27","2020-02-07 14:40:27","37"),
("3365","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/105/37\">37</a>","app\\models\\LoginHistory","105","1","0","2020-02-07 14:40:33","2020-02-07 14:40:33","0"),
("3366","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:40:33","2020-02-07 14:40:33","37"),
("3367","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/29/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","29","1","0","2020-02-07 14:40:33","2020-02-07 14:40:33","37"),
("3368","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/106/37\">37</a>","app\\models\\LoginHistory","106","1","0","2020-02-07 14:40:50","2020-02-07 14:40:50","0"),
("3369","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:40:50","2020-02-07 14:40:50","37"),
("3370","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/30/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","30","1","0","2020-02-07 14:40:50","2020-02-07 14:40:50","37"),
("3371","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","30","1","0","2020-02-07 14:42:38","2020-02-07 14:42:38","37"),
("3372","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","30","1","0","2020-02-07 14:45:45","2020-02-07 14:45:45","37"),
("3373","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/107/37\">37</a>","app\\models\\LoginHistory","107","1","0","2020-02-07 14:46:09","2020-02-07 14:46:09","0"),
("3374","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:46:09","2020-02-07 14:46:09","37"),
("3375","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/31/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","31","1","0","2020-02-07 14:46:09","2020-02-07 14:46:09","37"),
("3376","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/108/37\">37</a>","app\\models\\LoginHistory","108","1","0","2020-02-07 14:46:27","2020-02-07 14:46:27","0"),
("3377","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:46:27","2020-02-07 14:46:27","37"),
("3378","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/32/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","32","1","0","2020-02-07 14:46:27","2020-02-07 14:46:27","37"),
("3379","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/109/37\">37</a>","app\\models\\LoginHistory","109","1","0","2020-02-07 14:47:44","2020-02-07 14:47:44","0"),
("3380","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:47:44","2020-02-07 14:47:44","37"),
("3381","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/33/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","33","1","0","2020-02-07 14:47:44","2020-02-07 14:47:44","37"),
("3382","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/110/37\">37</a>","app\\models\\LoginHistory","110","1","0","2020-02-07 14:48:05","2020-02-07 14:48:05","0"),
("3383","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:48:05","2020-02-07 14:48:05","37"),
("3384","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/34/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","34","1","0","2020-02-07 14:48:05","2020-02-07 14:48:05","37"),
("3385","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/111/37\">37</a>","app\\models\\LoginHistory","111","1","0","2020-02-07 14:48:39","2020-02-07 14:48:39","0"),
("3386","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:48:39","2020-02-07 14:48:39","37"),
("3387","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/35/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","35","1","0","2020-02-07 14:48:39","2020-02-07 14:48:39","37"),
("3388","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","35","1","0","2020-02-07 14:50:25","2020-02-07 14:50:25","37"),
("3389","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","35","1","0","2020-02-07 14:51:22","2020-02-07 14:51:22","37"),
("3390","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","35","1","0","2020-02-07 14:52:16","2020-02-07 14:52:16","37"),
("3391","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","35","1","0","2020-02-07 14:52:57","2020-02-07 14:52:57","37"),
("3392","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","35","1","0","2020-02-07 14:55:00","2020-02-07 14:55:00","37"),
("3393","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/112/37\">37</a>","app\\models\\LoginHistory","112","1","0","2020-02-07 14:59:55","2020-02-07 14:59:55","0"),
("3394","Modified User ","app\\models\\User","37","1","0","2020-02-07 14:59:55","2020-02-07 14:59:55","37"),
("3395","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/36/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","36","1","0","2020-02-07 14:59:55","2020-02-07 14:59:55","37"),
("3396","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/113/37\">37</a>","app\\models\\LoginHistory","113","1","0","2020-02-07 15:01:05","2020-02-07 15:01:05","0"),
("3397","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:01:05","2020-02-07 15:01:05","37"),
("3398","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/37/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","37","1","0","2020-02-07 15:01:05","2020-02-07 15:01:05","37"),
("3399","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","37","1","0","2020-02-07 15:04:17","2020-02-07 15:04:17","37"),
("3400","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","37","1","0","2020-02-07 15:11:49","2020-02-07 15:11:49","37"),
("3401","Added new Ticket : <a href=\"http://192.168.2.143/argus-app-yii2-1275/ticket/view/3/ff\">Ff</a>","app\\models\\Ticket","3","1","0","2020-02-07 15:12:24","2020-02-07 15:12:24","37"),
("3402","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","37","1","0","2020-02-07 15:20:35","2020-02-07 15:20:35","37"),
("3403","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/114/37\">37</a>","app\\models\\LoginHistory","114","1","0","2020-02-07 15:22:19","2020-02-07 15:22:19","0"),
("3404","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:22:19","2020-02-07 15:22:19","37"),
("3405","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/38/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","38","1","0","2020-02-07 15:22:19","2020-02-07 15:22:19","37"),
("3406","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","38","1","0","2020-02-07 15:23:15","2020-02-07 15:23:15","37"),
("3407","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","38","1","0","2020-02-07 15:24:08","2020-02-07 15:24:08","37"),
("3408","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","38","1","0","2020-02-07 15:29:18","2020-02-07 15:29:18","37"),
("3409","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/115/37\">37</a>","app\\models\\LoginHistory","115","1","0","2020-02-07 15:29:19","2020-02-07 15:29:19","0"),
("3410","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:29:19","2020-02-07 15:29:19","37"),
("3411","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/39/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","39","1","0","2020-02-07 15:29:19","2020-02-07 15:29:19","37"),
("3412","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","39","1","0","2020-02-07 15:29:57","2020-02-07 15:29:57","37"),
("3413","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/116/37\">37</a>","app\\models\\LoginHistory","116","1","0","2020-02-07 15:33:07","2020-02-07 15:33:07","0"),
("3414","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:33:08","2020-02-07 15:33:08","37"),
("3415","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/40/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","40","1","0","2020-02-07 15:33:08","2020-02-07 15:33:08","37"),
("3416","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/117/37\">37</a>","app\\models\\LoginHistory","117","1","0","2020-02-07 15:34:58","2020-02-07 15:34:58","0"),
("3417","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:34:58","2020-02-07 15:34:58","37"),
("3418","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/41/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","41","1","0","2020-02-07 15:34:58","2020-02-07 15:34:58","37"),
("3419","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","41","1","0","2020-02-07 15:43:00","2020-02-07 15:43:00","37"),
("3420","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","41","1","0","2020-02-07 15:43:26","2020-02-07 15:43:26","37"),
("3421","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/118/37\">37</a>","app\\models\\LoginHistory","118","1","0","2020-02-07 15:43:31","2020-02-07 15:43:31","0"),
("3422","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:43:31","2020-02-07 15:43:31","37"),
("3423","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/42/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","42","1","0","2020-02-07 15:43:31","2020-02-07 15:43:31","37"),
("3424","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/119/37\">37</a>","app\\models\\LoginHistory","119","1","0","2020-02-07 15:44:46","2020-02-07 15:44:46","0"),
("3425","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:44:46","2020-02-07 15:44:46","37"),
("3426","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/43/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","43","1","0","2020-02-07 15:44:47","2020-02-07 15:44:47","37"),
("3427","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","43","1","0","2020-02-07 15:45:13","2020-02-07 15:45:13","37"),
("3428","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","43","1","0","2020-02-07 15:45:32","2020-02-07 15:45:32","37"),
("3429","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/120/37\">37</a>","app\\models\\LoginHistory","120","1","0","2020-02-07 15:45:53","2020-02-07 15:45:53","0"),
("3430","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:45:54","2020-02-07 15:45:54","37"),
("3431","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/44/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","44","1","0","2020-02-07 15:45:54","2020-02-07 15:45:54","37"),
("3432","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/121/37\">37</a>","app\\models\\LoginHistory","121","1","0","2020-02-07 15:46:28","2020-02-07 15:46:28","0"),
("3433","Modified User ","app\\models\\User","37","1","0","2020-02-07 15:46:28","2020-02-07 15:46:28","37"),
("3434","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/45/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","45","1","0","2020-02-07 15:46:28","2020-02-07 15:46:28","37"),
("3435","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","45","1","0","2020-02-07 15:50:24","2020-02-07 15:50:24","37"),
("3436","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","45","1","0","2020-02-07 15:51:28","2020-02-07 15:51:28","37"),
("3437","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","45","1","0","2020-02-07 15:52:05","2020-02-07 15:52:05","37"),
("3438","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","45","1","0","2020-02-07 15:52:57","2020-02-07 15:52:57","37"),
("3439","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","45","1","0","2020-02-07 15:56:17","2020-02-07 15:56:17","37"),
("3440","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/122/37\">37</a>","app\\models\\LoginHistory","122","1","0","2020-02-07 16:11:36","2020-02-07 16:11:36","0"),
("3441","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:11:36","2020-02-07 16:11:36","37"),
("3442","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/46/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","46","1","0","2020-02-07 16:11:37","2020-02-07 16:11:37","37"),
("3443","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:11:40","2020-02-07 16:11:40","0"),
("3444","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/123/37\">37</a>","app\\models\\LoginHistory","123","1","0","2020-02-07 16:11:42","2020-02-07 16:11:42","0"),
("3445","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:11:42","2020-02-07 16:11:42","37"),
("3446","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/47/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","47","1","0","2020-02-07 16:11:42","2020-02-07 16:11:42","37"),
("3447","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:11:45","2020-02-07 16:11:45","0"),
("3448","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/124/37\">37</a>","app\\models\\LoginHistory","124","1","0","2020-02-07 16:13:21","2020-02-07 16:13:21","0"),
("3449","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:13:21","2020-02-07 16:13:21","37"),
("3450","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/48/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","48","1","0","2020-02-07 16:13:21","2020-02-07 16:13:21","37"),
("3451","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/125/37\">37</a>","app\\models\\LoginHistory","125","1","0","2020-02-07 16:34:45","2020-02-07 16:34:45","0"),
("3452","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:34:46","2020-02-07 16:34:46","37"),
("3453","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/49/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","49","1","0","2020-02-07 16:34:46","2020-02-07 16:34:46","37"),
("3455","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/126/37\">37</a>","app\\models\\LoginHistory","126","1","0","2020-02-07 16:36:39","2020-02-07 16:36:39","0"),
("3456","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:36:39","2020-02-07 16:36:39","37");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3457","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/50/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","50","1","0","2020-02-07 16:36:39","2020-02-07 16:36:39","37"),
("3458","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","50","1","0","2020-02-07 16:38:16","2020-02-07 16:38:16","37"),
("3459","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","50","1","0","2020-02-07 16:38:30","2020-02-07 16:38:30","37"),
("3460","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/127/37\">37</a>","app\\models\\LoginHistory","127","1","0","2020-02-07 16:43:56","2020-02-07 16:43:56","0"),
("3461","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:43:56","2020-02-07 16:43:56","37"),
("3462","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/51/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","51","1","0","2020-02-07 16:43:56","2020-02-07 16:43:56","37"),
("3463","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=73&amp;title=jiiii\">jiiii</a>","app\\modules\\comment\\models\\Comment","73","1","0","2020-02-07 16:44:26","2020-02-07 16:44:26","37"),
("3464","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/128/37\">37</a>","app\\models\\LoginHistory","128","1","0","2020-02-07 16:44:41","2020-02-07 16:44:41","0"),
("3465","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:44:41","2020-02-07 16:44:41","37"),
("3466","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/52/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","52","1","0","2020-02-07 16:44:41","2020-02-07 16:44:41","37"),
("3467","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/129/37\">37</a>","app\\models\\LoginHistory","129","1","0","2020-02-07 16:44:46","2020-02-07 16:44:46","0"),
("3468","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:44:46","2020-02-07 16:44:46","37"),
("3469","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/53/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","53","1","0","2020-02-07 16:44:46","2020-02-07 16:44:46","37"),
("3470","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/130/37\">37</a>","app\\models\\LoginHistory","130","1","0","2020-02-07 16:44:52","2020-02-07 16:44:52","0"),
("3471","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:44:52","2020-02-07 16:44:52","37"),
("3472","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/54/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","54","1","0","2020-02-07 16:44:52","2020-02-07 16:44:52","37"),
("3473","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/131/37\">37</a>","app\\models\\LoginHistory","131","1","0","2020-02-07 16:45:24","2020-02-07 16:45:24","0"),
("3474","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:45:24","2020-02-07 16:45:24","37"),
("3475","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/55/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","55","1","0","2020-02-07 16:45:24","2020-02-07 16:45:24","37"),
("3477","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/132/37\">37</a>","app\\models\\LoginHistory","132","1","0","2020-02-07 16:46:25","2020-02-07 16:46:25","0"),
("3478","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:46:25","2020-02-07 16:46:25","37"),
("3479","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/56/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","56","1","0","2020-02-07 16:46:25","2020-02-07 16:46:25","37"),
("3480","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/133/37\">37</a>","app\\models\\LoginHistory","133","1","0","2020-02-07 16:46:49","2020-02-07 16:46:49","0"),
("3481","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:46:49","2020-02-07 16:46:49","37"),
("3482","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/57/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","57","1","0","2020-02-07 16:46:50","2020-02-07 16:46:50","37"),
("3483","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=75&amp;title=hii\">hii</a>","app\\modules\\comment\\models\\Comment","75","1","0","2020-02-07 16:46:59","2020-02-07 16:46:59","37"),
("3484","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/134/37\">37</a>","app\\models\\LoginHistory","134","1","0","2020-02-07 16:47:39","2020-02-07 16:47:39","0"),
("3485","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:47:39","2020-02-07 16:47:39","37"),
("3486","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/58/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","58","1","0","2020-02-07 16:47:39","2020-02-07 16:47:39","37"),
("3487","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/135/37\">37</a>","app\\models\\LoginHistory","135","1","0","2020-02-07 16:47:59","2020-02-07 16:47:59","0"),
("3488","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:47:59","2020-02-07 16:47:59","37"),
("3489","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/59/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","59","1","0","2020-02-07 16:47:59","2020-02-07 16:47:59","37"),
("3491","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/136/37\">37</a>","app\\models\\LoginHistory","136","1","0","2020-02-07 16:48:50","2020-02-07 16:48:50","0"),
("3492","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:48:50","2020-02-07 16:48:50","37"),
("3493","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/60/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","60","1","0","2020-02-07 16:48:50","2020-02-07 16:48:50","37"),
("3494","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","60","1","0","2020-02-07 16:49:19","2020-02-07 16:49:19","37"),
("3495","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","60","1","0","2020-02-07 16:49:43","2020-02-07 16:49:43","37"),
("3496","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","60","1","0","2020-02-07 16:51:37","2020-02-07 16:51:37","37"),
("3497","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/137/37\">37</a>","app\\models\\LoginHistory","137","1","0","2020-02-07 16:52:17","2020-02-07 16:52:17","0"),
("3498","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:52:17","2020-02-07 16:52:17","37"),
("3499","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/61/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","61","1","0","2020-02-07 16:52:18","2020-02-07 16:52:18","37"),
("3500","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/138/37\">37</a>","app\\models\\LoginHistory","138","1","0","2020-02-07 16:52:34","2020-02-07 16:52:34","0"),
("3501","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:52:34","2020-02-07 16:52:34","37"),
("3502","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/62/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","62","1","0","2020-02-07 16:52:34","2020-02-07 16:52:34","37"),
("3503","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/139/37\">37</a>","app\\models\\LoginHistory","139","1","0","2020-02-07 16:52:48","2020-02-07 16:52:48","0"),
("3504","Modified User ","app\\models\\User","37","1","0","2020-02-07 16:52:48","2020-02-07 16:52:48","37"),
("3505","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/63/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 16:52:48","2020-02-07 16:52:48","37"),
("3506","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=77&amp;title=hii\">hii</a>","app\\modules\\comment\\models\\Comment","77","1","0","2020-02-07 16:53:03","2020-02-07 16:53:03","37"),
("3507","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 16:57:03","2020-02-07 16:57:03","37"),
("3508","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=78&amp;title=ji\">ji</a>","app\\modules\\comment\\models\\Comment","78","1","0","2020-02-07 16:57:13","2020-02-07 16:57:13","37"),
("3509","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 16:58:35","2020-02-07 16:58:35","37"),
("3510","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 16:58:48","2020-02-07 16:58:48","37"),
("3512","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 16:59:24","2020-02-07 16:59:24","37"),
("3513","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=80&amp;title=hh\">hh</a>","app\\modules\\comment\\models\\Comment","80","1","0","2020-02-07 16:59:31","2020-02-07 16:59:31","37"),
("3514","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 17:00:38","2020-02-07 17:00:38","37"),
("3515","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 17:00:58","2020-02-07 17:00:58","37"),
("3518","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=83&amp;title=uu\">uu</a>","app\\modules\\comment\\models\\Comment","83","1","0","2020-02-07 17:01:20","2020-02-07 17:01:20","37"),
("3519","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 17:05:12","2020-02-07 17:05:12","37"),
("3523","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 17:06:49","2020-02-07 17:06:49","37"),
("3524","Added new Comment : <a href=\"http://192.168.2.143/argus-app-yii2-1275/comment/comment/view?id=87&amp;title=jii\">jii</a>","app\\modules\\comment\\models\\Comment","87","1","0","2020-02-07 17:06:57","2020-02-07 17:06:57","37"),
("3525","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 17:45:33","2020-02-07 17:45:33","37"),
("3526","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 17:55:25","2020-02-07 17:55:25","37"),
("3527","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 17:55:40","2020-02-07 17:55:40","37"),
("3528","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 17:57:14","2020-02-07 17:57:14","37"),
("3529","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:03:06","2020-02-07 18:03:06","37"),
("3530","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:03:43","2020-02-07 18:03:43","37"),
("3531","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:07:55","2020-02-07 18:07:55","37"),
("3532","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:08:16","2020-02-07 18:08:16","37"),
("3533","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:12:44","2020-02-07 18:12:44","37"),
("3534","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:15:01","2020-02-07 18:15:01","37"),
("3535","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:17:00","2020-02-07 18:17:00","37"),
("3536","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:17:44","2020-02-07 18:17:44","37"),
("3537","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","63","1","0","2020-02-07 18:18:14","2020-02-07 18:18:14","37"),
("3538","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/140/1\">1</a>","app\\models\\LoginHistory","140","1","0","2020-02-10 09:34:21","2020-02-10 09:34:21","0"),
("3539","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/141/1\">1</a>","app\\models\\LoginHistory","141","1","0","2020-02-10 09:34:27","2020-02-10 09:34:27","0"),
("3540","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/142/1\">1</a>","app\\models\\LoginHistory","142","1","0","2020-02-10 09:34:34","2020-02-10 09:34:34","0"),
("3541","Added new Client Bills : <a href=\"http://localhost/argus-app-yii2-1275/client-bills/view/1/1\">1</a>","app\\models\\ClientBills","1","1","0","2020-02-10 10:08:40","2020-02-10 10:08:40","1"),
("3542","Added new Department : <a href=\"http://localhost/argus-app-yii2-1275/department/view/3/distribution\">Distribution</a>","app\\models\\Department","3","1","0","2020-02-10 10:20:16","2020-02-10 10:20:16","1"),
("3543","Added new Case Report : <a href=\"http://localhost/argus-app-yii2-1275/case-report/view/1/gfdgfd\">gfdgfd</a>","app\\models\\CaseReport","1","1","0","2020-02-10 10:21:43","2020-02-10 10:21:43","1"),
("3544","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/143/37\">37</a>","app\\models\\LoginHistory","143","1","0","2020-02-10 10:34:34","2020-02-10 10:34:34","0"),
("3545","Modified User ","app\\models\\User","37","1","0","2020-02-10 10:34:34","2020-02-10 10:34:34","37"),
("3546","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/64/25237a1f500b3573\">25237a1f500b3573</a>","app\\modules\\api2\\models\\DeviceDetail","64","1","0","2020-02-10 10:34:34","2020-02-10 10:34:34","37"),
("3547","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/144/37\">37</a>","app\\models\\LoginHistory","144","1","0","2020-02-10 10:44:02","2020-02-10 10:44:02","0"),
("3548","Modified User ","app\\models\\User","37","1","0","2020-02-10 10:44:03","2020-02-10 10:44:03","37"),
("3549","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/65/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","65","1","0","2020-02-10 10:44:03","2020-02-10 10:44:03","37"),
("3550","Modified User ","app\\models\\User","37","1","0","2020-02-10 10:46:26","2020-02-10 10:46:26","0"),
("3551","Added new Login History : <a href=\"http://192.168.2.143/argus-app-yii2-1275/login-history/view/145/37\">37</a>","app\\models\\LoginHistory","145","1","0","2020-02-10 10:46:28","2020-02-10 10:46:28","0"),
("3552","Modified User ","app\\models\\User","37","1","0","2020-02-10 10:46:28","2020-02-10 10:46:28","37"),
("3553","Added new Device Detail : <a href=\"http://192.168.2.143/argus-app-yii2-1275/device-detail/view/66/db4b5d9629f93b4a\">db4b5d9629f93b4a</a>","app\\modules\\api2\\models\\DeviceDetail","66","1","0","2020-02-10 10:46:28","2020-02-10 10:46:28","37"),
("3554","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","66","1","0","2020-02-10 10:47:49","2020-02-10 10:47:49","37"),
("3555","Added new Designation : <a href=\"http://localhost/argus-app-yii2-1275/designation/view/3/distribution\">Distribution</a>","app\\models\\Designation","3","1","0","2020-02-10 10:51:06","2020-02-10 10:51:06","1"),
("3556","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","66","1","0","2020-02-10 10:51:54","2020-02-10 10:51:54","37"),
("3557","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","66","1","0","2020-02-10 10:52:38","2020-02-10 10:52:38","37"),
("3558","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/38/distributor-ios\">Distributor IOS</a>","app\\models\\User","38","1","0","2020-02-10 10:52:50","2020-02-10 10:52:50","1"),
("3559","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/39/test\">test</a>","app\\models\\User","39","1","0","2020-02-10 10:58:49","2020-02-10 10:58:49","1"),
("3560","Added new Client Reports : <a href=\"http://localhost/argus-app-yii2-1275/client-reports/view/1/1\">1</a>","app\\models\\ClientReports","1","1","0","2020-02-10 11:03:50","2020-02-10 11:03:50","1"),
("3561","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/40/testuser\">TestUser</a>","app\\models\\User","40","1","0","2020-02-10 11:09:16","2020-02-10 11:09:16","1"),
("3562","Added new Client : <a href=\"http://localhost/argus-app-yii2-1275/client/view/4/testuser\">TestUser</a>","app\\models\\Client","4","1","0","2020-02-10 11:09:16","2020-02-10 11:09:16","1"),
("3563","Added new Cases : <a href=\"http://localhost/argus-app-yii2-1275/cases/view/2/ecommercetest\">ecommerceTest</a>","app\\models\\Cases","2","1","0","2020-02-10 11:10:35","2020-02-10 11:10:35","1"),
("3564","Added new Case Report : <a href=\"http://localhost/argus-app-yii2-1275/case-report/view/2/test\">test</a>","app\\models\\CaseReport","2","1","0","2020-02-10 11:13:06","2020-02-10 11:13:06","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3565","Added new Team : <a href=\"http://localhost/argus-app-yii2-1275/team/view/5/ecommercetest\">ecommerceTest</a>","app\\models\\Team","5","1","0","2020-02-10 18:30:26","2020-02-10 18:30:26","1"),
("3566","Added new Team : <a href=\"http://localhost/argus-app-yii2-1275/team/view/6/shopping-website\">Shopping Website</a>","app\\models\\Team","6","1","0","2020-02-10 18:54:59","2020-02-10 18:54:59","1"),
("3567","Added new Team : <a href=\"http://localhost/argus-app-yii2-1275/team/view/7/shopping-website\">Shopping Website</a>","app\\models\\Team","7","1","0","2020-02-10 18:59:30","2020-02-10 18:59:30","29"),
("3568","Added new Team : <a href=\"http://localhost/argus-app-yii2-1275/team/view/8/shopping-website\">Shopping Website</a>","app\\models\\Team","8","1","0","2020-02-10 19:00:24","2020-02-10 19:00:24","29"),
("3569","Added new Team : <a href=\"http://localhost/argus-app-yii2-1275/team/view/9/shopping-website\">Shopping Website</a>","app\\models\\Team","9","1","0","2020-02-10 19:00:25","2020-02-10 19:00:25","29"),
("3570","Added new Team : <a href=\"http://localhost/argus-app-yii2-1275/team/view/10/shopping-website\">Shopping Website</a>","app\\models\\Team","10","1","0","2020-02-10 19:00:26","2020-02-10 19:00:26","29"),
("3571","Added new Team : <a href=\"http://localhost/argus-app-yii2-1275/team/view/11/shopping-website\">Shopping Website</a>","app\\models\\Team","11","1","0","2020-02-10 19:00:27","2020-02-10 19:00:27","29"),
("3572","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/146/1\">1</a>","app\\models\\LoginHistory","146","1","0","2020-02-11 09:36:35","2020-02-11 09:36:35",""),
("3573","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/147/1\">1</a>","app\\models\\LoginHistory","147","1","0","2020-02-11 09:36:56","2020-02-11 09:36:56",""),
("3574","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/148/1\">1</a>","app\\models\\LoginHistory","148","1","0","2020-02-11 09:38:26","2020-02-11 09:38:26",""),
("3575","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/149/1\">1</a>","app\\models\\LoginHistory","149","1","0","2020-02-11 09:39:38","2020-02-11 09:39:38",""),
("3576","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/150/1\">1</a>","app\\models\\LoginHistory","150","1","0","2020-02-11 09:39:52","2020-02-11 09:39:52",""),
("3577","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/151/1\">1</a>","app\\models\\LoginHistory","151","1","0","2020-02-11 09:42:31","2020-02-11 09:42:31",""),
("3578","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/152/1\">1</a>","app\\models\\LoginHistory","152","1","0","2020-02-11 09:43:01","2020-02-11 09:43:01",""),
("3579","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/153/1\">1</a>","app\\models\\LoginHistory","153","1","0","2020-02-11 09:43:11","2020-02-11 09:43:11",""),
("3580","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/154/1\">1</a>","app\\models\\LoginHistory","154","1","0","2020-02-11 09:43:26","2020-02-11 09:43:26",""),
("3581","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/4/test\">test</a>","app\\models\\Ticket","4","1","0","2020-02-11 09:57:38","2020-02-11 09:57:38","1"),
("3582","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/155/1\">1</a>","app\\models\\LoginHistory","155","1","0","2020-02-11 10:22:46","2020-02-11 10:22:46",""),
("3583","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/156/1\">1</a>","app\\models\\LoginHistory","156","1","0","2020-02-11 10:22:52","2020-02-11 10:22:52",""),
("3584","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/157/1\">1</a>","app\\models\\LoginHistory","157","1","0","2020-02-11 11:26:15","2020-02-11 11:26:15",""),
("3585","Added new Case Report : <a href=\"http://localhost/argus-app-yii2-1275/case-report/view/3/test\">test</a>","app\\models\\CaseReport","3","1","0","2020-02-11 11:38:59","2020-02-11 11:38:59","1"),
("3586","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/158/1\">1</a>","app\\models\\LoginHistory","158","1","0","2020-02-11 15:47:18","2020-02-11 15:47:18",""),
("3587","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/159/39\">39</a>","app\\models\\LoginHistory","159","1","0","2020-02-11 15:47:49","2020-02-11 15:47:49",""),
("3588","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/160/39\">39</a>","app\\models\\LoginHistory","160","1","0","2020-02-11 15:48:14","2020-02-11 15:48:14",""),
("3589","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/161/1\">1</a>","app\\models\\LoginHistory","161","1","0","2020-02-11 15:48:31","2020-02-11 15:48:31",""),
("3590","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/41/distri\">distri</a>","app\\models\\User","41","1","0","2020-02-11 15:50:03","2020-02-11 15:50:03","1"),
("3591","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/162/41\">41</a>","app\\models\\LoginHistory","162","1","0","2020-02-11 15:50:27","2020-02-11 15:50:27",""),
("3592","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/163/41\">41</a>","app\\models\\LoginHistory","163","1","0","2020-02-11 15:51:03","2020-02-11 15:51:03",""),
("3593","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/164/1\">1</a>","app\\models\\LoginHistory","164","1","0","2020-02-11 15:56:44","2020-02-11 15:56:44",""),
("3594","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/42/aman\">aman</a>","app\\models\\User","42","1","0","2020-02-11 15:58:15","2020-02-11 15:58:15","1"),
("3595","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/165/42\">42</a>","app\\models\\LoginHistory","165","1","0","2020-02-11 15:58:39","2020-02-11 15:58:39",""),
("3596","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/166/42\">42</a>","app\\models\\LoginHistory","166","1","0","2020-02-11 15:59:02","2020-02-11 15:59:02",""),
("3597","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/167/42\">42</a>","app\\models\\LoginHistory","167","1","0","2020-02-11 15:59:16","2020-02-11 15:59:16",""),
("3598","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/168/1\">1</a>","app\\models\\LoginHistory","168","1","0","2020-02-11 16:01:04","2020-02-11 16:01:04",""),
("3599","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/169/42\">42</a>","app\\models\\LoginHistory","169","1","0","2020-02-11 16:01:37","2020-02-11 16:01:37",""),
("3600","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/170/42\">42</a>","app\\models\\LoginHistory","170","1","0","2020-02-11 16:04:40","2020-02-11 16:04:40",""),
("3601","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/171/1\">1</a>","app\\models\\LoginHistory","171","1","0","2020-02-11 16:05:13","2020-02-11 16:05:13",""),
("3602","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=88&amp;title=state-changed-inactive-to-active\">State Changed : Inactive to Active</a>","app\\modules\\comment\\models\\Comment","88","1","0","2020-02-11 16:05:34","2020-02-11 16:05:34","1"),
("3603","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/172/42\">42</a>","app\\models\\LoginHistory","172","1","0","2020-02-11 16:05:53","2020-02-11 16:05:53",""),
("3604","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/173/1\">1</a>","app\\models\\LoginHistory","173","1","0","2020-02-11 16:06:24","2020-02-11 16:06:24",""),
("3605","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/174/41\">41</a>","app\\models\\LoginHistory","174","1","0","2020-02-11 16:06:51","2020-02-11 16:06:51",""),
("3606","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/175/1\">1</a>","app\\models\\LoginHistory","175","1","0","2020-02-11 16:06:58","2020-02-11 16:06:58",""),
("3607","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=89&amp;title=state-changed-inactive-to-active\">State Changed : Inactive to Active</a>","app\\modules\\comment\\models\\Comment","89","1","0","2020-02-11 16:07:10","2020-02-11 16:07:10","1"),
("3608","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/176/41\">41</a>","app\\models\\LoginHistory","176","1","0","2020-02-11 16:07:41","2020-02-11 16:07:41",""),
("3609","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/177/1\">1</a>","app\\models\\LoginHistory","177","1","0","2020-02-11 16:17:47","2020-02-11 16:17:47",""),
("3610","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/178/41\">41</a>","app\\models\\LoginHistory","178","1","0","2020-02-11 16:27:23","2020-02-11 16:27:23",""),
("3611","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/179/41\">41</a>","app\\models\\LoginHistory","179","1","0","2020-02-11 16:27:39","2020-02-11 16:27:39",""),
("3612","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/180/42\">42</a>","app\\models\\LoginHistory","180","1","0","2020-02-11 16:30:07","2020-02-11 16:30:07",""),
("3613","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/181/41\">41</a>","app\\models\\LoginHistory","181","1","0","2020-02-11 16:30:49","2020-02-11 16:30:49",""),
("3614","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/182/41\">41</a>","app\\models\\LoginHistory","182","1","0","2020-02-11 16:31:02","2020-02-11 16:31:02",""),
("3615","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/183/41\">41</a>","app\\models\\LoginHistory","183","1","0","2020-02-11 17:14:16","2020-02-11 17:14:16",""),
("3616","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/184/41\">41</a>","app\\models\\LoginHistory","184","1","0","2020-02-11 17:14:32","2020-02-11 17:14:32",""),
("3617","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/185/41\">41</a>","app\\models\\LoginHistory","185","1","0","2020-02-11 17:16:37","2020-02-11 17:16:37",""),
("3618","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/186/41\">41</a>","app\\models\\LoginHistory","186","1","0","2020-02-11 17:35:35","2020-02-11 17:35:35",""),
("3619","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/187/1\">1</a>","app\\models\\LoginHistory","187","1","0","2020-02-11 18:27:21","2020-02-11 18:27:21",""),
("3620","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/43/invasti\">invasti</a>","app\\models\\User","43","1","0","2020-02-11 18:29:42","2020-02-11 18:29:42","1"),
("3621","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/188/43\">43</a>","app\\models\\LoginHistory","188","1","0","2020-02-11 18:30:06","2020-02-11 18:30:06",""),
("3622","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/189/1\">1</a>","app\\models\\LoginHistory","189","1","0","2020-02-11 18:30:49","2020-02-11 18:30:49",""),
("3623","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/190/43\">43</a>","app\\models\\LoginHistory","190","1","0","2020-02-11 18:31:15","2020-02-11 18:31:15",""),
("3624","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/191/1\">1</a>","app\\models\\LoginHistory","191","1","0","2020-02-12 09:43:57","2020-02-12 09:43:57",""),
("3625","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/44/qtest\">qtest</a>","app\\models\\User","44","1","0","2020-02-12 09:44:51","2020-02-12 09:44:51","1"),
("3626","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=90&amp;title=state-changed-inactive-to-active\">State Changed : Inactive to Active</a>","app\\modules\\comment\\models\\Comment","90","1","0","2020-02-12 09:44:55","2020-02-12 09:44:55","1"),
("3627","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/192/44\">44</a>","app\\models\\LoginHistory","192","1","0","2020-02-12 09:45:17","2020-02-12 09:45:17",""),
("3628","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/193/1\">1</a>","app\\models\\LoginHistory","193","1","0","2020-02-12 09:49:42","2020-02-12 09:49:42",""),
("3629","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/194/44\">44</a>","app\\models\\LoginHistory","194","1","0","2020-02-12 09:50:02","2020-02-12 09:50:02",""),
("3630","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/195/44\">44</a>","app\\models\\LoginHistory","195","1","0","2020-02-12 09:52:33","2020-02-12 09:52:33",""),
("3631","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/196/30\">30</a>","app\\models\\LoginHistory","196","1","0","2020-02-12 09:58:32","2020-02-12 09:58:32",""),
("3632","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/197/1\">1</a>","app\\models\\LoginHistory","197","1","0","2020-02-12 10:02:22","2020-02-12 10:02:22",""),
("3633","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/198/44\">44</a>","app\\models\\LoginHistory","198","1","0","2020-02-12 10:02:41","2020-02-12 10:02:41",""),
("3634","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/199/1\">1</a>","app\\models\\LoginHistory","199","1","0","2020-02-12 10:03:20","2020-02-12 10:03:20",""),
("3635","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/200/1\">1</a>","app\\models\\LoginHistory","200","1","0","2020-02-12 10:03:29","2020-02-12 10:03:29",""),
("3636","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/201/44\">44</a>","app\\models\\LoginHistory","201","1","0","2020-02-12 10:07:58","2020-02-12 10:07:58",""),
("3637","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/202/44\">44</a>","app\\models\\LoginHistory","202","1","0","2020-02-12 10:14:52","2020-02-12 10:14:52",""),
("3638","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/203/44\">44</a>","app\\models\\LoginHistory","203","1","0","2020-02-12 10:17:28","2020-02-12 10:17:28",""),
("3639","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/204/1\">1</a>","app\\models\\LoginHistory","204","1","0","2020-02-12 11:22:35","2020-02-12 11:22:35",""),
("3640","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/205/44\">44</a>","app\\models\\LoginHistory","205","1","0","2020-02-12 11:22:50","2020-02-12 11:22:50",""),
("3641","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/206/1\">1</a>","app\\models\\LoginHistory","206","1","0","2020-02-12 11:23:58","2020-02-12 11:23:58",""),
("3642","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/207/44\">44</a>","app\\models\\LoginHistory","207","1","0","2020-02-12 11:24:12","2020-02-12 11:24:12",""),
("3643","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/208/44\">44</a>","app\\models\\LoginHistory","208","1","0","2020-02-12 11:25:38","2020-02-12 11:25:38",""),
("3644","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/209/44\">44</a>","app\\models\\LoginHistory","209","1","0","2020-02-12 11:39:49","2020-02-12 11:39:49",""),
("3645","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/210/44\">44</a>","app\\models\\LoginHistory","210","1","0","2020-02-12 11:42:02","2020-02-12 11:42:02",""),
("3646","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/5/fdxg\">fdxg</a>","app\\models\\Ticket","5","1","0","2020-02-12 12:30:12","2020-02-12 12:30:12","1"),
("3647","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=91&amp;title=state-changed-assigned-to-inprogress\">State Changed : Assigned to InProgress</a>","app\\modules\\comment\\models\\Comment","91","1","0","2020-02-12 12:30:33","2020-02-12 12:30:33","1"),
("3648","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=92&amp;title=state-changed-inprogress-to-assigned\">State Changed : InProgress to Assigned</a>","app\\modules\\comment\\models\\Comment","92","1","0","2020-02-12 12:30:37","2020-02-12 12:30:37","1"),
("3649","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/211/44\">44</a>","app\\models\\LoginHistory","211","1","0","2020-02-12 12:31:35","2020-02-12 12:31:35",""),
("3650","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=93&amp;title=state-changed-new-to-in-progress\">State Changed : New to In-Progress</a>","app\\modules\\comment\\models\\Comment","93","1","0","2020-02-12 12:36:06","2020-02-12 12:36:06","1"),
("3651","Added new Task : <a href=\"http://localhost/argus-app-yii2-1275/task/view/16/test\">test</a>","app\\models\\Task","16","1","0","2020-02-12 12:36:48","2020-02-12 12:36:48","1"),
("3652","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/212/29\">29</a>","app\\models\\LoginHistory","212","1","0","2020-02-12 12:39:52","2020-02-12 12:39:52",""),
("3653","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/213/44\">44</a>","app\\models\\LoginHistory","213","1","0","2020-02-12 12:40:59","2020-02-12 12:40:59",""),
("3654","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/214/29\">29</a>","app\\models\\LoginHistory","214","1","0","2020-02-12 12:41:13","2020-02-12 12:41:13",""),
("3655","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/215/1\">1</a>","app\\models\\LoginHistory","215","1","0","2020-02-12 12:41:20","2020-02-12 12:41:20",""),
("3656","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/216/29\">29</a>","app\\models\\LoginHistory","216","1","0","2020-02-12 12:41:58","2020-02-12 12:41:58",""),
("3657","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/217/1\">1</a>","app\\models\\LoginHistory","217","1","0","2020-02-12 12:52:11","2020-02-12 12:52:11",""),
("3658","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/218/1\">1</a>","app\\models\\LoginHistory","218","1","0","2020-02-12 12:52:22","2020-02-12 12:52:22",""),
("3659","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/219/29\">29</a>","app\\models\\LoginHistory","219","1","0","2020-02-12 12:54:46","2020-02-12 12:54:46",""),
("3660","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/220/1\">1</a>","app\\models\\LoginHistory","220","1","0","2020-02-12 13:19:52","2020-02-12 13:19:52",""),
("3661","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/221/1\">1</a>","app\\models\\LoginHistory","221","1","0","2020-02-12 13:19:57","2020-02-12 13:19:57",""),
("3662","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/222/1\">1</a>","app\\models\\LoginHistory","222","1","0","2020-02-12 13:20:03","2020-02-12 13:20:03",""),
("3663","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/223/29\">29</a>","app\\models\\LoginHistory","223","1","0","2020-02-12 13:23:45","2020-02-12 13:23:45",""),
("3664","Added new Timer : <a href=\"http://localhost/argus-app-yii2-1275/timer/view/167/0\">0</a>","app\\models\\Timer","167","1","0","2020-02-12 13:30:31","2020-02-12 13:30:31","29");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3665","Modified Timer ","app\\models\\Timer","167","1","0","2020-02-12 13:30:31","2020-02-12 13:30:31","29"),
("3666","Modified Timer ","app\\models\\Timer","167","1","0","2020-02-12 13:30:48","2020-02-12 13:30:48","29"),
("3667","Added new Timer : <a href=\"http://localhost/argus-app-yii2-1275/timer/view/168/0\">0</a>","app\\models\\Timer","168","1","0","2020-02-12 13:30:48","2020-02-12 13:30:48","29"),
("3668","Modified Timer ","app\\models\\Timer","168","1","0","2020-02-12 13:30:48","2020-02-12 13:30:48","29"),
("3669","Modified Timer ","app\\models\\Timer","168","1","0","2020-02-12 13:30:56","2020-02-12 13:30:56","29"),
("3670","Added new Timer : <a href=\"http://localhost/argus-app-yii2-1275/timer/view/169/0\">0</a>","app\\models\\Timer","169","1","0","2020-02-12 13:35:43","2020-02-12 13:35:43","44"),
("3671","Modified Task ","app\\models\\Task","16","1","0","2020-02-12 13:35:43","2020-02-12 13:35:43","44"),
("3672","Modified Timer ","app\\models\\Timer","169","1","0","2020-02-12 13:35:43","2020-02-12 13:35:43","44"),
("3673","Modified Timer ","app\\models\\Timer","169","1","0","2020-02-12 13:35:47","2020-02-12 13:35:47","44"),
("3674","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/224/1\">1</a>","app\\models\\LoginHistory","224","1","0","2020-02-12 13:37:38","2020-02-12 13:37:38",""),
("3675","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/225/1\">1</a>","app\\models\\LoginHistory","225","1","0","2020-02-12 13:37:45","2020-02-12 13:37:45",""),
("3676","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/226/29\">29</a>","app\\models\\LoginHistory","226","1","0","2020-02-12 13:41:12","2020-02-12 13:41:12",""),
("3677","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/227/1\">1</a>","app\\models\\LoginHistory","227","1","0","2020-02-12 14:34:01","2020-02-12 14:34:01",""),
("3678","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/228/44\">44</a>","app\\models\\LoginHistory","228","1","0","2020-02-12 14:34:18","2020-02-12 14:34:18",""),
("3679","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/229/44\">44</a>","app\\models\\LoginHistory","229","1","0","2020-02-12 14:34:36","2020-02-12 14:34:36",""),
("3680","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/230/44\">44</a>","app\\models\\LoginHistory","230","1","0","2020-02-12 14:50:34","2020-02-12 14:50:34",""),
("3681","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/231/1\">1</a>","app\\models\\LoginHistory","231","1","0","2020-02-12 14:51:41","2020-02-12 14:51:41",""),
("3682","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/232/1\">1</a>","app\\models\\LoginHistory","232","1","0","2020-02-12 14:51:47","2020-02-12 14:51:47",""),
("3683","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/45/intest\">intest</a>","app\\models\\User","45","1","0","2020-02-12 14:55:54","2020-02-12 14:55:54","1"),
("3684","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=94&amp;title=state-changed-inactive-to-active\">State Changed : Inactive to Active</a>","app\\modules\\comment\\models\\Comment","94","1","0","2020-02-12 14:55:57","2020-02-12 14:55:57","1"),
("3685","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/233/1\">1</a>","app\\models\\LoginHistory","233","1","0","2020-02-12 14:56:25","2020-02-12 14:56:25",""),
("3686","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/234/45\">45</a>","app\\models\\LoginHistory","234","1","0","2020-02-12 14:56:52","2020-02-12 14:56:52",""),
("3687","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/235/1\">1</a>","app\\models\\LoginHistory","235","1","0","2020-02-12 14:57:06","2020-02-12 14:57:06",""),
("3688","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/236/45\">45</a>","app\\models\\LoginHistory","236","1","0","2020-02-12 14:57:26","2020-02-12 14:57:26",""),
("3689","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/237/45\">45</a>","app\\models\\LoginHistory","237","1","0","2020-02-12 15:01:47","2020-02-12 15:01:47",""),
("3690","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/238/45\">45</a>","app\\models\\LoginHistory","238","1","0","2020-02-12 15:02:03","2020-02-12 15:02:03",""),
("3691","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/239/45\">45</a>","app\\models\\LoginHistory","239","1","0","2020-02-12 15:06:47","2020-02-12 15:06:47",""),
("3692","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/240/1\">1</a>","app\\models\\LoginHistory","240","1","0","2020-02-12 15:23:10","2020-02-12 15:23:10",""),
("3693","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/241/1\">1</a>","app\\models\\LoginHistory","241","1","0","2020-02-12 15:23:19","2020-02-12 15:23:19",""),
("3694","Added new Notice : <a href=\"http://localhost/argus-app-yii2-1275/notice/view/1/test\">test</a>","app\\models\\Notice","1","1","0","2020-02-12 15:25:00","2020-02-12 15:25:00","1"),
("3695","Added new Reminder : <a href=\"http://localhost/argus-app-yii2-1275/reminder/view/4/gdgr\">gdgr</a>","app\\models\\Reminder","4","1","0","2020-02-12 15:44:48","2020-02-12 15:44:48","1"),
("3696","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/46/fcdgd\">fcdgd</a>","app\\models\\User","46","1","0","2020-02-12 16:27:38","2020-02-12 16:27:38","1"),
("3697","Added new Client : <a href=\"http://localhost/argus-app-yii2-1275/client/view/5/fcdgd\">fcdgd</a>","app\\models\\Client","5","1","0","2020-02-12 16:27:38","2020-02-12 16:27:38","1"),
("3698","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/242/35\">35</a>","app\\models\\LoginHistory","242","1","0","2020-02-12 16:51:32","2020-02-12 16:51:32",""),
("3699","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/243/35\">35</a>","app\\models\\LoginHistory","243","1","0","2020-02-12 16:55:04","2020-02-12 16:55:04",""),
("3700","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/244/1\">1</a>","app\\models\\LoginHistory","244","1","0","2020-02-12 16:55:24","2020-02-12 16:55:24",""),
("3701","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/245/1\">1</a>","app\\models\\LoginHistory","245","1","0","2020-02-12 16:55:29","2020-02-12 16:55:29",""),
("3702","Added new Attendance : <a href=\"http://localhost/argus-app-yii2-1275/attendance/view/6/2020-02-12\">2020-02-12</a>","app\\models\\Attendance","6","1","0","2020-02-12 17:28:26","2020-02-12 17:28:26","1"),
("3703","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","66","1","0","2020-02-12 17:41:15","2020-02-12 17:41:15","37"),
("3704","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","66","1","0","2020-02-12 17:41:19","2020-02-12 17:41:19","37"),
("3705","Added new Leave Request : <a href=\"http://localhost/argus-app-yii2-1275/leave-request/view/33/employee-android\">employee android</a>","app\\models\\LeaveRequest","33","1","0","2020-02-12 17:44:41","2020-02-12 17:44:41","37"),
("3706","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/246/35\">35</a>","app\\models\\LoginHistory","246","1","0","2020-02-12 17:48:04","2020-02-12 17:48:04",""),
("3707","Modified User ","app\\models\\User","35","1","0","2020-02-12 17:48:04","2020-02-12 17:48:04","35"),
("3708","Modified User ","app\\models\\User","35","1","0","2020-02-12 17:48:05","2020-02-12 17:48:05","35"),
("3709","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/67/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","67","1","0","2020-02-12 17:48:05","2020-02-12 17:48:05","35"),
("3710","Modified User ","app\\models\\User","35","1","0","2020-02-12 17:48:09","2020-02-12 17:48:09","35"),
("3711","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/247/35\">35</a>","app\\models\\LoginHistory","247","1","0","2020-02-12 17:48:10","2020-02-12 17:48:10","35"),
("3712","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/68/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","68","1","0","2020-02-12 17:48:10","2020-02-12 17:48:10","35"),
("3713","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","68","1","0","2020-02-12 17:48:17","2020-02-12 17:48:17","35"),
("3714","Modified User ","app\\models\\User","35","1","0","2020-02-12 17:48:20","2020-02-12 17:48:20",""),
("3715","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/248/35\">35</a>","app\\models\\LoginHistory","248","1","0","2020-02-12 17:48:22","2020-02-12 17:48:22",""),
("3716","Modified User ","app\\models\\User","35","1","0","2020-02-12 17:48:22","2020-02-12 17:48:22","35"),
("3717","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/69/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","69","1","0","2020-02-12 17:48:23","2020-02-12 17:48:23","35"),
("3718","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/249/45\">45</a>","app\\models\\LoginHistory","249","1","0","2020-02-12 17:50:40","2020-02-12 17:50:40",""),
("3719","Added new Leave Request : <a href=\"http://localhost/argus-app-yii2-1275/leave-request/view/34/intest\">intest</a>","app\\models\\LeaveRequest","34","1","0","2020-02-12 17:52:37","2020-02-12 17:52:37","45"),
("3720","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","69","1","0","2020-02-12 18:04:20","2020-02-12 18:04:20","35"),
("3721","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","69","1","0","2020-02-12 18:04:51","2020-02-12 18:04:51","35"),
("3722","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","69","1","0","2020-02-12 18:05:37","2020-02-12 18:05:37","35"),
("3723","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/250/35\">35</a>","app\\models\\LoginHistory","250","1","0","2020-02-12 18:07:07","2020-02-12 18:07:07",""),
("3724","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/6/test-ticket\">test_ticket</a>","app\\models\\Ticket","6","1","0","2020-02-12 18:20:35","2020-02-12 18:20:35","45"),
("3725","Modified User ","app\\models\\User","35","1","0","2020-02-12 18:26:48","2020-02-12 18:26:48",""),
("3726","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/251/45\">45</a>","app\\models\\LoginHistory","251","1","0","2020-02-12 18:27:03","2020-02-12 18:27:03",""),
("3727","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/252/45\">45</a>","app\\models\\LoginHistory","252","1","0","2020-02-12 18:27:30","2020-02-12 18:27:30",""),
("3728","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/253/45\">45</a>","app\\models\\LoginHistory","253","1","0","2020-02-12 18:28:32","2020-02-12 18:28:32",""),
("3729","Modified User ","app\\models\\User","45","1","0","2020-02-12 18:28:33","2020-02-12 18:28:33","45"),
("3730","Modified User ","app\\models\\User","45","1","0","2020-02-12 18:28:33","2020-02-12 18:28:33","45"),
("3731","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/70/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","70","1","0","2020-02-12 18:28:33","2020-02-12 18:28:33","45"),
("3732","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/254/45\">45</a>","app\\models\\LoginHistory","254","1","0","2020-02-12 18:29:10","2020-02-12 18:29:10",""),
("3733","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","70","1","0","2020-02-12 18:31:14","2020-02-12 18:31:14","45"),
("3734","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/255/45\">45</a>","app\\models\\LoginHistory","255","1","0","2020-02-12 18:31:18","2020-02-12 18:31:18","45"),
("3735","Modified User ","app\\models\\User","45","1","0","2020-02-12 18:31:19","2020-02-12 18:31:19","45"),
("3736","Modified User ","app\\models\\User","45","1","0","2020-02-12 18:31:19","2020-02-12 18:31:19","45"),
("3737","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/71/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","71","1","0","2020-02-12 18:31:20","2020-02-12 18:31:20","45"),
("3738","Modified User ","app\\models\\User","45","1","0","2020-02-12 18:31:37","2020-02-12 18:31:37","45"),
("3739","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/256/45\">45</a>","app\\models\\LoginHistory","256","1","0","2020-02-12 18:31:37","2020-02-12 18:31:37","45"),
("3740","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/72/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","72","1","0","2020-02-12 18:31:38","2020-02-12 18:31:38","45"),
("3741","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","72","1","0","2020-02-12 18:32:35","2020-02-12 18:32:35","45"),
("3742","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","72","1","0","2020-02-12 18:32:55","2020-02-12 18:32:55","45"),
("3743","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","72","1","0","2020-02-12 18:33:45","2020-02-12 18:33:45","45"),
("3744","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/257/35\">35</a>","app\\models\\LoginHistory","257","1","0","2020-02-12 18:36:47","2020-02-12 18:36:47",""),
("3745","Modified User ","app\\models\\User","35","1","0","2020-02-12 18:36:47","2020-02-12 18:36:47","35"),
("3746","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/73/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","73","1","0","2020-02-12 18:36:47","2020-02-12 18:36:47","35"),
("3747","Modified User ","app\\models\\User","35","1","0","2020-02-12 18:36:57","2020-02-12 18:36:57",""),
("3748","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/258/45\">45</a>","app\\models\\LoginHistory","258","1","0","2020-02-12 18:37:09","2020-02-12 18:37:09",""),
("3749","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/259/45\">45</a>","app\\models\\LoginHistory","259","1","0","2020-02-12 18:37:20","2020-02-12 18:37:20",""),
("3750","Modified User ","app\\models\\User","45","1","0","2020-02-12 18:37:20","2020-02-12 18:37:20","45"),
("3751","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/74/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","74","1","0","2020-02-12 18:37:20","2020-02-12 18:37:20","45"),
("3752","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","74","1","0","2020-02-13 09:33:45","2020-02-13 09:33:45","45"),
("3753","Added new Leave Request : <a href=\"http://192.168.2.189/argus-app-yii2-1275/leave-request/view/35/intest\">intest</a>","app\\models\\LeaveRequest","35","1","0","2020-02-13 09:34:14","2020-02-13 09:34:14","45"),
("3754","Added new Leave Request : <a href=\"http://192.168.2.189/argus-app-yii2-1275/leave-request/view/36/intest\">intest</a>","app\\models\\LeaveRequest","36","1","0","2020-02-13 09:34:28","2020-02-13 09:34:28","45"),
("3755","Added new Leave Request : <a href=\"http://192.168.2.189/argus-app-yii2-1275/leave-request/view/37/intest\">intest</a>","app\\models\\LeaveRequest","37","1","0","2020-02-13 09:34:40","2020-02-13 09:34:40","45"),
("3756","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/260/1\">1</a>","app\\models\\LoginHistory","260","1","0","2020-02-13 09:44:18","2020-02-13 09:44:18",""),
("3757","Added new User : <a href=\"http://localhost/argus-app-yii2-1275/user/view/47/mangaer\">Mangaer</a>","app\\models\\User","47","1","0","2020-02-13 09:49:31","2020-02-13 09:49:31","1"),
("3758","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=95&amp;title=state-changed-inactive-to-active\">State Changed : Inactive to Active</a>","app\\modules\\comment\\models\\Comment","95","1","0","2020-02-13 09:49:36","2020-02-13 09:49:36","1"),
("3759","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/261/1\">1</a>","app\\models\\LoginHistory","261","1","0","2020-02-13 09:53:26","2020-02-13 09:53:26",""),
("3760","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/262/1\">1</a>","app\\models\\LoginHistory","262","1","0","2020-02-13 09:54:15","2020-02-13 09:54:15",""),
("3761","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/263/47\">47</a>","app\\models\\LoginHistory","263","1","0","2020-02-13 09:54:44","2020-02-13 09:54:44",""),
("3762","Added new Comment : <a href=\"http://192.168.2.189/argus-app-yii2-1275/comment/comment/view?id=96&amp;title=hi\">hi</a>","app\\modules\\comment\\models\\Comment","96","1","0","2020-02-13 09:55:01","2020-02-13 09:55:01","45"),
("3763","Added new Comment : <a href=\"http://192.168.2.189/argus-app-yii2-1275/comment/comment/view?id=97&amp;title=yyyy\">yyyy</a>","app\\modules\\comment\\models\\Comment","97","1","0","2020-02-13 09:55:11","2020-02-13 09:55:11","45"),
("3764","Added new Ticket : <a href=\"http://192.168.2.189/argus-app-yii2-1275/ticket/view/7/hiiii\">Hiiii</a>","app\\models\\Ticket","7","1","0","2020-02-13 09:55:48","2020-02-13 09:55:48","45");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3765","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/264/1\">1</a>","app\\models\\LoginHistory","264","1","0","2020-02-13 10:06:54","2020-02-13 10:06:54",""),
("3766","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/265/1\">1</a>","app\\models\\LoginHistory","265","1","0","2020-02-13 10:07:02","2020-02-13 10:07:02",""),
("3767","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:07:21","2020-02-13 10:07:21",""),
("3768","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/266/33\">33</a>","app\\models\\LoginHistory","266","1","0","2020-02-13 10:07:32","2020-02-13 10:07:32",""),
("3769","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/267/33\">33</a>","app\\models\\LoginHistory","267","1","0","2020-02-13 10:07:45","2020-02-13 10:07:45",""),
("3770","Modified User ","app\\models\\User","33","1","0","2020-02-13 10:07:45","2020-02-13 10:07:45","33"),
("3771","Modified User ","app\\models\\User","33","1","0","2020-02-13 10:07:45","2020-02-13 10:07:45","33"),
("3772","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/75/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","75","1","0","2020-02-13 10:07:46","2020-02-13 10:07:46","33"),
("3773","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/268/45\">45</a>","app\\models\\LoginHistory","268","1","0","2020-02-13 10:08:54","2020-02-13 10:08:54",""),
("3774","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/269/45\">45</a>","app\\models\\LoginHistory","269","1","0","2020-02-13 10:09:05","2020-02-13 10:09:05",""),
("3775","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:09:06","2020-02-13 10:09:06","45"),
("3776","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/76/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","76","1","0","2020-02-13 10:09:06","2020-02-13 10:09:06","45"),
("3777","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/270/1\">1</a>","app\\models\\LoginHistory","270","1","0","2020-02-13 10:10:36","2020-02-13 10:10:36",""),
("3778","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/271/1\">1</a>","app\\models\\LoginHistory","271","1","0","2020-02-13 10:10:41","2020-02-13 10:10:41",""),
("3779","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/272/45\">45</a>","app\\models\\LoginHistory","272","1","0","2020-02-13 10:11:59","2020-02-13 10:11:59",""),
("3780","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:11:59","2020-02-13 10:11:59","45"),
("3781","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/77/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","77","1","0","2020-02-13 10:12:00","2020-02-13 10:12:00","45"),
("3782","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","77","1","0","2020-02-13 10:17:32","2020-02-13 10:17:32","45"),
("3783","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/273/45\">45</a>","app\\models\\LoginHistory","273","1","0","2020-02-13 10:18:35","2020-02-13 10:18:35",""),
("3784","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:18:36","2020-02-13 10:18:36","45"),
("3785","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/78/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","78","1","0","2020-02-13 10:18:36","2020-02-13 10:18:36","45"),
("3786","Modified Cases ","app\\models\\Cases","2","1","0","2020-02-13 10:20:02","2020-02-13 10:20:02","47"),
("3789","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/33\">File</a>","app\\modules\\file\\models\\File","33","1","0","2020-02-13 10:39:05","2020-02-13 10:39:05","1"),
("3790","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/274/45\">45</a>","app\\models\\LoginHistory","274","1","0","2020-02-13 10:43:18","2020-02-13 10:43:18",""),
("3791","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:43:19","2020-02-13 10:43:19","45"),
("3792","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/79/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","79","1","0","2020-02-13 10:43:19","2020-02-13 10:43:19","45"),
("3793","Modified Leave Request ","app\\models\\LeaveRequest","37","1","0","2020-02-13 10:43:34","2020-02-13 10:43:34","45"),
("3794","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","79","1","0","2020-02-13 10:44:07","2020-02-13 10:44:07","45"),
("3795","Added new Comment : <a href=\"http://192.168.2.189/argus-app-yii2-1275/comment/comment/view?id=99&amp;title=jkljljkl\">Jkljljkl</a>","app\\modules\\comment\\models\\Comment","99","1","0","2020-02-13 10:44:14","2020-02-13 10:44:14","45"),
("3796","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","79","1","0","2020-02-13 10:45:26","2020-02-13 10:45:26","45"),
("3797","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/275/45\">45</a>","app\\models\\LoginHistory","275","1","0","2020-02-13 10:45:28","2020-02-13 10:45:28",""),
("3798","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:45:28","2020-02-13 10:45:28","45"),
("3799","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/80/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","80","1","0","2020-02-13 10:45:29","2020-02-13 10:45:29","45"),
("3800","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/276/45\">45</a>","app\\models\\LoginHistory","276","1","0","2020-02-13 10:45:31","2020-02-13 10:45:31","45"),
("3801","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:45:31","2020-02-13 10:45:31","45"),
("3802","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/81/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","81","1","0","2020-02-13 10:45:31","2020-02-13 10:45:31","45"),
("3803","Added new Leave Request : <a href=\"http://192.168.2.189/argus-app-yii2-1275/leave-request/view/38/intest\">intest</a>","app\\models\\LeaveRequest","38","1","0","2020-02-13 10:45:56","2020-02-13 10:45:56","45"),
("3804","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/277/45\">45</a>","app\\models\\LoginHistory","277","1","0","2020-02-13 10:46:05","2020-02-13 10:46:05",""),
("3805","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:46:06","2020-02-13 10:46:06","45"),
("3806","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/82/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","82","1","0","2020-02-13 10:46:06","2020-02-13 10:46:06","45"),
("3807","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/278/45\">45</a>","app\\models\\LoginHistory","278","1","0","2020-02-13 10:46:14","2020-02-13 10:46:14","45"),
("3808","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:46:14","2020-02-13 10:46:14","45"),
("3809","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/83/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","83","1","0","2020-02-13 10:46:14","2020-02-13 10:46:14","45"),
("3810","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 10:46:27","2020-02-13 10:46:27","45"),
("3811","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 10:46:38","2020-02-13 10:46:38","45"),
("3812","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 10:47:17","2020-02-13 10:47:17","45"),
("3813","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/279/45\">45</a>","app\\models\\LoginHistory","279","1","0","2020-02-13 10:48:11","2020-02-13 10:48:11",""),
("3814","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:48:11","2020-02-13 10:48:11","45"),
("3815","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/84/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","84","1","0","2020-02-13 10:48:11","2020-02-13 10:48:11","45"),
("3816","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/280/45\">45</a>","app\\models\\LoginHistory","280","1","0","2020-02-13 10:49:03","2020-02-13 10:49:03","45"),
("3817","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:49:03","2020-02-13 10:49:03","45"),
("3818","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/85/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","85","1","0","2020-02-13 10:49:03","2020-02-13 10:49:03","45"),
("3819","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/281/45\">45</a>","app\\models\\LoginHistory","281","1","0","2020-02-13 10:49:09","2020-02-13 10:49:09",""),
("3820","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:49:09","2020-02-13 10:49:09","45"),
("3821","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/86/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","86","1","0","2020-02-13 10:49:10","2020-02-13 10:49:10","45"),
("3822","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/282/45\">45</a>","app\\models\\LoginHistory","282","1","0","2020-02-13 10:49:49","2020-02-13 10:49:49","45"),
("3823","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:49:50","2020-02-13 10:49:50","45"),
("3824","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/87/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","87","1","0","2020-02-13 10:49:50","2020-02-13 10:49:50","45"),
("3825","Modified User ","app\\models\\User","43","1","0","2020-02-13 10:51:35","2020-02-13 10:51:35","1"),
("3826","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/283/45\">45</a>","app\\models\\LoginHistory","283","1","0","2020-02-13 10:52:02","2020-02-13 10:52:02",""),
("3827","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:52:02","2020-02-13 10:52:02","45"),
("3828","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/88/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","88","1","0","2020-02-13 10:52:03","2020-02-13 10:52:03","45"),
("3829","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:52:16","2020-02-13 10:52:16",""),
("3830","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/284/45\">45</a>","app\\models\\LoginHistory","284","1","0","2020-02-13 10:54:03","2020-02-13 10:54:03",""),
("3831","Modified User ","app\\models\\User","45","1","0","2020-02-13 10:54:03","2020-02-13 10:54:03","45"),
("3832","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/89/00000000055\">00000000055</a>","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 10:54:03","2020-02-13 10:54:03","45"),
("3833","Added new User : <a href=\"http://192.168.2.189/argus-app-yii2-1275/user/view/48/ashley\">Ashley</a>","app\\models\\User","48","1","0","2020-02-13 10:54:24","2020-02-13 10:54:24","1"),
("3834","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/285/48\">48</a>","app\\models\\LoginHistory","285","1","0","2020-02-13 10:54:40","2020-02-13 10:54:40",""),
("3835","Added new Comment : <a href=\"http://192.168.2.189/argus-app-yii2-1275/comment/comment/view?id=100&amp;title=state-changed-inactive-to-active\">State Changed : Inactive to Active</a>","app\\modules\\comment\\models\\Comment","100","1","0","2020-02-13 10:54:50","2020-02-13 10:54:50","1"),
("3836","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/286/48\">48</a>","app\\models\\LoginHistory","286","1","0","2020-02-13 10:54:54","2020-02-13 10:54:54",""),
("3837","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 10:55:01","2020-02-13 10:55:01","45"),
("3838","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/287/48\">48</a>","app\\models\\LoginHistory","287","1","0","2020-02-13 10:55:20","2020-02-13 10:55:20",""),
("3839","Modified User ","app\\models\\User","48","1","0","2020-02-13 10:55:20","2020-02-13 10:55:20","48"),
("3840","Modified User ","app\\models\\User","48","1","0","2020-02-13 10:55:21","2020-02-13 10:55:21","48"),
("3841","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/90/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","90","1","0","2020-02-13 10:55:21","2020-02-13 10:55:21","48"),
("3842","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 10:55:22","2020-02-13 10:55:22","45"),
("3843","Modified User ","app\\models\\User","48","1","0","2020-02-13 10:56:31","2020-02-13 10:56:31",""),
("3844","Added new Login History : <a href=\"http://192.168.2.189/argus-app-yii2-1275/login-history/view/288/48\">48</a>","app\\models\\LoginHistory","288","1","0","2020-02-13 10:56:31","2020-02-13 10:56:31",""),
("3845","Added new Device Detail : <a href=\"http://192.168.2.189/argus-app-yii2-1275/device-detail/view/91/f8870ef3e62baf05\">f8870ef3e62baf05</a>","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 10:56:32","2020-02-13 10:56:32","48"),
("3846","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 10:58:17","2020-02-13 10:58:17","45"),
("3847","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 10:59:13","2020-02-13 10:59:13","45"),
("3848","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 10:59:25","2020-02-13 10:59:25","45"),
("3849","Added new Reminder : <a href=\"http://localhost/argus-app-yii2-1275/reminder/view/5/test\">test</a>","app\\models\\Reminder","5","1","0","2020-02-13 10:59:35","2020-02-13 10:59:35","1"),
("3850","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 11:00:12","2020-02-13 11:00:12","48"),
("3851","Added new Reminder : <a href=\"http://localhost/argus-app-yii2-1275/reminder/view/6/dsgss\">dsgss</a>","app\\models\\Reminder","6","1","0","2020-02-13 11:03:29","2020-02-13 11:03:29","1"),
("3852","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 11:03:49","2020-02-13 11:03:49","48"),
("3853","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/289/48\">48</a>","app\\models\\LoginHistory","289","1","0","2020-02-13 11:06:24","2020-02-13 11:06:24",""),
("3854","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:06:30","2020-02-13 11:06:30","45"),
("3855","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:06:41","2020-02-13 11:06:41","45"),
("3856","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/290/48\">48</a>","app\\models\\LoginHistory","290","1","0","2020-02-13 11:06:45","2020-02-13 11:06:45",""),
("3857","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:07:00","2020-02-13 11:07:00","45"),
("3858","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:07:27","2020-02-13 11:07:27","45"),
("3859","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 11:07:46","2020-02-13 11:07:46","45"),
("3860","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 11:08:23","2020-02-13 11:08:23","45"),
("3861","Modified Leave Request ","app\\models\\LeaveRequest","37","1","0","2020-02-13 11:08:32","2020-02-13 11:08:32","45"),
("3862","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 11:09:31","2020-02-13 11:09:31","45"),
("3863","Modified Leave Request ","app\\models\\LeaveRequest","36","1","0","2020-02-13 11:09:39","2020-02-13 11:09:39","45"),
("3864","Modified Leave Request ","app\\models\\LeaveRequest","35","1","0","2020-02-13 11:09:49","2020-02-13 11:09:49","45"),
("3865","Modified Leave Request ","app\\models\\LeaveRequest","38","1","0","2020-02-13 11:09:57","2020-02-13 11:09:57","45"),
("3866","Added new Leave Request : <a href=\"http://192.168.2.189/argus-app-yii2-1275/leave-request/view/39/intest\">intest</a>","app\\models\\LeaveRequest","39","1","0","2020-02-13 11:11:21","2020-02-13 11:11:21","45");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3867","Modified Leave Request ","app\\models\\LeaveRequest","39","1","0","2020-02-13 11:11:34","2020-02-13 11:11:34","45"),
("3868","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:15:50","2020-02-13 11:15:50","45"),
("3869","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:16:55","2020-02-13 11:16:55","45"),
("3870","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/291/1\">1</a>","app\\models\\LoginHistory","291","1","0","2020-02-13 11:17:07","2020-02-13 11:17:07",""),
("3871","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/292/47\">47</a>","app\\models\\LoginHistory","292","1","0","2020-02-13 11:17:42","2020-02-13 11:17:42",""),
("3875","Added new Cases : <a href=\"http://localhost/argus-app-yii2-1275/cases/view/4/ttfy\">ttfy</a>","app\\models\\Cases","4","1","0","2020-02-13 11:25:23","2020-02-13 11:25:23","47"),
("3876","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=103&amp;title=state-changed-new-to-maintenance\">State Changed : New to Maintenance</a>","app\\modules\\comment\\models\\Comment","103","1","0","2020-02-13 11:26:26","2020-02-13 11:26:26","47"),
("3878","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=104&amp;title=state-changed-maintenance-to-onhold\">State Changed : Maintenance to OnHold</a>","app\\modules\\comment\\models\\Comment","104","1","0","2020-02-13 11:26:41","2020-02-13 11:26:41","47"),
("3879","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 11:26:58","2020-02-13 11:26:58","48"),
("3880","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=105&amp;title=state-changed-onhold-to-completed\">State Changed : OnHold to Completed</a>","app\\modules\\comment\\models\\Comment","105","1","0","2020-02-13 11:27:14","2020-02-13 11:27:14","47"),
("3881","Added new Task : <a href=\"http://localhost/argus-app-yii2-1275/task/view/19/ftyt\">ftyt</a>","app\\models\\Task","19","1","0","2020-02-13 11:27:18","2020-02-13 11:27:18","47"),
("3882","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:27:22","2020-02-13 11:27:22","45"),
("3883","Added new Comment : <a href=\"http://localhost/argus-app-yii2-1275/comment/comment/view?id=106&amp;title=state-changed-completed-to-assign\">State Changed : Completed to Assign</a>","app\\modules\\comment\\models\\Comment","106","1","0","2020-02-13 11:27:31","2020-02-13 11:27:31","47"),
("3884","Added new Task : <a href=\"http://localhost/argus-app-yii2-1275/task/view/20/fggh\">fggh</a>","app\\models\\Task","20","1","0","2020-02-13 11:27:58","2020-02-13 11:27:58","47"),
("3885","Modified Cases ","app\\models\\Cases","4","1","0","2020-02-13 11:29:31","2020-02-13 11:29:31","47"),
("3886","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 11:32:58","2020-02-13 11:32:58","48"),
("3887","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 11:34:46","2020-02-13 11:34:46","48"),
("3888","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:35:08","2020-02-13 11:35:08","45"),
("3889","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 11:35:13","2020-02-13 11:35:13","48"),
("3890","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:38:06","2020-02-13 11:38:06","45"),
("3891","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:39:18","2020-02-13 11:39:18","45"),
("3892","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:40:21","2020-02-13 11:40:21","45"),
("3893","Added new Comment : <a href=\"http://192.168.2.189/argus-app-yii2-1275/comment/comment/view?id=107&amp;title=zdxsdx\">ZDXSdx</a>","app\\modules\\comment\\models\\Comment","107","1","0","2020-02-13 11:40:51","2020-02-13 11:40:51","45"),
("3894","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:45:09","2020-02-13 11:45:09","45"),
("3895","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","23","1","0","2020-02-13 11:50:22","2020-02-13 11:50:22","29"),
("3896","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:51:48","2020-02-13 11:51:48","45"),
("3897","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 11:58:48","2020-02-13 11:58:48","45"),
("3898","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/293/1\">1</a>","app\\models\\LoginHistory","293","1","0","2020-02-13 12:00:19","2020-02-13 12:00:19",""),
("3899","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:05:01","2020-02-13 12:05:01","48"),
("3900","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 12:07:11","2020-02-13 12:07:11","45"),
("3901","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:14:30","2020-02-13 12:14:30","48"),
("3902","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:17:38","2020-02-13 12:17:38","48"),
("3903","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:17:44","2020-02-13 12:17:44","48"),
("3904","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:18:16","2020-02-13 12:18:16","48"),
("3905","Added new Leave Request : <a href=\"http://192.168.2.189/argus-app-yii2-1275/leave-request/view/40/intest\">intest</a>","app\\models\\LeaveRequest","40","1","0","2020-02-13 12:19:00","2020-02-13 12:19:00","45"),
("3906","Modified Case Report ","app\\models\\CaseReport","3","1","0","2020-02-13 12:20:21","2020-02-13 12:20:21","47"),
("3907","Added new Task : <a href=\"http://localhost/argus-app-yii2-1275/task/view/21/rwfv\">rwfv</a>","app\\models\\Task","21","1","0","2020-02-13 12:23:14","2020-02-13 12:23:14","1"),
("3908","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:23:39","2020-02-13 12:23:39","48"),
("3909","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:24:28","2020-02-13 12:24:28","48"),
("3910","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:26:01","2020-02-13 12:26:01","48"),
("3911","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:26:59","2020-02-13 12:26:59","48"),
("3912","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:28:07","2020-02-13 12:28:07","48"),
("3913","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:29:10","2020-02-13 12:29:10","48"),
("3914","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 12:31:11","2020-02-13 12:31:11","48"),
("3915","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/294/45\">45</a>","app\\models\\LoginHistory","294","1","0","2020-02-13 12:47:35","2020-02-13 12:47:35",""),
("3916","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 12:48:03","2020-02-13 12:48:03","45"),
("3917","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 12:48:30","2020-02-13 12:48:30","45"),
("3918","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/295/44\">44</a>","app\\models\\LoginHistory","295","1","0","2020-02-13 12:48:46","2020-02-13 12:48:46",""),
("3919","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 12:51:00","2020-02-13 12:51:00","45"),
("3920","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/296/1\">1</a>","app\\models\\LoginHistory","296","1","0","2020-02-13 12:51:08","2020-02-13 12:51:08",""),
("3921","Added new Attendance : <a href=\"http://localhost/argus-app-yii2-1275/attendance/view/7/2020-02-13\">2020-02-13</a>","app\\models\\Attendance","7","1","0","2020-02-13 12:51:50","2020-02-13 12:51:50","1"),
("3922","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 12:58:31","2020-02-13 12:58:31","45"),
("3923","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 13:02:34","2020-02-13 13:02:34","48"),
("3924","Added new File : <a href=\"http://localhost/argus-app-yii2-1275/file/file/view/34\">File</a>","app\\modules\\file\\models\\File","34","1","0","2020-02-13 13:06:33","2020-02-13 13:06:33","1"),
("3925","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:07:13","2020-02-13 13:07:13","45"),
("3926","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:07:54","2020-02-13 13:07:54","45"),
("3927","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:11:06","2020-02-13 13:11:06","45"),
("3928","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:12:53","2020-02-13 13:12:53","45"),
("3929","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 13:26:17","2020-02-13 13:26:17","48"),
("3930","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:26:18","2020-02-13 13:26:18","45"),
("3931","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 13:27:27","2020-02-13 13:27:27","48"),
("3932","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:27:50","2020-02-13 13:27:50","45"),
("3933","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 13:28:28","2020-02-13 13:28:28","48"),
("3934","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:28:29","2020-02-13 13:28:29","45"),
("3935","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:28:57","2020-02-13 13:28:57","45"),
("3936","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:33:35","2020-02-13 13:33:35","45"),
("3937","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:36:33","2020-02-13 13:36:33","45"),
("3938","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 13:37:26","2020-02-13 13:37:26","45"),
("3939","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 14:39:35","2020-02-13 14:39:35","48"),
("3940","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 14:40:49","2020-02-13 14:40:49","48"),
("3941","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 14:43:02","2020-02-13 14:43:02","48"),
("3942","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 14:44:28","2020-02-13 14:44:28","48"),
("3943","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 14:45:57","2020-02-13 14:45:57","48"),
("3944","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 14:46:36","2020-02-13 14:46:36","45"),
("3945","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 14:46:50","2020-02-13 14:46:50","48"),
("3946","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 14:55:41","2020-02-13 14:55:41","48"),
("3947","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 14:57:01","2020-02-13 14:57:01","48"),
("3948","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 15:00:10","2020-02-13 15:00:10","48"),
("3949","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 15:00:17","2020-02-13 15:00:17","48"),
("3950","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 15:07:15","2020-02-13 15:07:15","48"),
("3951","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 15:08:46","2020-02-13 15:08:46","48"),
("3952","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 15:10:22","2020-02-13 15:10:22","48"),
("3953","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 15:11:27","2020-02-13 15:11:27","48"),
("3954","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/297/1\">1</a>","app\\models\\LoginHistory","297","1","0","2020-02-13 15:14:59","2020-02-13 15:14:59",""),
("3955","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/298/1\">1</a>","app\\models\\LoginHistory","298","1","0","2020-02-13 15:15:06","2020-02-13 15:15:06",""),
("3956","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 15:17:12","2020-02-13 15:17:12","48"),
("3957","Added new Reminder : <a href=\"http://localhost/argus-app-yii2-1275/reminder/view/7/gfdgh\">gfdgh</a>","app\\models\\Reminder","7","1","0","2020-02-13 15:21:59","2020-02-13 15:21:59","1"),
("3958","Modified Reminder ","app\\models\\Reminder","7","1","0","2020-02-13 15:22:08","2020-02-13 15:22:08","1"),
("3959","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 15:35:40","2020-02-13 15:35:40","45"),
("3960","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 15:47:08","2020-02-13 15:47:08","45"),
("3961","Added new Leave Request : <a href=\"http://localhost/argus-app-yii2-1275/leave-request/view/41/mangaer\">Mangaer</a>","app\\models\\LeaveRequest","41","1","0","2020-02-13 15:53:53","2020-02-13 15:53:53","47"),
("3962","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 15:53:59","2020-02-13 15:53:59","45"),
("3963","Added new Ticket : <a href=\"http://localhost/argus-app-yii2-1275/ticket/view/8/gfdgd\">gfdgd</a>","app\\models\\Ticket","8","1","0","2020-02-13 15:54:11","2020-02-13 15:54:11","47"),
("3964","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 15:55:11","2020-02-13 15:55:11","45"),
("3965","Modified Leave Request ","app\\models\\LeaveRequest","41","1","0","2020-02-13 15:56:41","2020-02-13 15:56:41","47"),
("3966","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 15:58:01","2020-02-13 15:58:01","48"),
("3967","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 16:00:37","2020-02-13 16:00:37","48"),
("3968","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 16:01:11","2020-02-13 16:01:11","48"),
("3969","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 16:01:52","2020-02-13 16:01:52","48"),
("3970","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 16:02:22","2020-02-13 16:02:22","48");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_feed` (`id`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("3971","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 16:03:57","2020-02-13 16:03:57","48"),
("3972","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/299/1\">1</a>","app\\models\\LoginHistory","299","1","0","2020-02-13 16:10:10","2020-02-13 16:10:10",""),
("3973","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 16:10:11","2020-02-13 16:10:11","45"),
("3974","Added new Login History : <a href=\"http://localhost/argus-app-yii2-1275/login-history/view/300/47\">47</a>","app\\models\\LoginHistory","300","1","0","2020-02-13 16:10:47","2020-02-13 16:10:47",""),
("3975","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 16:12:34","2020-02-13 16:12:34","48"),
("3976","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 16:13:22","2020-02-13 16:13:22","48"),
("3977","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 16:16:36","2020-02-13 16:16:36","45"),
("3978","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 16:17:02","2020-02-13 16:17:02","45"),
("3979","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 16:18:31","2020-02-13 16:18:31","45"),
("3980","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 16:19:33","2020-02-13 16:19:33","45"),
("3981","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","91","1","0","2020-02-13 16:20:23","2020-02-13 16:20:23","48"),
("3982","Modified Device Detail ","app\\modules\\api2\\models\\DeviceDetail","89","1","0","2020-02-13 16:22:24","2020-02-13 16:22:24","45");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_files` (`id`,`title`,`model_id`,`model_type`,`target_url`,`description`,`filename_user`,`filename_path`,`extension`,`public`,`size`,`download_count`,`file_type`,`mimetype`,`seo_title`,`seo_alt`,`state_id`,`type_id`,`created_on`,`updated_on`,`updated_by_id`,`created_by_id`) VALUES
("2","","33","app\\models\\User","/argus-app-yii2-1275/user/33","","11.jpeg","f7537a3ee01b70df7671be1cde0499a3-11.jpeg","jpeg","1","15659","4","1","image/jpeg","","","1","0","2020-02-01 16:23:28","2020-02-01 16:23:28","0","1"),
("3","","33","app\\models\\User","/argus-app-yii2-1275/user/33","","2019-1.jpg","ed7dc5f7b44baafe28886483832fc6b5-2019-1.jpg","jpg","1","347339","4","1","image/jpeg","","","1","0","2020-02-01 16:23:28","2020-02-01 16:23:28","0","1"),
("6","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","images.jpeg","34864eacd07c9e81ce5ad680b2ba7c91-images.jpeg","jpeg","1","11039","62","1","image/jpeg","","","1","0","2020-02-01 17:49:21","2020-02-01 17:49:21","0","33"),
("8","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","11.jpeg","ec30bd06b347ac6ea2a718e2f545fb15-11.jpeg","jpeg","1","15659","80","1","image/jpeg","","","1","0","2020-02-01 17:49:21","2020-02-01 17:49:21","0","33"),
("9","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","tollwood-2017-hp-01.jpg","497dce99ec487a5fbd1800fe62018d9d-tollwood-2017-hp-01.jpg","jpg","1","254735","88","1","image/jpeg","","","1","0","2020-02-01 17:49:21","2020-02-01 17:49:21","0","33"),
("12","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","images.png","3640c13eedecc6108604ac6b7e0870a4-images.png","png","1","4911","48","1","image/png","","","1","0","2020-02-03 09:46:56","2020-02-03 09:46:56","0","30"),
("13","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","tollwood-2017-hp-01.jpg","9f474450db4db47d4cf6e17a8e650549-tollwood-2017-hp-01.jpg","jpg","1","254735","76","1","image/jpeg","","","1","0","2020-02-03 09:47:34","2020-02-03 09:47:34","0","30"),
("14","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","1.jpeg","2d46edf62fe5d86632274afab7181cdb-1.jpeg","jpeg","1","20579","75","1","image/jpeg","","","1","0","2020-02-03 09:51:57","2020-02-03 09:51:57","0","30"),
("15","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","2019-1.jpg","bf61fbbae094b31bb90bf49909eb46c9-2019-1.jpg","jpg","1","347339","75","1","image/jpeg","","","1","0","2020-02-03 09:52:12","2020-02-03 09:52:12","0","30"),
("16","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","tollwood-2017-hp-01.jpg","7fc9faa0b6f2ae09445c5b9e03a04d63-tollwood-2017-hp-01.jpg","jpg","1","254735","72","1","image/jpeg","","","1","0","2020-02-03 09:53:46","2020-02-03 09:53:46","0","30"),
("17","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","images.jpeg","4c352cd7583d67d9696dbde7792f5889-images.jpeg","jpeg","1","11039","71","1","image/jpeg","","","1","0","2020-02-03 09:54:31","2020-02-03 09:54:31","0","30"),
("19","","30","app\\models\\LeaveRequest","","","LeaveRequest_30_download.jpeg","","","0","6457","0","1","","","","0","0","2020-02-03 14:52:59","2020-02-03 14:52:59","0","31"),
("20","","30","app\\models\\LeaveRequest","","","LeaveRequest_30_images.png","","","0","4911","0","1","","","","0","0","2020-02-03 14:53:05","2020-02-03 14:53:05","0","31"),
("21","","30","app\\models\\LeaveRequest","","","LeaveRequest_30_images.jpeg","","","0","11039","0","1","","","","0","0","2020-02-03 14:53:15","2020-02-03 14:53:15","0","31"),
("22","","29","app\\models\\User","/argus-app-yii2-1275/user/29","","dteur.pdf","b930b6dca0a274ea8a06a3df665cece6-dteur.pdf","pdf","1","899336","1","1","application/pdf","","","1","0","2020-02-04 16:56:34","2020-02-04 16:56:34","0","31"),
("23","","4","app\\models\\Cases","/argus-app-yii2-1275/user/4","","2019-1.jpg","7c0306434fc24cc77a0b7ee319ec59fb-2019-1.jpg","jpg","1","347339","53","1","image/jpeg","","","1","0","2020-02-06 13:25:22","2020-02-06 13:25:22","0","1"),
("24","","4","app\\models\\Cases","/argus-app-yii2-1275/user/4","","1.jpeg","19dda28b1e8a918d71d735138d23dec6-1.jpeg","jpeg","1","20579","53","1","image/jpeg","","","1","0","2020-02-06 13:25:25","2020-02-06 13:25:25","0","1"),
("25","","4","app\\models\\Cases","/argus-app-yii2-1275/user/4","","download.jpeg","20d6ac505200d65ab4cc5a38501fa07c-download.jpeg","jpeg","1","6457","54","1","image/jpeg","","","1","0","2020-02-06 13:25:28","2020-02-06 13:25:28","0","1"),
("26","","4","app\\models\\Cases","/argus-app-yii2-1275/user/4","","11.jpeg","68c6b5039d9d1c094243496e2e5196db-11.jpeg","jpeg","1","15659","54","1","image/jpeg","","","1","0","2020-02-06 13:25:32","2020-02-06 13:25:32","0","1"),
("27","","4","app\\models\\Cases","/argus-app-yii2-1275/user/4","","images.jpeg","08d17a8438670d1b7051d0e6664a10b9-images.jpeg","jpeg","1","11039","54","1","image/jpeg","","","1","0","2020-02-06 13:25:35","2020-02-06 13:25:35","0","1"),
("28","","4","app\\models\\Cases","/argus-app-yii2-1275/user/4","","images.jpeg","53f9842325880fe39db81d91fdae5e12-images.jpeg","jpeg","1","11039","47","1","image/jpeg","","","1","0","2020-02-06 18:50:56","2020-02-06 18:50:56","0","1"),
("31","","4","app\\models\\Cases","/argus-app-yii2-1275/user/4","","how_to_delete_a_card.mp4","70c1d690c6478c68fa3aedbaf2f068bf-how_to_delete_a_card.mp4","mp4","1","66351672","78","1","video/mp4","","","1","0","2020-02-06 18:52:46","2020-02-06 18:52:46","0","1"),
("32","","1","app\\models\\ClientReports","/argus-app-yii2-1275/clientreports/1","","Untitled 1.odt","1144a4e652f5f4c4dce8b136d70403d6.odt","odt","1","21192","0","1","application/vnd.oasis.opendocument.text","","","1","0","2020-02-10 11:03:50","2020-02-10 11:03:50","","1"),
("33","","2","app\\models\\Cases","/argus-app-yii2-1275/user/2","","index.png","94777aacc7897a9b1138688527dc3dc0-index.png","png","1","4879","8","1","image/png","","","1","0","2020-02-13 10:39:04","2020-02-13 10:39:04","","1"),
("34","","4","app\\models\\Cases","/argus-app-yii2-1275/user/4","","images.png","8e6d2b51417ba63c4796e00c1c5d8ffd-images.png","png","1","5237","7","1","image/png","","","1","0","2020-02-13 13:06:33","2020-02-13 13:06:33","","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_leave_request` (`id`,`description`,`start_date`,`end_date`,`is_halfday`,`type_id`,`state_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("19","<p>urgent work</p>
","2020-02-07","0000-00-00","0","0","1","2020-01-31 00:00:00","2020-02-03 10:22:56","29"),
("20","asdfvcxz","2020-02-11, 2020-02-12","0000-00-00","0","0","1","2020-02-01 00:00:00","0000-00-00 00:00:00","32"),
("21","asc","2020-02-27","0000-00-00","1","0","1","2020-02-01 00:00:00","0000-00-00 00:00:00","32"),
("22","Fgfgfg","2020-02-12,2020-02-19,2020-02-26,2020-02-20,2020-02-13,2020-02-27","0000-00-00","1","0","1","2020-02-01 00:00:00","0000-00-00 00:00:00","31"),
("23","yyy","2020-02-18, 2020-02-19, 2020-02-20, 2020-02-21, 2020-02-22, 2020-02-25, 2020-02-26, 2020-02-27, 2020-02-28, 2020-02-29","0000-00-00","1","0","1","2020-02-01 00:00:00","0000-00-00 00:00:00","32"),
("24","Dfdf dfdfdf","2020-02-20,2020-02-19,2020-02-26,2020-02-25,2020-02-18","0000-00-00","0","0","0","2020-02-01 00:00:00","2020-02-01 16:27:19","31"),
("25","<p>urgent work</p>
","2020-02-19","0000-00-00","0","0","0","2020-02-01 00:00:00","2020-02-03 11:09:53","33"),
("26","sdfg","2020-02-05","0000-00-00","1","0","0","2020-02-01 00:00:00","0000-00-00 00:00:00","32"),
("27","we ff","2020-02-11","0000-00-00","0","0","0","2020-02-01 00:00:00","0000-00-00 00:00:00","32"),
("28","sefhjk","2020-02-10, 2020-02-11","0000-00-00","1","0","0","2020-02-01 00:00:00","2020-02-06 14:39:00","32"),
("30","<p>dsfsdf</p>
","2020-02-19 , 2020-02-20 , 2020-02-21","2020-02-21","0","0","3","2020-02-03 00:00:00","2020-02-03 11:30:31","1"),
("31","<p>newwww</p>
","2020-02-19","2020-02-19","0","0","0","2020-02-03 00:00:00","0000-00-00 00:00:00","29"),
("32","Dffdfd","2020-02-12,2020-02-13,2020-02-14,2020-02-21,2020-02-20","0000-00-00","1","0","0","2020-02-04 00:00:00","2020-02-06 17:59:29","29"),
("33","<p>fdsg</p>
","2020-02-12","","1","0","0","2020-02-12 00:00:00","","37"),
("34","<p>jhgjugjg</p>
","2020-02-12","2020-02-12","0","0","0","2020-02-12 00:00:00","","45"),
("35","Hi","2020-02-13, 2020-02-14","","1","0","0","2020-02-13 00:00:00","2020-02-13 11:09:49","45"),
("36","Yy","2020-02-27, 2020-02-28","","1","0","0","2020-02-13 00:00:00","2020-02-13 11:09:39","45"),
("37","Yy","2020-02-28","","1","0","0","2020-02-13 00:00:00","2020-02-13 11:08:32","45"),
("38","Sdfsfdsfdsfdsfsfdsfdsfdsfdsfdsfdsffffffffffffffffffffffffffffffffffff","2020-02-20,2020-02-19","","0","0","0","2020-02-13 00:00:00","2020-02-13 11:09:57","45"),
("39","Sdsdsdsdsdsd","2020-02-19,2020-02-18","","1","0","0","2020-02-13 00:00:00","2020-02-13 11:11:33","45"),
("40","Assad","2020-02-14,2020-02-15","","1","0","0","2020-02-13 00:00:00","","45"),
("41","<p>dfsfgd</p>
","2020-02-13","2020-02-13","0","0","0","2020-02-13 00:00:00","2020-02-13 15:56:41","47");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_login_history` (`id`,`user_id`,`user_ip`,`user_agent`,`failer_reason`,`state_id`,`type_id`,`code`,`created_on`) VALUES
("1","29","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-01-31 12:27:22"),
("2","28","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-01-31 12:30:29"),
("3","28","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-01-31 12:32:07"),
("4","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-01-31 12:33:04"),
("5","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:42:17"),
("6","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:42:25"),
("7","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:42:30"),
("8","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:42:34"),
("9","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:44:51"),
("10","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:44:54"),
("11","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:45:53"),
("12","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:45:56"),
("13","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:45:59"),
("14","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:46:03"),
("15","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:46:08"),
("16","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:46:13"),
("17","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:46:20"),
("18","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:46:37"),
("19","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:46:57"),
("20","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:47:23"),
("21","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:47:28"),
("22","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:47:28"),
("23","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:47:34"),
("24","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:47:41"),
("25","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","","1","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:48:00"),
("26","31","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-01 09:52:00"),
("27","1","192.168.3.101","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-01 09:56:11"),
("28","32","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-01 10:03:44"),
("29","29","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-01 14:42:15"),
("30","31","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-01 14:54:02"),
("31","31","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-01 14:54:24"),
("32","31","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-01 14:54:31"),
("33","31","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","{\"password\":[\"Incorrect username or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-01 14:55:03"),
("34","31","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-01 14:55:10"),
("35","33","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-01 15:08:47"),
("36","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-03 09:37:21"),
("37","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-03 09:40:30"),
("38","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-03 09:41:01"),
("39","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-03 09:41:11"),
("40","30","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-03 09:45:52"),
("41","30","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-03 09:46:01"),
("42","29","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-03 10:10:18"),
("43","32","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-03 10:19:06"),
("44","32","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-03 10:19:10"),
("45","32","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-03 10:19:21"),
("46","32","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-03 10:20:29"),
("47","29","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-04 09:27:02"),
("48","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-04 09:50:00"),
("49","30","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-04 09:50:12"),
("50","29","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-04 09:52:49"),
("51","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","","1","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-04 10:04:12"),
("52","1","192.168.4.126","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15","","1","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-04 10:04:13"),
("53","29","192.168.4.126","Development/ArgusWest/1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-04 10:05:59"),
("54","29","192.168.4.126","Development/ArgusWest/1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-04 10:06:29"),
("55","29","192.168.4.126","Development/ArgusWest/1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-04 10:06:48"),
("56","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-04 10:07:37"),
("57","31","192.168.4.126","Development/ArgusWest/1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-04 10:51:01"),
("58","31","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-04 10:51:08"),
("59","29","192.168.4.126","Development/ArgusWest/1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-04 11:16:30"),
("60","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-04 11:16:38"),
("61","28","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-04 17:19:17"),
("62","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-04 17:48:13"),
("63","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-04 17:51:37"),
("64","28","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-05 09:27:06"),
("65","1","192.168.3.101","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-05 13:40:19"),
("66","29","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-05 13:42:32"),
("67","32","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-05 13:43:06"),
("68","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-05 14:37:00"),
("69","29","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-05 14:47:04"),
("70","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-05 14:52:35"),
("71","32","192.168.3.101","Argus/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-05 17:19:57"),
("72","28","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-05 18:23:36"),
("73","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 09:27:18"),
("74","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 09:27:32"),
("75","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 09:28:19"),
("76","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 10:18:54"),
("77","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:36:01"),
("78","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:36:20"),
("79","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:38:02"),
("80","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:38:30"),
("81","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:38:37"),
("82","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:38:43"),
("83","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-06 11:42:21"),
("84","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:42:50"),
("85","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:43:00"),
("86","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:43:21"),
("87","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-06 11:44:18"),
("88","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 11:47:29"),
("89","35","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-06 12:31:59"),
("90","32","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-06 14:38:24"),
("91","29","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-06 14:40:31"),
("92","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-07 09:57:32"),
("93","34","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-07 09:57:38"),
("94","1","192.168.3.101","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-07 10:10:41"),
("95","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 10:14:20"),
("96","29","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-07 11:02:46"),
("97","29","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-07 11:02:53"),
("98","29","192.168.2.22","Development/ArgusWest/1.0","","1","0","","2020-02-07 12:51:35"),
("99","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 13:00:35"),
("100","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 13:37:40");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_login_history` (`id`,`user_id`,`user_ip`,`user_agent`,`failer_reason`,`state_id`,`type_id`,`code`,`created_on`) VALUES
("101","1","192.168.3.101","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://192.168.2.143/argus-app-yii2-1275/user/login","2020-02-07 13:41:30"),
("102","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 13:42:02"),
("103","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:40:14"),
("104","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:40:27"),
("105","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:40:33"),
("106","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:40:49"),
("107","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:46:08"),
("108","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:46:27"),
("109","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:47:43"),
("110","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:48:05"),
("111","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:48:39"),
("112","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 14:59:55"),
("113","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:01:05"),
("114","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:22:19"),
("115","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:29:19"),
("116","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:33:07"),
("117","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:34:58"),
("118","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:43:31"),
("119","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:44:46"),
("120","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:45:53"),
("121","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 15:46:28"),
("122","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:11:36"),
("123","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:11:42"),
("124","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:13:21"),
("125","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:34:45"),
("126","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:36:39"),
("127","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:43:56"),
("128","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:44:41"),
("129","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:44:46"),
("130","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:44:52"),
("131","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:45:24"),
("132","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:46:25"),
("133","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:46:49"),
("134","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:47:39"),
("135","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:47:59"),
("136","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:48:50"),
("137","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:52:17"),
("138","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:52:34"),
("139","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-07 16:52:48"),
("140","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-10 09:34:21"),
("141","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-10 09:34:27"),
("142","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-10 09:34:34"),
("143","37","192.168.3.101","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. AOSP on IA Emulator /Android. 9 /Version. 1.0","","1","0","","2020-02-10 10:34:33"),
("144","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-10 10:44:02"),
("145","37","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-10 10:46:28"),
("146","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:36:35"),
("147","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:36:56"),
("148","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:38:26"),
("149","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:39:38"),
("150","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:39:52"),
("151","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:42:31"),
("152","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:43:01"),
("153","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:43:11"),
("154","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 09:43:26"),
("155","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 10:22:46"),
("156","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 10:22:52"),
("157","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 11:26:15"),
("158","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:47:18"),
("159","39","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:47:49"),
("160","39","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:48:14"),
("161","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:48:31"),
("162","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:50:27"),
("163","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:51:03"),
("164","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:56:43"),
("165","42","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:58:39"),
("166","42","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:59:02"),
("167","42","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 15:59:15"),
("168","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:01:04"),
("169","42","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:01:36"),
("170","42","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:04:40"),
("171","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:05:13"),
("172","42","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:05:53"),
("173","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:06:24"),
("174","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:06:51"),
("175","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:06:58"),
("176","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:07:40"),
("177","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:17:47"),
("178","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:27:23"),
("179","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:27:39"),
("180","42","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:30:07"),
("181","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:30:49"),
("182","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 16:31:02"),
("183","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 17:14:16"),
("184","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 17:14:32"),
("185","41","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 17:16:37"),
("186","41","192.168.2.175","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://192.168.2.189/argus-app-yii2-1275/user/login","2020-02-11 17:35:35"),
("187","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 18:27:21"),
("188","43","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"User is Inactive\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 18:30:06"),
("189","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 18:30:49"),
("190","43","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-11 18:31:15"),
("191","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 09:43:56"),
("192","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 09:45:17"),
("193","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 09:49:42"),
("194","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 09:50:02"),
("195","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 09:52:33"),
("196","30","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 09:58:32"),
("197","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 10:02:22"),
("198","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 10:02:40"),
("199","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 10:03:20"),
("200","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 10:03:29");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_login_history` (`id`,`user_id`,`user_ip`,`user_agent`,`failer_reason`,`state_id`,`type_id`,`code`,`created_on`) VALUES
("201","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 10:07:58"),
("202","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 10:14:51"),
("203","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 10:17:28"),
("204","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 11:22:35"),
("205","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 11:22:50"),
("206","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 11:23:58"),
("207","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 11:24:12"),
("208","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 11:25:38"),
("209","44","192.168.2.143","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://192.168.2.189/argus-app-yii2-1275/user/login","2020-02-12 11:39:49"),
("210","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 11:42:02"),
("211","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:31:35"),
("212","29","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:39:52"),
("213","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:40:59"),
("214","29","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:41:13"),
("215","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:41:20"),
("216","29","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:41:58"),
("217","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:52:11"),
("218","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:52:22"),
("219","29","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 12:54:45"),
("220","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 13:19:52"),
("221","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 13:19:57"),
("222","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 13:20:03"),
("223","29","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 13:23:45"),
("224","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 13:37:38"),
("225","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 13:37:44"),
("226","29","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 13:41:12"),
("227","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:34:01"),
("228","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:34:18"),
("229","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:34:36"),
("230","44","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:50:33"),
("231","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:51:40"),
("232","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:51:47"),
("233","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:56:25"),
("234","45","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:56:52"),
("235","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:57:06"),
("236","45","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 14:57:26"),
("237","45","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 15:01:47"),
("238","45","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 15:02:02"),
("239","45","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 15:06:46"),
("240","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 15:23:10"),
("241","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 15:23:18"),
("242","35","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 16:51:32"),
("243","35","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 16:55:04"),
("244","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 16:55:24"),
("245","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 16:55:29"),
("246","35","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-12 17:48:04"),
("247","35","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-12 17:48:10"),
("248","35","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-12 17:48:22"),
("249","45","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 17:50:40"),
("250","35","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 18:07:07"),
("251","45","192.168.4.126","Development/ArgusWest/1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-12 18:27:03"),
("252","45","192.168.4.126","Development/ArgusWest/1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-12 18:27:30"),
("253","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-12 18:28:32"),
("254","45","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-12 18:29:10"),
("255","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-12 18:31:18"),
("256","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-12 18:31:37"),
("257","35","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-12 18:36:46"),
("258","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-12 18:37:09"),
("259","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-12 18:37:19"),
("260","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 09:44:18"),
("261","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 09:53:26"),
("262","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 09:54:15"),
("263","47","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 09:54:44"),
("264","1","192.168.3.106","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://192.168.2.189/argus-app-yii2-1275/user/login","2020-02-13 10:06:54"),
("265","1","192.168.3.106","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://192.168.2.189/argus-app-yii2-1275/user/login","2020-02-13 10:07:02"),
("266","33","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-13 10:07:32"),
("267","33","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:07:44"),
("268","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-13 10:08:54"),
("269","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:09:05"),
("270","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 10:10:36"),
("271","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 10:10:41"),
("272","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-13 10:11:58"),
("273","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:18:35"),
("274","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-13 10:43:18"),
("275","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:45:28"),
("276","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-13 10:45:30"),
("277","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:46:05"),
("278","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-13 10:46:14"),
("279","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:48:10"),
("280","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-13 10:49:03"),
("281","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:49:09"),
("282","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-13 10:49:49"),
("283","45","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:52:02"),
("284","45","192.168.4.126","Development/ArgusWest/1.0","","1","0","","2020-02-13 10:54:02"),
("285","48","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","{\"username\":[\"User is Inactive\"]}","0","0","","2020-02-13 10:54:40"),
("286","48","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","","2020-02-13 10:54:54"),
("287","48","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:55:20"),
("288","48","192.168.3.106","ArgusWest/language/en/timezone/Asia/Kolkata /BuildConfig. Debug /DeviceName. Google /Model. Android SDK built for x86 /Android. 9 /Version. 1.0","","1","0","","2020-02-13 10:56:31"),
("289","48","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 11:06:24"),
("290","48","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 11:06:44"),
("291","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 11:17:07"),
("292","47","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 11:17:42"),
("293","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 12:00:19"),
("294","45","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 12:47:35"),
("295","44","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 12:48:45"),
("296","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 12:51:08"),
("297","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","{\"password\":[\"Incorrect contact no or password.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 15:14:59"),
("298","1","::1","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 15:15:06"),
("299","1","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","{\"username\":[\"Incorrect Email.\"]}","0","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 16:10:10"),
("300","47","127.0.0.1","Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0","","1","0","http://localhost/argus-app-yii2-1275/user/login","2020-02-13 16:10:47");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_migration` (`version`,`apply_time`) VALUES
("m191221_070115_create_table_tbl_case_report","1578320074"),
("m200110_071943_add_column_dept_id_to_tbl_team","1578641346"),
("m200110_092117_relate_column_department_to_tbl_department_in_tbl_user","1578648399"),
("m200116_121518_add_column_budget_tbl_case","1579243754"),
("m200117_063127_add_column_start_time_and_column_end_time_in_tbl_task","1579243758"),
("m200121_072732_create_tbl_task_event","1579593855"),
("m200127_085629_add_column_attendance_id_to_tbl_attendance_event","1580115862"),
("m200129_071451_add_column_month_to_tbl_payroll","1580282261"),
("m200131_043536_add_column_time_differenec_to_tbl_task_event","1580975954"),
("m200206_075703_add_table_case_type","1580975973"),
("m200207_044801_add_column_case_platform_in_tbl_case","1581051170"),
("m200207_044828_add_column_departmentcode_in_tbl_deparment","1581051171"),
("m200210_121353_createTable_tbl_team_department","1581337024");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_notice` (`id`,`title`,`content`,`model_type`,`model_id`,`state_id`,`type_id`,`created_on`,`created_by_id`) VALUES
("1","test","<p>tesst</p>
","app\\models\\User","1","1","0","2020-02-12 15:25:00","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_payroll` (`id`,`department_id`,`day`,`salary`,`user_id`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`,`month`) VALUES
("2","6","31","26000","31","1","0","2020-02-01 11:21:13","0000-00-00 00:00:00","1","1"),
("3","6","30","25000","33","1","0","2020-02-01 15:26:44","0000-00-00 00:00:00","1","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_platform` (`id`,`title`,`description`,`state_id`,`type_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1","Ios","Ios Platform","1","0","2019-12-27 15:32:56","2019-12-27 15:32:56","1"),
("2","Android","Android Platform","1","0","2019-12-27 15:34:13","2019-12-27 15:46:39","1"),
("4","PHP","<p>PHP</p>
","1","0","2020-01-31 12:52:29","2020-01-31 12:52:29","1"),
("5","Hr","<p>Hr</p>
","1","0","2020-02-01 15:17:05","2020-02-01 15:17:05","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_reminder` (`id`,`title`,`description`,`case_id`,`department_id`,`time`,`state_id`,`type_id`,`created_on`,`user_id`,`created_by_id`) VALUES
("3","test","<p>test reminder</p>
","1","1","15:55:16","1","0","2020-02-06 15:55:16","29","1"),
("4","gdgr","<p>gttrgt</p>
","2","1","15:44:48","1","0","2020-02-12 15:44:48","45","1"),
("5","test","<p>dsfdsgds</p>
","1","1","10:59:35","1","0","2020-02-13 10:59:35","48","1"),
("6","dsgss","<p>dfgdh</p>
","2","3","11:03:29","1","0","2020-02-13 11:03:29","48","1"),
("7","gfdgh","","1","1","15:21:59","1","0","2020-02-13 15:21:59","48","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_setting` (`id`,`key`,`title`,`value`,`type_id`,`state_id`,`created_by_id`) VALUES
("1","appConfig","App Configration","{\"companyUrl\":{\"type\":0,\"value\":\"https://www.toxsl.com\",\"required\":true},\"company\":{\"type\":0,\"value\":\"ToXSL Technologies\",\"required\":true},\"companyEmail\":{\"type\":0,\"value\":\"admin@toxsl.in\",\"required\":true},\"companyContactEmail\":{\"type\":0,\"value\":\"admin@toxsl.in\",\"required\":false},\"companyContactNo\":{\"type\":0,\"value\":\"9569127788\",\"required\":false},\"companyAddress\":{\"type\":0,\"value\":\"C-127, 2nd floor, Phase 8, Industrial Area, Sector 72, Mohali, Punjab\",\"required\":false},\"loginCount\":{\"type\":2,\"value\":2,\"required\":false}}","","0","1"),
("2","smtp","SMTP Configration","{\"host\":{\"type\":0,\"value\":\"\",\"required\":true},\"username\":{\"type\":0,\"value\":\"\",\"required\":true},\"password\":{\"type\":0,\"value\":\"\",\"required\":true},\"port\":{\"type\":0,\"value\":\"\",\"required\":true},\"encryption\":{\"type\":0,\"value\":\"\",\"required\":false}}","","0","1"),
("3","firebaseSettings","Firebase Configration","{\"authKey\":{\"type\":0,\"value\":\"\",\"required\":true}}","","0","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_task` (`id`,`title`,`description`,`case_id`,`type_id`,`state_id`,`platform_id`,`department_id`,`start_date`,`end_date`,`estimated_time`,`actual_time`,`created_on`,`updated_on`,`assign_to_id`,`created_by_id`,`start_time`,`end_time`) VALUES
("13","Project Setup","<p>Project Setup</p>
","1","0","2","4","0","2020-02-06 00:00:00","2020-02-08 00:00:00","10:39:00","4238","2020-02-06 12:34:13","2020-02-07 10:39:48","29","1","00:00:00","00:00:00"),
("14","Project Setup","<p>Project Setup</p>
","1","0","2","2","0","2020-02-07 00:00:00","2020-02-08 00:00:00","10:59:00","17","2020-02-07 11:00:14","2020-02-07 11:10:23","37","1","00:00:00","00:00:00"),
("15","fdgfd","<p>gfdgfd</p>
","1","0","1","1","0","2020-02-12 00:00:00","2020-02-15 00:00:00","13:31:00","0","2020-02-07 13:32:08","2020-02-07 13:32:08","35","1","00:00:00","00:00:00"),
("16","test","<p>test</p>
","2","0","2","1","1","2020-02-12 00:00:00","2020-02-13 00:00:00","12:36:00","5","2020-02-12 12:36:48","2020-02-12 13:35:43","44","1","",""),
("19","ftyt","<p>yfuj</p>
","4","0","1","4","1","2020-02-13 00:00:00","2020-02-20 00:00:00","11:27:00","0","2020-02-13 11:27:18","2020-02-13 11:27:18","45","47","",""),
("20","fggh","<p>yhtfg</p>
","4","0","1","4","2","2020-02-13 00:00:00","2020-02-14 00:00:00","11:27:00","0","2020-02-13 11:27:58","2020-02-13 11:27:58","45","47","",""),
("21","rwfv","<p>srfsedcfsx</p>
","4","0","1","4","2","2020-02-13 00:00:00","2020-02-14 00:00:00","12:22:00","0","2020-02-13 12:23:14","2020-02-13 12:23:14","47","1","","");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_team` (`id`,`case_id`,`user_id`,`type_id`,`state_id`,`created_on`,`updated_on`,`created_by_id`,`dept_id`) VALUES
("4","4","29","0","1","2020-02-06 12:32:36","2020-02-06 12:32:36","1","0"),
("5","2","37","0","1","2020-02-10 18:30:26","2020-02-10 18:30:26","1",""),
("6","1","37","3","1","2020-02-10 18:54:58","2020-02-10 18:54:58","1",""),
("7","1","32","2","1","2020-02-10 18:59:30","2020-02-10 18:59:30","29",""),
("8","1","32","2","1","2020-02-10 19:00:24","2020-02-10 19:00:24","29",""),
("9","1","32","2","1","2020-02-10 19:00:25","2020-02-10 19:00:25","29",""),
("10","1","32","2","1","2020-02-10 19:00:26","2020-02-10 19:00:26","29",""),
("11","1","32","2","1","2020-02-10 19:00:26","2020-02-10 19:00:26","29","");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_ticket` (`id`,`title`,`description`,`type_id`,`state_id`,`created_on`,`assign_to_id`,`created_by_id`) VALUES
("1","Regarding Salary Slip","<p>Regarding Salary Slip</p>
","1","0","2020-02-07 12:24:44","30","1"),
("2","notebook","<p>notebook</p>
","1","0","2020-02-07 12:27:32","30","29"),
("3","Ff","Jj","1","0","2020-02-07 15:12:24","33","37"),
("4","test","<p>test</p>
","2","0","2020-02-11 09:57:38","29","1"),
("5","fdxg","<p>dszdf</p>
","1","0","2020-02-12 12:30:12","44","1"),
("6","test_ticket","<p>gdfgg</p>
","1","0","2020-02-12 18:20:35","30","45"),
("7","Hiiii","Get a chance","2","0","2020-02-13 09:55:48","35","45"),
("8","gfdgd","<p>fghfhf</p>
","1","0","2020-02-13 15:54:11","30","47");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_ticket_type` (`id`,`title`,`type_id`,`state_id`,`created_on`,`updated_on`,`created_by_id`) VALUES
("2","Hr/Admin","0","1","2020-01-31 13:01:47","2020-01-31 13:01:47","1"),
("3","IT","0","1","2020-02-07 12:01:55","2020-02-07 12:01:55","1");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_timer` (`id`,`work_time`,`task_id`,`type_id`,`state_id`,`created_on`,`started_on`,`stopped_on`,`created_by_id`) VALUES
("71","5","13","0","1","2020-02-06 12:52:18","0000-00-00 00:00:00","2020-02-06 12:52:23","29"),
("72","5","13","0","1","2020-02-06 15:32:46","0000-00-00 00:00:00","2020-02-06 15:32:51","29"),
("73","54","13","0","1","2020-02-06 17:21:19","0000-00-00 00:00:00","2020-02-06 17:22:13","29"),
("74","1","13","0","1","2020-02-06 17:22:14","0000-00-00 00:00:00","2020-02-06 17:22:15","29"),
("75","4","13","0","1","2020-02-06 17:22:15","0000-00-00 00:00:00","2020-02-06 17:22:19","29"),
("76","2","13","0","1","2020-02-06 17:22:19","0000-00-00 00:00:00","2020-02-06 17:22:21","29"),
("77","2","13","0","1","2020-02-06 17:22:21","0000-00-00 00:00:00","2020-02-06 17:22:23","29"),
("78","2","13","0","1","2020-02-06 17:22:25","0000-00-00 00:00:00","2020-02-06 17:22:27","29"),
("79","26","13","0","1","2020-02-06 17:27:31","0000-00-00 00:00:00","2020-02-06 17:27:57","29"),
("80","7","13","0","1","2020-02-06 17:27:57","0000-00-00 00:00:00","2020-02-06 17:28:04","29"),
("81","5","13","0","1","2020-02-06 17:28:04","0000-00-00 00:00:00","2020-02-06 17:28:09","29"),
("82","1","13","0","1","2020-02-06 17:28:11","0000-00-00 00:00:00","2020-02-06 17:28:12","29"),
("83","4","13","0","1","2020-02-06 17:28:12","0000-00-00 00:00:00","2020-02-06 17:28:16","29"),
("84","1","13","0","1","2020-02-06 17:28:16","0000-00-00 00:00:00","2020-02-06 17:28:17","29"),
("85","2","13","0","1","2020-02-06 17:28:17","0000-00-00 00:00:00","2020-02-06 17:28:19","29"),
("86","1","13","0","1","2020-02-06 17:28:19","0000-00-00 00:00:00","2020-02-06 17:28:20","29"),
("87","3","13","0","1","2020-02-06 17:28:20","0000-00-00 00:00:00","2020-02-06 17:28:23","29"),
("88","2","13","0","1","2020-02-06 17:28:23","0000-00-00 00:00:00","2020-02-06 17:28:25","29"),
("89","1","13","0","1","2020-02-06 17:28:25","0000-00-00 00:00:00","2020-02-06 17:28:26","29"),
("90","54","13","0","1","2020-02-06 17:30:22","0000-00-00 00:00:00","2020-02-06 17:31:16","29"),
("91","11","13","0","1","2020-02-06 17:31:16","0000-00-00 00:00:00","2020-02-06 17:31:27","29"),
("92","7","13","0","1","2020-02-06 17:31:27","0000-00-00 00:00:00","2020-02-06 17:31:34","29"),
("93","33","13","0","1","2020-02-06 17:31:34","0000-00-00 00:00:00","2020-02-06 17:32:07","29"),
("94","151","13","0","1","2020-02-06 17:32:08","0000-00-00 00:00:00","2020-02-06 17:34:39","29"),
("95","101","13","0","1","2020-02-06 17:34:39","0000-00-00 00:00:00","2020-02-06 17:36:20","29"),
("96","5","13","0","1","2020-02-06 17:38:07","0000-00-00 00:00:00","2020-02-06 17:38:12","29"),
("97","3","13","0","1","2020-02-06 17:38:12","0000-00-00 00:00:00","2020-02-06 17:38:15","29"),
("98","36","13","0","1","2020-02-06 17:38:24","0000-00-00 00:00:00","2020-02-06 17:39:00","29"),
("99","8","13","0","1","2020-02-06 17:39:00","0000-00-00 00:00:00","2020-02-06 17:39:08","29"),
("100","2","13","0","1","2020-02-06 17:39:08","0000-00-00 00:00:00","2020-02-06 17:39:10","29"),
("101","19","13","0","1","2020-02-06 17:41:54","0000-00-00 00:00:00","2020-02-06 17:42:13","29"),
("102","5","13","0","1","2020-02-06 17:42:13","0000-00-00 00:00:00","2020-02-06 17:42:18","29"),
("103","3","13","0","1","2020-02-06 17:42:18","0000-00-00 00:00:00","2020-02-06 17:42:21","29"),
("104","16","13","0","1","2020-02-06 17:42:26","0000-00-00 00:00:00","2020-02-06 17:42:42","29"),
("105","7","13","0","1","2020-02-06 17:42:43","0000-00-00 00:00:00","2020-02-06 17:42:50","29"),
("106","11","13","0","1","2020-02-06 17:42:50","0000-00-00 00:00:00","2020-02-06 17:43:01","29"),
("107","2","13","0","1","2020-02-06 17:43:01","0000-00-00 00:00:00","2020-02-06 17:43:03","29"),
("108","6","13","0","1","2020-02-06 17:47:04","0000-00-00 00:00:00","2020-02-06 17:47:10","29"),
("109","7","13","0","1","2020-02-06 17:47:10","0000-00-00 00:00:00","2020-02-06 17:47:17","29"),
("110","11","13","0","1","2020-02-06 17:47:17","0000-00-00 00:00:00","2020-02-06 17:47:28","29"),
("111","296","13","0","1","2020-02-06 17:47:28","0000-00-00 00:00:00","2020-02-06 17:52:24","29"),
("112","4","13","0","1","2020-02-06 17:52:24","0000-00-00 00:00:00","2020-02-06 17:52:28","29"),
("113","5","13","0","1","2020-02-06 17:52:29","0000-00-00 00:00:00","2020-02-06 17:52:34","29"),
("114","60","13","0","1","2020-02-06 17:59:06","0000-00-00 00:00:00","2020-02-06 18:00:06","29"),
("115","22","13","0","1","2020-02-06 18:00:06","0000-00-00 00:00:00","2020-02-06 18:00:28","29"),
("116","231","13","0","1","2020-02-06 18:00:28","0000-00-00 00:00:00","2020-02-06 18:04:19","29"),
("117","16","13","0","1","2020-02-06 18:04:19","0000-00-00 00:00:00","2020-02-06 18:04:35","29"),
("118","177","13","0","1","2020-02-06 18:04:35","0000-00-00 00:00:00","2020-02-06 18:07:32","29"),
("119","9","13","0","1","2020-02-06 18:07:32","0000-00-00 00:00:00","2020-02-06 18:07:41","29"),
("120","92","13","0","1","2020-02-06 18:07:41","0000-00-00 00:00:00","2020-02-06 18:09:13","29"),
("121","13","13","0","1","2020-02-06 18:09:13","0000-00-00 00:00:00","2020-02-06 18:09:26","29"),
("122","294","13","0","1","2020-02-06 18:09:27","0000-00-00 00:00:00","2020-02-06 18:14:21","29"),
("123","274","13","0","1","2020-02-06 18:14:44","0000-00-00 00:00:00","2020-02-06 18:19:18","29"),
("124","3","13","0","1","2020-02-06 18:19:21","0000-00-00 00:00:00","2020-02-06 18:19:24","29"),
("125","13","13","0","1","2020-02-06 18:19:47","0000-00-00 00:00:00","2020-02-06 18:20:00","29"),
("126","8","13","0","1","2020-02-06 18:22:16","0000-00-00 00:00:00","2020-02-06 18:22:24","29"),
("127","195","13","0","1","2020-02-06 18:29:33","0000-00-00 00:00:00","2020-02-06 18:32:48","29"),
("128","2","13","0","1","2020-02-06 18:32:53","0000-00-00 00:00:00","2020-02-06 18:32:55","29"),
("129","170","13","0","1","2020-02-06 18:33:58","0000-00-00 00:00:00","2020-02-06 18:36:48","29"),
("130","2","13","0","1","2020-02-06 18:36:50","0000-00-00 00:00:00","2020-02-06 18:36:52","29"),
("131","1","13","0","1","2020-02-06 18:36:53","0000-00-00 00:00:00","2020-02-06 18:36:54","29"),
("138","70","13","0","1","2020-02-06 18:37:01","0000-00-00 00:00:00","2020-02-06 18:38:11","29"),
("139","9","13","0","1","2020-02-06 18:46:40","0000-00-00 00:00:00","2020-02-06 18:46:49","29"),
("140","25","13","0","1","2020-02-06 18:47:19","0000-00-00 00:00:00","2020-02-06 18:47:44","29"),
("141","120","13","0","1","2020-02-06 18:52:27","0000-00-00 00:00:00","2020-02-06 18:54:27","29"),
("142","565","13","0","1","2020-02-06 18:54:35","0000-00-00 00:00:00","2020-02-06 19:04:00","29"),
("143","14","13","0","1","2020-02-06 19:04:09","0000-00-00 00:00:00","2020-02-06 19:04:23","29"),
("144","13","13","0","1","2020-02-06 19:12:12","0000-00-00 00:00:00","2020-02-06 19:12:25","29"),
("145","23","13","0","1","2020-02-06 19:13:43","0000-00-00 00:00:00","2020-02-06 19:14:06","29"),
("146","16","13","0","1","2020-02-06 19:17:19","0000-00-00 00:00:00","2020-02-06 19:17:35","29"),
("147","9","13","0","1","2020-02-06 19:17:39","0000-00-00 00:00:00","2020-02-06 19:17:48","29"),
("148","26","13","0","1","2020-02-06 19:18:01","0000-00-00 00:00:00","2020-02-06 19:18:27","29"),
("149","10","13","0","1","2020-02-06 19:18:39","0000-00-00 00:00:00","2020-02-06 19:18:49","29"),
("150","9","13","0","1","2020-02-06 19:19:03","0000-00-00 00:00:00","2020-02-06 19:19:12","29"),
("151","9","13","0","1","2020-02-07 10:07:04","0000-00-00 00:00:00","2020-02-07 10:07:13","29"),
("152","11","13","0","1","2020-02-07 10:07:16","0000-00-00 00:00:00","2020-02-07 10:07:27","29"),
("153","15","13","0","1","2020-02-07 10:07:29","0000-00-00 00:00:00","2020-02-07 10:07:44","29"),
("154","5","13","0","1","2020-02-07 10:09:06","0000-00-00 00:00:00","2020-02-07 10:09:11","29"),
("155","12","13","0","1","2020-02-07 10:09:13","0000-00-00 00:00:00","2020-02-07 10:09:25","29"),
("156","76","13","0","1","2020-02-07 10:09:27","0000-00-00 00:00:00","2020-02-07 10:10:43","29"),
("157","285","13","0","1","2020-02-07 10:10:52","0000-00-00 00:00:00","2020-02-07 10:15:37","29"),
("158","27","13","0","1","2020-02-07 10:16:00","0000-00-00 00:00:00","2020-02-07 10:16:27","29"),
("159","232","13","0","1","2020-02-07 10:16:40","0000-00-00 00:00:00","2020-02-07 10:20:32","29"),
("160","5","13","0","1","2020-02-07 10:40:31","0000-00-00 00:00:00","2020-02-07 10:40:36","29"),
("161","13","13","0","1","2020-02-07 10:45:05","0000-00-00 00:00:00","2020-02-07 10:45:18","29"),
("162","99","13","0","1","2020-02-07 10:45:23","0000-00-00 00:00:00","2020-02-07 10:47:02","29"),
("163","4","13","0","1","2020-02-07 10:47:05","0000-00-00 00:00:00","2020-02-07 10:47:09","29"),
("164","4","14","0","1","2020-02-07 11:10:23","0000-00-00 00:00:00","2020-02-07 11:10:27","29"),
("165","3","14","0","1","2020-02-07 12:24:32","0000-00-00 00:00:00","2020-02-07 12:24:35","29"),
("166","10","14","0","1","2020-02-07 13:00:47","0000-00-00 00:00:00","2020-02-07 13:00:57","29"),
("167","16","13","0","1","2020-02-12 13:30:31","0000-00-00 00:00:00","2020-02-12 13:30:47","29"),
("168","7","13","0","1","2020-02-12 13:30:48","0000-00-00 00:00:00","2020-02-12 13:30:55","29"),
("169","5","16","0","1","2020-02-12 13:35:42","0000-00-00 00:00:00","2020-02-12 13:35:47","44");

 -- -------AutobackUpStart------ 


INSERT INTO `tbl_user` (`id`,`first_name`,`last_name`,`full_name`,`email`,`password`,`date_of_birth`,`gender`,`about_me`,`contact_no`,`address`,`latitude`,`longitude`,`city`,`country`,`zipcode`,`language`,`otp`,`department_id`,`designation`,`employee_id`,`joining_date`,`company_id`,`email_verified`,`otp_verified`,`profile_file`,`tos`,`role_id`,`state_id`,`type_id`,`last_visit_time`,`last_action_time`,`last_password_change`,`login_error_count`,`activation_key`,`access_token`,`timezone`,`created_on`,`updated_on`,`created_by_id`) VALUES
("1","","","Admin","admin@toxsl.in","$2y$13$AmuG38fXEKazZvH9RBggDu4WkwxbfKVGDmrPdVzpBr/j9KaEocncq","0000-00-00","0","","","","","","","","","","0","0","0","0","0000-00-00","0","0","0","","0","0","1","0","2020-02-13 15:15:06","0000-00-00 00:00:00","0000-00-00 00:00:00","0","KYhf_nlzoWc2T6O7fso7DCXGrncpJZUz_1576665743","1GM-h6Bz7YYUnU6_gxpR9xFYHnJVxfoZ","","2019-12-18 16:12:22","2019-12-18 16:12:22","0"),
("29","","","lalit Employee","lalitemployee@toxsl.in","$2y$13$7iAjdeXRRXI529pBD8lK2eoiyxBAQZmRTvQWrl2A25LwcqsatIaPO","0000-00-00","0","","768645453","","","","","","","","0","2","1","123","0000-00-00","0","0","1","user-1580453008-profile_file-user_id_1.jpeg","0","7","1","0","2020-02-13 11:50:22","0000-00-00 00:00:00","0000-00-00 00:00:00","0","cJvJv6eR12uMRiFtf5Y1Uf-rSJXRpY87_1580453008","y_ytSKsupan7mHbgAXi6es1il-6x5sfm","","2020-01-31 12:13:28","2020-02-07 10:39:28","1"),
("30","","","Hr manager","Hr@toxsl.in","$2y$13$uxS6XK66KFqsG2Z5ZOiRfeSbATo/zC9JxAyrzLmACQscMHlcNYbkO","0000-00-00","0","","435435657","","","","","","","","1234","1","2","0","0000-00-00","0","0","0","","0","3","1","0","2020-02-13 16:13:22","0000-00-00 00:00:00","0000-00-00 00:00:00","0","_p46UkdN6i_aGkN_-k67ltoECAvX-Qod_1580456144","","","2020-01-31 13:05:44","2020-02-07 11:56:42","1"),
("31","","","Ios jaspreet","jas@hr.com","$2y$13$42IqwMz9J8Lk/5bTTQLmYu6P0vSqPboJagRuLWj3EqHo4Nz5bD716","0000-00-00","0","","9988776655","","","","","","","","1234","1","2","0","0000-00-00","0","0","1","user-1580530845-profile_file-user_id_1.jpeg","0","3","1","0","2020-02-06 12:31:47","0000-00-00 00:00:00","0000-00-00 00:00:00","0","ox0xoMSqPhn7Wm7LP8gjaoRiAGuU-cOJ_1580530845","","","2020-02-01 09:50:45","2020-02-07 11:56:37","1"),
("32","","","bikram","bs@hr.com","$2y$13$1MmG0MKb.u6TEq1diYzqYO1HiTOM0ljNtVdAC23HO.m1v40PfouP6","0000-00-00","0","","9988101095","","","","","","","","1234","1","2","0","0000-00-00","0","0","1","user-1580531452-profile_file-user_id_1.jpg","0","3","1","0","2020-02-06 14:39:05","0000-00-00 00:00:00","0000-00-00 00:00:00","0","ukVTnXrewA6kucIg8s5OC07dLGaj1-I8_1580531452","","","2020-02-01 10:00:52","2020-02-07 11:56:33","1"),
("33","","","Ankan","ankan@hr.com","$2y$13$z5ShoPj9AS1WrOleZqRf3uKgzxItue2r.dC3kHuVKdHpWqilwY/va","0000-00-00","0","","9988777777","","","","","","","","9268","1","2","0","0000-00-00","0","0","0","","0","3","1","0","2020-02-13 10:07:45","0000-00-00 00:00:00","0000-00-00 00:00:00","0","aNI1NFBcWTg3vYhxbHuX8Y18Pl_vMw7x_1580532177","motq0dmbRFHdiP3UqH1GzXTZaMhDUwbl","","2020-02-01 10:12:57","2020-02-07 11:56:27","1"),
("34","","","MR Johnson","johnsan@toxsl.com","$2y$13$7iAjdeXRRXI529pBD8lK2eoiyxBAQZmRTvQWrl2A25LwcqsatIaPO","0000-00-00","0","","+1025685656586","","","","","","","","0","0","0","0","0000-00-00","0","0","0","","0","5","1","0","2020-02-07 09:57:38","0000-00-00 00:00:00","0000-00-00 00:00:00","0","","","","2020-02-06 10:16:01","2020-02-06 11:37:48","1"),
("35","","","Naresh Saini","nareshmanager@toxsl.in","$2y$13$JfwMyfJTqcPlRtbxITHP6OEVcakD2m9/OmgGpHXxtxUojxxWK6f2K","0000-00-00","0","","75464654","","","","","","","","","2","1","201","0000-00-00","0","0","1","","0","1","1","0","2020-02-13 09:50:45","0000-00-00 00:00:00","0000-00-00 00:00:00","0","tQzxUhSZ9fnROOLoOXzFiQWDzFUGzOhq_1580970600","","","2020-02-06 12:00:00","2020-02-07 10:38:36","1"),
("36","","","poll","pool@gmail.com","$2y$13$9BJtJtN1MCNZ9F6q3bnN1ON4AxH6G8Iw4ZJu1Giwe5oRTkkacdtIG","0000-00-00","0","","9966332255","","","","","","","","1234","1","2","203","0000-00-00","0","0","0","user-1580980678-profile_file-user_id_1.jpg","0","3","0","0","0000-00-00 00:00:00","0000-00-00 00:00:00","0000-00-00 00:00:00","0","2i6Hn_zUJNiC0MZAq4CRrv2gYvV_3a_o_1580980678","","","2020-02-06 14:47:58","2020-02-07 11:55:50","1"),
("37","","","employee android","as@gmail.com","$2y$13$AUQtrdAvIh37Ai326YE6eutdnKgfdfp2QmPxWNn9IB7b1X5Y1yUH.","0000-00-00","0","","9865327410","","","","","","","","0","2","1","963","0000-00-00","0","0","1","","0","7","1","0","2020-02-12 17:41:18","0000-00-00 00:00:00","0000-00-00 00:00:00","0","jLNgB0on_gaIuAK8IMwmq4BtSPHugQ3i_1581050530","aOh5SqG9LFBMVMMdFXp7yU7Q_7FfUT24","","2020-02-07 10:12:10","2020-02-07 10:57:26","1"),
("38","","","Distributor IOS","distributor@toxsl.in","$2y$13$vs6IVklTCpUx5H3fbgZhfurCcXC91gHn2ipfXxEPMacuc9FedoRJO","0000-00-00","0","","9876543201","","","","","","","","1234","3","3","301","0000-00-00","0","0","0","","0","6","0","0","0000-00-00 00:00:00","0000-00-00 00:00:00","0000-00-00 00:00:00","0","ZY7HOj-c2ItwkQFjmK8EVAhPs_ERjwiO_1581312169","","","2020-02-10 10:52:49","0000-00-00 00:00:00","1"),
("39","","","test","test@gmail.com","$2y$13$v3vQ0lXIfI7zBqSyrzJZsOCACzG25kfL5lB.rb83XcfDORmubBABG","","0","","1234567890","","","","","","","","1234","1","2","","","","0","0","","","6","0","0","","","","","JmYOEOvDQjcCHvJMPdMurtqyhILC9wtd_1581312528","","","2020-02-10 10:58:48","","1"),
("40","","","TestUser","testuser@gmail.com","$2y$13$6Cr5em.SJKr7bUDgYaCm5e2Q8bOyoPYYLRUeH1rwZuctDzIgOMRbO","","0","","","","","","","","","","","","0","","","","0","0","","","5","1","0","","","","","","","","2020-02-10 11:09:16","","1"),
("41","","","distri","distri@gmail.com","$2y$13$.rgvDVS4i8rxm2lf1.7kpOhMuWTVY6LjlHDe6yOzjM0wzzD6TILa6","","0","","1234567893","","","","","","","","1234","3","1","","","","0","0","","","6","1","0","2020-02-11 17:35:35","","","","vjWruYH53DRQsY-PWF-4brRtAcazvfBi_1581416402","","","2020-02-11 15:50:02","","1"),
("42","","","aman","aman@gmail.com","$2y$13$3u4jZaVeTrPz8b.q.6MSQOPDl6queInjEbA4kIKUt43Qh9BHe40gS","","0","","32323424242","","","","","","","","1234","1","2","","","","0","0","","","3","1","0","2020-02-11 16:30:07","","","","DxlgfhLI8d3gZF7gVFsdc6muQZfj7fgF_1581416895","","","2020-02-11 15:58:15","","1"),
("43","","","invasti","invasti@gmail.com","$2y$13$UKPi2DtXbWCoFeMeQP8K2OhiSrH5LXaOqd5mOyYw5RV9fdJQd3qTC","","0","","","","","","","","","","1234","1","1","","","","0","0","","","4","1","0","2020-02-11 18:31:15","","","","PQgD4k3SA6Rln-BlPBYUgzCDLRlxlAqM_1581425982","","","2020-02-11 18:29:42","2020-02-13 10:51:35","1"),
("44","","","qtest","qtest@toxsl.com","$2y$13$uQbJy2Lt3vOx8rtAuKwbAu/B0jxwkq9RDMc3P3JbD6idSg0HZWjLS","","0","","","","","","","","","","1234","1","2","","","","0","0","","","8","1","0","2020-02-13 12:48:46","","","","Y2bVsr1cOXPSSHsUuwkAa1n3pNbeAea5_1581480890","","","2020-02-12 09:44:50","","1"),
("45","","","intest","intest@toxsl.in","$2y$13$jjdZPuDT1jdcJSwIdeQyfOMAsJIGNn2Hk3499TFty32EN.g6GrZei","","0","","1234556781","","","","","","","","1234","1","2","90","","","0","1","","","4","1","0","2020-02-13 16:22:24","","","","XqWziUlzGAwsS6omUdUjI7cvxp98Q7MD_1581499553","nJpQ0W496p_-XLAS4akgnevoSNEDtGft","","2020-02-12 14:55:53","","1"),
("46","","","fcdgd","sdff@toxsl.com","$2y$13$KVmxyVhcepikwR5t.3r.KO.zNAQ2Ew3VgkRlKDt9iJcAuLyV91zo6","","0","","","","","","","","","","","","0","","","","0","0","","","5","1","0","","","","","","","","2020-02-12 16:27:38","","1"),
("47","","","Mangaer","Mangaer@toxsl.in","$2y$13$gQ7XZhYm.s2L29yULPVEjul8226884PXi9NZUwhgRRvUdgpR1hDT2","","0","","1256789345","","","","","","","","1234","1","2","","","","0","0","","","1","1","0","2020-02-13 16:10:47","","","","W1qxueLnq5MXOubW3Wl-leCfdnr5_qUS_1581567571","","","2020-02-13 09:49:31","","1"),
("48","","","Ashley","admin@toxsl.ine","$2y$13$tG2PR.j9pxWFoI4UlPYO2uyeBKvROUZjquuYnFEZdJrnkdYG4K1SW","","0","","123123123","","","","","","","","","3","1","5","","","0","1","","","4","1","0","2020-02-13 16:20:31","","","","LzY4U3pYLDWcz2NCZU0r6zDE9LIvfSbM_1581571463","F0OH35BKoEfprgQGjvFEfdxrx0r5rNWY","","2020-02-13 10:54:23","","1");

 -- -------AutobackUpStart------ 
COMMIT;
-- -------------------------------------------
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
 -- -------AutobackUpStart------ -- -------------------------------------------

-- -------------------------------------------

-- END BACKUP

-- -------------------------------------------
