<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Timer */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Timers'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="timer-view card-body">
			<?=  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'timer-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            'work_time:datetime',
            [
                'attribute' => 'task_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('task_id')
            ],
            [
                'attribute' => 'type_id',
                'value' => $model->getType()
            ],

            'created_on:datetime',
            'started_on:datetime',
            'stopped_on:datetime',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>

		</div>
	</div>

</div>
