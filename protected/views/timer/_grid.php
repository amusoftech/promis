<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Timer $searchModel
 */

?>
<?php

echo MassAction::widget([
    'url' => Url::toRoute([
        '/timer/mass'
    ]),
    'grid_id' => 'timer-grid',
    'pjax_grid_id' => 'timer-pjax-grid'
]);

?>
<div class='table table-responsive'>

<?php Pjax::begin(['id'=>'timer-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'timer-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',
            [
                'attribute' => 'task_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('task_id');
                }
            ],

            'created_on:datetime',
            'stopped_on:datetime',

            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'template' => '{view}',
                'header' => '<a>Actions</a>' /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

