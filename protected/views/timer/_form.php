<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Timer */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'timer-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>


<div class="row">




		<div class="col-md-6">
   
		 <?php echo $form->field($model, 'task_id')->dropDownList($model->getTaskOptions(), ['prompt' => '']) ?>
	 		</div>




		<div class="col-md-6">
   
		 <?php

echo $form->field($model, 'started_on')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'minDate' => date('Y-m-d'),
        'maxDate' => date('Y-m-d', strtotime('+30 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
	 		</div>


	</div>



	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'timer-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>