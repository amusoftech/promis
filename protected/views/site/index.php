<?php
use yii\helpers\Url;

/**
 *
 * @copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * @author : Shiv Charan Panjeta < shiv@toxsl.com >
 */
/* @var $this yii\web\View */
$this->title = Yii::$app->name;

?>


<div class="hero-wrap js-fullheight a fullheight" style="background-image:url(<?=$this->theme->getUrl('assets/images/background/login-register.jpg ')?>);">

	<div class="overlay"></div>
	<div class="inner-section nofixed">
		<div class="container">
			<div
				class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
				<div class="col-md-12 ftco-animate text-center">
					<div class="account-logo center">
						<a href=""><img src="<?=$this->theme->getUrl('img/output.png')?>"
							alt="logo"></a>
					</div>
					<h1>Welcome to Amusoftech</h1>
					<br> <br> <a href="<?= Url::toRoute(['/user/login'])?>"
						class="btn btn-info">Log In</a>
				</div>
				<div class="mouse">
					<a href="#" class="mouse-icon">
						<div class="mouse-wheel">
							<span class="ion-ios-arrow-down"> </span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

