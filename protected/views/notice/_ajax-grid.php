<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Notice $searchModel
 */

?>
<?php

Pjax::begin([
    "enablePushState" => false,
    "enableReplaceState" => false,
    "id" => 'Notice-pjax-ajax-grid'
]);
?>
    <?php

    echo TGridView::widget([
        'id' => 'notice-ajax-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [

            'id',
            'title',
            'model_type',
            'model_id',
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],
            [
                'attribute' => 'type_id',
                'filter' => isset($searchModel) ? $searchModel->getTypeOptions() : null,
                'value' => function ($data) {
                    return $data->getType();
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'header' => "<a>" . Yii::t("app", 'Actions') . "</a>"
            ]
        ]
    ]);
    ?>
<?php

Pjax::end();
?>

