<?php
use yii\helpers\Url;
use app\components\TActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Department;
use app\models\Designation;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $model app\models\User*/
?>
<!-- Add Employee Modal -->
<div class="page-header">
	<div class="row align-items-center">
		<div class="col">
			<h3 class="page-title"><?= $model->isNewRecord ? Yii::t('app' , 'Add Employee') : Yii::t('app' , 'Update Employee')?></h3>
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?= Url::home()?>">Home</a></li>
				<li class="breadcrumb-item"><a
					href="<?= Url::toRoute(['/dashboard/hr-index'])?>">Employee List</a></li>
				<li class="breadcrumb-item active"><?= $model->isNewRecord ? Yii::t('app' , 'Add Employee') : Yii::t('app' , 'Update Employee')?></li>
			</ul>
		</div>
	</div>
</div>
<?php

$form = TActiveForm::begin([
    'id' => 'employee-form',
    'enableClientValidation' => true
]);
?>
<div class="row">
	<div class="col-sm-6">
		<?=$form->field($model, 'first_name')->textInput(['maxlength' => 55])?>
	</div>
	<div class="col-sm-6">
		<?=$form->field($model, 'last_name')->textInput(['maxlength' => 55])?>
	</div>

	<div class="col-sm-6">
		<?=$form->field($model, 'email')->textInput(['maxlength' => 128])?>
	</div>

	<div class="col-sm-6">
		<?php

echo $form->field($model, 'joining_date')->widget(DatePicker::classname(), [

    'options' => [
        'value' => $model->joining_date,
        'class' => 'form-control'
    ],
    'pickerIcon' => '<i class="fa fa-calendar kv-dp-icon"></i>',
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd',
        'startDate' => date("yyyy-MM-dd H:i:s"),
        'todayHighlight' => true,
        'changeMonth' => true,
        'changeYear' => true
    ]
]);
?>
	</div>
	<?php if($model->isNewRecord){?>
	<div class="col-sm-6">
		<?=$form->field($model, 'employee_id')->textInput(['maxlength' => 128])?>
	</div>

	<div class="col-sm-6">
		<?=$form->field($model, 'password')->passwordInput(['maxlength' => true])?>
	</div>
	<div class="col-sm-6">
		<?=$form->field($model, 'confirm_password')->passwordInput(['maxlength' => true])?>
	</div>
	<?php }?>
	<div class="col-sm-6">
		<?=$form->field($model, 'contact_no')->textInput(['maxlength' => 128])?>
	</div>
	<div class="col-sm-6">
		<?=$form->field($model, 'gender')->dropDownList($model->getGenderOptions())?>
	</div>
	<div class="col-sm-6">
		<?php

echo $form->field($model, 'date_of_birth')->widget(DatePicker::classname(), [

    'options' => [
        'value' => $model->date_of_birth,
        'class' => 'form-control'
    ],
    'pickerIcon' => '<i class="fa fa-calendar kv-dp-icon"></i>',
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd',
        'endDate' => date('Y-m-d', strtotime('-18 year')),
        'changeMonth' => true,
        'changeYear' => true
    ]
]);
?>
</div>
	<div class="col-sm-6">
		<?=$form->field($model, 'address')->textInput(['maxlength' => 128])?>
	</div>
	<div class="col-sm-6">
		<?=$form->field($model, 'city')->textInput(['maxlength' => 128])?>
	</div>
	<div class="col-sm-6">
		<?=$form->field($model, 'country')->textInput(['maxlength' => 128])?>
	</div>
	<div class="col-sm-6">
		<?=$form->field($model, 'zipcode')->textInput(['maxlength' => 128])?>
	</div>

	<div class="col-md-6">
<?php

echo $form->field($model, 'department_id')->widget(Select2::classname(), [
    'data' => $model->getDepartmentOptions(),
    'options' => [
        'placeholder' => 'Select',
        'multiple' => false,
        'id' => 'department_id'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
?>
</div>

	<div class="col-md-6">
		 
		 		<?php
    echo $form->field($model, 'designation')->widget(DepDrop::classname(), [
        'data' => ArrayHelper::Map(Designation::find()->each(), 'id', 'title'),
        'options' => [
            'placeholder' => 'Select'
        ],
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options' => [
            'pluginOptions' => [
                'allowClear' => true
            ]
        ],
        'pluginOptions' => [
            'depends' => [
                'department_id'
            ],
            'url' => Url::to([
                '/user/designation'
            ])
        ]
    ]);
    ?>

</div>


</div>
<div class="submit-section">

<?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Submit') : Yii::t('app', 'Update'), ['id' => 'user-form-submit','class' => 'btn btn-primary submit-btn'])?>

</div>
<?php TActiveForm::end()?>
<!-- /Add Employee Modal -->



<!-- Delete Employee Modal -->
<div class="modal custom-modal fade" id="delete_employee" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body">
				<div class="form-header">
					<h3>Delete Employee</h3>
					<p>Are you sure want to delete?</p>
				</div>
				<div class="modal-btn delete-action">
					<div class="row">
						<div class="col-6">
							<a href="javascript:void(0);"
								class="btn btn-primary continue-btn">Delete</a>
						</div>
						<div class="col-6">
							<a href="javascript:void(0);" data-dismiss="modal"
								class="btn btn-primary cancel-btn">Cancel</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Delete Employee Modal -->