<?php
use yii\helpers\Url;
?>
<!-- Delete Employee Modal -->

<div class="modal-body">
	<div class="form-header">
		<h3>Delete Employee</h3>
		<p>Are you sure want to delete?</p>
	</div>
	<div class="modal-btn delete-action">
		<div class="row">
			<div class="col-6">
				<a
					href="<?=Url::toRoute(['/user/delete-employee','id' => $model->id])?>"
					data-method="post" class="btn btn-primary continue-btn">Delete</a>
			</div>
			<div class="col-6">
				<a href="javascript:void(0);" data-dismiss="modal"
					class="btn btn-primary cancel-btn">Cancel</a>


			</div>
		</div>
	</div>
</div>

<!-- /Delete Employee Modal -->


