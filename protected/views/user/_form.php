    <?php
    /**
     *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
     *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
     */
    use app\components\TActiveForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\models\User;
    /* @var $this yii\web\View */
    /* @var $model app\models\User */
    /* @var $form yii\widgets\ActiveForm */

    $id = Yii::$app->request->get('id');

    $userModel = User::findOne($id);
    ?>

<div class="card-body">
    <?php

    $form = TActiveForm::begin([
        'id' => 'user-form',

        'enableClientValidation' => true
        /*
     * 'options' => [
     * 'class' => 'row'
     * ]
     */
    ]);
    ?>
		<div class="col-lg-6 col-md-12 col-sm-12">
	<?=$form->field($model, 'full_name')->textInput(['maxlength' => 55])?>
	</div>
	<div class="col-lg-6 col-md-12 col-sm-12">
 	<?=$form->field($model, 'email')->textInput(['maxlength' => 128])?>

   </div>
		
   <?php

if (Yii::$app->controller->action->id != 'update') {
    ?>
    <div class="col-lg-6 col-md-12 col-sm-12">
		<?=$form->field($model, 'password')->passwordInput(['maxlength' => true])?>
	</div>
	<div class="col-lg-6 col-md-12 col-sm-12">
		<?=$form->field($model, 'confirm_password')->passwordInput(['maxlength' => true])?>
	</div>
	
     <?php
}
?>
		<div class="col-lg-6 col-md-12 col-sm-12">
	 <?=$form->field($model, 'contact_no')->textInput(['maxlength' => 11])?>
</div>

	<div class="col-lg-6 col-md-12 col-sm-12">
	 <?=$form->field($model, 'profile_file')->fileInput()?>
</div>
	<div class="col-lg-6 col-md-12 col-sm-12">
   
   		<?php if ($model->isNewRecord){?>
   			 <?=$form->field($model, 'employee_id')->textInput(['maxlength' => 11])?>
   		
   		<?php echo $form->field($model, 'role_id')->dropDownList($model->getRoleOptions(), ['prompt' => '']) ?>
	
	<?php echo $form->field($model, 'department_id')->dropDownList($model->getTypeOptions(),['prompt' => 'Select Department'])->label('Department') ?>	
		<?php echo $form->field($model, 'designation')->dropDownList($model->getDesignationOptions(),['prompt' => 'Select Designation'])->label('Designation') ?>	
		<?php

}
    if (! empty($userModel) && $userModel->role_id != User::ROLE_ADMIN) {
        ?>
        	 <?=$form->field($model, 'employee_id')->textInput(['maxlength' => 11])?>
        
	<?php echo $form->field($model, 'role_id')->dropDownList($model->getRoleOptions(), ['prompt' => '']) ?>
	
	<?php echo $form->field($model, 'department_id')->dropDownList($model->getTypeOptions(),['prompt' => 'Select Department'])->label('Department') ?>	
		<?php echo $form->field($model, 'designation')->dropDownList($model->getDesignationOptions(),['prompt' => 'Select Designation'])->label('Designation') ?>	
	<?php }?>
	</div>

	<div class="col-md-12 bottom-admin-button btn-space-bottom">
		<div class="form-group text-right">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id' => 'user-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success'])?>
    </div>
	</div>

    <?php

    TActiveForm::end();
    ?>

</div>

<script>




</script>