<?php
use yii\helpers\Url;
use app\components\TActiveForm;
use app\modules\file\widgets\DocumentViewerWidget;

/* @var $model app\models\User*/
/* @var $this yii\web\View */

?>


<!-- Page Content -->
<div class="content container-fluid">
	<!-- Page Header -->
	<div class="page-header">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="page-title">Profile</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?= Url::home()?>">Home</a></li>
					<li class="breadcrumb-item active">Profile</li>
				</ul>
			</div>

		</div>
	</div>
	<!-- /Page Header -->
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<div class="profile-view">
						<div class="profile-img-wrap">
						 <?php

    $form = TActiveForm::begin([
        'id' => 'id_user_profile_file',
        'action' => [
            '/user/update-employee',
            'id' => $model->id
        ],
        'enableClientValidation' => false,
        'options' => [
            'class' => 'row'
        ]
    ]);
    ?>
    

					 <?=$form->field($model, 'profile_file', ['options' => ['tags' => false],'template' => '{input}<label
									for="choose-file" class="upload-file"><i class="fa fa-pencil"></i></label>{error}'])->fileInput(["class" => "choose-file","id" => "choose-file"]);?>
							
							<div class="profile-img">
								<?=$model->displayImage($model->profile_file, [], 'default.jpg', true);?>
							</div>
							  <?php

        TActiveForm::end();
        ?>
						</div>

						<div class="profile-basic">
							<div class="row">
								<div class="col-md-4">
									<div class="profile-info-left h-100">
										<h3 class="user-name m-t-0 mb-0"><?= !empty($model->full_name) ? $model->full_name : ''?></h3>
										<h6 class="text-muted"><?= !empty($model->department) ? $model->department->title : ''?></h6>
										<small class="text-muted"><?= !empty($model->email) ? $model->email : ''?></small><br>
										<small class="text-muted"><?= !empty($model->designations) ? $model->designations->title : ''?></small>

									</div>
								</div>
								<div class="col-md-8">
									<ul class="personal-info">
										<li>
											<div class="title">Attendance:</div>
											<div class="text">
											
											<?php

        $class = ! empty($model->attendance) ? "badge-success" : "badge-danger";?>
												<span class="badge <?=  $class ?> "><?= !empty($model->attendance)?$model->attendance->getStateBadge():"Absent" ?></span>
											</div>
										</li>
										<li>
											<div class="title">Currently:</div>
											<div class="text">
												<span class="badge badge-success">In</span>
											</div>
										</li>
										<li>
											<div class="title">Designation:</div>
											<div class="text"><?= !empty($model->designations) ? $model->designations->title : ''?></div>
										</li>
										<li>
											<div class="title">Department:</div>
											<div class="text"><?= !empty($model->department) ? $model->department->title : ''?></div>
										</li>
										<li>
											<div class="title">Joined Date:</div>
											<div class="text"><?= !empty($model->joining_date) ? $model->joining_date : 'not set'?></div>
										</li>
										<li>
											<div class="title">ID:</div>
											<div class="text"><?= !empty($model->employee_id) ? $model->employee_id : ''?></div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="pro-edit">
							<a class="edit-icon"
								href="<?= Url::toRoute(['/user/update-employee' , 'id' => $model->id])?>"><i
								class="fa fa-pencil"></i></a>
						</div>

						<a id="tool-btn-update" class="btn btn-success"
							href="<?= Url::toRoute(['/user/changepassword' , 'id' => $model->id])?>"
							title="Change password"><span class="fa fa-paste"></span></a>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h3 class="pb-3">Add <?= !empty($model->full_name) ? $model->full_name : ''?> Documents</h3>
		 
	<?php
if ((\Yii::$app->hasModule('file')) && (! $model->isNewRecord)) {
    $path = '//../modules/file/views/file/_upload';
    echo $this->render($path, [
        'model' => $model,
        'options' => [
            'multiple' => true
        ], // optional

        'uploadExtraData' => [
            'public' => true
        ], // uploaded files are automatically public (default is: protected). optional.
        'target_url' => Url::to([
            'user/view',
            'id' => $model->id
        ]) // optional
    ]);
}

?>
<div class="mt-5">
		 <?php

echo DocumentViewerWidget::widget([
    'model' => $model
]);
?>
		</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
				<?php

    $this->context->startPanel();
    $this->context->addPanel('Attendance list', 'attendanceList', 'Attendance', $model);
    $this->context->addPanel('Leaves', 'leave', 'leaveRequest', $model);
    $this->context->addPanel('Tickets', 'tickets', 'Ticket', $model);
    $this->context->addPanel('Salary', 'payroll', 'Payroll', $model);
    $this->context->addPanel('Working Hours', 'workingHour', 'timer', $model);
    $this->context->addPanel('Working Activities', 'payroll', 'Payroll', $model);
    $this->context->endPanel();
    ?>
			</div>
	</div>
</div>
<script>
$("#choose-file").on('change',function(){
	$('#id_user_profile_file').submit();
});
        </script>


<!-- /Page Wrapper -->