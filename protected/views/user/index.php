<?php
use app\models\User;

/**
 *
 * @copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 * @author : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
// $this->title = Yii::t ( 'app', 'Users' );

$id = Yii::$app->request->get('id');
$title = 'User';
if (! empty($id)) {

    if ($id == User::ROLE_HR) {
        $title = 'Hr';
    } elseif ($id == User::ROLE_DISTRIBUTOR) {

        $title = 'Distributers';
    } elseif ($id == User::ROLE_EMPLOYEE) {

        $title = 'Employees';
    } elseif ($id == User::ROLE_INVESTIGATOR) {

        $title = 'Investigators';
    } elseif ($id == User::ROLE_MANAGER) {

        $title = 'Managers';
    } elseif ($id == User::ROLE_QUALITY_CONTROL) {

        $title = 'Quality Controllers';
    }
}

$this->params['breadcrumbs'][] = [
    'label' => $title
];

?>
<div class="wrapper">
	<div class="user-index">
		<div class="card ">
			<?=  \app\components\PageHeader::widget(['title'=>$title]); ?>
		</div>

		<div class="card panel-margin">
			<div class="card-body">
				<div class="content-section clearfix">
					<?php echo $this->render('_grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
				</div>
			</div>
		</div>
	</div>
</div>

