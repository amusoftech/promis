<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
use app\components\PageHeader;
use app\components\TDetailView;
use app\components\useraction\UserAction;
use app\models\User;
use app\modules\file\widgets\DocumentViewerWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->params['breadcrumbs'][] = [
    'label' => 'Users',
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = [
    'label' => $model->full_name
];

$visible = true;

if ($model->role_id == User::ROLE_ADMIN) {

    $visible = false;
}
?>
<div class="wrapper">

	<div class="card">
		<?php

echo PageHeader::widget([
    'model' => $model
]);
?>
	</div>
	<div class="card">
		<div class="card-body ">
			<div class="row">
				<div class="col-md-2 pr0">
					<div class="profileimage">
    				<?=$model->displayImage($model->profile_file, ['class' => 'profile-pic'], 'default.jpg', true);?>
    			</div>
				</div>
				<div class="col-md-10">
			<?php
echo TDetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'full_name',
        'email:email',
        'contact_no',
        [
            'attribute' => 'employee_id',
            'format' => 'raw',
            'visible' => $visible,
            'value' => $model->employee_id
        ],
        [
            'attribute' => 'role_id',
            'format' => 'raw',
            'visible' => $visible,
            'value' => $model->getRoleOptions($model->role_id)
        ],
        [
            'attribute' => 'department_id',
            'format' => 'html',
            'visible' => $visible,
            'value' => function ($model) {
                return ! empty($model->department) ? $model->department->title : \Yii::t('app', "not Set");
            }
        ],
        [
            'attribute' => 'designation',
            'format' => 'html',
            'visible' => $visible,
            'value' => function ($model) {
                return ! empty($model->designation) ? $model->getDesignation() : \Yii::t('app', "not Set");
            }
        ],

        'created_on:datetime'
    ]
])?>
			</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
				<?php
    if ((User::isAdmin()) && (\Yii::$app->user->id != $model->id)) {
        $actions = $model->getStateOptions();
        array_shift($actions);
        echo UserAction::widget([
            'model' => $model,
            'attribute' => 'state_id',
            'states' => $model->getStateOptions(),
            'allowed' => $actions
        ]);
    }

    ?>
			</div>
	</div>
	<div class="card">
		<div class="card-body">
			<h3 class="pb-3">Add <?=! empty($model->full_name) ? $model->full_name : ''?> Documents</h3>
		 
	<?php
if ((\Yii::$app->hasModule('file')) && (! $model->isNewRecord)) {
    $path = '//../modules/file/views/file/_upload';
    echo $this->render($path, [
        'model' => $model,
        'options' => [
            'multiple' => true
        ], // optional

        'uploadExtraData' => [
            'public' => true
        ], // uploaded files are automatically public (default is: protected). optional.
        'target_url' => Url::to([
            'user/view',
            'id' => $model->id
        ]) // optional
    ]);
}

?>
<div class="mt-5">
		 <?php

echo DocumentViewerWidget::widget([
    'model' => $model
]);
?>
		</div>
		</div>
	</div>

</div>

