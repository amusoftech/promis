<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */

/* @var $this \yii\web\View */
/* @var $model app\models\User */
use yii\helpers\Url;
use app\components\TActiveForm;
use yii\helpers\Html;
?>

<div class="main-wrapper">
	<div class="account-content">
		<div class="container">
			<!-- Account Logo -->
			<div class="account-logo">
				<a href="<?= Url::home()?>"><img
					src="<?=$this->theme->getUrl('img/log4o.png')?>" alt="logo"></a>
			</div>
			<!-- /Account Logo -->
			<div class="account-box">
				<div class="account-wrapper">
					<h3 class="account-title">Reset password</h3>
					<p class="account-subtitle"><?=\Yii::t("app", "Please fill out your email. A link to reset password will be sent there.")?></p>

					<!-- Account Form -->
			<?php

$form = TActiveForm::begin([
    'id' => 'request-password-reset-form',
    'enableClientValidation' => true,
    'enableAjaxValidation' => false
]);
?>
			 <?=$form->field($model, 'email')?>
			  <?=Html::submitButton('Send', ['class' => 'btn btn-primary account-btn','name' => 'send-button'])?>
					<?php TActiveForm::end()?>
					
					<!-- /Account Form -->
				</div>
			</div>
		</div>
	</div>
</div>
