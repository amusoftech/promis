<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
use app\components\TGridView;
use app\models\User;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\components\MassAction;

Pjax::begin();

/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\User $searchModel
 */

?>

<?php
if (User::isAdmin())
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/user/mass'
        ]),
        'grid_id' => 'user-grid',
        'pjax_grid_id' => 'user-pjax-grid'
    ]);
?>
<div class="table table-responsive">
	 <?php
Pjax::begin([
    'id' => 'user-pjax-grid'
]);
echo TGridView::widget([
    'id' => 'user-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        [
            'name' => 'check',
            'class' => 'yii\grid\CheckboxColumn',
            'visible' => User::isAdmin()
        ],
        'id',
        [
            'attribute' => 'employee_id',
            'format' => 'html',
            'visible' => !User::ROLE_CLIENT,
            'value' => function ($data) {
            return $data->employee_id;
            }
            ],
        'full_name',
        'email:email',

        'contact_no',
        [
            'attribute' => 'department_id',
            'format' => 'html',
            'visible' => !User::ROLE_CLIENT,
            'value' => function ($data) {
                return ! empty($data->department) ? $data->department->title : \Yii::t('app', "not Set");
            }
        ],
        [
            'attribute' => 'role_id',
            'filter' => $searchModel->getRoleOptions(),
            'format' => 'html',
            'value' => function ($data) {
                return $data->getRoleOptions($data->role_id);
            }
        ],
        [
            'attribute' => 'state_id',
            'filter' => $searchModel->getStateOptions(),
            'format' => 'html',
            'value' => function ($data) {
                return $data->getStateBadge();
            }
        ],
        [
            'attribute' => 'created_on',
            'format' => 'raw',
            'filter' => \yii\jui\DatePicker::widget([
                'inline' => false,
                'clientOptions' => [
                    'autoclose' => true
                ],
                'model' => $searchModel,
                'attribute' => 'created_on',
                'options' => [
                    'id' => 'created_on',
                    'class' => 'form-control'
                ]
            ]),
            'value' => function ($data) {
                return date('Y-m-d H:i:s', strtotime($data->created_on));
            }
        ],

        [
            'class' => 'app\components\TActionColumn',
            'header' => "<a>" . Yii::t("app", 'Actions') . "</a>"
        ]
    ]
]);

?>
<?php

Pjax::end()?>
</div>
