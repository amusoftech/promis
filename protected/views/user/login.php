<?php
/* @var $this \yii\web\View */
/* @var $model app\models\LoginForm */

use yii\helpers\Url;
use app\components\TActiveForm;
use yii\helpers\Html;
?>

<div class="main-wrapper">
	<div class="account-content">
		<div class="container">
			<!-- Account Logo -->
			<div class="account-logo">
				<a href="<?= Url::home() ?>">
					<img src="<?= $this->theme->getUrl('img/output.png') ?>" alt="logo">
				</a>
			</div>
			<!-- /Account Logo -->
			<div class="account-box">
				<div class="account-wrapper">
					<h3 class="account-title">Login</h3>
					<p class="account-subtitle">Access to our dashboard</p>

					<!-- Account Form -->
					<?php

					$form = TActiveForm::begin([
						'id' => 'login-form',
						'enableAjaxValidation' => false,

						'options' => [
							'class' => 'login-form form'
						]
					]);
					?>
					<?= $form->field($model, 'username')->textInput()->label('Email Address') ?>

					<?= $form->field($model, 'password')->passwordInput() ?>
					<div class="col-md-12">
						<a class="text-muted float-right mb-3" href="<?= Url::toRoute(['user/recover']) ?>"> Forgot password? </a>
					</div>

					<?= Html::submitButton('Log in', ['class' => 'btn btn-primary account-btn', 'id' => 'login', 'name' => 'login-button']) ?>


					<?php TActiveForm::end() ?>

					<!-- /Account Form -->
				</div>
			</div>
		</div>
	</div>
</div>