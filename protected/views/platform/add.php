<?php

/* @var $this yii\web\View */
/* @var $model app\models\Platform */
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Platforms'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Add');
?>

<div class="wrapper">
	<div class="card">

		<div class="platform-create">
	<?=  \app\components\PageHeader::widget(); ?>
</div>

	</div>

	<div class="content-section clearfix card">

		<?= $this->render ( '_form', [ 'model' => $model ] )?></div>
</div>


