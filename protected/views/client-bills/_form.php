<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\ClientBills */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">

    <?php
    $form = TActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'client-bills-form'
       
    ]);

    ?>





<div class="row">
		<div class="col-md-12"> <?php echo $form->field($model, 'case_id')->dropDownList($model->getCaseOptions(), ['prompt' => ''])  ?></div>

		<div class="col-md-12"> <?php echo $form->field($model, 'full_name')->textInput(['maxlength' => 128]) ?></div>
		<div class="col-md-12"> <?php echo $form->field($model, 'amount')->textInput(['type' => 'number']) ?></div>
		<div class="col-md-12"> <?php echo $form->field($model, 'sent_by')->dropDownList($model->getClientReportSentByOptions(), ['prompt' => ''])  ?></div>

		<div class="col-md-12"><?php

echo $form->field($model, 'date')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'minDate' => date('Y-m-d'),
        'maxDate' => date('Y-m-d', strtotime('+30 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
	 		</div>
	

		<div class="col-md-12">
			<div class="form-group">
				<div
					class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'client-bills-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
			</div>
		</div>


</div>
    <?php TActiveForm::end(); ?>

</div>