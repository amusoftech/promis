<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\User;

use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\MassAction;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\ClientBills $searchModel
 */

?>
<?php
if (User::isAdmin())
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/client-bills/mass'
        ]),
        'grid_id' => 'client-bills-grid-view',
        'pjax_grid_id' => 'client-bills-pjax-grid'
    ]);
?>



<div class='table table-responsive'>


<?php Pjax::begin(['id'=>'client-bills-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'client-bills-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',

            'full_name',
            'amount',
            /* 'sent_by',*/
            'date:date',
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return ! empty($data->cases) ? $data->cases->title : Yii::t('app', "Not Set");
                }
            ],
            [
                'attribute' => 'sent_by',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getClientReportSentByOptions() : null,
                'value' => function ($data) {
                return $data->getSentbyBadge();
                }
            ],

            [
                'attribute' => 'created_on',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'created_on',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return date('Y-m-d H:i:s', strtotime($data->created_on));
                }
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>' /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>
</div>