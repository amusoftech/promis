<?php
use app\modules\comment\widgets\CommentsWidget;
use yii\helpers\Html;
use app\components\TActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Tasks'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" panel ">
		<div class="text-center">
			<h2>Are you sure you want to delete this item? All related data is
				deleted</h2>
		</div>
		<div class="task-view panel-body">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>
	<div class=" panel ">
		<div class=" panel-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'task-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            /*'title',*/
            /*'description:html',*/
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('case_id')
            ],
            [
                'attribute' => 'type_id',
                'value' => $model->getType()
            ],
            /*[
			'attribute' => 'state_id',
			'format'=>'raw',
			'value' => $model->getStateBadge(),],*/
            [
                'attribute' => 'platform_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('platform_id')
            ],
            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('department_id')
            ],
            'start_date:datetime',
            'end_date:datetime',
            'estimated_time:datetime',
            'actual_time:datetime',
            'created_on:datetime',
            'updated_on:datetime',
            [
                'attribute' => 'assign_to_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('assign_to_id')
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>


<?php  echo $model->description;?>



<?php
$form = TActiveForm::begin([

    'id' => 'task-form'
]);

echo $form->errorSummary($model);
?>

	 <div class="form-group">
				<div
					class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
			
        <?= Html::submitButton('Confirm', ['id'=> 'task-form-submit','class' =>'btn btn-success']) ?>
    </div>
			</div>

    <?php TActiveForm::end(); ?>

		</div>
	</div>



	<div class=" panel ">
		<div class=" panel-body ">
			<div class="task-panel">

<?php
$this->context->startPanel();
$this->context->addPanel('TaskTimers', 'taskTimers', 'TaskTimer', $model /* ,null,true */);
$this->context->endPanel();
?>
				</div>
		</div>
	</div>

</div>
