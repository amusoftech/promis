<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Task $searchModel
 */

?>




<?php Pjax::begin(['id'=>'task-pjax-ajax-grid','enablePushState'=>false,'enableReplaceState'=>false]); ?>
    <?php

    echo TGridView::widget([
        'id' => 'task-ajax-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [

            'id',
            'title',
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('case_id');
                }
            ],

            [
                'attribute' => 'platform_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('platform_id');
                }
            ],
          /*   [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('department_id');
                }
            ], */
            
            [
                'attribute' => 'department',
                'format' => 'raw',
                'value' => function ($data) {
                return !empty($data->assignTo)?$data->assignTo->department->title:\Yii::t('app',"not set");
                }
                ],
            'start_date:datetime',
            'estimated_time:datetime',

            [
                'attribute' => 'assign_to_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('assign_to_id');
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>'
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>

