<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Task $searchModel
 */

?>
<?php
if (User::isManager()) {
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/task/mass'
        ]),
        'grid_id' => 'task-grid',
        'pjax_grid_id' => 'task-pjax-grid'
    ]);
}
?>
<?php
if (User::isQualityControl()) {
    $temp = '{view}';
} else {
    $temp = '{view}{delete}{update}';
}
?>
<div class='table table-responsive'>

<?php

Pjax::begin([
    'id' => 'task-pjax-grid'
]);
?>
    <?php

    echo TGridView::widget([
        'id' => 'task-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin() || User::isManager()
            ],

            'id',
            'title',
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('case_id');
                }
            ],

            [
                'attribute' => 'platform_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('platform_id');
                }
            ],

            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return ! empty($data->assignTo->department) ? $data->assignTo->department->title : \Yii::t('app', "not set");
                }
            ],
            [
                'attribute' => 'start_date',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return date('Y-m-d H:i:s', strtotime($data->start_date));
                }
            ],

            'estimated_time:datetime',

            [
                'attribute' => 'assign_to_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('assign_to_id');
                }
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],
             /* Will use in Future  */
         /*    [
                'attribute' => 'actual_time',
                'format' => 'raw',
                'value' => function ($data) {
                return  !empty($data->taskEvent)?$data->getTotalTaskTime(): 0;
                }
            ],
            
            [
                'attribute' => 'extra_time',
                'format' => 'raw',
                'value' => function ($data) {
                return  !empty($data->taskEvent)?$data->getTaskExtraTime():0;
                }
                ], */

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>', /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
                'template' => $temp
            ]
        ]
    ]);
    ?>
<?php

Pjax::end();
?>
</div>

