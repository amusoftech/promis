<?php
use app\components\useraction\UserAction;
use app\models\Comment;
use app\models\User;
use app\modules\comment\widgets\CommentsWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Tasks'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="task-view card-body">
			<?=\app\components\PageHeader::widget(['model' => $model]);?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'task-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('case_id')
            ],

            [
                'attribute' => 'platform_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('platform_id')
            ],
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'value' => $model->getStateBadge()
            ],

            [
                'attribute' => 'assign_to_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('assign_to_id')
            ],
           'estimated_time',
           /*  [
                'attribute' => 'actual_time',
                'format' => 'raw',
                'visible' => User::isEmployee() || User::isQualityControl() || User::isManager(),
                'value' => \app\widgets\TimerWidget::widget([
                    'model' => $model
                ])
            ], */
            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return ! empty($model->assignTo->department) ? $model->assignTo->department->title : \Yii::t('app', "not set");
                }
            ],

            'start_date:date',
            'end_date:date',
           
    /*  Will use in future  */
         /*    [
                'attribute' => 'actual_time',
                'format' => 'raw',
                'value' => function ($model) {
                    return ! empty($model->taskEvent) ? $model->getTotalTaskTime() : 0;
                }
            ],

            [
                'attribute' => 'extra_time',
                'format' => 'raw',
                'value' => function ($model) {
                    return ! empty($model->taskEvent) ? $model->getTaskExtraTime() : 0;
                }
            ], */

            'created_on:datetime',
            'updated_on:datetime',

            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>


<?php

echo $model->description;
?>


		<?php
if (User::isManager() || User::isAdmin()) {
    echo UserAction::widget([
        'model' => $model,
        'attribute' => 'state_id',
        'states' => $model->getStateOptions()
    ]);
}
?>

		</div>
	</div>	
	<?php

if (! User::isClient()) {
    ?>
		<div class=" card-body ">
		<div class="task-panel">
    <?php
    $this->context->startPanel();
    //$this->context->addPanel('Timers', 'timers', 'Timer', $model /* ,null,true */);
    $this->context->addPanel('Feeds', 'feeds', 'Feed', $model /* ,null,true */);
    $this->context->endPanel();
    ?>
				</div>
	</div>
		<?php
}
?>

<?php echo CommentsWidget::widget(['model'=>$model]); ?>
	</div>

