<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'task-form',
        'options' => [
            'class' => 'row'
        ]
    ]);
    ?>

	
		<div class="col-md-6">

		
		 <?php echo $form->field($model, 'title')->textInput(['maxlength' => 256]) ?>
	 
   
		 <?php echo  $form->field($model, 'description')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'description')->textarea(['rows' => 6]); ?>
	 	
	 	
		 <?php echo $form->field($model, 'case_id')->dropDownList($model->getCaseOptions(), ['prompt' => '']) ?>
	 		</div>


	<div class="col-md-6">
   
		 <?php echo $form->field($model, 'platform_id')->dropDownList($model->getPlatformOptions(), ['prompt' => '']) ?>
	 
    
		 <?php echo $form->field($model, 'department_id')->dropDownList($model->getDepartmentOptions(), ['prompt' => '']) ?>
	    
		<?php
		$model->estimated_time = ! empty($in) ? $in->estimated_time : '';
echo $form->field($model, 'estimated_time')->widget(TimePicker::classname(), [
    'pluginOptions' => [
        'defaultTime' => date('H:i'),
        'showMeridian' => false
    ]
]);

echo $form->field($model, 'actual_time')->widget(TimePicker::classname(), [
    'pluginOptions' => [
        'defaultTime' => date('H:i'),
        'showMeridian' => false
    ]
]);

?>
   
		 <?php

echo $form->field($model, 'start_date')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'minDate' => date('Y-m-d'),
        'maxDate' => date('Y-m-d', strtotime('+30 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
	 		
   
		 <?php

echo $form->field($model, 'end_date')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'minDate' => date('Y-m-d'),
        'maxDate' => date('Y-m-d', strtotime('+30 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
	<?php  if(!User::isEmployee()){?>
		 <?php echo $form->field($model, 'assign_to_id')->dropDownList($model->getAssignToOptions(), ['prompt' => '']) ?>
    <?php } ?>		
	</div>

</div>

<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'task-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php TActiveForm::end(); ?>
