<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use app\models\Cases;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Analytics */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?=strtoupper(Yii::$app->controller->action->id);?>
                        </header>
<div class="card-body">

    <?php
    $form = TActiveForm::begin([
        // 'layout' => 'horizontal',
        'id' => 'analytics-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    echo $form->errorSummary($model);
    ?>





<div class="col-md-6">

	
		 <?php

echo $form->field($model, 'name')->textInput([
    'maxlength' => 255
])?>
	 		


		 <?php

echo $form->field($model, 'case_id')->widget(Select2::classname(), [
    'data' => $model->getCaseOptions(),
    'options' => [
        'placeholder' => 'Select',
        'multiple' => false,
        'id' => 'case_id'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
?>
	 		


		<?php

echo $form->field($model, 'department_id')->widget(DepDrop::classname(), [
    'data' => $model->getDepartmentOptions(),
    'options' => [
        'placeholder' => 'Select',
        'prompt' => 'Select Department'
    ],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => [
        'pluginOptions' => [
            'allowClear' => true
        ]
    ],
    'pluginOptions' => [

        'depends' => [
            'case_id'
        ],
        'url' => Url::to([
            '/analytics/cases-department'
        ])
    ]
]);
?>

<?php

echo $form->field($model, 'user_id')->widget(DepDrop::classname(), [
    'data' => $model->getUserOptions(),
    'options' => [
        'placeholder' => 'Select',
        'prompt' => 'Select User'
    ],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => [
        'pluginOptions' => [
            'allowClear' => true
        ]
    ],
    'pluginOptions' => [

        'depends' => [
            'department_id'
        ],
        'url' => Url::to([
            '/analytics/department-user'
        ])
    ]
]);
?>
	 		


<?php

echo $form->field($model, 'manager_id')->widget(DepDrop::classname(), [
    'data' => $model->getUserOptions(),
    'options' => [
        'placeholder' => 'Select'
    ],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => [
        'pluginOptions' => [
            'allowClear' => true
        ]
    ],
    'pluginOptions' => [

        'depends' => [
            'case_id'
        ],
        'url' => Url::to([
            '/analytics/cases-manager'
        ])
    ]
]);
?>

<?php

echo $form->field($model, 'client_id')->widget(DepDrop::classname(), [
    'data' => $model->getClientOptions(),
    'options' => [
        'placeholder' => 'Select'
    ],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => [
        'pluginOptions' => [
            'allowClear' => true
        ]
    ],
    'pluginOptions' => [

        'depends' => [
            'case_id'
        ],
        'url' => Url::to([
            '/analytics/cases-client'
        ])
    ]
]);
?>
	 		
	 		

	</div>
	<div class="col-md-6">
    
		 <?php

echo $form->field($model, 'date')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'minDate' => date('Y-m-d'),
        'maxDate' => date('Y-m-d', strtotime('+30 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
	 		


		 <?php
echo $form->field($model, 'billing_item')->textInput([
    'maxlength' => 255
])?>
	 		


		 <?php

echo $form->field($model, 'cost')->textInput()?>
	 		


		 <?php

echo $form->field($model, 'value')->textInput()?>
	 		


	<?php

if (User::isAdmin()) {
    ?>	 <?php
    /* echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) */
    ?>
	 <?php
}
?>		


		 <?php
/* echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) */
?>
	 			</div>




	<div class="form-group">
		<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id' => 'analytics-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
	</div>

    <?php

    TActiveForm::end();
    ?>

</div>
<script type="text/javascript">
 $('#case-id').change(function() {
    $.get('get-cost', function(data) {
        $('#analytics-cost').text(data.budget);
    }).fail(function() {
        alert('Failed to get course details');
    });
}); 
</script>

