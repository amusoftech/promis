<?php

use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Analytics */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Analytics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = (string)$model;
?>

<div class="wrapper">
	<div class=" card ">

		<div
			class="analytics-view card-body">
			<?=  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php echo \app\components\TDetailView::widget([
    	'id'	=> 'analytics-detail-view',
        'model' => $model,
        'options'=>['class'=>'table table-bordered'],
        'attributes' => [
            'id',
            'name',
            [
			'attribute' => 'case_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('case_id'),
			],
            [
			'attribute' => 'department_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('department_id'),
			],
            [
			'attribute' => 'user_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('user_id'),
			],
            [
			'attribute' => 'manager_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('manager_id'),
			],
            [
			'attribute' => 'client_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('client_id'),
			],
            'date:datetime',
            'billing_item',
            'cost',
            'value',
            [
			'attribute' => 'created_by_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('created_by_id'),
			],
            /*[
			'attribute' => 'state_id',
			'format'=>'raw',
			'value' => $model->getStateBadge(),],*/
            [
			'attribute' => 'type_id',
			'value' => $model->getType(),
			],
            'created_on:datetime',
            'updated_on:datetime',
        ],
    ]) ?>


<?php  ?>


		<?php				echo UserAction::widget ( [
						'model' => $model,
						'attribute' => 'state_id',
						'states' => $model->getStateOptions ()
				] );
				?>

		</div>
</div>
 


	<div class=" card ">
				<div class=" card-body ">
					<div
						class="analytics-panel">

<?php
$this->context->startPanel();
	$this->context->addPanel('Feeds', 'feeds', 'Feed',$model /*,null,true*/);

$this->context->endPanel();
?>
				</div>
				</div>
			</div>

	<div class=" card ">
		<div class=" card-body ">

<?= CommentsWidget::widget(['model'=>$model]); ?>
			</div>
	</div>
</div>
