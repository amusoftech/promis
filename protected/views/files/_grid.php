<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\User;

use yii\grid\GridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Files $searchModel
 */

?>
<?php if (User::isAdmin()) echo Html::a('','#',['class'=>'multiple-delete glyphicon glyphicon-trash','id'=>"bulk_delete_files-grid"])?>
<?php Pjax::begin(['id'=>'files-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'files-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',
            /* 'title:html',*/
            /* 'model_id:html',*/
            /* 'model_type:html',*/
            /* 'target_url:html',*/
            /* 'description:html',*/
            /* 'filename_user:html',*/
            /* 'filename_path:html',*/
            'extension',
            'public',
            'size',
            /* 'download_count',*/
            /* ['attribute' => 'file_type','filter'=>$searchModel->getFileOptions(),
			'value' => function ($data) { return $data->getFileOptions($data->file_type);  },],*/
            'mimetype',
            /* 'seo_title',*/
            /* 'seo_alt',*/
            /* [
			'attribute' => 'state_id','format'=>'raw','filter'=>isset($searchModel)?$searchModel->getStateOptions():null,
			'value' => function ($data) { return $data->getStateBadge();  },],*/
            /* ['attribute' => 'type_id','filter'=>isset($searchModel)?$searchModel->getTypeOptions():null,
			'value' => function ($data) { return $data->getType();  },],*/
            /* [
				'attribute' => 'created_on',
				'format'=>'raw',
                'filter' => \yii\jui\DatePicker::widget([
                        'inline' => false,
                        'clientOptions' => [
                            'autoclose' => true
                        ],
                        'model' => $searchModel,
                        'attribute' => 'created_on',
                        'options' => [
                            'id' => 'created_on',
                            'class' => 'form-control'
                        ]
                    ]),
				'value' => function ($data) { return date('Y-m-d H:i:s', strtotime('created_on'));  },
				],*/
            /* [
				'attribute' => 'updated_on',
				'format'=>'raw',
                'filter' => \yii\jui\DatePicker::widget([
                        'inline' => false,
                        'clientOptions' => [
                            'autoclose' => true
                        ],
                        'model' => $searchModel,
                        'attribute' => 'created_on',
                        'options' => [
                            'id' => 'created_on',
                            'class' => 'form-control'
                        ]
                    ]),
				'value' => function ($data) { return date('Y-m-d H:i:s', strtotime('updated_on'));  },
				],*/
            /* [
				'attribute' => 'updated_by_id',
				'format'=>'raw',
				'value' => function ($data) { return $data->getRelatedDataLink('updated_by_id');  },
				],*/
            /* [
				'attribute' => 'created_by_id',
				'format'=>'raw',
				'value' => function ($data) { return $data->getRelatedDataLink('created_by_id');  },
				],*/

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>' /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>
<script> 
$('#bulk_delete_files-grid').click(function(e) {
	e.preventDefault();
	 var keys = $('#files-grid-view').yiiGridView('getSelectedRows');

	 if ( keys != '' ) {
		var ok = confirm("Do you really want to delete these items?");

		if( ok ) {
			$.ajax({
				url  : '<?php echo Url::toRoute(['files/mass','action'=>'delete','model'=>get_class($searchModel)])?>', 
				type : "POST",
				data : {
					ids : keys,
				},
				success : function( response ) {
					if ( response.status == "OK" ) {
						 $.pjax.reload({container: '#files-pjax-grid'});
					}
				}
		     });
		}
	 } else {
		alert('Please select items to delete');
	 }
});

</script>

