<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Files */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">

    <?php
    $form = TActiveForm::begin([
        // 'layout' => 'horizontal',
        'id' => 'files-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    echo $form->errorSummary($model);
    ?>





<div class="col-md-6">

	
		 <?php /*echo  $form->field($model, 'title')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'title')->textarea(['rows' => 6]); */ ?>
	 		


		 <?php echo  $form->field($model, 'model_id')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'model_id')->textarea(['rows' => 6]); ?>
	 		


		 <?php echo  $form->field($model, 'model_type')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'model_type')->textarea(['rows' => 6]); ?>
	 		


		 <?php /*echo  $form->field($model, 'target_url')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'target_url')->textarea(['rows' => 6]); */ ?>
	 		


		 <?php /*echo  $form->field($model, 'description')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'description')->textarea(['rows' => 6]); */ ?>
	 		


		 <?php echo  $form->field($model, 'filename_user')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'filename_user')->textarea(['rows' => 6]); ?>
	 		


		 <?php echo  $form->field($model, 'filename_path')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'filename_path')->textarea(['rows' => 6]); ?>
	 		


		 <?php echo $form->field($model, 'extension')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php echo $form->field($model, 'public')->textInput() ?>
	 		

	</div>
	<div class="col-md-6">
    
		 <?php echo $form->field($model, 'size')->textInput() ?>
	 		


		 <?php /*echo $form->field($model, 'download_count')->textInput() */ ?>
	 		


		 <?php /*echo $form->field($model, 'file_type')->fileInput() */ ?>
	 		


		 <?php echo $form->field($model, 'mimetype')->textInput(['maxlength' => 255]) ?>
	 		


		 <?php /*echo $form->field($model, 'seo_title')->textInput(['maxlength' => 255]) */ ?>
	 		


		 <?php /*echo $form->field($model, 'seo_alt')->textInput(['maxlength' => 255]) */ ?>
	 		


	<?php if(User::isAdmin()){?>	 <?php /*echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) */ ?>
	 <?php }?>		


		 <?php /*echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) */ ?>
	 		


		 <?php /*echo $form->field($model, 'updated_by_id')->dropDownList($model->getUpdatedByOptions(), ['prompt' => '']) */ ?>
	 			</div>




	<div class="form-group">
		<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'files-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
