<?php
use app\modules\comment\widgets\CommentsWidget;
use yii\helpers\Html;
use app\components\TActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Cases */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Cases'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" panel ">
		<div class="text-center">
			<h2>Are you sure you want to delete this item? All related data is
				deleted</h2>
		</div>
		<div class="cases-view panel-body">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>
	<div class=" panel ">
		<div class=" panel-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'cases-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            /*'title',*/
            /*'description:html',*/
            [
                'attribute' => 'client_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('client_id')
            ],
            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('department_id')
            ],
            'start_date:date',
            'end_date:date',
            [
                'attribute' => 'type_id',
                'value' => $model->getType()
            ],

            'created_on:datetime',
            'updated_on:datetime',
            'assign_to_id',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>


<?php  echo $model->description;?>



<?php
$form = TActiveForm::begin([

    'id' => 'cases-form'
]);

echo $form->errorSummary($model);
?>

	 <div class="form-group">
				<div
					class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
			
        <?= Html::submitButton('Confirm', ['id'=> 'cases-form-submit','class' =>'btn btn-success']) ?>
    </div>
			</div>

    <?php TActiveForm::end(); ?>

		</div>
	</div>



	<div class=" panel ">
		<div class=" panel-body ">
			<div class="cases-panel">

<?php
$this->context->startPanel();
$this->context->addPanel('CasePlatforms', 'casePlatforms', 'CasePlatform', $model /* ,null,true */);
$this->context->addPanel('Reminders', 'reminders', 'Reminder', $model /* ,null,true */);
$this->context->addPanel('Teams', 'teams', 'Team', $model /* ,null,true */);

$this->context->endPanel();
?>
				</div>
		</div>
	</div>
</div>
