<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
use app\models\User;
use app\models\Task;
use yii\helpers\Url;
use SebastianBergmann\CodeCoverage\Report\PHP;
use app\models\TaskEvent;
use app\modules\file\widgets\DocumentViewerWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Cases */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Cases'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="cases-view card-body">
			<?=\app\components\PageHeader::widget(['model' => $model]);?>



		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xl-12 d-flex">
			<div class="card flex-fill">
				<div class="card-body">
					<h4 class="card-title">Task Statistics</h4>
					<div class="statistics">
						<div class="row">

							<div class="col-md-3 col-6 text-center">
								<div class="stats-box mb-4">
									<p>Pending Task</p>
									<h3><?=Task::find()->Where(['state_id' => Task::STATE_INPROGRESS])->count();?></h3>
								</div>
							</div>

							<div class="col-md-3 col-6 text-center">
								<div class="stats-box mb-4">
									<p>Working Hours</p>
									<h3><?=$model->getTotalHours()?></h3>
								</div>
							</div>
							<div class="col-md-3 col-6 text-center">
								<a
									href='<?=Url::toRoute(['//task//index/' . User::ROLE_MANAGER]);?>'>
									<div class="stats-box mb-4">
										<p>Total Tasks</p>
										<h3><?=Task::find()->count();?></h3>
									</div>
								</a>
							</div>

						</div>
					</div>

				</div>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-lg-8 col-xl-8">
			<div class="card">
				<div class="card-body">
					<div class="project-title">
						<h5 class="card-title"><?=! empty($model->title) ? $model->title : ''?></h5>
						<small class="block text-ellipsis m-b-15"><span class="text-xs"><?=Task::find()->Where(['state_id' => Task::STATE_INPROGRESS])->count();?></span>
							<span class="text-muted">open tasks, </span><span class="text-xs"><?=Task::find()->Where(['state_id' => Task::STATE_COMPLETED])->count();?></span>
							<span class="text-muted">tasks completed</span></small>
					</div>
					<h3>Description</h3>
					<p><?=! empty($model->description) ? $model->description : ''?></p>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-xl-4">
			<div class="card">
				<div class="card-body">
					<h6 class="card-title m-b-15">Case details</h6>
					<table class="table table-striped table-border">
						<tbody>
							<tr>
								<td>Client Name:</td>
								<td class="text-right"><?=! empty($model->client) ? $model->client->full_name : ''?></td>
							</tr>
							<tr>
								<td>Date of Creation:</td>
								<td class="text-right"><?=! empty($model->created_on) ? date('Y-M-d', strtotime($model->created_on)) : ''?></td>
							</tr>
							<tr>
								<td>Case Created By:</td>
								<td class="text-right"><a href="#0"><?=! empty($model->createdBy) ? $model->createdBy->full_name : \Yii::t('app', "Not Set");?></a></td>
							</tr>
							<tr>
								<td>Case Type</td>
								<td class="text-right"><?=! empty($model->department) ? $model->department->title : ''?></td>
							</tr>
							<tr>
								<td>Case Platform</td>
								<td class="text-right"><?=! empty($model->case_platform) ? $model->getPlatform() : ''?></td>
							</tr>
							<tr>
								<td>Total Hours Spent:</td>
								<td class="text-right"><?=! empty($model->getCaseTotalTime()) ? $model->getCaseTotalTime() : ''?></td>
							</tr>
							<tr>
								<td>Status:</td>
								<td class="text-right"><?=! empty($model->state_id) ? $model->getStateBadge() : ''?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php

if (User::isAdmin() || User::isManager()) {
    ?>
	<div class=" card ">
		<div class=" card-body ">
		<?php

    echo UserAction::widget([
        'model' => $model,
        'attribute' => 'state_id',
        'states' => $model->getStateOptions()
    ]);

    ?>

		</div>
	</div>
	<?php
}
?>
	<div class=" card ">
		<div class=" card-body ">
			<div class="cases-panel">

<?php
$this->context->startPanel();
// $this->context->addPanel('CasePlatforms', 'casePlatforms', 'CasePlatform', $model /* ,null,true */);

$this->context->addPanel('Tasks', 'tasks', 'Task', $model);

if (! (User::isEmployee() || User::isQualityControl())) {
    $this->context->addPanel('Team', 'teams', 'Team', $model);
}
if (! (User::isEmployee() || User::isQualityControl())) {
    $this->context->addPanel('Client Detail', 'client', 'Client', $model);
}
$this->context->addPanel('Reminders', 'reminders', 'Reminder', $model);

$this->context->endPanel();
?>
				</div>
		</div>
	</div>
	
<?php

if (User::isClient()) {
    ?>
	<div class=" card-body ">

<?=CommentsWidget::widget(['model' => $model]);?>
			</div>
<?php

}
?>
	<?php

if (User::isAdmin()) {
    ?>
	<div class="card">
		<div class="card-body">
			<h3 class="pb-3">Add <?=! empty($model->title) ? $model->title : ''?> Documents</h3>
		 
	<?php
    if ((\Yii::$app->hasModule('file')) && (! $model->isNewRecord)) {
        $path = '//../modules/file/views/file/_upload';
        echo $this->render($path, [
            'model' => $model,
            'options' => [
                'multiple' => true
            ], // optional

            'uploadExtraData' => [
                'public' => true
            ], // uploaded files are automatically public (default is: protected). optional.
            'target_url' => Url::to([
                'user/view',
                'id' => $model->id
            ]) // optional
        ]);
    }

    ?>
<?php
}
?>
<div class="mt-5">
		 <?php

echo DocumentViewerWidget::widget([
    'model' => $model
]);
?>
		</div>
		</div>
	</div>

</div>
