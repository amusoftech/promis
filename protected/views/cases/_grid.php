<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Cases $searchModel
 */

?>
<?php
if (User::isAdmin() || User::isManager()) {
    $action = '{view}{update}{delete}';
} else {
    $action = '{view}';
}
?>
<?php
if (User::isAdmin() || User::isManager()) {
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/cases/mass'
        ]),
        'grid_id' => 'cases-grid',
        'pjax_grid_id' => 'cases-pjax-grid'
    ]);
}

?>

<div class='table table-responsive'>

<?php

Pjax::begin([
    'id' => 'cases-pjax-grid'
]);
?>
    <?php

    echo TGridView::widget([
        'id' => 'cases-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin() || User::isManager()
            ],

            'id',
            'title',
            [
                'attribute' => 'client_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('client_id');
                }
            ],
            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('department_id');
                },
                'visible' => User::isAdmin()
            ],
            [
                'attribute' => 'start_date',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return date('Y-m-d', strtotime($data->start_date));
                }
            ],
            [
                'attribute' => 'end_date',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'end_date',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return date('Y-m-d', strtotime($data->end_date));
                }
            ],

            [
                'attribute' => 'assign_to_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('assign_to_id');
                },
                'visible' => User::isAdmin()
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],
            [
                'attribute' => 'state_id',
                'filter' => $searchModel->getStateOptions(),
                'format' => 'html',
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],

            [
                'attribute' => 'created_on',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'created_on',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return date('Y-m-d H:i:s', strtotime($data->created_on));
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'template' => $action,
                'header' => '<a>Actions</a>' /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
            ]
        ]
    ]);
    ?>
<?php

Pjax::end();
?>
</div>

