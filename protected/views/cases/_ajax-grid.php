<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Cases $searchModel
 */

?>



<?php Pjax::begin(['id'=>'cases-pjax-ajax-grid','enablePushState'=>false,'enableReplaceState'=>false]); ?>
    <?php

    echo TGridView::widget([
        'id' => 'cases-ajax-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [

            'id',
            'title',
            [
                'attribute' => 'client_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('client_id');
                }
            ],
            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('department_id');
                }
            ],
            'start_date:date',
            'end_date:date',

            'assign_to_id',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>'
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>

