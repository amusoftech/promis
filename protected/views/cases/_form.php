<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Cases */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'cases-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>



	
		<div class="col-md-6">

		
		 <?php echo $form->field($model, 'title')->textInput(['maxlength' => 128]) ?>

   
		 <?php echo  $form->field($model, 'description')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'description')->textarea(['rows' => 6]); ?>


</div>

	<div class="col-md-6">
	<?php

echo $form->field($model, 'client_id')->widget(Select2::classname(), [
    'data' => $model->getClientOptions(),
    'options' => [
        'placeholder' => 'Select',
        'multiple' => false,
        'id' => 'client_id'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
?>
<div class="row">
			<div class="col-md-6">
<?php

echo $form->field($model, 'department_id')->widget(Select2::classname(), [
    'data' => $model->getDepartmentOptions(),
    'options' => [
        'placeholder' => 'Select',
        'multiple' => false,
        'id' => 'department_id'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
?>
</div>
			<div class="col-md-6">
	<?php
echo $form->field($model, 'assign_to_id')->widget(DepDrop::classname(), [
    'data' => $model->getAssignToOptions(),
    'options' => [
        'placeholder' => 'Select'
    ],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => [
        'pluginOptions' => [
            'allowClear' => true
        ]
    ],
    'pluginOptions' => [
        'depends' => [
            'department_id'
        ],
        'url' => Url::to([
            '/cases/assign'
        ])
    ]
]);
?>
</div>
		</div>




		<div class="row">
			<div class="col-md-6">
   <?php

echo $form->field($model, 'start_date')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'minDate' => date('Y-m-d'),
        'maxDate' => date('Y-m-d', strtotime('+360 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
   </div>
			<div class="col-md-6">
    <?php

    echo $form->field($model, 'end_date')->widget(yii\jui\DatePicker::class, [
        // 'dateFormat' => 'php:Y-m-d',
        'options' => [
            'class' => 'form-control'
        ],
        'clientOptions' => [
            'minDate' => date('Y-m-d'),
            'maxDate' => date('Y-m-d', strtotime('+360 days')),
            'changeMonth' => true,
            'changeYear' => true
        ]
    ])?>
	 	</div>
		</div>
		<div class="row">
			<div class="col-md-6">
							 <?php echo $form->field($model, 'budget')->textInput(['maxlength' => 128]) ?>
					
				</div>
<div class="col-md-6">
	 <?php echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) ?>
					
				</div>
				<div class="col-md-6">
	 <?php echo $form->field($model, 'case_platform')->dropDownList($model->getPlatformOptions(), ['prompt' => '']) ?>
					
				</div>
			<div class="col-md-6">
   
	<?php if(User::isAdmin()){?>	
	
	 <?php echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>
	 <?php }?>		</div>
		</div>



	</div>



	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'cases-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>