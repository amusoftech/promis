<?php
use yii\widgets\ListView;
?>
<div class="tab-content">
	<div class="activity-table">
		<table class="table">
			<tbody>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_list'
]);
?>
			</tbody>
		</table>
	</div>
</div>