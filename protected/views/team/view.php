<?php
use app\components\useraction\UserAction;
/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Teams'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="team-view card-body">
	
		

           <?=  \app\components\PageHeader::widget(['title'=> $model->case->title]); ?>
         


		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'team-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('case_id')
            ],
            
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('user_id')
            ],
            
            
            [
                'attribute' => 'department',
                'format' => 'raw',
                'value' => function ($model){
                return  !empty($model->user)?$model->user->department->title:\Yii::t('app',"Not Set");
                }
            ],
            
           
          /*   [
                'attribute' => 'type_id',
                'value' => $model->getType()
            ], */

            'updated_on:datetime',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>


<?php  ?>


		<?php

echo UserAction::widget([
    'model' => $model,
    'attribute' => 'state_id',
    'states' => $model->getStateOptions()
]);
?>

		</div>
	</div>
</div>
