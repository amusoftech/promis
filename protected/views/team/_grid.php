<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Team $searchModel
 */

?>
<?php

echo MassAction::widget([
    'url' => Url::toRoute([
        '/team/mass'
    ]),
    'grid_id' => 'team-grid',
    'pjax_grid_id' => 'team-pjax-grid'
]);

?>
<div class='table table-responsive'>

<?php Pjax::begin(['id'=>'team-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'team-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('case_id');
                }
            ],
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('user_id');
                }
            ],

            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>' /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

