<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;

use yii\grid\GridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\CasePlatform $searchModel
 */

?>
<?php

echo MassAction::widget([
    'url' => Url::toRoute([
        '/case-platform/mass'
    ]),
    'grid_id' => 'case-platform-grid',
    'pjax_grid_id' => 'case-platform-pjax-grid'
]);

?>
<div class='table table-responsive'>

<?php Pjax::begin(['id'=>'case-platform-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'case-platform-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',
            [
                'attribute' => 'type_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('type_id');
                }
            ],
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('case_id');
                }
            ],
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],

            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>' /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

