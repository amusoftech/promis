<?php
use yii\helpers\Html;
use app\models\User;
?>
<tr>
	<td><?= Html::encode($model->id) ?></td>
	<td><?= Html::encode($model->title) ?></td>
	<td><?= Html::encode(!empty($model->case)?$model->case->title:"") ?></td>
	<td><span class="badge badge-success"><?= $model->getTimerStatus(); ?></span></td>
	<td class="sorting_1">
		<h2 class="table-avatar">
			<a href="#0"><?= Html::encode(!empty($model->assignTo)?$model->assignTo->full_name:"") ?> <span>Web
					Designer</span></a>
		</h2>
	</td>
	<td><?= Html::encode($model->estimated_time) ?> hrs</td>
	<td><?= !empty($model->actual_time)? Html::encode($model->actualTime):"not set" ?> </td>
	<td><?= Html::encode(date("d M Y",strtotime($model->created_on))) ?></td>
	<td class="text-right">
		<div class="dropdown dropdown-action">
			<a href="#" class="action-icon dropdown-toggle"
				data-toggle="dropdown" aria-expanded="false"><i
				class="la la-ellipsis-v"></i></a>
			<div class="dropdown-menu dropdown-menu-right">
				<a class="dropdown-item" href="<?= $model->getUrl('view') ?>"><i
					class="fa fa-eye m-r-5"></i> View</a>
								</div>
		</div>
	</td>
</tr>