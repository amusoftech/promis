<?php
use app\models\LeaveRequest;
use app\models\Ticket;
use app\models\Cases;
use app\models\User;
use yii\helpers\Url;
use app\models\Task;
/* @var $this \yii\web\View */

?>

<!-- Page Content -->
<div class="content container-fluid">
	<!-- Page Header -->
	<div class="page-header">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="page-title">Welcome <?=! empty(Yii::$app->user->identity) ? Yii::$app->user->identity->full_name : ''?> !</h3>
				<ul class="breadcrumb">
					<li class="breadcrumb-item active">Dashboard</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /Page Header -->
	<div class="row">
		<div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
		<a href='<?=Url::toRoute(['//cases/index/']);?>'>
			<div class="card dash-widget">
				<div class="card-body">
					<span class="dash-widget-icon"><i class="fa fa-cubes"></i></span>
					<div class="dash-widget-info">
						<h3><?=Cases::find()->Where(['assign_to_id' => \Yii::$app->user->id])->count();?></h3>
						<span>Cases</span>
					</div>
				</div>
			</div>
			</a>
		</div>

	
		<div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
         <a href='<?=Url::toRoute(['//task/index/']);?>'>
			<div class="card dash-widget">
				<div class="card-body">
					<span class="dash-widget-icon"><i class="fa fa-usd"></i></span>
					<div class="dash-widget-info">
									<h3><?=Task::find()->count();?></h3>
						<span>Tasks</span>
					</div>
				</div>
			</div>
			</a>
		</div>
	
		<div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
		<a href='<?=Url::toRoute(['//leave-request//index/']);?>'>
			<div class="card dash-widget">
				<div class="card-body">
					<span class="dash-widget-icon"><i class="fa fa-diamond"></i></span>
					<div class="dash-widget-info">
						<h3><?=LeaveRequest::find()->where(['created_by_id' => Yii::$app->user->id])->count()?></h3>
						<span>Leave Request</span>
					</div>
				</div>
			</div>
			</a>
		</div>
		<div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
		<a href='<?=Url::toRoute(['//ticket//index/' . User::ROLE_MANAGER]);?>'>
			<div class="card dash-widget">
				<div class="card-body">
					<span class="dash-widget-icon"><i class="fa fa-user"></i></span>
					<div class="dash-widget-info">
						<h3><?=Ticket::find()->where(['created_by_id' => Yii::$app->user->id])->count()?></h3>
						<span>Tickets</span>
					</div>
				</div>
			</div>
			</a>
		</div>
	</div>
	<!-- Statistics Widget -->
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xl-5 d-flex">
			<div class="card flex-fill dash-statistics">
				<div class="card-body">
					<h5 class="card-title">Statistics</h5>
					<div class="stats-list">
					
							<div class="stats-info">
							<p>
								Today Leave <strong><?=LeaveRequest::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => LeaveRequest::STATE_APPROVED])->count();?> <small>/<?=LeaveRequest::findActive()->Where(['created_by_id' => \Yii::$app->user->id])->count();?></small></strong>
							</p>
							<div class="progress">
								<div class="progress-bar bg-primary" role="progressbar"
									style="width: <?=LeaveRequest::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => LeaveRequest::STATE_APPROVED])->count();?>%"></div>
							</div>
						</div>
					
						
						
					
						<div class="stats-info">
							<p>
								Completed Projects <strong><?=Cases::findActive()->Where(['assign_to_id' => \Yii::$app->user->id,'state_id' => Cases::STATE_CLOSED])->count();?> <small>/ <?=Cases::findActive()->Where(['assign_to_id' => \Yii::$app->user->id])->count();?></small></strong>
							</p>
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar"
									style="width: <?=Cases::findActive()->Where(['assign_to_id' => \Yii::$app->user->id,'state_id' => Cases::STATE_CLOSED])->count();?>%"></div>
							</div>
						</div>
						
					
						<div class="stats-info">
							<p>
								Open Tickets <strong><?=Ticket::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Ticket::STATE_INPROGRESS])->count();?> <small>/ <?=Cases::findActive()->Where(['assign_to_id' => \Yii::$app->user->id])->count();?> </small></strong>
							</p>
							<div class="progress">
								<div class="progress-bar bg-danger" role="progressbar"
									style="width: <?=Ticket::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Ticket::STATE_INPROGRESS])->count();?> %"></div>
							</div>
						</div>
							<div class="stats-info">
							<p>
								Closed Tickets <strong><?=Ticket::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Ticket::STATE_COMPLETE])->count();?>  <small>/ <?=Cases::findActive()->Where(['assign_to_id' => \Yii::$app->user->id])->count();?></small></strong>
							</p>
							<div class="progress">
								<div class="progress-bar bg-info" role="progressbar"
									style="width: <?=Ticket::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Ticket::STATE_COMPLETE])->count();?>%"></div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-lg-6 col-xl-7 d-flex">
			<div class="card flex-fill">
				<div class="card-body">
					<h4 class="card-title">Task Statistics</h4>
					<div class="statistics">
						<div class="row">
							<div class="col-md-6 col-6 text-center">
							   <a href='<?=Url::toRoute(['//task/index/']);?>'>
								<div class="stats-box mb-4">
									<p>Total Tasks</p>
									<h3><?=Task::findActive()->count();?></h3>
								</div>
								</a>
							</div>
							<div class="col-md-6 col-6 text-center">
								<div class="stats-box mb-4">
									<p>InProgress Task</p>
									<h3><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_INPROGRESS])->count();?></h3>
								</div>
							</div>
						</div>
					</div>
					<div class="progress mb-4">
						<div class="progress-bar bg-purple" style="width: <?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_CLOSED])->count();?>%"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_CLOSED])->count();?>%</div>
						<div class="progress-bar bg-warning" style="width: <?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_INPROGRESS])->count();?>%"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_INPROGRESS])->count();?>%</div>
						<div class="progress-bar bg-success" style="width: <?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_ON_HOLD])->count();?>%"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_ON_HOLD])->count();?>%</div>
						<div class="progress-bar bg-danger" style="width: <?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_PENDING])->count();?>%"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_PENDING])->count();?>%</div>
						<div class="progress-bar bg-info" style="width: <?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_IN_REVIEW])->count();?>%"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_IN_REVIEW])->count();?>%</div>
					</div>
					<div>
						
						<p>
							<i class="fa fa-dot-circle-o text-purple mr-2"></i>Completed
							Tasks <span class="float-right"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_CLOSED])->count();?></span>
						</p>
						<p>
							<i class="fa fa-dot-circle-o text-warning mr-2"></i>Inprogress
							Tasks <span class="float-right"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_INPROGRESS])->count();?></span>
						</p>
						<p>
							<i class="fa fa-dot-circle-o text-success mr-2"></i>On Hold Tasks
							<span class="float-right"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_ON_HOLD])->count();?></span>
						</p>
						<p>
							<i class="fa fa-dot-circle-o text-danger mr-2"></i>Pending Tasks
							<span class="float-right"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_PENDING])->count();?></span>
						</p>
						<p class="mb-0">
							<i class="fa fa-dot-circle-o text-info mr-2"></i>Review Tasks <span
								class="float-right"><?=Task::findActive()->Where(['created_by_id' => \Yii::$app->user->id,'state_id' => Task::STATE_IN_REVIEW])->count();?></span>
						</p>
					</div>
				</div>
			</div>
		</div>


	</div>
	<!-- /Statistics Widget -->

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<header class="card-title card-header"><?=Yii::t("app", 'Case List')?></header>
				<div class="card-body">
		
				<?php

    echo $this->render('//cases/_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel
    ]);

    ?>		
		</div>
			</div>
		</div>
	</div>
</div>

<!-- /Page Content -->


