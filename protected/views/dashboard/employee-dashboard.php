    <?php

	use app\models\LeaveRequest;
	use app\models\Ticket;
	use app\models\Task;
	use app\models\Team;
	use yii\helpers\Url;
	use yii\widgets\ListView;
	?>

    <!-- Page Header -->
    <div class="page-header">
    	<div class="row">
    		<div class="col-sm-12">
    			<h3 class="page-title">Welcome <?= !empty(Yii::$app->user->identity) ? Yii::$app->user->identity->full_name : '' ?></h3>
    			<ul class="breadcrumb">
    				<li class="breadcrumb-item active">Dashboard</li>
    			</ul>
    		</div>
    	</div>
    </div>
    <!-- /Page Header -->

    <div class="row">
    	<div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
    		<div class="card dash-widget">
			<a href="<?= Url::toRoute(['cases/index']) ?>">
    			<div class="card-body">
    				<span class="dash-widget-icon"><i class="fa fa-cubes"></i></span>
    				<div class="dash-widget-info">
    					<h3><?= Team::find()->where(['user_id' => Yii::$app->user->id])->count() ?></h3>
    					<span>Projects</span>
    				</div>
				</div>
			</a>
    		</div>
    	</div>
    	<div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
    		<div class="card dash-widget">
    			<a href="<?= Url::toRoute(['task/index']) ?>">
    				<div class="card-body">
    					<span class="dash-widget-icon"><i class="fa fa-usd"></i></span>
    					<div class="dash-widget-info">
    						<h3><?= Task::find()->where(['assign_to_id' => Yii::$app->user->id])->count() ?></h3>
    						<span>Tasks</span>
    					</div>
    				</div>
    			</a>
    		</div>
    	</div>
    	<div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
    		<div class="card dash-widget">
    			<div class="card-body">
    				<span class="dash-widget-icon"><i class="fa fa-diamond"></i></span>
    				<div class="dash-widget-info">
    					<h3><?= LeaveRequest::find()->where(['created_by_id' => Yii::$app->user->id])->count() ?></h3>
    					<span>Leave Request</span>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
    		<div class="card dash-widget">
    			<div class="card-body">
    				<span class="dash-widget-icon"><i class="fa fa-user"></i></span>
    				<div class="dash-widget-info">
    					<h3><?= Ticket::find()->where(['created_by_id' => Yii::$app->user->id])->count() ?></h3>
    					<span>Tickets</span>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

    <!-- Statistics Widget -->
    <div class="row">
    	<div class="col-md-12 col-lg-12 col-xl-5 d-flex">
    		<div class="card flex-fill dash-statistics">
    			<div class="card-body">
    				<h5 class="card-title">Statistics</h5>
    				<div class="stats-list">
    					<div class="stats-info">
    						<p>
    							Today Leave <strong>4 <small>/ 65</small></strong>
    						</p>
    						<div class="progress">
    							<div class="progress-bar bg-primary" role="progressbar" style="width: 31%"></div>
    						</div>
    					</div>
    					<div class="stats-info">
    						<p>
    							Pending Invoice <strong>15 <small>/ 92</small></strong>
    						</p>
    						<div class="progress">
    							<div class="progress-bar bg-warning" role="progressbar" style="width: 31%"></div>
    						</div>
    					</div>
    					<div class="stats-info">
    						<p>
    							Completed Projects <strong>85 <small>/ 112</small></strong>
    						</p>
    						<div class="progress">
    							<div class="progress-bar bg-success" role="progressbar" style="width: 62%"></div>
    						</div>
    					</div>
    					<div class="stats-info">
    						<p>
    							Open Tickets <strong>190 <small>/ 212</small></strong>
    						</p>
    						<div class="progress">
    							<div class="progress-bar bg-danger" role="progressbar" style="width: 62%"></div>
    						</div>
    					</div>
    					<div class="stats-info">
    						<p>
    							Closed Tickets <strong>22 <small>/ 212</small></strong>
    						</p>
    						<div class="progress">
    							<div class="progress-bar bg-info" role="progressbar" style="width: 22%"></div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>

    	<div class="col-md-12 col-lg-6 col-xl-7 d-flex">
    		<div class="card flex-fill">
    			<div class="card-body">
    				<h4 class="card-title">Task Statistics</h4>
    				<div class="statistics">
    					<div class="row">
    						<div class="col-md-6 col-6 text-center">
    							<div class="stats-box mb-4">
    								<p>Total Tasks</p>
    								<h3><?= Task::find()->where(['assign_to_id' => Yii::$app->user->id])->count() ?></h3>
    							</div>
    						</div>
    						<div class="col-md-6 col-6 text-center">
    							<div class="stats-box mb-4">
    								<p>Overdue Tasks</p>
    								<h3>19</h3>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="progress mb-4">
    					<div class="progress-bar bg-purple" style="width: 30%">30%</div>
    					<div class="progress-bar bg-warning" style="width: 22%">22%</div>
    					<div class="progress-bar bg-success" style="width: 24%">24%</div>
    					<div class="progress-bar bg-danger" style="width: 26%">21%</div>
    					<div class="progress-bar bg-info" style="width: 10%">10%</div>
    				</div>
    				<div>
    					<p>
    						<i class="fa fa-dot-circle-o text-purple mr-2"></i>Completed Tasks
    						<span class="float-right"><?= Task::getTaskStatus(Task::STATE_COMPLETED) ?></span>
    					</p>
    					<p>
    						<i class="fa fa-dot-circle-o text-warning mr-2"></i>Inprogress
    						Tasks <span class="float-right"><?= Task::getTaskStatus(Task::STATE_INPROGRESS) ?></span>
    					</p>
    					<p>
    						<i class="fa fa-dot-circle-o text-success mr-2"></i>On Hold Tasks
    						<span class="float-right"><?= Task::getTaskStatus(Task::STATE_ON_HOLD) ?></span>
    					</p>
    					<p>
    						<i class="fa fa-dot-circle-o text-danger mr-2"></i>Pending Tasks <span class="float-right"><?= Task::getTaskStatus(Task::STATE_PENDING) ?></span>
    					</p>
    					<p class="mb-0">
    						<i class="fa fa-dot-circle-o text-info mr-2"></i>Review Tasks <span class="float-right"><?= Task::getTaskStatus(Task::STATE_IN_REVIEW) ?></span>
    					</p>
    				</div>
    			</div>
    		</div>
    	</div>

    </div>
    <!-- /Statistics Widget -->

    <div class="row">

    	<div class="col-md-12 d-flex">
    		<div class="table-responsive">
    			<table class="table table-striped custom-table mb-0 ">
    				<thead>
    					<tr>
    						<th>ID</th>
    						<th>Task</th>
    						<th>Projects</th>
    						<th>Timer</th>
    						<th>Assign To</th>
    						<th>Estimate Time</th>
    						<th>Actual Time</th>
    						<th>Created On</th>
    						<th class="text-right">Actions</th>
    					</tr>
    				</thead>
    				<tbody>
    					<?php

						echo ListView::widget([
							'dataProvider' => $dataProvider,
							'itemView' => '_taskList',
						]); ?>
    				</tbody>
    			</table>
    		</div>
    	</div>
    </div>
    <!-- /Page Content -->