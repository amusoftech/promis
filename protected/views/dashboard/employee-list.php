<?php
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Designation;
use app\components\TActiveForm;
use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\User $searchModel
 * @var $user app\models\User
 */
?>

<!-- Page Header -->
<div class="page-header">
	<div class="row align-items-center">
		<div class="col">
			<h3 class="page-title">Employee List</h3>
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?= Url::home()?>">Home</a></li>
				<li class="breadcrumb-item active">Employee List</li>
			</ul>
		</div>
		<div class="col-auto float-right ml-auto">
			<a href="<?= Url::toRoute(['/user/add-employee'])?>"
				class="btn add-btn ml-2"><i class="fa fa-plus"></i> Add Employee</a>
		</div>
	</div>
</div>
<!-- /Page Header -->

<!-- Search Filter -->
<?php

$form = TActiveForm::begin([
    'id' => 'employee-search-form',
    'enableAjaxValidation' => false,
    'action' => Url::toRoute([
        '/dashboard/hr-index'
    ])
]);
?><div class="row filter-row">

	<div class="col-sm-6 col-md-3">
		<?=$form->field($user, 'employee_id', ['options' => ['class' => 'form-group form-focus']] )->textInput(['maxlength' => 55 , 'class' => 'form-control floating' , 'value' => !empty($search['User']['employee_id']) ? $search['User']['employee_id'] : ''])->label('Employee ID',['class'=>"focus-label"])?>
	</div>
	<div class="col-sm-6 col-md-3">
		<?=$form->field($user, 'full_name', ['options' => ['class' => 'form-group form-focus']] )->textInput(['maxlength' => 55 , 'class' => 'form-control floating', 'value' => !empty($search['User']['full_name']) ? $search['User']['full_name'] : ''])->label('Employee Name',['class'=>"focus-label"])?>
	</div>
	<div class="col-sm-6 col-md-3">
		<?= $form->field($user, 'designation', ['options' => ['class' => 'form-group form-focus select-focus focused']] )->dropDownList(ArrayHelper::Map(Designation::find()->all(), 'id', 'title') , ['prompt' => 'Select Designation', 'value' => !empty($search['User']['designation']) ? $search['User']['designation'] : ''])->label('Designation',['class'=>"focus-label"])?>
	</div>
	<div class="col-sm-6 col-md-3">
		<?=Html::submitButton( Yii::t('app', 'Search'), ['class' => 'btn btn-primary submit-btn'])?>
	</div>
</div>
<?php TActiveForm::end()?>
<!-- /Search Filter -->

<div class="staff-grid-row">

<?php

echo ListView::widget([
    'summary' => false,
    'dataProvider' => $dataProvider,

    'options' => [
        'class' => 'list-view row'
    ],

    'itemOptions' => [
        'class' => 'item col-md-4 col-sm-6 col-12 col-lg-4 col-xl-3'
    ],
    'itemView' => '_employee-list'
]);
?>

</div>
<!-- /Page Content -->














