<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php
/** @var $model **/
?>
<div class="profile-widget">
	<div class="profile-img">
		<a href="<?= $model->getUrl('view-employee')?>" class="avatar"><?=$model->displayImage($model->profile_file, [], 'default.jpg', true);?> </a>
	</div>

	<div class="dropdown profile-action">
		<a href="javascript:void(0);" class="action-icon dropdown-toggle"
			data-toggle="dropdown" aria-expanded="false"><i
			class="fa fa-ellipsis-v"></i></a>
		<div class="dropdown-menu dropdown-menu-right">
			<a class="dropdown-item"
				href="<?= Url::toRoute(['/user/update-employee' , 'id' => $model->id])?>"><i
				class="fa fa-pencil m-r-5"></i> Edit</a> 
				
			
												<?php
            $options = [
                'title' => Yii::t('app', 'Delete'),
                'aria-label' => Yii::t('app', 'Delete'),
                'data-pjax' => '0',
                'class' => 'dropdown-item showActionModalButton',
                'data-id' => $model->id,
                'value' => Url::toRoute([
                    '/user/delete-employee',
                    'id' => $model->id
                ])
            ];

            echo Html::a('<i class="fa fa-trash-o m-r-5"></i>Delete', false, $options);
            ?>
			
		</div>
	</div>
	<h4 class="user-name m-t-10 mb-0 text-ellipsis">
		<a href="<?= $model->getUrl('view-employee')?>"><?= !empty($model->full_name) ? $model->full_name : ''?></a>
	</h4>
	<div class="small text-muted"><?= !empty($model->department) ? $model->department->title : ''?></div>
</div>