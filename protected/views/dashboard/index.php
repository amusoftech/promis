<?php
use app\components\notice\Notices;
use app\controllers\DashboardController;
use app\models\EmailQueue;
use app\models\User;
use app\modules\page\models\Page;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Url;
use app\modules\logger\models\Log;
use app\models\Cases;
use app\models\Client;
/* @var $this \yii\web\View */
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Dashboard')
];
?>
	<?php
$isConfig = \Yii::$app->settings->isConfig;
if (! $isConfig) {
    ?>
<div>
	<div class="alert alert-info">
		<strong> Info !! </strong> Your app is not configure properly <b><a
			href="<?=Url::toRoute(['/setting/index'])?>"> Click Here </a></b> To
		configure..
	</div>
</div>
<?php
}
?>

<div class="row">
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a
			href='<?=Url::toRoute(['//user/index/' . User::ROLE_EMPLOYEE]);?>'>
			<div class="card card-inverse card-success">
				<div class="box  text-center">
					<h1 class="font-light text-white"><?=User::findActive()->Where(['role_id' => User::ROLE_EMPLOYEE])->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Employees')?></h6>
				</div>
			</div>
		</a>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a href='<?=Url::toRoute(['//user/index/' . User::ROLE_MANAGER]);?>'>
			<div class="card card-inverse card-info">
				<div class="box  text-center">
					<h1 class="font-light text-white"><?=User::findActive()->Where(['role_id' => User::ROLE_MANAGER])->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Managers')?></h6>
				</div>
			</div>
		</a>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a
			href='<?=Url::toRoute(['//user/index/' . User::ROLE_INVESTIGATOR]);?>'>
			<div class="card card-inverse card-primary">
				<div class="box  text-center">
					<h1 class="font-light text-white"><?=User::findActive()->Where(['role_id' => User::ROLE_INVESTIGATOR])->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Investigators')?></h6>
				</div>
			</div>
		</a>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a
			href='<?=Url::toRoute(['//user/index/' . User::ROLE_DISTRIBUTOR]);?>'>
			<div class="card card-inverse card-info">
				<div class="box  text-center">
					<h1 class="font-light text-white"><?=User::findActive()->Where(['role_id' => User::ROLE_DISTRIBUTOR])->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Distributors')?></h6>
				</div>
			</div>
		</a>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a
			href='<?=Url::toRoute(['//user/index/' . User::ROLE_QUALITY_CONTROL]);?>'>
			<div class="card card-inverse card-info">
				<div class="box  text-center">
					<h1 class="font-light text-white"><?=User::findActive()->Where(['role_id' => User::ROLE_QUALITY_CONTROL])->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Quality Controllers')?></h6>
				</div>
			</div>
		</a>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a href='<?=Url::toRoute(['//user/index/' . User::ROLE_HR]);?>'>
			<div class="card card-inverse card-primary">
				<div class="box  text-center">
					<h1 class="font-light text-white"><?=User::findActive()->Where(['role_id' => User::ROLE_HR])->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total HR')?></h6>
				</div>
			</div>
		</a>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a href='<?=Url::toRoute(['cases/index']);?>'>
			<div class="card card-inverse card-warning">
				<div class="box text-center">
					<h1 class="font-light text-white"><?=Cases::find()->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Cases')?></h6>
				</div>
			</div>
		</a>
	</div>
	<div class="col-md-6 col-lg-3">
		<a href='<?=Url::toRoute(['client/index']);?>'>
			<div class="card card-inverse card-success">
				<div class="box text-center">
					<h1 class="font-light text-white"><?=User::find()->where(['role_id' => User::ROLE_CLIENT])->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Client')?></h6>
				</div>
			</div>
		</a>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a href='<?=Url::toRoute(['email-queue/index']);?>'>
			<div class="card card-inverse card-success">
				<div class="box text-center">
					<h1 class="font-light text-white"><?=EmailQueue::find()->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Emails')?></h6>
				</div>
			</div> 
		</a>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-md-6 col-lg-3">
		<a href='<?=Url::toRoute(['//log/index/']);?>'>
			<div class="card card-inverse card-warning">
				<div class="box text-center">
					<h1 class="font-light text-white"><?=Log::find()->count();?></h1>
					<h6 class="text-white"><?=Yii::t("app", 'Total Logs')?></h6>
				</div>
			</div>
		</a>
	</div>
	<!-- Column -->
</div>

<div class="row">
	<div class="col-lg-4 col-md-12">
		<div class="card">
			<div class="card-block">
		<?php
$data = DashboardController::MonthlySignups();

echo Highcharts::widget([
    'options' => [
        'credits' => array(
            'enabled' => false
        ),

        'title' => [
            'text' => 'Monthly Users Registered'
        ],
        'chart' => [
            'type' => 'column'
        ],
        'xAxis' => [
            'categories' => array_keys($data)
        ],
        'yAxis' => [
            'title' => [
                'text' => 'Count'
            ]
        ],
        'series' => [
            [
                'name' => 'Users',
                'data' => array_values($data)
            ]
        ]
    ]
]);
?>
</div>
		</div>
	</div>

	<div class="col-lg-4 col-md-12">
		<div class="card">
			<div class="card-block">
<?php
$data = Cases::monthly();
echo Highcharts::widget([
    'options' => [
        'credits' => array(
            'enabled' => false
        ),

        'title' => [
            'text' => 'Monthly Cases Registered '
        ],
        'chart' => [
            'type' => 'column'
        ],
        'xAxis' => [
            'categories' => array_keys($data)
        ],
        'yAxis' => [
            'title' => [
                'text' => 'Count'
            ]
        ],
        'series' => [
            [
                'name' => 'Cases',
                'data' => array_values($data)
            ]
        ]
    ]
]);
?>
</div>
		</div>
	</div>

	<div class="col-lg-4 col-md-12">
		<div class="card">
			<div class="card-block">
<?php
$data = Client::monthly();
echo Highcharts::widget([
    'options' => [
        'credits' => array(
            'enabled' => false
        ),

        'title' => [
            'text' => 'Clients '
        ],
        'chart' => [
            'type' => 'column'
        ],
        'xAxis' => [
            'categories' => array_keys($data)
        ],
        'yAxis' => [
            'title' => [
                'text' => 'Count'
            ]
        ],
        'series' => [
            [
                'name' => 'Clients',
                'data' => array_values($data)
            ]
        ]
    ]
]);
?>
</div>
		</div>
	</div>
</div>

<!-- Row -->
<!-- .right-sidebar -->
<!-- <div class="right-sidebar">
	<div class="slimscrollright">
		<div class="rpanel-title">
			Service Panel <span><i class="ti-close right-side-toggle"></i></span>
		</div>
		<div class="r-panel-body">
			<ul id="themecolors" class="m-t-20">
				<li><b>With Light sidebar</b></li>
				<li><a href="javascript:void(0)" data-theme="default"
					class="default-theme">1</a></li>
				<li><a href="javascript:void(0)" data-theme="green"
					class="green-theme">2</a></li>
				<li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
				<li><a href="javascript:void(0)" data-theme="blue"
					class="blue-theme working">4</a></li>
				<li><a href="javascript:void(0)" data-theme="purple"
					class="purple-theme">5</a></li>
				<li><a href="javascript:void(0)" data-theme="megna"
					class="megna-theme">6</a></li>
				<li class="d-block m-t-30"><b>With Dark sidebar</b></li>
				<li><a href="javascript:void(0)" data-theme="default-dark"
					class="default-dark-theme">7</a></li>
				<li><a href="javascript:void(0)" data-theme="green-dark"
					class="green-dark-theme">8</a></li>
				<li><a href="javascript:void(0)" data-theme="red-dark"
					class="red-dark-theme">9</a></li>
				<li><a href="javascript:void(0)" data-theme="blue-dark"
					class="blue-dark-theme">10</a></li>
				<li><a href="javascript:void(0)" data-theme="purple-dark"
					class="purple-dark-theme">11</a></li>
				<li><a href="javascript:void(0)" data-theme="megna-dark"
					class="megna-dark-theme ">12</a></li>
			</ul>
		</div>
	</div>
</div> -->