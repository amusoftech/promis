<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\AttendanceEvent */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'attendance-event-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>


<div class="row">




		<div class="col-md-12">
   
		 <?php echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) ?>
	 		</div>


		<div class="col-md-12">
   
		 <?php

echo $form->field($model, 'time')->widget(TimePicker::classname(), [
    'pluginOptions' => [
        'defaultTime' => date('H:i'),
        'showMeridian' => false
    ]
]);
?>
	 		</div>


	</div>



	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'attendance-event-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>