<?php

/* @var $this yii\web\View */
/* @var $model app\models\AttendanceEvent */
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Attendance Events'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Add');
?>

<div class="wrapper">
	<div class="card">

		<div class="attendance-event-create">
	<?=  \app\components\PageHeader::widget(); ?>
</div>

	</div>

	<div class="content-section clearfix card">

		<?= $this->render ( '_form', [ 'model' => $model ] )?></div>
</div>


