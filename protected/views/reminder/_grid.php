<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Reminder $searchModel
 */

?>
<?php
if (! User::isInvestigator()) {
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/reminder/mass'
        ]),
        'grid_id' => 'reminder-grid',
        'pjax_grid_id' => 'reminder-pjax-grid'
    ]);
}
?>
<?php

if (User::isInvestigator()) {
    $temp = '{view}';
} else {
    $temp = '{view}{delete}{update}';
}
?>
<div class='table table-responsive'>

<?php

Pjax::begin([
    'id' => 'reminder-pjax-grid'
]);
?>
    <?php

    echo TGridView::widget([
        'id' => 'reminder-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',
            'title',
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('case_id');
                }
            ],
            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('department_id');
                }
            ],
            'time:time',
            'created_on:datetime',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('user_id');
                }
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                // 'template' => $temp,
                'header' => '<a>Actions</a>' /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
            ]
        ]
    ]);
    ?>
<?php

Pjax::end();
?>
</div>

