<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Reminder */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'reminder-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>


<div class="row">


		<div class="col-md-6">

		
		 <?php echo $form->field($model, 'title')->textInput(['maxlength' => 1024]) ?>
	 		</div>


		<div class="col-md-6">
   
		 <?php echo  $form->field($model, 'description')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'description')->textarea(['rows' => 6]); ?>
	 		</div>


		<div class="col-md-6">
   
		 <?php echo $form->field($model, 'case_id')->dropDownList($model->getCaseOptions(), ['prompt' => '']) ?>
	 		</div>


		<div class="col-md-6">
   
		 <?php echo $form->field($model, 'department_id')->dropDownList($model->getDepartmentOptions(), ['prompt' => '']) ?>
	 		</div>

		<div class="col-md-6">
   
		 <?php echo $form->field($model, 'user_id')->dropDownList($model->getUserOptions(), ['prompt' => '']) ?>
	 		</div>

	</div>



	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'reminder-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>