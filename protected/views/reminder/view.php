<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\Reminder */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Reminders'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="reminder-view card-body">
			<?=\app\components\PageHeader::widget(['model' => $model]);?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'reminder-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('case_id')
            ],
            [
                'attribute' => 'department_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('department_id')
            ],
            'time:time',

            'created_on:datetime',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('user_id')
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>


<?php

echo $model->description;
?>


		<?php

echo UserAction::widget([
    'model' => $model,
    'attribute' => 'state_id',
    'states' => $model->getStateOptions()
]);

?>

		</div>
	</div>

</div>
