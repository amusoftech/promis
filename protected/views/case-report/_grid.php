<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;

use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\CaseReport $searchModel
 */

?>
<?php
if (User::isAdmin()) {
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/case-report/mass'
        ]),
        'grid_id' => 'case-report-grid',
        'pjax_grid_id' => 'case-report-pjax-grid'
    ]);
}
?>
 <?php
if (User::isInvestigator() || User::isClient()) {
    $action = '{view}';
} else {
    $action = '{view}{update}{delete}';
}
?>
<div class='table table-responsive'>

<?php

Pjax::begin([
    'id' => 'case-report-pjax-grid'
]);
?>
    <?php

    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],
        [
            'name' => 'check',
            'class' => 'yii\grid\CheckboxColumn',
            'visible' => User::isAdmin()
        ],

        'id',
        'title',
        /* 'description:html',*/
        [
            'attribute' => 'case_id',
            'format' => 'raw',
            'value' => function ($data) {
                return $data->getRelatedDataLink('case_id');
            }
        ],
        [
            'attribute' => 'submitted_to',
            'format' => 'raw',
            'value' => function ($data) {
                return $data->getRelatedDataLink('submitted_to');
            }
        ],
        [
            'attribute' => 'created_by_id',
            'format' => 'raw',
            'value' => function ($data) {
                return $data->getRelatedDataLink('created_by_id');
            }
        ],
        [
            'attribute' => 'state_id',
            'format' => 'raw',
            'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
            'value' => function ($data) {
                return $data->getStateBadge();
            }
        ],

        [
            'attribute' => 'created_on',
            'format' => 'raw',
            'filter' => \yii\jui\DatePicker::widget([
                'inline' => false,
                'clientOptions' => [
                    'autoclose' => true
                ],
                'model' => $searchModel,
                'attribute' => 'created_on',
                'options' => [
                    'id' => 'created_on',
                    'class' => 'form-control'
                ]
            ]),
            'value' => function ($data) {
                return date('Y-m-d H:i:s', strtotime($data->created_on));
            }
        ],

        [
            'class' => 'app\components\TActionColumn',
            'template' => $action,
            'header' => '<a>Actions</a>' /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
        ]
    ];
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]);

    echo TGridView::widget([
        'id' => 'case-report-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => $gridColumns
    ]);

    ?>
<?php

Pjax::end();
?>
</div>

