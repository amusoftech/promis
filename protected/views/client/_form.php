<?php
use yii\helpers\Html;
use app\components\TActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?=strtoupper(Yii::$app->controller->action->id);?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'client-form',
        'options' => [
            'class' => 'row'
        ]
    ]);
    ?>


<div class="row">
		<div class="col-md-6">		
		 <?php

echo $form->field($model, 'name')->textInput([
    'maxlength' => 255
])?>
	 		</div>

		<div class="col-md-6">
   
		 <?php

echo $form->field($model, 'email')->textInput([
    'maxlength' => 255
])?>
	 		</div>

<?php

if ($model->isNewRecord) {
    ?>
<div class="col-md-6">
   
		 <?php

    echo $form->field($model, 'password')->passwordInput([
        'maxlength' => 255
    ])?>
	 		</div>
	 		<?php
}
?>
		<div class="col-md-6">
   
		 <?php

echo $form->field($model, 'phone')->textInput([
    'maxlength' => true
])?>
	 		</div>


		<div class="col-md-6">
   
		 <?php

echo $form->field($model, 'alternate_email')->textInput([
    'maxlength' => 255
])?>
	 		</div>

		<div class="col-md-6">
   
		 <?php

echo $form->field($model, 'skype_id')->textInput([
    'maxlength' => 255
])?>
	 		</div>

		<div class="col-md-6">
   
		 <?php
// echo $form->field($model, 'skype_id')->dropDownList($model->getSkypeOptions(), ['prompt' => '']) ?>
	 		</div>



		<div class="col-md-6">
    
		 <?php

echo $form->field($model, 'address')->textInput([
    'maxlength' => 255
])?>
	 		</div>


		<div class="col-md-6">
   
		 <?php

echo $form->field($model, 'country')->textInput([
    'maxlength' => 255
])?>
	 		</div>
		<div class="col-lg-6">
	 <?=$form->field($model, 'profile_file')->fileInput()?>
</div>
	</div>




	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id' => 'client-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php

    TActiveForm::end();
    ?>
</div>