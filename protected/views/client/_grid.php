<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Client $searchModel
 */

?>
<?php

if (User::isAdmin()) {
    $actions = '{view}{delete}{update}';
} else {
    $actions = '{view}';
}

?>
<?php

echo MassAction::widget([
    'url' => Url::toRoute([
        '/client/mass'
    ]),
    'grid_id' => 'client-grid',
    'pjax_grid_id' => 'client-pjax-grid'
]);

?>
<div class='table table-responsive'>

<?php Pjax::begin(['id'=>'client-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'client-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',
            'name',
            'email:email',
            'phone',

            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],

            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],
            [
                'attribute' => 'created_on',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'created_on',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return date('Y-m-d H:i:s', strtotime($data->created_on));
                }
            ],
            [
                'attribute' => 'updated_on',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'updated_on',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return date('Y-m-d H:i:s', strtotime($data->updated_on));
                }
            ],

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>',
                'template' => $actions
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

