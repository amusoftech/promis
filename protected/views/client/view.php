<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Clients'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
$userModel = new User();

?>

<div class="wrapper">
	<div class=" card ">

		<div class="client-view card-body">
			<?=\app\components\PageHeader::widget(['model' => $model]);?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
		<div class="profileimage">
    				<?=$userModel->displayImage($userModel->profile_file, ['class' => 'profile-pic'], 'default.jpg', true);?>
    			</div>
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'client-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            'name',
            'email:email',
            'phone',
            // 'alternate_email:email',

            'address',
            // 'country',

            'created_on:datetime',
            'updated_on:datetime',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>


<?php

?>


		<?php

echo UserAction::widget([
    'model' => $model,
    'attribute' => 'state_id',
    'states' => $model->getStateOptions()
]);
?>

		</div>
	</div>




</div>
