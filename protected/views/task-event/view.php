<?php

use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
/* @var $this yii\web\View */
/* @var $model app\models\TaskEvent */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = (string)$model;
?>

<div class="wrapper">
	<div class=" card ">

		<div
			class="task-event-view card-body">
			<?=  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php echo \app\components\TDetailView::widget([
    	'id'	=> 'task-event-detail-view',
        'model' => $model,
        'options'=>['class'=>'table table-bordered'],
        'attributes' => [
            'id',
           // 'date:date',
            //'time:time',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function ($model) {
                return  !empty($model->userName)?$model->userName->full_name:"not set";
                }
                ],
            [
			'attribute' => 'task_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('task_id'),
			],
            [
                'attribute' => 'start_time',
                'format'=>'raw',
                'value' => !empty($model->start_time)?$model->start_time:\Yii::t('app',"Not set"),
            ],
            [
                'attribute' => 'end_time',
                'format'=>'raw',
                'value' => !empty($model->end_time)?$model->end_time:\Yii::t('app',"Not set"),
            ],
            //'start_time:datetime',
           // 'end_time:datetime',
           /*  [
			'attribute' => 'type_id',
			'value' => $model->getType(),
			], */
           
            'created_on:datetime',
            'updated_on:datetime',
            [
			'attribute' => 'created_by_id',
			'format'=>'raw',
			'value' => $model->getRelatedDataLink('created_by_id'),
			],
        ],
    ]) ?>


<?php  ?>


		<?php				echo UserAction::widget ( [
						'model' => $model,
						'attribute' => 'state_id',
						'states' => $model->getStateOptions ()
				] );
				?>

		</div>
</div>
 


	<div class=" card ">
				<div class=" card-body ">
					<div
						class="task-event-panel">

<?php
$this->context->startPanel();
	$this->context->addPanel('Feeds', 'feeds', 'Feed',$model /*,null,true*/);

$this->context->endPanel();
?>
				</div>
				</div>
			</div>


</div>
