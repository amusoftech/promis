<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use kartik\time\TimePicker;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\TaskEvent */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">

    <?php
    $form = TActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'task-event-form'
    ]);

    // echo $form->errorSummary($model);
    ?>





	<div class="col-md-6">

		
		 <?php

echo $form->field($model, 'date')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'minDate' => date('Y-m-d'),
        'maxDate' => date('Y-m-d', strtotime('+360 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
	 		


		 <?php //echo $form->field($model, 'time')->widget(TimePicker::class,[]) ?>
	 		


		 <?php //echo $form->field($model, 'user_id')->dropDownList($model->getUserOptions(), ['prompt' => '']) ?>
	 		


		 <?php echo $form->field($model, 'task_id')->dropDownList($model->getTaskOptions(), ['prompt' => '']) ?>
	 		

 		


 <?php

/* echo $form->field($model, 'start_time')->widget(TimePicker::class, [

    'options' => [
        'class' => 'form-control'
    ]
]) */?>
 <?php

/* echo $form->field($model, 'end_time')->widget(TimePicker::class, [

    'options' => [
        'class' => 'form-control'
    ]
]) */?>


	 <?php 

      echo $form->field($model, 'start_time')->widget(DateTimePicker::class)
     
?>
	 		


		 <?php 

      echo $form->field($model, 'end_time')->widget(DateTimePicker::class)
     
?>




		 <?php 

    /*   echo $form->field($model, 'start_time')->widget(yii\jui\DatePicker::class,
      [
      //'dateFormat' => 'php:Y-m-d',
      'options' => [ 'class' => 'form-control' ],
      'clientOptions' =>
      [
      'minDate' => date('Y-m-d'),
      'maxDate' => date('Y-m-d',strtotime('+30 days')),
      'changeMonth' => true,'changeYear' => true ] ]) */
     
?>
	 		


		 <?php 

     /*  echo $form->field($model, 'end_time')->widget(yii\jui\DatePicker::class,
      [
      //'dateFormat' => 'php:Y-m-d',
      'options' => [ 'class' => 'form-control' ],
      'clientOptions' =>
      [
      'minDate' => date('Y-m-d'),
      'maxDate' => date('Y-m-d',strtotime('+30 days')),
      'changeMonth' => true,'changeYear' => true ] ])
      */
?>
	 		


		 <?php /*echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) */ ?>
	 		


	<?php if(User::isAdmin()){?>	 <?php /*echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) */ ?>
	 <?php }?>		


	   <div class="form-group">
			<div
				class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'task-event-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
		</div>

    <?php TActiveForm::end(); ?>

</div>
</div>