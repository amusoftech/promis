<?php
use app\components\TGridView;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\EmailQueue $searchModel
 */

?>
<?php Pjax::begin(["enablePushState"=>false,"enableReplaceState"=>false,'id' => 'email-queue-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'email-queue-ajax-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [

            'id',
            'to_email:email',
            'subject',
            'date_sent:datetime',

            [
                'class' => 'app\components\TActionColumn',
                'header' => "<a>" . Yii::t("app", 'Actions') . "</a>"
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>

