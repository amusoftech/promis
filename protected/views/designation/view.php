<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Designation */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Designations'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="designation-view card-body">
			<?=  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'designation-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
           'title',
            
            /*[
			'attribute' => 'state_id',
			'format'=>'raw',
			'value' => $model->getStateBadge(),],*/
            'created_on:datetime',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($model) {
                return ! empty($model->createdBy) ? $model->createdBy->full_name : Yii::t('app', "not set");
                }
                ]
        ]
    ])?>


<?php  ?>


		<?php

echo UserAction::widget([
    'model' => $model,
    'attribute' => 'state_id',
    'states' => $model->getStateOptions()
]);
?>

		</div>
	</div>



	<div class=" card ">
		<div class=" card-body ">
			<div class="designation-panel">

<?php
$this->context->startPanel();
$this->context->addPanel('Feeds', 'feeds', 'Feed', $model /* ,null,true */);

$this->context->endPanel();
?>
				</div>
		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">

<?= CommentsWidget::widget(['model'=>$model]); ?>
			</div>
	</div>
</div>
