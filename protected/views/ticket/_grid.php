<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Ticket $searchModel
 */

?>
<?php
$actions = '';
if ((User::isAdmin()) || (User::isHR())) {
    $actions = '{view}{update}{delete}';
} else {
    $actions = '{view}{update}';
}

?>
<?php
if (User::isAdmin())
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/ticket/mass'
        ]),
        'grid_id' => 'ticket-grid',
        'pjax_grid_id' => 'ticket-pjax-grid'
    ]);
?>
<div class='table table-responsive'>

<?php Pjax::begin(['id'=>'ticket-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'ticket-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',
            'title',
            [
                'attribute' => 'type_id',
                'filter' => isset($searchModel) ? $searchModel->getDepartmentOptions() : null,
                'value' => function ($data) {
                return $data->getDepartment();
                }
            ],

            // [
            // 'attribute' => 'department_name',
            // 'format' => 'raw',
            // 'value' => function ($data) {
            // return ! empty($data->assignTo->department) ? $data->assignTo->department->title : '';
            // }

            // ],

            [
                'attribute' => 'assign_to_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('assign_to_id');
                }
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],
            [
                'attribute' => 'created_on',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'created_on',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ])
            ],

            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>',
                'template' => $actions
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

