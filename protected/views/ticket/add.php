<?php

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Tickets'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Add');
?>

<div class="wrapper">
	<div class="card">

		<div class="ticket-create">
	<?=  \app\components\PageHeader::widget(); ?>
</div>

	</div>

	<div class="content-section clearfix card">

		<?= $this->render ( '_form', [ 'model' => $model ] )?></div>
</div>


