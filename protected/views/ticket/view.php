<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Tickets'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="ticket-view card-body">
			<?=  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'ticket-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'description',
                'value' => strip_tags($model->description)
            ],
           
            [
                'attribute' => 'department_name',
                'format' => 'raw',
                'value' => ! empty($model->assignTo) ? !empty($model->assignTo->department)?$model->assignTo->department->title:\Yii::t('app',"Not Set"):\Yii::t('app',"Not Set")

            ],
            [
                'attribute' => 'assign_to_id',
                'format' => 'raw',
                'value' => ! empty($model->assignTo)?$model->assignTo->full_name:\Yii::t('app',"Not Set")
            ],
            'created_on:datetime',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>




		<?php
if (User::isAdmin() || User::isHR()) {
    echo UserAction::widget([
        'model' => $model,
        'attribute' => 'state_id',
        'states' => $model->getStateOptions()
]);
}
?>

		</div>
	</div>

</div>
