<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'ticket-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>


<div class="col-md-3">

		 <?php echo $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
	 	
		 <?php //echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions(), ['prompt' => '']) ?>
		 <div class="row">
			<div class="col-md-6">
<?php

echo $form->field($model, 'type_id')->widget(Select2::classname(), [
    'data' => $model->getDepartmentOptions(),
    'options' => [
        'placeholder' => 'Select',
        'multiple' => false,
        'id' => 'type_id'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
?>
</div>
			<div class="col-md-6">
	<?php
echo $form->field($model, 'assign_to_id')->widget(DepDrop::classname(), [
    'data' => $model->getAssignToOptions(),
    'options' => [
        'placeholder' => 'Select'
    ],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => [
        'pluginOptions' => [
            'allowClear' => true
        ]
    ],
    'pluginOptions' => [
        'depends' => [
            'type_id'
        ],
        'url' => Url::to([
            '/cases/assign'
        ])
    ]
]);
?>
</div>
		</div>
	 	
	 		</div>

	<div class="col-md-9">
	 		
	 		 <?php echo  $form->field($model, 'description')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'description')->textarea(['rows' => 6]); ?>
	 		 
	 		 </div>

	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'ticket-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>