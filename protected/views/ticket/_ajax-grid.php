<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Ticket $searchModel
 */

?>

<?php
if (! empty($menu))
    echo Html::a($menu['label'], $menu['url'], $menu['htmlOptions']);
?>



<?php Pjax::begin(['id'=>'ticket-pjax-ajax-grid','enablePushState'=>false,'enableReplaceState'=>false]); ?>
    <?php

    echo TGridView::widget([
        'id' => 'ticket-ajax-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [

            'id',
            'title',
            [
                'attribute' => 'assign_to_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('assign_to_id');
                }
            ],

            [
                'attribute' => 'state_id',
                'format' => 'html',
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>

