<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\LeaveRequest */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Leave Requests'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="leave-request-view card-body">
			<?=\app\components\PageHeader::widget(['model' => $model]);?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'leave-request-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            'start_date:html',
            'is_halfday:boolean',
            'created_on:datetime',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>


<?php

echo $model->description;
?>


		<?php
if (User::isAdmin() || User::isHR()) {

    echo UserAction::widget([
        'model' => $model,
        'attribute' => 'state_id',

        'states' => $model->getStateOptions()
    ]);
} else {
    echo UserAction::widget([
        'model' => $model,
        'attribute' => 'state_id',

        'states' => $model->getUserStateOptions()
    ]);
}
?>




		</div>
	</div>

<div class=" card ">
		<div class=" card-body ">

<?=CommentsWidget::widget(['model' => $model]);?>
			</div>
	</div>


</div>
