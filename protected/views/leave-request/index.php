<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LeaveRequest */
/* @var $dataProvider yii\data\ActiveDataProvider */
$params = Yii::$app->request->getQueryParam('type');
if (! empty($params)) {
    $title = 'Employee Leave Requests';
} else {
    $title = 'Leave Requests';
}
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', $title),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Index');
;
?>
<div class="wrapper">
	<div class="user-index">
		<div class=" card ">
			<div class="leave-request-index">
				<?=\app\components\PageHeader::widget(['title' => $title]);?>
			</div>
		</div>
		<div class="card card-margin">
			<header class="card-header head-border">   <?=strtoupper(Yii::$app->controller->action->id);?> </header>
			<div class="card-body">
				<div class="content-section clearfix">					
					<?php

    echo $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel
    ]);
    ?>
				</div>
			</div>
		</div>
	</div>
</div>