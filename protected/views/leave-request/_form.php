<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\LeaveRequest */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([
        'id' => 'leave-request-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>



        <div class="col-md-3">
   
		 <?php

echo $form->field($model, 'start_date')
    ->widget(DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',

    'options' => [
        'class' => 'form-control'
    ],
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'pickerIcon' => '<i class="fa fa-calendar kv-dp-icon"></i>',
    'pluginOptions' => [
        'minDate' => 0,
        'format' => 'yyyy-mm-dd',
        'multidate' => true,
        'multidateSeparator' => ' , ',
        'todayHighlight' => true,
        'startDate' => date("Y-m-d")
    ]
])
    ->label('Date')?>

	
		 <?php echo $form->field($model, 'is_halfday')->checkbox() ?>
			</div>

	<div class="col-md-9">

	
		 <?php echo  $form->field($model, 'description')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'description')->textarea(['rows' => 6]); ?>
	 		</div>






	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'leave-request-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>