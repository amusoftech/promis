<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
use app\models\LeaveRequest;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\LeaveRequest $searchModel
 */
?>
<?php
if (User::isAdmin() || User::isManager())
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/leave-request/mass'
        ]),
        'grid_id' => 'leave-request-grid',
        'pjax_grid_id' => 'leave-request-pjax-grid'
    ]);
?>
<div class='table table-responsive'>

<?php

Pjax::begin([
    'id' => 'leave-request-pjax-grid'
]);
?>
    <?php
    echo TGridView::widget([
        'id' => 'leave-request-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],

        'columns' => [

            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin() || User::isManager()
            ],

            'id',
            [
                'attribute' => 'start_date',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'options' => [
                        'id' => 'start_date',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return $data->start_date;
                },
                'label' => 'Date'
            ],
           /*  [
                'header' => "<a> Days </a>",
                'value' => function ($data) {
                    return $data->getDays();
                }
            ], */
            'is_halfday:boolean',
            
          /*   [
                'attribute' => 'created_on',
                'label' => 'day',
                'format' => 'raw',
                'value' => function ($data) {
                return date("l", strtotime($data->date));
                }
                ], */
            
            
            
            
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],
            [
                'attribute' => 'created_on',
                'format' => 'raw',
                'filter' => \yii\jui\DatePicker::widget([
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true
                    ],
                    'model' => $searchModel,
                    'attribute' => 'created_on',
                    'options' => [
                        'id' => 'created_on',
                        'class' => 'form-control'
                    ]
                ]),
                'value' => function ($data) {
                    return $data->created_on;
                }
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],
            [
                'class' => 'app\components\TActionColumn',
                'template' => '{view}{update}',
                'header' => '<a>Actions</a>', /* 'showModal' => \Yii::$app->params['useCrudModals'] = false */
                'visibleButtons' => [
                    'update' => function ($data) {
                        return (($data->created_by_id == Yii::$app->user->id) && ($data->state_id != LeaveRequest::STATE_APPROVED)) ? true : false;
                    },
                    'view' => true
                ]
            ]
        ]
    ]);
    ?>
<?php
Pjax::end();
?>
</div>

