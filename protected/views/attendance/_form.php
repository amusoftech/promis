<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use app\models\AttendanceEvent;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Attendance */
/* @var $form yii\widgets\ActiveForm */
?>



<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'attendance-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>

<div class="col-md-6">



 <?php
echo $form->field($model, 'date')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'maxDate' => date('Y-m-d'),
        'minDate' => date('Y-m-d', strtotime('-360 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>
<?php
echo $form->field($model, 'user_id')->widget(Select2::classname(), [
    'data' => $model->getUserOptions(),
    'options' => [
        'placeholder' => 'Select',
        'multiple' => false,
        'id' => 'user_id'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
?>
<?php
echo $form->field($model, 'in_time')->widget(TimePicker::classname(), [
    'pluginOptions' => [
        'defaultTime' => date('H:i'),
        'showMeridian' => false
    ]
]);
?>
 
 <?php

echo $form->field($model, 'out_time')->widget(TimePicker::classname(), [

    'pluginOptions' => [
        'defaultTime' => date('H:i'),
        'showMeridian' => false
    ]
]);
?>

<?php echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>




	
		

	
 
 
</div>



	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'attendance-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>