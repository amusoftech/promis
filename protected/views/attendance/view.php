<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\Attendance */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Attendances'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;


?>

<div class="wrapper">
	<div class=" card ">

		<div class="attendance-view card-body">
			<?=  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'attendance-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            'date:date',
            'in_time',
            'out_time',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('user_id')
            ],
            'total_time',
            'created_on:datetime',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>



		<?php

echo UserAction::widget([
    'model' => $model,
    'attribute' => 'state_id',
    'states' => $model->getStateOptions()
]);
?>



			</div>
			<?php if((User::isAdmin())||User::isHR()){?>
		<div class="card">
			<div class="card-body">
				<?php
    $this->context->startPanel();

    $this->context->addPanel('Attendance Events', 'attendanceEvent', 'AttendanceEvent', $model);


    $this->context->endPanel();
    ?>
			</div>
		</div>
		
		<?php  }?>
	
	</div>
</div>
