<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Attendance */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Attendances'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Index');
$title = "Attendance";
?>
<div class="wrapper">
	<div class="user-index">
		<div class=" card ">

			<div class="attendance-index">

<?=  \app\components\PageHeader::widget(['title'=>$title]); ?>


  </div>

		</div>
		<div class="card card-margin">
			<header class="card-header head-border">   <?= strtoupper(Yii::$app->controller->action->id); ?> </header>
			<div class="card-body">
				<div class="content-section clearfix">					
		<?php echo $this->render('_grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
</div>
			</div>
		</div>
	</div>

</div>

