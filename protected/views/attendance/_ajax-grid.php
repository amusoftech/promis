<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Attendance $searchModel
 */



?>




<?php Pjax::begin(['id'=>'attendance-pjax-ajax-grid','enablePushState'=>false,'enableReplaceState'=>false]); ?>
    <?php

    echo TGridView::widget([
        'id' => 'attendance-ajax-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [

            'id',
            'date:date',
            [
                'attribute' => 'day',
                'format' => 'raw',
                'value' => function ($data) {
                    return date("l", strtotime($data->date));
                }
            ],
            'in_time',
            'out_time',
            'total_time',
           
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ]

            // [
            // 'class' => 'app\components\TActionColumn',
            // 'header' => '<a>Actions</a>'
            // ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>

