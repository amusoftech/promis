<?php
use app\components\TGridView;
use yii\helpers\Url;
use app\components\MassAction;
use app\models\User;
use yii\widgets\Pjax;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\Attendance $searchModel
 */

?>
<?php 
$actions = '';
if((User::isAdmin())||(User::isHR()))
{
    $actions = '{view}{update}{delete}';
}
else {
    $actions = '{view}';
}
?>
<?php
if (User::isAdmin())
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/attendance/mass'
        ]),
        'grid_id' => 'attendance-grid',
        'pjax_grid_id' => 'attendance-pjax-grid'
    ]);
?>
<div class='table table-responsive'>

<?php Pjax::begin(['id'=>'attendance-pjax-grid']); ?>
    <?php

    echo TGridView::widget([
        'id' => 'attendance-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            [
                'name' => 'check',
                'class' => 'yii\grid\CheckboxColumn',
                'visible' => User::isAdmin()
            ],

            'id',
            
            'date:date',
            'in_time',
            'out_time',
            [
                'attribute' => 'created_on',
                'label' => 'day',
                'format' => 'raw',
                'value' => function ($data) {
                    return date("l", strtotime($data->date));
                }
            ],
            
         'total_time',
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
                'value' => function ($data) {
                    return $data->getStateBadge();
                }
            ],
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('user_id');
                }
            ],
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->getRelatedDataLink('created_by_id');
                }
            ],
            'created_on:datetime',
            [
                'class' => 'app\components\TActionColumn',
                'header' => '<a>Actions</a>',
                'template'=> $actions
            ]
        ]
    ]);
    ?>
<?php Pjax::end(); ?>
</div>

