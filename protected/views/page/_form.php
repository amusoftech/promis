<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
    <?php echo strtoupper(Yii::$app->controller->action->id); ?>
 </header>
<div class="card-body">

    <?php
    $form = TActiveForm::begin([
        'id' => 'page-form',
        'options' => [
            'class' => 'row'
        ]
    ]);
    ?>
<div class="col-md-4">
<?php echo $form->field($model, 'title')->textInput()?>
</div>
	<div class="col-md-4">
    <?php echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions())?>
</div>
	<div class="col-md-4">
    <?php echo $form->field($model, 'type_id')->dropDownList($model->getTypeOptions())?>
</div>
	<div class="col-md-12">
     <?php

    echo $form->field($model, 'description')->widget(app\components\TRichTextEditor::className(), [
        'options' => [
            'rows' => 6
        ],
        'preset' => 'full'
    ]);
    ?>

</div>


	<div class="form-group col-md-12 text-right">
	
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['name' => 'page-button','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>



    <?php

    TActiveForm::end();
    ?>

</div>
