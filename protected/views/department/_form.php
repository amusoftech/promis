<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Department */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'department-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>


<div class="row">


		<div class="col-md-12 offset-md-8">

	
		 <?php echo $form->field($model, 'title')->textInput(['maxlength' => 32]) ?>
		 
		 <?php echo $form->field($model, 'department_code')->textInput(['maxlength' => 32]) ?>
		  <?php //echo $form->field($model, 'manager_id')->dropDownList($model->getManagerOptions(), ['prompt' => '']) ?>
	 		</div>



	</div>



	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'department-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>