<?php
use app\components\useraction\UserAction;
/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Departments'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="department-view card-body">
			<?=  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'department-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
          'title',
            'department_code',
            /* [
                'attribute' => 'manager_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('manager_id')
            ], */
            'created_on:datetime',
         
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return ! empty($model->createdBy) ? $model->createdBy->full_name : Yii::t('app', "not set");
                }
            ]
        ]
    ])?>


<?php  ?>


		<?php

echo UserAction::widget([
    'model' => $model,
    'attribute' => 'state_id',
    'states' => $model->getStateOptions()
]);
?>

		</div>
	</div>



	<div class=" card ">
		<div class=" card-body ">
			<div class="department-panel">

<?php
/*
 * $this->context->startPanel();
 * $this->context->addPanel('Cases', 'cases', 'Cases', $model);
 * $this->context->addPanel('Payrolls', 'payrolls', 'Payroll', $model );
 * $this->context->addPanel('Reminders', 'reminders', 'Reminder', $model);
 * $this->context->addPanel('Tasks', 'tasks', 'Task', $model );
 *
 * $this->context->endPanel();
 */
?>
				</div>
		</div>
	</div>
</div>
