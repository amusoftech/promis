<?php
use app\modules\comment\widgets\CommentsWidget;
use yii\helpers\Html;
use app\components\TActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Departments'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" panel ">
		<div class="text-center">
			<h2>Are you sure you want to delete this item? All related data is
				deleted</h2>
		</div>
		<div class="department-view panel-body">
			<?php echo  \app\components\PageHeader::widget(['model'=>$model]); ?>



		</div>
	</div>
	<div class=" panel ">
		<div class=" panel-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'department-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            [
                'attribute' => 'type_id',
                'value' => $model->getType()
            ],
            [
                'attribute' => 'manager_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('manager_id')
            ],
            'created_on:datetime',
            'created_by_id'
        ]
    ])?>


<?php  ?>



<?php
$form = TActiveForm::begin([

    'id' => 'department-form'
]);

echo $form->errorSummary($model);
?>

	 <div class="form-group">
				<div
					class="col-md-6 col-md-offset-3 bottom-admin-button btn-space-bottom text-right">
			
        <?= Html::submitButton('Confirm', ['id'=> 'department-form-submit','class' =>'btn btn-success']) ?>
    </div>
			</div>

    <?php TActiveForm::end(); ?>

		</div>
	</div>



	<div class=" panel ">
		<div class=" panel-body ">
			<div class="department-panel">

<?php
$this->context->startPanel();
$this->context->addPanel('Cases', 'cases', 'Case', $model /* ,null,true */);
$this->context->addPanel('Payrolls', 'payrolls', 'Payroll', $model /* ,null,true */);
$this->context->addPanel('Reminders', 'reminders', 'Reminder', $model /* ,null,true */);

$this->context->endPanel();
?>
				</div>
		</div>
	</div>

</div>
