<?php
use app\components\useraction\UserAction;
use app\modules\comment\widgets\CommentsWidget;
use app\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\ClientReports */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Client Reports'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = (string) $model;
?>

<div class="wrapper">
	<div class=" card ">

		<div class="client-reports-view card-body">
			<?=\app\components\PageHeader::widget(['model' => $model]);?>



		</div>
	</div>

	<div class=" card ">
		<div class=" card-body ">
    <?php

    echo \app\components\TDetailView::widget([
        'id' => 'client-reports-detail-view',
        'model' => $model,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            'id',
            'report:html',

            [
                'attribute' => 'case_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return ! empty($model->cases) ? $model->cases->title : Yii::t('app', "Not Set");
                }
            ],
           /*  [
                'attribute' => 'sent_by',
                'format' => 'raw',
                'value' => function ($model) {
                    return ! empty($model->reportSender) ? $model->reportSender->name : Yii::t('app', "Not Set");
                }
            ], */
            
            [
                'attribute' => 'sent_by',
                'format' => 'raw',
                'value' => $model->getSentbyBadge()
            ],

            'date:date',
            [
                'attribute' => 'state_id',
                'format' => 'raw',
                'value' => $model->getStateBadge()
            ],

            'created_on:datetime',
            [
                'attribute' => 'created_by_id',
                'format' => 'raw',
                'value' => $model->getRelatedDataLink('created_by_id')
            ]
        ]
    ])?>


<?php

?>


		<?php
if (! User::isQualityControl()) {
    echo UserAction::widget([
        'model' => $model,
        'attribute' => 'state_id',
        'states' => $model->getStateOptions()
    ]);
}
?>

		</div>
	</div>



	<div class="panel">
		<div class="panel-body">
<?php

/*
 * echo $model->displayFileImages($model, [
 * 'class' => 'img-thumbnail',
 * 'width' => 200,
 * 'height' => 200
 * ]);
 */
?>
</div>
	</div>
	
	
	
	<?php

if (! empty($model->file)) {
    ?>
<div class=" card ">
		<div class=" card-body ">
			<div class="col-md-2 pr0 user-img">
				<div class="profileimage">
    				<?=$model->displayImage($model->file->filename_path, ['class' => 'profile-pic'], 'default.jpg', true);?>
  
    			</div>
			</div>
		</div>
	</div>
<?php
}
?>
	
	
	
	
	
	


	<div class=" card ">
		<div class=" card-body ">
			<div class="client-reports-panel">

<?php
$this->context->startPanel();
$this->context->addPanel('Feeds', 'feeds', 'Feed', $model /* ,null,true */);

$this->context->endPanel();
?>
				</div>
		</div>
	</div>

</div>
