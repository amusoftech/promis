<?php
use app\components\TGridView;
use yii\helpers\Html;
use yii\helpers\Url;

use app\models\User;

use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\MassAction;
use kartik\export\ExportMenu;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\ClientReports $searchModel
 */

?>
<?php
if (User::isAdmin())
    echo MassAction::widget([
        'url' => Url::toRoute([
            '/client-reports/mass'
        ]),
        'grid_id' => 'client-reports-grid-view',
        'pjax_grid_id' => 'client-reports-pjax-grid'
    ]);
?>



<div class='table table-responsive'>


<?php

Pjax::begin([
    'id' => 'client-reports-pjax-grid'
]);
?>
    <?php
    if (User::isQualityControl()) {
        $temp = '{view}';
    } else {
        $temp = '{view}{delete}{update}';
    }
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn','header'=>'<a>S.No.<a/>'],
        [
            'name' => 'check',
            'class' => 'yii\grid\CheckboxColumn',
            'visible' => User::isAdmin()
        ],

        'id',
    /* 'report:html',*/
    
    'date:date',
        [
            'attribute' => 'state_id',
            'format' => 'raw',
            'filter' => isset($searchModel) ? $searchModel->getStateOptions() : null,
            'value' => function ($data) {
                return $data->getStateBadge();
            }
        ],
        [
            'attribute' => 'case_id',
            'format' => 'raw',
            'value' => function ($data) {
                return ! empty($data->cases) ? $data->cases->title : Yii::t('app', "Not Set");
            }
        ],
            /* [
             'attribute' => 'sent_by',
             'format' => 'raw',
             'value' => function ($data) {
             return ! empty($data->reportSender) ? $data->reportSender->name : Yii::t('app', "Not Set");
             }
             ], */
            
            [
            'attribute' => 'sent_by',
            'format' => 'raw',
            'filter' => isset($searchModel) ? $searchModel->getClientReportSentByOptions() : null,
            'value' => function ($data) {
                return $data->getSentbyBadge();
            }
        ],

        [
            'attribute' => 'created_by_id',
            'format' => 'raw',
            'value' => function ($data) {
                return $data->getRelatedDataLink('created_by_id');
            }
        ],

        [
            'class' => 'app\components\TActionColumn',
            'header' => '<a>Actions</a>',
            'template' => $temp
        ]
    ];

    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]);

    echo TGridView::widget([
        'id' => 'client-reports-grid-view',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => $gridColumns
    ]);
    ?>
<?php

Pjax::end();
?>
</div>


