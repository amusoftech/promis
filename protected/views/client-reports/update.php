<?php

/* @var $this yii\web\View */
/* @var $model app\models\ClientReports */
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Client Reports'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = [
    'label' => $model->id,
    'url' => [
        'view',
        'id' => $model->id
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="wrapper">
	<div class=" card ">
		<div class="client-reports-update">
	<?=  \app\components\PageHeader::widget(['model' => $model]); ?>
	</div>
	</div>


	<div class="content-section clearfix card">
		<?= $this->render ( '_form', [ 'model' => $model,'fileModel' => $fileModel ] )?></div>
</div>

