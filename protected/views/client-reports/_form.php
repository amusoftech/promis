<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use kartik\file\FileInput;
use yii\helpers\Url;
use app\modules\file\models\File;

/* @var $this yii\web\View */
/* @var $model app\models\ClientReports */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">

    <?php
    $form = TActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'client-reports-form'
    ]);

    ?>



<div class="col-md-12">

	
		 <?php echo $form->field($model, 'case_id')->dropDownList($model->getCaseOptions(), ['prompt' => ''])  ?>
	 		


		 <?php echo  $form->field($model, 'report')->widget ( app\components\TRichTextEditor::className (), [ 'options' => [ 'rows' => 6 ],'preset' => 'basic' ] ); //$form->field($model, 'report')->textarea(['rows' => 6]); ?>
	 		
           <?php //echo $form->field($model, 'sent_by')->dropDownList($model->getReportSenderOptions(), ['prompt' => ''])  ?>
           
      
                        <?php echo $form->field($model, 'sent_by')->dropDownList($model->getClientReportSentByOptions(), ['prompt' => ''])  ?>

		 <?php //echo $form->field($model, 'sent_by')->textInput()  ?>
	 		


		 <?php

echo $form->field($model, 'date')->widget(yii\jui\DatePicker::class, [
    // 'dateFormat' => 'php:Y-m-d',
    'options' => [
        'class' => 'form-control'
    ],
    'clientOptions' => [
        'minDate' => date('Y-m-d'),
        'maxDate' => date('Y-m-d', strtotime('+360 days')),
        'changeMonth' => true,
        'changeYear' => true
    ]
])?>



    
    <?php
    /* @var $fileModel File*/
    echo FileInput::widget([
        'model' => $fileModel,
        'attribute' => 'title',
        'name' => 'title',
        'options' => [
            // 'accept' => 'image/*',
            'multiple' => false
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'allowedFileExtensions' => [
                'jpg',
                'jpeg',
                'png',
                'docx',
                'odt'
            ],
            'overwriteInitial' => false,
            'required' => $model->isNewRecord == true,
            'maxFileSize' => 2000
        ]
    ]);

    ?>
    
    
   
    
    


	<?php if(User::isAdmin()){?>	 <?php echo $form->field($model, 'state_id')->dropDownList($model->getStateOptions(), ['prompt' => '']) ?>
	 <?php }?>		


	 		


	   <div class="form-group">
			<div
				class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'client-reports-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
		</div>

    <?php TActiveForm::end(); ?>

</div>
</div>
