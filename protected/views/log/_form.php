<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Log */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">

    <?php
    $form = TActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'log-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    echo $form->errorSummary($model);
    ?>




	</div>
<div class="col-md-6">

		
		 <?php echo $form->field($model, 'error')->textInput(['maxlength' => 256]) ?>
	
		 <?php echo $form->field($model, 'link')->textInput(['maxlength' => 255]) ?>
	 		


	   <div class="form-group">
		<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'log-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>

    <?php TActiveForm::end(); ?>

</div>
