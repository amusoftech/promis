<?php
use yii\helpers\Html;
use app\components\TActiveForm;
use app\models\User;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Payroll */
/* @var $form yii\widgets\ActiveForm */
?>
<header class="card-header">
                            <?= strtoupper(Yii::$app->controller->action->id); ?>
                        </header>
<div class="card-body">


    <?php
    $form = TActiveForm::begin([

        'id' => 'payroll-form',
        'options' => [
            'class' => 'row'
        ]
    ]);

    ?>


	<div class="col-md-6">
	
	
		<?php

echo $form->field($model, 'department_id')->widget(Select2::classname(), [
    'data' => $model->getDepartmentOptions(),
    'options' => [
        'placeholder' => 'Select',
        'multiple' => false,
        'id' => 'department_id'
    ],
    'pluginOptions' => [
        'allowClear' => true
    ]
]);
?>


		 
		 		<?php
    echo $form->field($model, 'user_id')->widget(DepDrop::classname(), [
        'data' => $model->getUserOptions(),
        'options' => [
            'placeholder' => 'Select'
        ],
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options' => [
            'pluginOptions' => [
                'allowClear' => true
            ]
        ],
        'pluginOptions' => [
            'depends' => [
                'department_id'
            ],
            'url' => Url::to([
                '/payroll/user'
            ])
        ]
    ]);
    ?>
		 
	<?php echo $form->field($model, 'month')->dropDownList($model->getMonthOptions());?>	

   </div>

	<div class="col-md-6">
   
		  <?php echo $form->field($model, 'day')->textInput() ?>
		  
		   <?php echo $form->field($model, 'salary')->textInput() ?>

  	
</div>
	<div class="col-md-12 bottom-admin-button btn-space-bottom text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['id'=> 'payroll-form-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php TActiveForm::end(); ?>
</div>