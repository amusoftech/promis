<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Payroll */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Payroll'),
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Index');
$title = "Payroll";
?>
<div class="wrapper">
	<div class="user-index">
		<div class=" card ">

			<div class="payroll-index">

				<?=  \app\components\PageHeader::widget(['title'=>$title]); ?>


			</div>

		</div>
		<div class="card card-margin">
			<header class="card-header head-border">   <?= strtoupper(Yii::$app->controller->action->id); ?> </header>
			<div class="card-body">
				<div class="content-section clearfix">					
					<?php echo $this->render('_grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
				</div>
			</div>
		</div>
	</div>

</div>

