<?php

/**
 *@copyright : ToXSL Techno0logies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\controllers;

use app\components\TActiveForm;
use app\components\TController;
use app\models\LoginForm;
use app\models\User;
use app\models\search\User as UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\Json;
use app\models\Designation;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'add-employee',
                            'update-employee',
                            'delete-employee',
                            'view-employee',
                            'changepassword',
                            'index',
                            'view',
                            'designation',
                            'ajax'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isHR();
                        }
                    ],
                    [
                        'actions' => [
                            'index',
                            'view',
                            'ajax'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isManager();
                        }
                    ],
                    [
                        'actions' => [
                            'index',
                            'delete',
                            'add',
                            'view',
                            'update',
                            'delete',
                            'logout',
                            'changepassword',
                            'resetpassword',
                            'dashboard',
                            'profile-image',
                            'toggle',
                            'clear',
                            'recover',
                            'add-admin',
                            'mass',
                            'update-employee',
                            'index'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isAdmin();
                        }
                    ],

                    [
                        'actions' => [
                            'login',
                            'resetpassword',
                            'recover',
                            'add-admin',
                            'confirm-email',
                            'profile-image'
                            // 'update'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '*'
                        ]
                    ],
                    [
                        'actions' => [
                            'logout',
                            'update',
                            'view',
                            'changepassword',
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * actionMass delete in mass as items are checked
     *
     * @param string $action
     * @return string
     */
    public function actionMass($action = 'delete')
    {
        \Yii::$app->response->format = 'json';
        $response['status'] = 'NOK';
        $status = User::massDelete();
        if ($status == true) {
            $response['status'] = 'OK';
        }
        return $response;
    }

    public function actionDesignation()
    {
        $out = [];
        $post = \Yii::$app->request->post();
        if (isset($post['depdrop_parents'])) {

            $id = end($post['depdrop_parents']);

            $list = Designation::find();
            $selected = null;
            if ($id != null && $list->count() > 0) {
                $selected = '';
                foreach ($list->each() as $i => $account) {
                    $out[] = [
                        'id' => $account['id'],
                        'name' => $account['title']
                    ];
                    if ($i == 0) {
                        $selected = $account['id'];
                    }
                }
                // Shows how you can preselect a value
                \Yii::$app->response->data = Json::encode([
                    'output' => $out,
                    'selected' => $selected
                ]);
                return;
            }
        }
        \Yii::$app->response->data = Json::encode([
            'output' => '',
            'selected' => ''
        ]);
    }

    public function actionClear()
    {
        $runtime = Yii::getAlias('@runtime');
        $this->cleanRuntimeDir($runtime);
        $this->cleanAssetsDir();
        \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Cleared"));
        return $this->goBack();
    }

    public function actionDeleteEmployee($id)
    {
        $model = User::findOne($id);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('delete-employee-modal', [
                'model' => $model
            ]);
        }

        if ($model->delete()) {

            \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Employee has been deleted successfully'));
        } else {
            \Yii::$app->getSession()->setFlash('error', $model->getErrorsString());
        }

        return $this->redirect([
            '/dashboard/hr-index'
        ]);
    }

    public function actionViewEmployee($id)
    {
        $model = User::findOne($id);

        $this->updateMenuItems($model);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('employee-view', [
                'model' => $model
            ]);
        }
        return $this->render('employee-view', [
            'model' => $model
        ]);
    }

    public function actionIndex($id = null)
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $this->updateMenuItems();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        $this->updateMenuItems($model);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $model
            ]);
        }
        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionAddAdmin()
    {
        $this->layout = User::LAYOUT_GUEST_MAIN;
        $count = User::find()->count();
        if ($count != 0) {
            return $this->redirect([
                '/'
            ]);
        }
        $model = new User();
        $model->scenario = User::SCENARIO_ADD_ADMIN;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->role_id = User::ROLE_ADMIN;
            $model->state_id = User::STATE_ACTIVE;
            if ($model->validate()) {
                $model->setPassword($model->password);
                $model->generatePasswordResetToken();
                if ($model->save()) {
                    \Yii::$app->user->login($model);
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Welcome $model->full_name"));
                    return $this->goBack([
                        'dashboard/index'
                    ]);
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
                }
            } else {
                \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
            }
        }
        return $this->render('add-admin', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $this->layout = User::LAYOUT_MAIN;
        $post = Yii::$app->request->post();
        $model = new User();
        $existManager = false;
        // $model->role_id = isset($post['User']['role_id']) ? $post['User']['role_id'] : User::ROLE_USER;

        $model->state_id = User::STATE_INACTIVE;
        $model->scenario = User::SCENARIO_SIGNUP;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post)) {

            $existManager = User::find()->where([
                'department_id' => $model->department_id,
                'role_id' => User::ROLE_MANAGER
            ])->one();

            if (! empty($existManager)) {
                \Yii::$app->getSession()->setFlash('error', "Error !!  this department has already assigned a manager");
            } else {
                $model->saveUploadedFile($model, 'profile_file');
                if ($model->validate()) {

                    $model->scenario = User::SCENARIO_ADD;
                    $model->generatePasswordResetToken();
                    // $model->sendRegistrationMailtoUser($model);
                    $model->otp_verified = User::OTP_VERIFIED_NO;
                    $model->otp = '1234';
                    $model->setPassword($model->password);
                    if ($model->save()) {
                        \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Record has been Added Successfully.'));
                        return $this->redirect([
                            'view',
                            'id' => $model->id
                        ]);
                    } else {
                        \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
                    }
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
                }
            }
        }
        $this->updateMenuItems($model);
        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAddEmployee()
    {
        $this->layout = User::LAYOUT_MAIN;
        $post = Yii::$app->request->post();
        $model = new User();
        $model->scenario = User::SCENARIO_ADD_EMPLOYEE;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post)) {
            if ($model->validate()) {
                $model->scenario = User::SCENARIO_ADD;
                $model->role_id = User::ROLE_EMPLOYEE;
                $model->full_name = $model->first_name . ' ' . $model->last_name;
                $model->generatePasswordResetToken();
                $model->sendRegistrationMailtoUser($model);
                $model->state_id = User::STATE_ACTIVE;
                $model->setPassword($model->password);
                if ($model->save()) {
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('app', 'Employee Added Successfully.'));
                    return $this->redirect([
                        'dashboard/hr-index'
                    ]);
                } else {
                    \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
                }
            } else {
                \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
            }
        }
        $this->updateMenuItems($model);
        return $this->render('add-employee', [
            'model' => $model
        ]);
    }

    public function actionRecover()
    {
        $this->layout = User::LAYOUT_GUEST_MAIN;
        $model = new User();
        $model->scenario = User::SCENARIO_TOKEN_REQUEST;

        $post = \Yii::$app->request->post();
        if (isset($post['User'])) {
            $email = trim($post['User']['email']);
            if ($email != null) {

                $user = User::findOne([
                    'email' => $email
                ]);
                if ($user) {
                    $user->generatePasswordResetToken();
                    if (! $user->save(false, [
                        'activation_key'
                    ])) {
                        throw new \yii\base\Exception(\Yii::t('app', "Cant Generate Authentication Key"));
                    }
                    $user->sendEmail();
                    \Yii::$app->session->setFlash('success', \Yii::t('app', 'Please check your email to reset your password.'));
                    return $this->redirect([
                        '/user/login'
                    ]);
                } else {
                    \Yii::$app->session->setFlash('error', \Yii::t('app', 'Email is not registered.'));
                }
            } else {
                $model->addError('error', \Yii::t('app', 'Email cannot be blank'));
            }
        }
        $this->updateMenuItems($model);
        return $this->render('requestPasswordResetToken', [
            'model' => $model
        ]);
    }

    public function actionResetpassword($token)
    {
        $this->layout = User::LAYOUT_GUEST_MAIN;
        $model = User::findByPasswordResetToken($token);
        if (! ($model)) {
            \Yii::$app->session->setFlash('error', \Yii::t('app', 'This URL is expired.'));
        }
        $newModel = new User([
            'scenario' => User::SCENARIO_RESETPASSWORD
        ]);
        if ($newModel->load(Yii::$app->request->post()) && $newModel->validate() && $model) {

            $model->setPassword($newModel->password);
            $model->removePasswordResetToken();
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'New password is saved successfully.'));
            } else {
                \Yii::$app->session->setFlash('error', \Yii::t('app', 'Error while saving new password.'));
            }
        }
        $this->updateMenuItems($model);
        return $this->render('resetpassword', [
            'model' => $newModel
        ]);
    }

    public function actionUpdateEmployee($id)
    {
        $this->layout = User::LAYOUT_MAIN;
        $model = User::findOne($id);
        $model->scenario = User::SCENARIO_UPDATE_EMPLOYEE;
        $post = \yii::$app->request->post();
        $old_image = $model->profile_file;
        if (Yii::$app->request->isAjax && $model->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }

        if ($model->load($post)) {
            $model->full_name = $model->first_name . ' ' . $model->last_name;

            if ($_FILES) {
                if (! $model->saveUploadedFile($model, 'profile_file', $old_image)) {
                    $model->profile_file = $old_image;
                }
            }
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'User Updated successfully.'));
                return $this->redirect([
                    '/user/view-employee',
                    'id' => $id
                ]);
            } else {
                \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
            }
        }
        $this->updateMenuItems($model);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('add-employee', [
                'model' => $model
            ]);
        }
        return $this->render('add-employee', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = User::LAYOUT_MAIN;
        $model = $this->findModel($id);
        $model->scenario = User::SCENARIO_UPDATE;
        $post = \yii::$app->request->post();
        $old_image = $model->profile_file;
        $password = $model->password;

        if (Yii::$app->request->isAjax && $model->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }

        if ($model->load($post)) {
            if (! empty($post['User']['password'])) {
                $model->setPassword($post['User']['password']);
            } else {
                $model->password = $password;
            }

            if ($_FILES) {
                if (! $model->saveUploadedFile($model, 'profile_file', $old_image)) {
                    $model->profile_file = $old_image;
                }
            }

            if ($model->save()) {
                \Yii::$app->session->setFlash('success', \Yii::t('app', 'User Updated successfully.'));
                return $this->redirect($model->getUrl());
            } else {
                \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
            }
        }
        $model->password = '';
        $this->updateMenuItems($model);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model
            ]);
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->updateMenuItems($model);

        if (\Yii::$app->user->id == $model->id || $model->role_id == User::ROLE_ADMIN) {
            \Yii::$app->session->setFlash('warrning', 'You are not allowed to perform this operation.');
            return $this->redirect(\Yii::$app->request->referrer);
        }
        $model->delete();
        if (\Yii::$app->request->isAjax) {
            return true;
        }
        \Yii::$app->session->setFlash('success', \Yii::t('app', 'User Deleted successfully.'));
        return $this->redirect([
            '/'
        ]);
    }

    public function actionConfirmEmail($id)
    {
        $user = User::find()->where([
            'activation_key' => $id
        ])->one();
        if (! empty($user)) {
            $user->email_verified = User::EMAIL_VERIFIED;
            if ($user->save()) {
                if (Yii::$app->user->login($user, 3600 * 24 * 30)) {
                    \Yii::$app->getSession()->setFlash('success', 'Congratulations! your account is verified');
                    return $this->redirect([
                        '/dashboard'
                    ]);
                }
            }
        } else {
            \Yii::$app->getSession()->setFlash('error', 'Token is Expired Please Resend Again');
            return $this->redirect([
                'dashboard'
            ]);
        }
    }

    public function actionLogin()
    {
        $this->layout = User::LAYOUT_GUEST_MAIN;
        if (! \Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        $model->scenario = 'website-login';

        if ($model->load(Yii::$app->request->post())) {

            if ($model->login()) {
                if (User::isHR()) {
                    return $this->goBack([
                        'dashboard/hr-index'
                    ]);
                } else if (User::isClient()) {
                    return $this->redirect([
                        '/cases/index'
                    ]);
                } else if (User::isManager()) {
                    return $this->goBack([
                        'dashboard/manager-index'
                    ]);
                } else if (User::isDistributor()) {
                    return $this->goBack([
                        '/client-report/index'
                    ]);
                } else if (User::isQualityControl()) {
                    return $this->goBack([
                        '/cases/index'
                    ]);
                } else if (User::isInvestigator()) {
                    return $this->goBack([
                        '/cases/index'
                    ]);
                } else if (User::isDistributor()) {
                    return $this->goBack([
                        'client-reports/index'
                    ]);
                }
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $model->getErrorsString()));
            }
            return $this->goBack([
                'dashboard/index'
            ]);
        }
        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionChangepassword($id)
    {
        $this->layout = User::LAYOUT_MAIN;
        $model = $this->findModel($id);
        if (! ($model->isAllowed()))
            throw new \yii\web\HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

        $newModel = new User([
            'scenario' => User::SCENARIO_CHANGEPASSWORD
        ]);
        if (Yii::$app->request->isAjax && $newModel->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return TActiveForm::validate($newModel);
        }
        if ($newModel->load(Yii::$app->request->post()) && $newModel->validate()) {
            $model->setPassword($newModel->newPassword);
            if ($model->save(false, [
                'password'
            ])) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Password Changed successfully'));
                return $this->redirect([
                    'dashboard/index'
                ]);
            } else {
                \Yii::$app->getSession()->setFlash('error', "Error !!" . $model->getErrorsString());
            }
        }
        $this->updateMenuItems($model);
        return $this->render('changepassword', [
            'model' => $newModel
        ]);
    }

    public function actionDashboard()
    {
        return $this->redirect([
            'dashboard/index'
        ]);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {

            if (! ($model->isAllowed()))
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateMenuItems($model = null)
    {
        $role_id = '';
        $request = Yii::$app->request->get();
        $role_id = isset($request['id']) ? $request['id'] : '';
        switch (\Yii::$app->controller->action->id) {

            case 'add':
                {
                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],
                        'visible' => User::isAdmin()
                    ];
                }
                break;
            case 'index':
                {
                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'Add'),
                        'url' => [
                            'add',
                            'id' => $role_id
                        ],
                        'visible' => User::isAdmin()
                    ];
                }

                break;
            case 'update':
                {
                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'add'),
                        'url' => [
                            'add'
                        ],
                        'visible' => User::isAdmin()
                    ];
                }
                break;
            case 'view':
                {
                    if ($model != null && ($model->role_id != User::ROLE_ADMIN))
                    /*     $this->menu['shadow'] = [
                            'label' => '<span class="glyphicon glyphicon-refresh ">Shadow</span>',
                            'title' => Yii::t('app', 'Login as ' . $model),
                            'url' => [
                                '/shadow/session/login',
                                'id' => $model->id
                            ],

                            'visible' => User::isAdmin()
                        ]; */

                    if ($model->role_id != User::ROLE_ADMIN) {

                        if ($model != null)
                            $this->menu['delete'] = [
                                'label' => '<span class="glyphicon glyphicon-trash"></span>',
                                'title' => Yii::t('app', 'Delete'),
                                'url' => $model->getUrl('delete'),
                                'htmlOptions' => [
                                    'data-method' => 'post'
                                ],
                                'visible' => User::isAdmin()
                            ];
                    }

                    if ($model != null)
                        $this->menu['changepassword'] = [
                            'label' => '<span class="glyphicon glyphicon-paste"></span>',
                            'title' => Yii::t('app', 'changepassword'),
                            'url' => $model->getUrl('changepassword'),

                            //'visible' => User::isAdmin() ||
                        ];
                    if ($model != null)
                        $this->menu['update'] = [
                            'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                            'title' => Yii::t('app', 'Update'),
                            'url' => $model->getUrl('update'),

                            'visible' => User::isAdmin()
                        ];
                }
                break;
        }
    }
}
