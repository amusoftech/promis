<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\controllers;

use Yii;
use app\models\Analytics;
use app\models\search\Analytics as AnalyticsSearch;
use app\components\TController;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\User;
use yii\web\HttpException;
use app\components\TActiveForm;
use app\models\Cases;
use app\models\Department;
use yii\helpers\Json;

/**
 * AnalyticsController implements the CRUD actions for Analytics model.
 */
class AnalyticsController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'clear',
                            'delete',
                            'cases-department',
                            'cases-manager',
                            'cases-client',
                            'department-user'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isAdmin();
                        }
                    ],
                    [
                        'actions' => [
                            'index',
                            'add',
                            'view',
                            'update',
                            'clone',
                            'ajax',
                            'mass'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ],
                    [
                        'actions' => [

                            'view'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '*'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Analytics models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AnalyticsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Analytics model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $this->updateMenuItems($model);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $model
            ]);
        }
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Analytics model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd(/* $id*/)
    {
        $model = new Analytics();
        $model->loadDefaultValues();
        $model->state_id = Analytics::STATE_ACTIVE;

        /*
         * if (is_numeric($id)) {
         * $post = Post::findOne($id);
         * if ($post == null)
         * {
         * throw new NotFoundHttpException('The requested post does not exist.');
         * }
         * $model->id = $id;
         *
         * }
         */

        $model->checkRelatedData([
            'case_id' => Cases::class,
            'client_id' => User::class,
            'created_by_id' => User::class,
            'department_id' => Department::class,
            'manager_id' => User::class,
            'user_id' => User::class
        ]);
        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been added Successfully."));
            return $this->redirect($model->getUrl());
        }
        $this->updateMenuItems();
        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing Analytics model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been updated Successfully."));
            return $this->redirect($model->getUrl());
        }
        $this->updateMenuItems($model);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model
            ]);
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Clone an existing Analytics model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionClone($id)
    {
        $old = $this->findModel($id);

        $model = new Analytics();
        $model->loadDefaultValues();
        $model->state_id = Analytics::STATE_ACTIVE;

        $model->name = $old->name;
        $model->case_id = $old->case_id;
        $model->department_id = $old->department_id;
        // $model->user_id = $old->user_id $model->user_id = $old->user_id;
        $model->manager_id = $old->manager_id;
        $model->client_id = $old->client_id;
        $model->date = $old->date;
        $model->billing_item = $old->billing_item;
        $model->cost = $old->cost;
        $model->value = $old->value;
        // $model->created_by_id = $old->created_by_id $model->created_by_id = $old->created_by_id;
        // $model->state_id = $old->state_id $model->state_id = $old->state_id;
        $model->type_id = $old->type_id;
        // $model->created_on = $old->created_on $model->created_on = $old->created_on;
        $model->updated_on = $old->updated_on;

        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post) && $model->save()) {
            return $this->redirect($model->getUrl());
        }
        $this->updateMenuItems($model);
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Analytics model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (\yii::$app->request->post()) {
            $model->delete();
            \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been deleted Successfully."));
            return $this->redirect([
                'index'
            ]);
        }
        return $this->render('delete', [
            'model' => $model
        ]);
    }

    /**
     * Truncate an existing Analytics model.
     * If truncate is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionClear($truncate = true)
    {
        $query = Analytics::find();
        foreach ($query->each() as $model) {
            $model->delete();
        }
        if ($truncate) {
            Analytics::truncate();
        }
        \Yii::$app->session->setFlash('success', 'Analytics Cleared !!!');
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Finds the Analytics model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Analytics the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $accessCheck = true)
    {
        if (($model = Analytics::findOne($id)) !== null) {

            if ($accessCheck && ! ($model->isAllowed()))
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCasesManager()
    {
        $out = [];
        $post = \Yii::$app->request->post();
        if (isset($post['depdrop_parents'])) {

            $id = end($post['depdrop_parents']);

            $list = Cases::find()->where([
                'id' => $id
            ]);
            $selected = null;
            if ($id != null && $list->count() > 0) {
                $selected = '';
                foreach ($list->each() as $i => $account) {

                    $departmentName = $account->assignTo->full_name;
                    $departmentId = $account->assignTo->id;

                    $out[] = [
                        'id' => $departmentId,
                        'name' => $departmentName,
                        'department_id' => $departmentId
                    ];
                    if ($i == 0) {
                        $selected = $account['id'];
                    }
                }
                // Shows how you can preselect a value
                \Yii::$app->response->data = Json::encode([
                    'output' => $out,
                    'selected' => $selected
                ]);
                return;
            }
        }
        \Yii::$app->response->data = Json::encode([
            'output' => '',
            'selected' => ''
        ]);
    }

    public function actionCasesClient()
    {
        $out = [];
        $post = \Yii::$app->request->post();
        if (isset($post['depdrop_parents'])) {

            $id = end($post['depdrop_parents']);

            $list = Cases::find()->where([
                'id' => $id
            ]);
            $selected = null;
            if ($id != null && $list->count() > 0) {
                $selected = '';
                foreach ($list->each() as $i => $account) {

                    $ClientName = $account->client->full_name;
                    $Clientid = $account->client->id;

                    $out[] = [
                        'id' => $Clientid,
                        'name' => $ClientName,
                        'client_id' => $Clientid
                    ];
                    if ($i == 0) {
                        $selected = $account['id'];
                    }
                }
                // Shows how you can preselect a value
                \Yii::$app->response->data = Json::encode([
                    'output' => $out,
                    'selected' => $selected
                ]);
                return;
            }
        }
        \Yii::$app->response->data = Json::encode([
            'output' => '',
            'selected' => ''
        ]);
    }

    public function actionCasesDepartment()
    {
        $out = [];
        $post = \Yii::$app->request->post();
        if (isset($post['depdrop_parents'])) {

            $id = end($post['depdrop_parents']);

            $list = Cases::find()->where([
                'id' => $id
            ]);
            $selected = null;
            if ($id != null && $list->count() > 0) {
                $selected = '';
                foreach ($list->each() as $i => $account) {

                    $departmentName = $account->department->title;
                    $departmentId = $account->department->id;

                    $out[] = [
                        'id' => $departmentId,
                        'name' => $departmentName, // $account['title'],
                        'department_id' => $departmentId
                    ];
                    if ($i == 0) {
                        $selected = $account['id'];
                    }
                }
                // Shows how you can preselect a value
                \Yii::$app->response->data = Json::encode([
                    'output' => $out,
                    'selected' => $selected
                ]);
                return;
            }
        }
        \Yii::$app->response->data = Json::encode([
            'output' => '',
            'selected' => ''
        ]);
    }

    public function actionDepartmentUser()
    {
        $out = [];
        $post = \Yii::$app->request->post();
        if (isset($post['depdrop_parents'])) {

            $id = end($post['depdrop_parents']);

            $list = User::find()->where([
                'department_id' => $id
            ]);
            $selected = null;
            if ($id != null && $list->count() > 0) {
                $selected = '';
                foreach ($list->each() as $i => $account) {

                    $userName = $account->User->full_name;
                    $userId = $account->User->id;
                    $out[] = [
                        'id' => $userId,
                        'name' => $userName, // $account['title'],
                        'user_id' => $userId
                    ];
                    if ($i == 0) {
                        $selected = $account['id'];
                    }
                }
                // Shows how you can preselect a value
                \Yii::$app->response->data = Json::encode([
                    'output' => $out,
                    'selected' => $selected
                ]);
                return;
            }
        }
        \Yii::$app->response->data = Json::encode([
            'output' => '',
            'selected' => ''
        ]);
    }

    protected function updateMenuItems($model = null)
    {
        switch (\Yii::$app->controller->action->id) {

            case 'add':
                {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                }
                break;
            case 'index':
                {
                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'Add'),
                        'url' => [
                            'add'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                    $this->menu['clear'] = [
                        'label' => '<span class="glyphicon glyphicon-remove"></span>',
                        'title' => Yii::t('app', 'Clear'),
                        'url' => [
                            'clear'
                        ],
                        'htmlOptions' => [
                            'data-confirm' => "Are you sure to delete these items?"
                        ],
                        'visible' => User::isAdmin()
                    ];
                }
                break;
            case 'update':
                {
                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'add'),
                        'url' => [
                            'add'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                }
                break;

            default:
            case 'view':
                {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                    if ($model != null) {
                        $this->menu['clone'] = array(
                            'label' => '<span class="glyphicon glyphicon-file">Clone</span>',
                            'title' => Yii::t('app', 'Clone'),
                            'url' => $model->getUrl('clone')
                            // 'visible' => User::isAdmin ()
                        );
                        $this->menu['update'] = [
                            'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                            'title' => Yii::t('app', 'Update'),
                            'url' => $model->getUrl('update')
                            // 'visible' => User::isAdmin ()
                        ];
                        $this->menu['delete'] = [
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'title' => Yii::t('app', 'Delete'),
                            'url' => $model->getUrl('delete')
                            // 'visible' => User::isAdmin ()
                        ];
                    }
                }
        }
    }
}
