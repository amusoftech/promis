<?php

/**
 *
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author     : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 */

namespace app\controllers;

use app\components\TActiveForm;
use app\components\TController;
use app\models\User;
use app\models\Task;
use app\models\Timer;
use app\models\search\Timer as TimerSearch;
use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\AccessRule;

/**
 * TimerController implements the CRUD actions for Timer model.
 */
class TimerController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'clear',
                            'delete',
                            'mass'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isAdmin();
                        }
                    ],
                    [
                        'actions' => [
                            'index',
                            'view',
                            'start',
                            'update',
                            'stop',
                            'ajax',
                            'start-timer'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Timer models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Timer model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $this->updateMenuItems($model);
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Timer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd(/* $id*/)
    {
        $model = new Timer();
        $model->loadDefaultValues();
        $model->state_id = Timer::STATE_ACTIVE;
        /*
         * if (is_numeric($id)) {
         * $resume = Resume::findOne($id);
         * if ($resume)
         * $model->id = $id;
         * else
         * throw new NotFoundHttpException('The requested resume does not exist.');
         * }
         */

        $model->checkRelatedData([
            'created_by_id' => User::class,
            'task_id' => Task::class
        ]);
        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post) && $model->save()) {
            return $this->redirect($model->getUrl());
        }
        $this->updateMenuItems();
        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing Timer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $timer = Timer::find()->where([
            'task_id' => $id,
            'state_id' => Timer::STATE_START
        ])->one();

        if ($timer != null) {
            $timer->stopped_on = date('Y-m-d H:i:s');
            $timer->save();
        }
    }

    /**
     * Deletes an existing Timer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (\yii::$app->request->post()) {
            $model->delete();
            return $this->redirect([
                'index'
            ]);
        }
        return $this->render('delete', [
            'model' => $model
        ]);
    }

    public function actionMass($action = 'delete')
    {
        \Yii::$app->response->format = 'json';
        $response['status'] = 'NOK';

        $status = Timer::massDelete('delete');
        if ($status == true) {
            $response['status'] = 'OK';
        }
        return $response;
    }

    /**
     * Truncate an existing Timer model.
     * If truncate is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionClear($truncate = true)
    {
        $query = Timer::find();
        foreach ($query->each() as $model) {
            $model->delete();
        }
        if ($truncate) {
            Timer::truncate();
        }
        \Yii::$app->session->setFlash('success', 'Timer Cleared !!!');
        return $this->redirect([
            'index'
        ]);
    }

    public function actionStartTimer()
    {

        \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [
            'status' => 'NOK'
        ]; 

        $request = Yii::$app->request;
        $task_id = $request->post('task_id');
        $time = $request->post('time');

        $userID = Yii::$app->user->id;


        $model = new Timer();
        $model->task_id = $task_id;
        $model->work_time = $time;
        $model->created_on = date("Y-m-d H:i:s");
        $model->created_by_id = $userID;
        if ($model->save()) {
            $response['status'] = "OK";
            $response['state'] = "1";
            $response['message'] = "Added to list Successfully";
        } else {
            $response['message'] = $model->getErrorsString();
        }


        return $response;
    }
    public function actionStart($id)
    {
        \Yii::$app->response->format = 'json';
        $response['status'] = 'NOK';
        $model = Task::find()->where([
            'id' => $id
        ])->one();
        if (!empty($model)) {
            if (!$model->isAllowed())
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));
        }

        Timer::start($model);
        $timer = Timer::getCurrentTimer();

        $response['html'] = '<div class="left-data">' . $timer->task->linkify() . '</div>
									 	
									 	<input type="hidden" id="model-id"
										value="' . $timer->task_id . '"><a
										class="search-submit" id="stop-btn"> <i
											class="fa fa-stop"></i>
									</a>';
        $response['status'] = 'OK';
        return $response;
    }

    public function actionStop($id)
    {
        \Yii::$app->response->format = 'json';
        $response['status'] = 'NOK';
        $model = Task::find()->where([
            'id' => $id
        ])->one();

        if (!empty($model)) {
            if (!$model->isAllowed())
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));
        }

        Timer::stop($model);
        $response['html'] = '<div class="left-data  text-center"><a href="" class="font12">Timer not Running</a></div>';
        $response['status'] = 'OK';

        return $response;
    }

    /**
     * Finds the Timer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Timer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $accessCheck = true)
    {
        if (($model = Timer::findOne($id)) !== null) {

            if ($accessCheck && !($model->isAllowed()))
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateMenuItems($model = null)
    {
        switch (\Yii::$app->controller->action->id) {

            case 'add': {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                }

                break;
            case 'update': {

                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                }
                break;

            default:
            case 'view': {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                }
        }
    }
}
