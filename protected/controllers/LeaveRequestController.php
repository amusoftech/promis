<?php
/**
 *
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author     : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 */
namespace app\controllers;

use app\components\TActiveForm;
use app\components\TController;
use app\components\filters\AccessControl;
use app\components\filters\AccessRule;
use app\models\Department;
use app\models\User;
use app\models\LeaveRequest;
use app\models\search\LeaveRequest as LeaveRequestSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * LeaveRequestController implements the CRUD actions for LeaveRequest model.
 */
class LeaveRequestController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'clear',
                            'delete',
                            'mass'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isAdmin();
                        }
                    ],

                    [
                        'actions' => [
                            'index',
                            'add',
                            'view',
                            'update',
                            'clone',
                            'my',
                            'ajax',
                            'pending',
                            'monthwise-leave',
                            'cancel',
                            'monthwise-attendance'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all LeaveRequest models.
     *
     * @return mixed
     */
    public function actionIndex($my = false)
    {
        $searchModel = new LeaveRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if ($my) {
            $dataProvider->query->andWhere([
                'created_by_id' => Yii::$app->user->id
            ]);
        }

        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Lists all LeaveRequest models.
     *
     * @return mixed
     */
    public function actionPending()
    {
        $searchModel = new \app\models\search\LeaveRequest();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere([
            'in',
            'leave_request_model.state_id',
            [
                LeaveRequest::STATE_PENDING,
                LeaveRequest::STATE_REJECTED
            ]
        ]);
        $dataProvider->query->andWhere([
            'leave_request_model.created_by_id' => Yii::$app->user->id
        ]);

        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionMy()
    {
        $searchModel = new LeaveRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere([
            'l.created_by_id' => Yii::$app->user->id
        ]);

        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionMonthwiseLeave($id, $month = 1)
    {
        $searchModel = new LeaveRequestSearch();
        $params = Yii::$app->request->queryParams;
        if (is_numeric($id)) {
            $leave = LeaveRequest::findOne($id);
            if (is_null($leave)) {
                throw new NotFoundHttpException('The requested leave does not exist.');
            }
            $searchModel->created_by_id = $leave->created_by_id;
            $params['LeaveRequest']['created_by_id'] = $leave->created_by_id;
        }
        $ids = [];
        $date = date('Y-m-d', strtotime('-30 days' . date('Y-m-d')));
        $months[] = date('m');
        for ($i = 1; $i < $month; $i ++) {
            $months[] = date("m", strtotime(" -$i month"));
        }
        $leaves = \app\models\LeaveRequest::find()->where([
            'created_by_id' => $leave->created_by_id
        ])
            ->andWhere([
            'in',
            'state_id',
            [
                \app\models\LeaveRequest::STATE_PENDING,
                \app\models\LeaveRequest::STATE_APPROVED
            ]
        ])
            ->all();

        if ($leaves != null) {
            foreach ($leaves as $leave) {
                $dates = explode(",", $leave->start_date);
                foreach ($dates as $leave_date) {
                    $leave_month = date('m', strtotime($leave_date));
                    if (in_array($leave_month, $months)) {
                        $ids[] = $leave->id;
                    }
                }
            }
        }
        if (! empty($ids)) {
            $ids = array_unique($ids);
        }
        $dataProvider = $searchModel->search($params);

        $dataProvider->query->andWhere([
            'in',
            'l.id',
            $ids
        ]);
        $this->updateMenuItems();
        return $this->renderAjax('_ajax-grid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'month' => $month
        ]);
    }

    /**
     * Displays a single LeaveRequest model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $this->updateMenuItems($model);
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new LeaveRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd($id = null)
    {
        $model = new LeaveRequest();
        $model->loadDefaultValues();
        $model->state_id = LeaveRequest::STATE_PENDING;
        $current_date = date('Y-m-d');
        $model->checkRelatedData([
            'created_by_id' => User::class
        ]);
        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post)) {
            $new_date = $post['LeaveRequest']['start_date'];
            $is_halfday = $post['LeaveRequest']['is_halfday'];
            $dates = explode(",", $new_date);
            $count = count($dates);
            foreach ($dates as $key => $date) {
                if ($key != 0) {
                    if (strtotime($date . '-1 day') != strtotime($dates[$key - 1])) {
                        $previousDay = date('l', strtotime($date . '-1 day'));
                        if (! in_array($previousDay, [
                            'Sunday',
                            'Saturday'
                        ])) {
                            if (strtotime($dates[$key - 1]) > strtotime($date)) {
                                $model->addError('start_date', "You are only allowed to add Leave in Incremental Order Only");
                                return $this->render('add', [
                                    'model' => $model
                                ]);
                            }
                            $model->addError('start_date', "You are only allowed to add consecutive Leaves ");
                            return $this->render('add', [
                                'model' => $model
                            ]);
                        }
                    }
                }
            }
            $end_date = ArrayHelper::getValue($dates, ($count - 1));
            if (! ($is_halfday)) {
                $havetoapplyon = date('Y-m-d', strtotime($dates[0] . ' - 3 day'));
                $model->start_date = $new_date;
                $model->end_date = $end_date;
                if (! empty($id)) {
                    $model->created_by_id = $id;
                }

                if ($model->save()) {
                    if ($current_date <= $havetoapplyon) {
                        Yii::$app->session->setFlash('success', 'Your leave request has been applied successfully!');
                    } else {
                        // Yii::$app->session->setFlash('success', 'You must apply 3 days before.');
                        Yii::$app->session->setFlash('success', 'You must apply 3 days before.');
                    }
                    return $this->redirect($model->getUrl());
                }
            } elseif ($is_halfday) {
                $havetoapplyon = date('Y-m-d', strtotime($new_date . ' -1 day'));

                $model->start_date = $new_date;
                if ($model->save()) {
                    if ($current_date <= $havetoapplyon) {
                        Yii::$app->session->setFlash('success', 'Your leave request has been applied successfully!');
                    } else {
                        Yii::$app->session->setFlash('success', 'You must apply 1 day before.');
                    }
                    return $this->redirect($model->getUrl());
                }
            }
        }

        $this->updateMenuItems();
        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing LeaveRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $current_date = date('Y-m-d');
        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }

        if ($model->load($post)) {

            $new_date = $post['LeaveRequest']['start_date'];
            $is_halfday = $post['LeaveRequest']['is_halfday'];
            $dates = explode(",", $new_date);
            $count = count($dates);

            foreach ($dates as $key => $date) {
                if ($key != 0) {
                    if (strtotime($date . '-1 day') != strtotime($dates[$key - 1])) {
                        $previousDay = date('l', strtotime($date . '-1 day'));
                        if (! in_array($previousDay, [
                            'Sunday',
                            'Saturday'
                        ])) {
                            if (strtotime($dates[$key - 1]) > strtotime($date)) {
                                $model->addError('start_date', "You are only allowed to add Leave in Incremental Order Only");
                                return $this->render('add', [
                                    'model' => $model
                                ]);
                            }
                            $model->addError('start_date', "You are only allowed to add consecutive Leaves ");
                            return $this->render('add', [
                                'model' => $model
                            ]);
                        }
                    }
                }
            }
            $end_date = ArrayHelper::getValue($dates, ($count - 1));
            if (! ($is_halfday)) {
                $havetoapplyon = date('Y-m-d', strtotime($dates[0] . ' - 3 day'));
                $model->start_date = $new_date;
                $model->is_halfday = LeaveRequest::IS_HALFDAY_NO;
                $model->end_date = $end_date;
                if ($model->save()) {
                    if ($current_date <= $havetoapplyon) {
                        Yii::$app->session->setFlash('success', 'Your leave request has been updated successfully!');
                    } else {
                        Yii::$app->session->setFlash('success', 'You must apply 3 days before.');
                    }
                    return $this->redirect($model->getUrl());
                }
            } elseif ($is_halfday) {
                $havetoapplyon = date('Y-m-d', strtotime($new_date . ' -1 day'));
                $model->start_date = $new_date;
                $model->is_halfday = LeaveRequest::IS_HALF_DAY_YES;
                if ($model->save()) {
                    if ($current_date <= $havetoapplyon) {
                        Yii::$app->session->setFlash('success', 'Your leave request has been update successfully!');
                    } else {
                        Yii::$app->session->setFlash('success', 'You must apply 1 day before.');
                    }
                    return $this->redirect($model->getUrl());
                }
            }
        }

        $this->updateMenuItems($model);
        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionClone($id)
    {
        $old = $this->findModel($id);
        $model = new LeaveRequest();
        $model->loadDefaultValues();
        $model->state_id = LeaveRequest::STATE_ACTIVE;

        $model->start_date = $old->start_date;

        $model->end_date = $old->end_date;

        $model->is_halfday = $old->is_halfday;

        $model->is_shortleave = $old->is_shortleave;

        $model->type_id = $old->type_id;

        $model->description = $old->description;

        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post) && $model->save()) {
            return $this->redirect($model->getUrl());
        }
        $this->updateMenuItems($model);
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing LeaveRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionCancel($id)
    {
        $model = $this->findModel($id);
        if ($model->state_id == LeaveRequest::STATE_PENDING && $model->created_by_id == \Yii::$app->user->identity->id) {
            $model->state_id = LeaveRequest::STATE_CANCELLED;

            if ($model->save()) {
                $model->updateHistory("Leave request cancelled by" . $model->createdBy . " on " . date('Y-m-d'));
            }
            return $this->redirect($model->getUrl('view'));
        }
        $model->addError('start_date', 'Approved leave Cannot be updated');
        return $this->redirect($model->getUrl('view'));
    }

    /**
     * Deletes an existing LeaveRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (\yii::$app->request->post()) {
            $model->delete();
            return $this->redirect([
                'index'
            ]);
        }
        return $this->render('delete', [
            'model' => $model
        ]);
    }

    /**
     * Truncate an existing LeaveRequest model.
     * If truncate is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionClear($truncate = true)
    {
        $query = LeaveRequest::find();
        foreach ($query->each() as $model) {
            $model->delete();
        }
        if ($truncate) {
            LeaveRequest::truncate();
        }
        \Yii::$app->session->setFlash('success', 'LeaveRequest Cleared !!!');
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Finds the LeaveRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return LeaveRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $accessCheck = true)
    {
        if (($model = LeaveRequest::findOne($id)) !== null) {
            if ($accessCheck && ! ($model->isAllowed()))
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateMenuItems($model = null)
    {
        $params = Yii::$app->request->getQueryParam('type');
        switch (\Yii::$app->controller->action->id) {

            case 'add':
                {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => (!(User::isAdmin())
                    ];
                }
                break;
            case 'index':
                {

                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'Add'),
                        'url' => [
                            'add'
                        ],
                        'visible' => ! (User::isAdmin() || $params == User::ROLE_EMPLOYEE)
                    ];
                }
                break;
            case 'clone':
                {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                }
                break;
            case 'update':
                {

                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                }
                break;
            case 'monthwise-attendance':
                if (isset($model->user_id)) {
                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'Add'),
                        'url' => [
                            'add',
                            'id' => $model->user_id
                        ]
                        // 'visible' => User::isAdmin ()
                    ];
                }
                break;

            default:
            case 'view':
                {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],
                        'visible' => User::isAdmin()
                    ];
                    if ($model != null) {
                        $this->menu['clone'] = array(
                            'label' => '<span class="glyphicon glyphicon-copy"></span>',
                            'title' => Yii::t('app', 'Clone'),
                            'url' => [
                                'clone',
                                'id' => $model->id
                            ]
                            // 'visible' => User::isAdmin ()
                        );
                        $this->menu['update'] = [
                            'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                            'title' => Yii::t('app', 'Update'),
                            'url' => $model->getUrl('update')
                            // 'visible' => User::isAdmin ()
                        ];
                        $this->menu['delete'] = [
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'title' => Yii::t('app', 'Delete'),
                            'url' => $model->getUrl('delete')
                            // 'visible' => User::isAdmin ()
                        ];
                    }
                }
        }
    }

    public function actionMass($action = 'delete')
    {
        \Yii::$app->response->format = 'json';
        $response['status'] = 'NOK';
        $status = LeaveRequest::massDelete('delete');
        if ($status == true) {
            $response['status'] = 'OK';
        }
        return $response;
    }
}
