<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\controllers;

use Yii;
use app\models\AttendanceEvent;
use app\models\search\AttendanceEvent as AttendanceEventSearch;
use app\components\TController;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\User;
use yii\web\HttpException;
use app\components\TActiveForm;
use app\models\Attendance;

/**
 * AttendanceEventController implements the CRUD actions for AttendanceEvent model.
 */
class AttendanceEventController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'clear',
                            'delete',
                            'index',
                            'add',
                            'view',
                            'update',
                            'clone',
                            'ajax',
                            'mass'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isAdmin() || User::isHR();
                        }
                    ],
                    
                    [
                        'actions' => [                           
                            'index',                           
                            'view',  
                            'ajax'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                        return User::isManager();
                        }
                        ],
                        [
                            'actions' => [
                                'index',
                                'view',
                                'ajax'
                            ],
                            'allow' => true,
                            'matchCallback' => function () {
                            return User::isEmployee();
                            }
                            ]
                ]
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all AttendanceEvent models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttendanceEventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single AttendanceEvent model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id,false);
        $this->updateMenuItems($model);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $model
            ]);
        }
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * actionMass delete in mass as items are checked
     *
     * @param string $action
     * @return string
     */
    public function actionMass($action = 'delete')
    {
        \Yii::$app->response->format = 'json';
        $response['status'] = 'NOK';
        $status = AttendanceEvent::massDelete();
        if ($status == true) {
            $response['status'] = 'OK';
        }
        return $response;
    }

    /**
     * Creates a new AttendanceEvent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd($id)
    {
        $model = new AttendanceEvent();
        $model->loadDefaultValues();
        $model->state_id = AttendanceEvent::STATE_ACTIVE;

        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post)) {
            $attendance = Attendance::findOne($id);
            if (! empty($attendance)) {
                $model->user_id = $attendance->user_id;
                $model->date = $attendance->date;
                if ($model->save()) {
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been added Successfully."));
                    return $this->redirect([
                        '/attendance/view',
                        'id' => $attendance->id
                    ]);
                }
            } else {
                \Yii::$app->getSession()->setFlash('error', \Yii::t('app', "Invalid attendance ID"));
            }
        }

        $this->updateMenuItems();
        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing AttendanceEvent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post)) {
            $attendance = Attendance::find()->where([
                'user_id' => $model->user_id,
                'date' => $model->date
            ])->one();
            if (! empty($attendance)) {

                if ($model->save()) {
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been updated Successfully."));
                    return $this->redirect([
                        '/attendance/view',
                        'id' => $attendance->id
                    ]);
                }
            } else {
                \Yii::$app->getSession()->setFlash('error', \Yii::t('app', "Invalid attendance ID"));
            }
        }
        $this->updateMenuItems($model);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model
            ]);
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing AttendanceEvent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (\yii::$app->request->post()) {
            $attendance = Attendance::find([
                'user_id' => $model->user_id,
                'date' => $model->date
            ])->one();
            $model->delete();
            \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been deleted Successfully."));
            if (! empty($attendance)) {
                return $this->redirect([
                    '/attendance/view',
                    'id' => $attendance->id
                ]);
            }
        }
        return $this->render('delete', [
            'model' => $model
        ]);
    }

    /**
     * Truncate an existing AttendanceEvent model.
     * If truncate is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionClear($truncate = true)
    {
        $query = AttendanceEvent::find();
        foreach ($query->each() as $model) {
            $model->delete();
        }
        if ($truncate) {
            AttendanceEvent::truncate();
        }
        \Yii::$app->session->setFlash('success', 'AttendanceEvent Cleared !!!');
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Finds the AttendanceEvent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return AttendanceEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $accessCheck = true)
    {
        if (($model = AttendanceEvent::findOne($id)) !== null) {

            if ($accessCheck && ! ($model->isAllowed()))
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateMenuItems($model = null)
    {
        switch (\Yii::$app->controller->action->id) {

            case 'add':
                {
                    if (! empty(\Yii::$app->request->get('id'))) {

                        $this->menu['manage'] = [
                            'label' => '<span class="glyphicon glyphicon-list"></span>',
                            'title' => Yii::t('app', 'Manage'),
                            'url' => [
                                '/attendance/view',
                                'id' => \Yii::$app->request->get('id')
                            ]
                        ];
                    }
                }

                break;

            case 'update':
                {

                    if (! empty($model)) {
                        $attendance = Attendance::find()->where([
                            'user_id' => $model->user_id,
                            'date' => $model->date
                        ])->one();

                        if (! empty($attendance)) {

                            $this->menu['manage'] = [
                                'label' => '<span class="glyphicon glyphicon-list"></span>',
                                'title' => Yii::t('app', 'Manage'),
                                'url' => [
                                    '/attendance/view',
                                    'id' => $attendance->id
                                ]
                            ];
                        }
                    }
                }
                break;

            default:
            case 'view':
                {
                    if ($model != null) {
                        $attendance = Attendance::find()->where([
                            'user_id' => $model->user_id,
                            'date' => $model->date
                        ])->one();

                        if (! empty($attendance)) {
                            $this->menu['manage'] = [
                                'label' => '<span class="glyphicon glyphicon-list"></span>',
                                'title' => Yii::t('app', 'Manage'),
                                'url' => [
                                    '/attendance/view',
                                    'id' => $attendance->id
                                ]
                            ];
                        }

                        $this->menu['delete'] = [
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'title' => Yii::t('app', 'Delete'),
                            'htmlOptions' => [
                                'data-method' => 'post'
                            ],
                            'url' => $model->getUrl('delete')
                        ];
                    }
                }
        }
    }
}
