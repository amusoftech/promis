<?php

/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author	 : Shiv Charan Panjeta < shiv@toxsl.com >
 */
namespace app\controllers;

use app\components\TController;
use app\models\User;
use yii\filters\AccessControl;
use app\models\Setting;
use app\models\search\User as UserSearch;
use app\models\search\Task;

class DashboardController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'default-data'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isAdmin();
                        }
                    ],
                    [
                        'actions' => [
                            'hr-index',
                            'index'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isHR();
                        }
                    ],
                    [
                        'actions' => [
                            'employee-index',
                            'index'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isEmployee();
                        }
                    ],
                    [
                        'actions' => [
                            'manager-index',
                            'index'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                            return User::isManager();
                        }
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        if (User::isHR()) {
            return $this->redirect([
                'hr-index'
            ]);
        } else if (User::isEmployee()) {
            return $this->redirect([
                'employee-index'
            ]);
        } else if (User::isClient()) {
            return $this->redirect([
                '/client/index'
            ]);
        } else if (User::isManager()) {
            return $this->redirect([
                'manager-index'
            ]);
        }
        $this->updateMenuItems();
        $smtpConfig = isset(\Yii::$app->settings) ? \Yii::$app->settings->smtp : null;
        if (empty($smtpConfig)) {
            Setting::setDefaultConfig();
        }
        return $this->render('index');
    }

    public function actionHrIndex()
    {
        $user = new User();
        $searchModel = new UserSearch();
        $post = \Yii::$app->request->post();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, User::ROLE_EMPLOYEE, $post);
        $this->updateMenuItems();

        return $this->render('employee-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'search' => $post
        ]);
    }

    public function actionEmployeeIndex()
    {
        $searchModel = new Task();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('employee-dashboard', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionManagerIndex()
    {
        $searchModel = new \app\models\search\Cases();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = 10;
        return $this->render('manager-dashboard', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public static function MonthlySignups()
    {
        $date = new \DateTime();
        $date->modify('-12  months');
        $count = array();
        for ($i = 1; $i <= 12; $i ++) {
            $date->modify('+1 months');
            $month = $date->format('Y-m');

            $count[$month] = (int) User::find()->where([
                'like',
                'created_on',
                $month
            ])
                ->andWhere([
                '!=',
                'role_id',
                User::ROLE_ADMIN
            ])
                ->count();
        }
        return $count;
    }

    public function actionDefaultData()
    {
        Setting::setDefaultConfig();
        $msg = 'Done !! Setting reset succefully!!!';
        \Yii::$app->session->setFlash('success', $msg);
        return $this->redirect(\Yii::$app->request->referrer);
    }
}
