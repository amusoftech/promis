<?php
/**
 *@copyright : ToXSL Technologies Pvt. Ltd. < www.toxsl.com >
 *@author    : Shiv Charan Panjeta < shiv@toxsl.com >
 *
 * All Rights Reserved.
 * Proprietary and confidential :  All information contained herein is, and remains
 * the property of ToXSL Technologies Pvt. Ltd. and its partners.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 */
namespace app\controllers;

use Yii;
use app\models\Attendance;
use app\models\search\Attendance as AttendanceSearch;
use app\components\TController;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\models\User;
use yii\web\HttpException;
use app\components\TActiveForm;
use app\models\AttendanceEvent;

/**
 * AttendanceController implements the CRUD actions for Attendance model.
 */
class AttendanceController extends TController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'actions' => [
                            'clear',
                            'delete',
                            'index',
                            'add',
                            'view',
                            'update',
                            'clone',
                            'ajax',
                            'mass'
                        ],
                        'allow' => true,
                        'matchCallback' => function () {
                        return  User::isAdmin() || User::isHR() ||User::isManager() || User::isClient() || User::isInvestigator() || User::isDistributor();
                        
                        }
                    ]
                ]
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Attendance models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttendanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->updateMenuItems();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Attendance model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id, false);
        $this->updateMenuItems($model);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $model
            ]);
        }
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * actionMass delete in mass as items are checked
     *
     * @param string $action
     * @return string
     */
    public function actionMass($action = 'delete')
    {
        \Yii::$app->response->format = 'json';
        $response['status'] = 'NOK';
        $status = Attendance::massDelete();
        if ($status == true) {
            $response['status'] = 'OK';
        }
        return $response;
    }

    /**
     * Creates a new Attendance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $model = new Attendance();
        $model->loadDefaultValues();

        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }

        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            if ($model->load($post)) {
                $total_time = date_create($model->in_time)->diff(date_create($model->out_time))->format('%H:%i:%s');

                $model->total_time = $total_time;
                if ((User::isAdmin()) || (User::isHR())) {
                    $attendance = Attendance::find()->where([
                        'user_id' => $model->user_id,
                        'date' => $model->date,
                        'state_id' => $model->state_id
                    ])->one();
                } else {
                    $attendance = Attendance::find()->where([
                        'user_id' => \Yii::$app->user->id,
                        'date' => $model->date,
                        'state_id' => $model->state_id
                    ])->one();
                }

                if (empty($attendance)) {
                    /* Will check and update later */

                    // if (! empty($model->inTime)) {

                    // $in_event = new AttendanceEvent();
                    // $in_event->time = $model->inTime;
                    // $in_event->type_id = AttendanceEvent::TYPE_IN;
                    // $in_event->date = $model->date;
                    // $in_event->user_id = $model->user_id;
                    // $in_event->state_id = $model->state_id;
                    // //$event->attendance_id = $model->id;

                    // if (! $in_event->save()) {
                    // $transaction->rollBack();
                    // \Yii::$app->getSession()->setFlash('error', $in_event->getErrorsString());
                    // return;
                    // }
                    // }

                    // if (! empty($model->outTime)) {
                    // $out_event = new AttendanceEvent();
                    // $out_event->time = $model->outTime;
                    // $out_event->type_id = AttendanceEvent::TYPE_OUT;
                    // $out_event->date = $model->date;
                    // $out_event->user_id = $model->user_id;
                    // $out_event->state_id = $model->state_id;
                    // // $event->attendance_id = $model->id;
                    // if (! $out_event->save()) {
                    // $transaction->rollBack();
                    // \Yii::$app->getSession()->setFlash('error', $out_event->getErrorsString());
                    // return;
                    // }
                    // }

                    if ($model->save()) {
                        // $in_event->attendance_id = $model->id;
                        // $out_event->attendance_id = $model->id;
                        // $in_event->save(false,['attendance_id']);
                        // $out_event->save(false,['attendance_id']);
                        $transaction->commit();
                        \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been added Successfully."));
                        return $this->redirect($model->getUrl());
                    } else {
                        $transaction->rollBack();
                        \Yii::$app->getSession()->setFlash('error', $model->getErrorsString());
                        return;
                    }

                    $transaction->commit();
                } else {
                    \Yii::$app->getSession()->setFlash('error', \Yii::t('app', "Already enter this attendance"));
                    return $this->redirect('index');
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::$app->getSession()->setFlash('error', Yii::t('app', "Error !! ") . $e->getMessage());
        }
        $this->updateMenuItems();
        return $this->render('add', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing Attendance model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = \yii::$app->request->post();
        if (\yii::$app->request->isAjax && $model->load($post)) {
            \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return TActiveForm::validate($model);
        }
        if ($model->load($post)) {

            $total_time = date_create($model->in_time)->diff(date_create($model->out_time))->format('%H:%i:%s');

            $model->total_time = $total_time;

            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been added Successfully."));
                return $this->redirect($model->getUrl());
            }
        }
        $this->updateMenuItems($model);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model
            ]);
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Attendance model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (\yii::$app->request->post()) {
            $model->delete();
            \Yii::$app->getSession()->setFlash('success', \Yii::t('app', "Record has been deleted Successfully."));
            return $this->redirect([
                'index'
            ]);
        }
        return $this->render('delete', [
            'model' => $model
        ]);
    }

    /**
     * Truncate an existing Attendance model.
     * If truncate is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionClear($truncate = true)
    {
        $query = Attendance::find();
        foreach ($query->each() as $model) {
            $model->delete();
        }
        if ($truncate) {
            Attendance::truncate();
        }
        \Yii::$app->session->setFlash('success', 'Attendance Cleared !!!');
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Attendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $accessCheck = true)
    {
        if (($model = Attendance::findOne($id)) !== null) {

            if ($accessCheck && ! ($model->isAllowed()))
                throw new HttpException(403, Yii::t('app', 'You are not allowed to access this page.'));

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function updateMenuItems($model = null)
    {
        switch (\Yii::$app->controller->action->id) {

            case 'add':
                {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],

                        'visible' => true
                    ];
                }
                break;
            case 'index':
                {
                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'Add'),
                        'url' => [
                            'add'
                        ],
                        'visible' => User::isAdmin() || User::isHR()
                    ];
                }
                break;
            case 'update':
                {
                    $this->menu['add'] = [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'title' => Yii::t('app', 'add'),
                        'url' => [
                            'add'
                        ],
                        'visible' => (User::isAdmin()) || (User::isHR())
                    ];
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],
                        'visible' => (User::isAdmin()) || (User::isHR())
                    ];
                }
                break;

            default:
            case 'view':
                {
                    $this->menu['manage'] = [
                        'label' => '<span class="glyphicon glyphicon-list"></span>',
                        'title' => Yii::t('app', 'Manage'),
                        'url' => [
                            'index'
                        ],
                        'visible' => true
                    ];
                    if ($model != null) {

                        $this->menu['update'] = [
                            'label' => '<span class="glyphicon glyphicon-pencil"></span>',
                            'title' => Yii::t('app', 'Update'),
                            'url' => $model->getUrl('update'),
                            'visible' => (User::isAdmin()) || (User::isHR())
                        ];
                        $this->menu['delete'] = [
                            'label' => '<span class="glyphicon glyphicon-trash"></span>',
                            'title' => Yii::t('app', 'Delete'),
                            'htmlOptions' => [
                                'data-method' => 'post'
                            ],
                            'url' => $model->getUrl('delete')
                        ];
                    }
                }
        }
    }
}
